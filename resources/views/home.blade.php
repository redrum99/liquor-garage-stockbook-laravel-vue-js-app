<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="MDBS Tech Private Limited">
    <meta name="keyword" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('public/js/app.js') }}" defer></script>
    <link href="{{ asset('public/images/favicon.ico') }}" rel="shortcut icon" type="image/png">
    <link href="{{ asset('public/vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('public/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
</head>

<body class="app header-fixed footer-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <div id="app">
        <Myheader :org="{{ $org }}" :setting="{{ $settings }}" :preference="{{ $preferences }}"
            :user="{{ Auth::User() }}" :count="{{ $count }}" v-on:windowreload="windowreload"></Myheader>
        <div class="app-body">
            <Mymenu :setting="{{ $settings }}" :user="{{ Auth::User() }}" v-on:windowreload="windowreload"></Mymenu>
            <Myaside></Myaside>
            <router-view :org="{{ $org }}" :setting="{{ $settings }}" :preference="{{ $preferences }}"
                :user="{{ Auth::User() }}" v-on:windowreload="windowreload"></router-view>
        </div>
        <Myfooter></Myfooter>
    </div>

    <script src="{{ asset('public/vendors/jquery/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/vendors/pace-progress/js/pace.min.js') }}"></script>
    <script src="{{ asset('public/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('public/vendors/@coreui/coreui/js/coreui.min.js') }}"></script>
</body>

</html>