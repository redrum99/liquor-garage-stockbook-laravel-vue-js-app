@php
if($request->display_type=="excel")
{
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=stock-report.xls");
}
@endphp

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('public/pdf/pdf.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="text-center">
        <div>
            <p><strong style="font-size: 18px">{{ $org->org_name }}</strong></p>
            <p><strong>STOCK REPORT AS ON {{ date('d-m-Y',strtotime($request->date)) }}</strong></p>
        </div>
    </div>
    <br />

    <table border="1" width="100%" class="table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-left">Product Name</th>
                <th class="text-left">Category</th>
                <th class="text-left">Unit Price</th>
                <th class="text-right">Opening Stock</th>
                <th class="text-right">Purchase Stock</th>
                <th class="text-right">Total Qty</th>
                <th class="text-right">Sales Stock</th>
                <th class="text-right">Closing Stock</th>
                <th class="text-right">Value</th>
            </tr>
        </thead>
        <tbody>
            @php $key = 1; $total = 0; @endphp
            @foreach($products as $product)
            <tr>
                <td class="text-center">{{ $key++ }}</td>
                <td>{{ $product['product_name'] }}</td>
                <td>{{ $product['category_name'] }}</td>
                <td class="text-right">{{ number_format($product['up'],2) }}</td>
                <td class="text-right">{{ number_format($product['os'],2) }}</td>
                <td class="text-right">{{ number_format($product['ps'],2) }}</td>
                <td class="text-right">{{ number_format($product['tq'],2) }}</td>
                <td class="text-right">{{ number_format($product['ss'],2) }}</td>
                <td class="text-right">{{ number_format($product['cs'],2) }}</td>
                <td class="text-right">{{ number_format($product['value'],2) }}</td>
            </tr>
            @php
            $total += $product['value'];
            @endphp
            @endforeach
            <tr>
                <th colspan="7" class="text-right">Total</th>
                <th class="text-right">{{ number_format($total,2) }}</th>
            </tr>
        </tbody>
    </table>
</body>

</html>