@extends('layouts.app2')
 
@section('body')
    <section class="wrapper">
        <input type="hidden" id="sid" name="sid" value="{{Session::getId()}}" />
            <div class="row p-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                           <i class="fa fa-cog"></i> Printter Settings
                        </div>
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-md-5">
                                        <label class="checkbox">
                                            <input type="checkbox" id="useDefaultPrinter" checked="true"/> <strong>Print to Default printer</strong> 
                                        </label>
                                        <div id="loadPrinters">
                                            <span class="text-danger">Note:</span> if printer not printing then please 
                                            click to load and select one of the installed printers!
                                            <br />
                                            <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success btn-lg">Referesh</a>
                                            <br />
                                        </div>
                                        <div id="installedPrinters" >
                                            <label for="installedPrinterName">Select an installed Printer:</label>
                                            <select name="installedPrinterName" id="installedPrinterName" ></select>
                                        </div>
                                        <script type="text/javascript">
                                            //var wcppGetPrintersDelay_ms = 5000; //5 sec
                                            
                                            var wcppGetPrintersTimeout_ms = 10000; //10 sec
                                            var wcppGetPrintersTimeoutStep_ms = 500; //0.5 sec

                                            function wcpGetPrintersOnSuccess() {
                                                // Display client installed printers
                                                if (arguments[0].length > 0) {
                                                    var p = arguments[0].split("|");
                                                    var options = '';
                                                    for(var i = 0; i < p.length; i++) {
                                                        options += '<option>' + p[i] + '</option>'; 
                                                    }
                                                    $('#installedPrinters').css('visibility', 'visible');
                                                    $('#installedPrinterName').html(options);
                                                    $('#installedPrinterName').focus();
                                                    $('#loadPrinters').hide();
                                                } else {
                                                    alert("No printers are installed in your system.");
                                                }
                                            }
                                            function wcpGetPrintersOnFailure() {
                                                // Do something if printers cannot be got from the client
                                                alert("No printers are installed in your system.");
                                            }
                                        </script>
                                      
                                    </div>
                                    <div class="col-md-7">
                                           <iframe width="100%" height="300" src="{{ url('/product/barcode/'.$product->product_id) }}"></iframe> 
                                    </div>
                            </div>
                        </div> 
                        <div class="card-footer bg-inverse">
                            <div id="fileToPrint" class="text-right">
                                <label style="display: none" for="ddlFileType">Select a sample File to print:</label>
                                <select style="display: none" id="ddlFileType">
                                    <option>PDF</option>
                                </select>
                                @php
                                    $name = $product->product_id.'.pdf';
                                @endphp
                                <a class="btn btn-success btn-large" onclick="javascript:jsWebClientPrint.print('useDefaultPrinter=' + $('#useDefaultPrinter').attr('checked') + '&printerName=' + $('#installedPrinterName').val() + '&filetype=' + $('#ddlFileType').val() + '&name={{ $name }}');">Print File...</a> 
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
    </section>    
@endsection

@section('scripts')

{!! 

// Register the WebClientPrint script code
// The $wcpScript was generated by DemoPrintFileController@index

$wcpScript;

!!}
@endsection
