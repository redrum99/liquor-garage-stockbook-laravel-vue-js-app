@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=stock-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Stock</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/bill/report.css') }}">
</head>
<body>
    <table border="1">
        <tr>
            <th colspan="9" style="text-align: center;">STOCK REPORT</th>
        </tr>
        <tr>
            <td colspan="8" style="text-align: center;">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
        </tr>
        <tr>
            <th class="text-center">#</th>
            <th>Product Name</th>
            <th>P.Rate (Exc)</th>
            <th>Opening Stock</th>
            <th class="text-right">Purchase Stock</th>
            <th class="text-right">Sales Stock</th>
            <th class="text-right">Closing Stock</th>
            <th class="text-right">Total Value</th>
            
        </tr>
        @foreach($bills as $bill)
            <tr>
                <td class="text-center">
                    {{ ++$i }}
                </td>
                <td>
                    {{ $bill->bill_no }}
                </td>
                <td>
                    {{ date('d-m-Y',strtotime($bill->bill_date)) }}
                </td>
                <td>
                    {{ $bill->Vendor->contact_name }}
                </td>
                <td class="text-right">
                    {{ number_format($bill->sub_total,2) }}
                </td>
                <td class="text-right">
                    {{ number_format($bill->discount_amount,2) }}
                </td>
                <td class="text-right">
                    {{ number_format($bill->tax_amount,2) }}
                </td>
                <td class="text-right">
                    {{ number_format($bill->round_off,2) }}
                </td>
                <td class="text-right">
                    {{ number_format($bill->grand_total,2) }}
                </td>
                @php
                    $sub_total += $bill->sub_total;
                    $discount_amount += $bill->discount_amount;
                    $tax_amount += $bill->tax_amount;
                    $round_off += $bill->round_off;
                    $grand_total += $bill->grand_total;
                @endphp
            </tr>
         @endforeach
    </table>
    <htmlpagefooter name="page-footer">
        <span class="text-right">PAGE {PAGENO}</span>
    </htmlpagefooter>
</body>
</html>