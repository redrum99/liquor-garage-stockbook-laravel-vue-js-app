@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=purchase-order-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Purchase Order</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/purchase_order/report.css') }}">
</head>
<body>
    <table class="table-100" border="1">
        <tr>
            <th colspan="10">PURCHASE ORDER REPORT</th>
        </tr>
        <tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
                @if($vendor!='')
                    <div>
                        <strong>{{ $vendor->contact_name }}</strong>
                    </div>
                    <div>{{ $vendor->billing_address }}</div>
                    <div>Phone: {{ $vendor->mobile_no }}</div>
                    <div>GSTIN: {{ $vendor->gstin_no }}</div>
                @endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
            <tr>
                <th class="text-center">#</th>
                <th>PO No</th>
                <th>PO Date</th>
                <th>Vendor</th>
                <th class="text-right">Sub Total</th>
                @if($setting->discounts_in=='inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($setting->discounts_in=='exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
                <th class="text-right">Round off</th>
                <th class="text-right">Grand Total</th>
            </tr>
            @php
                $i=0;
                $sub_total = 0;
                $discount_amount = 0;
                $tax_amount = 0;
                $round_off = 0;
                $grand_total = 0;
                $total_amount = 0;
            @endphp
            @foreach($purchase_orders as $purchase_order)

                <tr>
                    <td class="text-center">
                        {{ ++$i }}
                    </td>
                    <td>
                        {{ $purchase_order->purchase_order_no }}
                    </td>
                    <td>
                        {{ date('d-m-Y',strtotime($purchase_order->purchase_order_date)) }}
                    </td>
                    <td>
                        {{ $purchase_order->Vendor->contact_name }}
                    </td>
                    <td class="text-right">
                        {{ number_format($purchase_order->sub_total,2) }}
                    </td>
                    @if($setting->discounts_in=='inc')
                        <td class="text-right">
                            {{ number_format($purchase_order->tax_amount,2) }}
                        </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($purchase_order->discount_amount,2) }}
                    </td>
                    @if($setting->discounts_in=='exc')
                        <td class="text-right">
                            {{ number_format($purchase_order->tax_amount,2) }}
                        </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($purchase_order->total_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($purchase_order->round_off,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($purchase_order->grand_total,2) }}
                    </td>
                    @php
                        $sub_total += $purchase_order->sub_total;
                        $discount_amount += $purchase_order->discount_amount;
                        $tax_amount += $purchase_order->tax_amount;
                        $round_off += $purchase_order->round_off;
                        $grand_total += $purchase_order->grand_total;
                        $total_amount += $purchase_order->total_amount;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <th colspan="4" class="text-right">Total</th>
                <th class="text-right">{{ number_format($sub_total,2) }}</th>
                @if($setting->discounts_in=='inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($setting->discounts_in=='exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif

                <th class="text-right">{{ number_format($total_amount,2) }}</th>
                <th class="text-right">{{ number_format($round_off,2) }}</th>
                <th class="text-right">{{ number_format($grand_total,2) }}</th>
            </tr>
        @endif
        @if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($purchase_orders as $purchase_order)
                <tr>
                    <th colspan="9">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $purchase_order->Vendor->contact_name }}</div>
                        <div>{{ $purchase_order->Vendor->billing_address }}</div>
                        <div>{{ $purchase_order->Vendor->shipping_address }}</div>
                        <div>{{ $purchase_order->Vendor->email }}</div>
                        <div>{{ $purchase_order->Vendor->mobile_no }}</div>
                        <div>{{ $purchase_order->Vendor->pan_no }}</div>
                        <div>{{ $purchase_order->Vendor->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>PO No:{{ $purchase_order->purchase_order_no }}</div>
                        <div>PO Date:{{ date('d-m-Y',strtotime($purchase_order->purchase_order_date)) }}</div>
                        <div> Ref No :{{ $purchase_order->reference_no }}</div>
                        <div>Delivery Date :{{ date('d-m-Y',strtotime($purchase_order->delivery_date)) }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">P.Rate(Exc)</th>
                    <th class="text-right">P.Rate(Inc)</th>
                    @if($setting->discounts_in=='inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($setting->discounts_in=='exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($purchase_order->PurchaseOrderProducts as $purchase_order_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $purchase_order_product->product_code }}
                        </td>
                        <td>
                            {{ $purchase_order_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->purchase_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($purchase_order_product->purchase_rate_inc,2) }}
                        </td>
                        @if($setting->discounts_in=='inc')
                            <td>
                                {{ number_format($purchase_order_product->tax_amount,2) }}
                            </td>
                        @endif
                        <td>
                            {{ number_format($purchase_order_product->discount_amount,2) }}
                        </td>
                        @if($setting->discounts_in=='exc')
                            <td>
                                {{ number_format($purchase_order_product->tax_amount,2) }}
                            </td>
                        @endif
                        <td>
                            {{ number_format($purchase_order_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $purchase_order->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->sub_total,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in=='inc')
                    <tr>
                        <th class="text-right" colspan="2">Tax</th>
                        <th class="text-right" colspan="2">
                            {{ number_format($purchase_order->tax_amount,2) }}
                        </th>
                    </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->discount_amount,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in=='exc')
                    <tr>
                        <th class="text-right" colspan="2">Tax</th>
                        <th class="text-right" colspan="2">
                            {{ number_format($purchase_order->tax_amount,2) }}
                        </th>
                    </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $purchase_order->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
    </table>
    <htmlpagefooter name="page-footer">
        <span class="text-right">PAGE {PAGENO}</span>
    </htmlpagefooter>
</body>
</html>