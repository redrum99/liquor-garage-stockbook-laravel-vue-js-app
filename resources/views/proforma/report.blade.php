@php
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;Filename=proforma-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Proforma</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/proforma/report.css') }}">
</head>
<body>
	<table border="1">
		<tr>
			<th colspan="10">PROFORMA REPORT</th>
		</tr>
		<tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
            	@if($customer!='')
            		<div>
                        <strong>{{ $customer->contact_name }}</strong>
                    </div>
                    <div>{{ $customer->billing_address }}</div>
                    <div>Phone: {{ $customer->mobile_no }}</div>
                    <div>GSTIN: {{ $customer->gstin_no }}</div>
            	@endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
			<tr>
	            <th class="text-center">#</th>
	            <th>Proforma No</th>
	            <th>Proforma Date</th>
	            <th>Customer</th>
	            <th class="text-right">Sub Total</th>
	            @if($setting->discounts_in == 'inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($setting->discounts_in == 'exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
	            <th class="text-right">Round off</th>
	            <th class="text-right">Grand Total</th>
	        </tr>
	        @php
	        	$i=0;
	        	$sub_total = 0;
	        	$discount_amount = 0;
	        	$tax_amount = 0;
                $total_amount = 0;
	        	$round_off = 0;
	        	$grand_total = 0;
	        @endphp
	        @foreach($proformas as $proforma)
		        <tr>
		            <td class="text-center">
		                {{ ++$i }}
		            </td>
		            <td>
		                {{ $proforma->proforma_no }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($proforma->proforma_date)) }}
		            </td>
		            <td>
		                {{ $proforma->Customer->contact_name }}
		            </td>
		            <td class="text-right">
		                {{ number_format($proforma->sub_total,2) }}
		            </td>
                    @if($setting->discounts_in == 'inc')
                    <td class="text-right">
                        {{ number_format($proforma->tax_amount,2) }}
                    </td>
                    @endif
		            <td class="text-right">
		            	{{ number_format($proforma->discount_amount,2) }}
		            </td>
                    @if($setting->discounts_in == 'exc')
		            <td class="text-right">
		            	{{ number_format($proforma->tax_amount,2) }}
		            </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($proforma->total_amount,2) }}
                    </td>
		            <td class="text-right">
		            	{{ number_format($proforma->round_off,2) }}
		            </td>
		            <td class="text-right">
		            	{{ number_format($proforma->grand_total,2) }}
		            </td>
		            @php
		            	$sub_total += $proforma->sub_total;
		            	$discount_amount += $proforma->discount_amount;
		            	$tax_amount += $proforma->tax_amount;
                        $total_amount += $proforma->total_amount;
		            	$round_off += $proforma->round_off;
		            	$grand_total += $proforma->grand_total;
		            @endphp
		        </tr>
		    @endforeach
		    <tr>
		    	<th colspan="4" class="text-right">Total</th>
		    	<th class="text-right">{{ number_format($sub_total,2) }}</th>
		    	@if($setting->discounts_in == 'inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($setting->discounts_in == 'exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
		    	<th class="text-right">{{ number_format($round_off,2) }}</th>
		    	<th class="text-right">{{ number_format($grand_total,2) }}</th>
		    </tr>
		@endif
		@if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($proformas as $proforma)
                <tr>
                    <th colspan="9">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $proforma->Customer->contact_name }}</div>
                        <div>{{ $proforma->Customer->billing_address }}</div>
                        <div>{{ $proforma->Customer->shipping_address }}</div>
                        <div>{{ $proforma->Customer->email }}</div>
                        <div>{{ $proforma->Customer->mobile_no }}</div>
                        <div>{{ $proforma->Customer->pan_no }}</div>
                        <div>{{ $proforma->Customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>Proforma No:{{ $proforma->proforma_no }}</div>
                        <div>Proforma Date:{{ date('d-m-Y',strtotime($proforma->proforma_date)) }}</div>
                        <div> Ref No :{{ $proforma->reference_no }}</div>
                        <div>Ref Date :{{ $proforma->reference_date ? date('d-m-Y',strtotime($proforma->reference_date)) : '' }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">S.Rate(Exc)</th>
                    <th class="text-right">S.Rate(Inc)</th>
                    @if($setting->discounts_in == 'inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($setting->discounts_in == 'exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($proforma->ProformaProducts as $proforma_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $proforma_product->product_code }}
                        </td>
                        <td>
                            {{ $proforma_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($proforma_product->sales_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($proforma_product->sales_rate_inc,2) }}
                        </td>
                        @if($setting->discounts_in == 'inc')
                        <td>
                            {{ number_format($proforma_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($proforma_product->discount_amount,2) }}
                        </td>
                        @if($setting->discounts_in == 'exc')
                        <td>
                            {{ number_format($proforma_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($proforma_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($proforma_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $proforma->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->sub_total,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->discount_amount,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $proforma->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($proforma->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
	</table>
	<htmlpagefooter name="page-footer">
		<span class="text-right">PAGE {PAGENO}</span>
	</htmlpagefooter>
</body>
</html>