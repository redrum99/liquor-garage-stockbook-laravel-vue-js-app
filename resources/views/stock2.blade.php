@php
    if($request->display_type=="excel")
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=stock-report.xls");
    }
@endphp

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link  href="{{ asset('public/pdf/pdf.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <table border="1" width="100%" class="table">
        <thead>
            <tr>
                <th class="text-center" colspan="2">
                    Stock Report 
                </th>
            </tr>
            <tr>
                <td class="text-left" width="50%"> 
                    <div>
                        <strong style="font-size: 18px">{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
                <td width="50%">
                    @if($request->date!='')
                        <div>Date : {{ date('d-m-Y',strtotime($request->date)) }}</div>
                    @endif    
                </td>
            </tr>
        </thead>
    </table>

    <table border="1" width="100%" class="table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-left">Product Name</th>
                <th class="text-left">Category</th>
                <th class="text-right">Opening Stock</th>
                <th class="text-right">P.Rate</th>
                <th class="text-right">P.Stock</th>
                <th class="text-right">S.Rate</th>
                <th class="text-right">S.Stock</th>
                <th class="text-right">Closing Stock</th>
                <th class="text-right">P.Value</th>
                <th class="text-right">S.Value</th>
            </tr>
        </thead>
        <tbody>
            @php 
                $key = 1; 
                $total = 0; 
                $pRateTotal = 0;
                $pRateCategory = 0;
                $sRateCategory = 0;
                $category_name = ''; 
            @endphp
            @foreach($products as $product)
                @php
                    $pRateXQty = $product['pRate'] * $product['cs'];
                    $pRateTotal += $pRateXQty;
                    $total += $product['value'];

                    if($category_name != $product['category_name']) {
                        $category_name = $product['category_name'];
                        echo "<tr><td colspan='11' class='text-center'>".$category_name."</td></tr>";
                    }
                @endphp
                <tr>
                    <td class="text-center">{{ $key++ }}</td>
                    <td>{{ $product['product_name'] }}</td>
                    <td>{{ $product['category_name'] }}</td>
                    <td class="text-right">{{ number_format($product['os'],2) }}</td>
                    <td class="text-right">{{ number_format($product['pRate'],2) }}</td>
                    <td class="text-right">{{ number_format($product['ps'],2) }}</td>
                    <td class="text-right">{{ number_format($product['sRate'],2) }}</td>
                    <td class="text-right">{{ number_format($product['ss'],2) }}</td>
                    <td class="text-right">{{ number_format($product['cs'],2) }}</td>
                    <td class="text-right">{{ number_format($pRateXQty,2) }}</td>
                    <td class="text-right">{{ number_format($product['value'],2) }}</td>
                </tr>
            @endforeach    
            <tr>
                <th colspan="9" class="text-right">Total</th>
                <th class="text-right">{{ number_format($pRateTotal,2) }}</th>
                <th class="text-right">{{ number_format($total,2) }}</th>
            </tr>
        </tbody>
    </table>
</body>
</html>