<!DOCTYPE html>
<html>

<head>
    <title>PDF</title>
    <style type="text/css">
        td {
            line-height: 10px;
        }

        hr {
            line-height: 8px;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }

        body {
            font-family: sans-serif;
        }

        a {
            color: #000066;
            text-decoration: none;
        }

        table {
            border-collapse: collapse;
        }

        thead {
            vertical-align: bottom;
            text-align: center;
            font-weight: bold;
        }

        tfoot {
            text-align: center;
            font-weight: bold;
        }

        th {
            text-align: left;
            padding-left: 0.35em;
            padding-right: 0.35em;
            padding-top: 0.35em;
            padding-bottom: 0.35em;
            vertical-align: top;
            font-size: 12px;
        }

        td {
            padding-left: 0.35em;
            padding-right: 0.35em;
            padding-top: 0.35em;
            padding-bottom: 0.35em;
            vertical-align: top;
            font-size: 12px;
        }

        img {
            margin: 0.2em;
            vertical-align: middle;
        }

        .table-100 {
            width: 100%;
        }

        .table-border {
            border: 1px solid #c5c5c5;
        }

        .table-100 {
            width: 100%;
        }

        .table-border {
            border: 1px solid #c5c5c5;
        }

        .div-border {
            border: 1px solid #c5c5c5;
        }

        .page_break {
            page-break-after: always;
            page-break-inside: avoid;
        }

        .pagenum:before {
            content: counter(page);
        }

        .text-right {
            position: absolute;
            right: 10px !important;
            z-index: 999;
        }

        .th-font {
            font-size: 10px;
        }

        .table-50 {
            width: 50%;
        }

        .table-30 {
            width: 30%;
        }
    </style>
</head>

<body>
    <table class="table-100">
        <tr>
            <td align="center">
                <h2>{{ $org->org_name }}</h2>
            </td>
        </tr>
        <tr>
            <td align="center">{{ $org->address }}</td>
        </tr>
        @if($org->gstin_no!='')
        <tr>
            <td align="center">GSTIN : {{ $org->gstin_no }}</td>
        </tr>
        @endif
        @if($org->mobile_no!='')
        <tr>
            <td align="center">Mobile No : {{ $org->mobile_no }} </td>
        </tr>
        @endif
        @if($org->phone_no!='')
        <tr>
            <td align="center">Phone No : {{ $org->phone_no }} </td>
        </tr>
        @endif
        <tr>
            <td align="center">
                <hr>
                <h3>INVOICE</h3>
                <hr>
            </td>
        </tr>
    </table>
    <table class="table-100">
        <tr>
            <td>
                <b>Invoice No : </b>
                {{ $invoice->invoice_no }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Invoice Date:</b>
                {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Invoice Time:</b>
                {{ date('h:i A',strtotime($invoice->created_at)) }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Name :</b>
                {{ $invoice->Customer->contact_name }}
            </td>
        </tr>
        @if($invoice->Customer->mobile_no!='')
        <tr>
            <td>
                <b>Mobile No :</b>
                {{ $invoice->Customer->mobile_no }}
            </td>
        </tr>
        @endif
        @if($invoice->Customer->gstin_no)
        <tr>
            <td>
                <b>GSTIN :</b>
                {{ $invoice->Customer->gstin_no }}
            </td>
        </tr>
        @endif
    </table>

    <table class="table-border table-100 th-font" border="1">
        <tr>
            <td align="center"><b>#</b></td>
            <td align="center"><b>Items</b></td>
            <td align="center"><b>Rate</b></td>
            <td align="center"><b>Qty</b></td>
            <td align="center"><b>Total</b></td>
        </tr>
        @foreach($invoice->InvoiceProducts as $key=>$invoice_product)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $invoice_product->product_name }}</td>
            <td align="right">{{ number_format($invoice_product->sales_rate_exc,2) }}</td>
            <td align="right">{{ number_format($invoice_product->quantity,2) }}</td>
            <td align="right">{{ number_format($invoice_product->amount,2) }}</td>
        </tr>
        @endforeach
        @php
        $total_amount = $invoice->SubTotal($invoice->invoice_id);
        $discount = $invoice->Discount($invoice->invoice_id);
        $sub_total = $total_amount-$discount;
        $total_tax = $invoice->TotalTax($invoice->invoice_id);
        $total = $sub_total + $total_tax + $invoice->extra_amount;
        $grand_total = round($total);
        $round_off = $grand_total - $total ;
        $final_taxable_value = 0;
        $final_gst = 0;
        $final_cgst = 0;
        $final_sgst = 0;
        $final_igst = 0;
        $final_grand_total = 0;
        @endphp
        @foreach($invoice->Taxes($invoice->invoice_id) as $tax)
        @php
        $taxable_value = $invoice->TaxableValue($invoice->invoice_id,$tax->tax_id);
        $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
        $sgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
        $igst = round(($taxable_value * $tax->Tax->igst_rate)/100,2);
        if($invoice->source_id==$invoice->destination_id)
        {
        $gst = $cgst + $sgst;
        }
        else
        {
        $gst = $igst;
        }
        $grand_total = round($gst + $taxable_value);
        $final_taxable_value += $taxable_value;
        $final_cgst +=$cgst;
        $final_sgst +=$sgst;
        $final_igst +=$igst;
        $final_gst +=$gst;
        $final_grand_total +=$grand_total;
        @endphp
        @endforeach
        <tr>
            <td colspan="4" align="right">Sub Total</td>
            <td align="right"><b>{{ number_format($total_amount,2) }}</b></td>
        </tr>
        @if($final_cgst != 0 || $final_sgst != 0)
        <tr>
            <td colspan="4" align="right">SGST({{ $tax->Tax->sgst_name }})</td>
            <td align="right"><b>{{ number_format($final_sgst,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="4" align="right">CGST({{ $tax->Tax->cgst_name }})</td>
            <td align="right"><b>{{ number_format($final_cgst,2) }}</b></td>
        </tr>
        @endif
        @if(!$discount==0)
        <tr>
            <td colspan="4" align="right">Discount</td>
            <td align="right"><b>{{ number_format($discount,2) }}</b></td>
        </tr>
        @endif
        <tr>
            <td colspan="4" align="right">Round Off</td>
            <td align="right"><b>{{ number_format($round_off,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="4" align="right">Grand Total</td>
            <td align="right"><b>{{ number_format($final_grand_total,2) }}</b></td>
        </tr>
    </table>

    <table class="table-100">
        <tr>
            <td align="center">No Exchange, No Credit, No Bargain</td>
        </tr>
        <tr>
            <td align="center">Thank you visit again</td>
        </tr>
    </table>
</body>

</html>