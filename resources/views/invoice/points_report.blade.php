@if($request->display_type=="excel")
@php
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;Filename=Points-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Points</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/invoice/report.css') }}">
</head>
<body>
    <table border="1" width="100%">
        <tr>
            <td align="center" colspan="5">
                <span><b>{{$org->org_name}}</b></span><br>
                <span>{{ $org->address }}</span><br>
                <span>{{ $org->mobile_no }}</span><br>
                <span> {{ $org->email }}</span><br>
                <span> {{ $org->mobile_no }}</span><br>
            </td>
        </tr>
	@if($contacts!='')
	   @foreach($contacts as $contact)
		<tr>
            <td colspan="3">
        		<div>
                    <strong>Customer : {{ $contact->contact_name }}</strong>
                    @php
                        $op_invpoints = 0;
                    	$inv_points = 0;
                    	$op_rpoints = 0;
                    	$invoices = '';
                    	$redeems = '';
                    	$inv_points = $contact->GetInvPoints($contact->contact_id,$request->from_date);
                    	$op_rpoints = $contact->GetRedeemPoints($contact->contact_id,$request->from_date);$contact->GetInvPoints($contact->contact_id,$request->from_date);
                    	$invoices  = $contact->CustInvoices($contact->contact_id,$request);
                    	$redeems  = $contact->CustRedeems($contact->contact_id,$request);
                        $op_invpoints = $inv_points-$op_rpoints;
                   	@endphp
                </div>
                <div>Mobile No: {{ $contact->mobile_no }}</div>
            </td>
            <td colspan="2">
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @php
            $i=0;
            $total = 0;
            $total_points =  $op_invpoints;
        @endphp
       
            <tr> 
                <th>#</th>
                <th>Invoice No</th>
                <th>Invoice Date</th>
                <th >Grand Total</th>
                <th >Points</th>
            </tr>
            <tr>
                <td class="text-center">{{ ++$i}}</td>
                <td>Opening Points</td>
                <td>{{ date('d-m-Y',strtotime($request->from_date)) }}</td>
                <td>-</td>
                <td align="right">{{ number_format($op_invpoints,2) }}</td>
            </tr>
        @if(count($invoices)!=0)
        @foreach($invoices as $invoice)
            <tr>
                <td class="text-center">
                    {{ ++$i }}
                </td>
                <td>
                    {{ $invoice->invoice_no }}
                </td>
                <td>
                    {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
                </td>
                <td align="right">
                    {{ number_format($invoice->grand_total,2) }}
                </td>
                <td align="right">
                    {{ number_format($invoice->points,2) }}
                </td>
                @php
                    
                	$total += $invoice->grand_total;
                	$total_points += $invoice->points;
                @endphp
            </tr>
	    @endforeach
	    <tr>
	    	<td align="right" colspan="4"><b>Total</b></td>
            <!-- <td align="right"><b>{{ number_format($total,2) }}</b></td> -->
	    	<td align="right"><b>{{ number_format($total_points,2)}}</b></td>
	    </tr>
        @endif
        @php
            $i=0;
            $redeem_total_points = 0;
            $balance_points = 0;
        @endphp
        @if(count($redeems)!=0)
    	    <tr>
                <th>#</th>
                <th colspan="3">Redeem Date</th>
                <th  >Points</th>
            </tr>
            @foreach($redeems as $redeem)
            <tr>
                <td class="text-center">
                    {{ ++$i }}
                </td>
                <td colspan="3">
                    {{ date('d-m-Y',strtotime($redeem->redeem_date)) }}
                </td>
                 <td align="right">
                    {{ number_format($redeem->redeem_points,2) }}
                </td>
                @php
                	$redeem_total_points += $redeem->redeem_points;
                @endphp
            </tr>
    	    @endforeach
    	    <tr>
    	    	<td align="right" colspan="4"><b>Total</b></td>
    	    	<td align="right"><b>{{ number_format($redeem_total_points,2) }}</b></td>
    	    </tr>
        @endif
        @php
            $balance_points = $total_points-$redeem_total_points;
        @endphp
        @if(count($invoices)==0 && count($redeems)==0)
            <tr>
                <td align="center" colspan="5"><b>No Data Found</b>
           </tr>
        @else
            <tr>
    	   	    <td align="right" colspan="4"><b>Total Remaing Points</b>
    	   		<td align="right"><b>{{ number_format($balance_points,2) }}</b></td>
    	   </tr>
        @endif
	@endforeach
	@else
	<tr>
            <td colspan="3">
                <div>
                    <strong>Customer : {{ $contact->contact_name }}</strong>
                    @php
                        $op_invpoints = 0;
                        $inv_points = 0;
                        $op_rpoints = 0;
                        $invoices = '';
                        $redeems = '';
                        $inv_points = $contact->GetInvPoints($contact->contact_id,$request->from_date);
                        $op_rpoints = $contact->GetRedeemPoints($contact->contact_id,$request->from_date);$contact->GetInvPoints($contact->contact_id,$request->from_date);
                        $invoices  = $contact->CustInvoices($contact->contact_id,$request);
                        $redeems  = $contact->CustRedeems($contact->contact_id,$request);
                        $op_invpoints = $inv_points-$op_rpoints;
                    @endphp
                </div>
                <div>Mobile No: {{ $contact->mobile_no }}</div>
            </td>
            <td colspan="2">
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @php
            $i=0;
            $total = 0;
            $total_points =  $op_invpoints;
        @endphp
       
            <tr> 
                <th>#</th>
                <th>Invoice No</th>
                <th>Invoice Date</th>
                <th >Grand Total</th>
                <th >Points</th>
            </tr>
            <tr>
                <td class="text-center">{{ ++$i}}</td>
                <td>Opening Points</td>
                <td>{{ date('d-m-Y',strtotime($request->from_date)) }}</td>
                <td>-</td>
                <td align="right">{{ number_format($op_invpoints,2) }}</td>
            </tr>
        @if(count($invoices)!=0)
        @foreach($invoices as $invoice)
            <tr>
                <td class="text-center">
                    {{ ++$i }}
                </td>
                <td>
                    {{ $invoice->invoice_no }}
                </td>
                <td>
                    {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
                </td>
                <td align="right">
                    {{ number_format($invoice->grand_total,2) }}
                </td>
                <td align="right">
                    {{ number_format($invoice->points,2) }}
                </td>
                @php
                    
                    $total += $invoice->grand_total;
                    $total_points += $invoice->points;
                @endphp
            </tr>
        @endforeach
        <tr>
            <td align="right" colspan="4"><b>Total</b></td>
            <!-- <td align="right"><b>{{ number_format($total,2) }}</b></td> -->
            <td align="right"><b>{{ number_format($total_points,2)}}</b></td>
        </tr>
        @endif
        @php
            $i=0;
            $redeem_total_points = 0;
            $balance_points = 0;
        @endphp
        @if(count($redeems)!=0)
            <tr>
                <th>#</th>
                <th colspan="3">Redeem Date</th>
                <th  >Points</th>
            </tr>
            @foreach($redeems as $redeem)
            <tr>
                <td class="text-center">
                    {{ ++$i }}
                </td>
                <td colspan="3">
                    {{ date('d-m-Y',strtotime($redeem->redeem_date)) }}
                </td>
                 <td align="right">
                    {{ number_format($redeem->redeem_points,2) }}
                </td>
                @php
                    $redeem_total_points += $redeem->redeem_points;
                @endphp
            </tr>
            @endforeach
            <tr>
                <td align="right" colspan="4"><b>Total</b></td>
                <td align="right"><b>{{ number_format($redeem_total_points,2) }}</b></td>
            </tr>
        @endif
        @php
            $balance_points = $total_points-$redeem_total_points;
        @endphp
        @if(count($invoices)==0 && count($redeems)==0)
            <tr>
                <td align="center" colspan="5"><b>No Data Found</b>
           </tr>
        @else
            <tr>
                <td align="right" colspan="4"><b>Total Remaing Points</b>
                <td align="right"><b>{{ number_format($balance_points,2) }}</b></td>
           </tr>
        @endif
	@endif
</table>
</body>
</html>