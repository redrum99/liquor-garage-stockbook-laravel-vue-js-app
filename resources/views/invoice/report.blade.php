@php
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;Filename=invoice-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/invoice/report.css') }}">
</head>
<body>
	<table border="1">
		<tr>
			<th colspan="11">INVOICE REPORT</th>
		</tr>
		<tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="5">
            	@if($customer!='')
            		<div>
                        <strong>{{ $customer->contact_name }}</strong>
                    </div>
                    <div>{{ $customer->billing_address }}</div>
                    <div>Phone: {{ $customer->mobile_no }}</div>
                    <div>GSTIN: {{ $customer->gstin_no }}</div>
            	@endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
                @if($request->username!='')
                    <div>User: {{ $request->username }}</div>
                @endif    
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
			<tr>
	            <th class="text-center">#</th>
                <th>Invoice Type</th>
	            <th>Invoice No</th>
	            <th>Invoice Date</th>
	            <th>Customer</th>
	            <th class="text-right">Sub Total</th>
                @if($setting->discounts_in == 'inc')
	            <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($setting->discounts_in == 'exc')
	            <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
	            <th class="text-right">Round off</th>
	            <th class="text-right">Grand Total</th>
	        </tr>
	        @php
	        	$i=0;
	        	$sub_total = 0;
	        	$discount_amount = 0;
	        	$tax_amount = 0;
	        	$total_amount = 0;
                $round_off = 0;
	        	$grand_total = 0;
	        @endphp
	        @foreach($invoices as $invoice)
		        <tr>
		            <td class="text-center">
		                {{ ++$i }}
		            </td>
                    <td>
                        {{ $invoice->invoice_type }}
                    </td>
		            <td>
		                {{ $invoice->invoice_no }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
		            </td>
		            <td>
		                {{ $invoice->Customer->contact_name }}
		            </td>
		            <td class="text-right">
		                {{ number_format($invoice->sub_total,2) }}
		            </td>
                    @if($setting->discounts_in == 'inc')
		            <td class="text-right">
                        {{ number_format($invoice->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
		            	{{ number_format($invoice->discount_amount,2) }}
		            </td>
                    @if($setting->discounts_in == 'exc')
		            <td class="text-right">
		            	{{ number_format($invoice->tax_amount,2) }}
		            </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($invoice->total_amount,2) }}
                    </td>
		            <td class="text-right">
		            	{{ number_format($invoice->round_off,2) }}
		            </td>
		            <td class="text-right">
		            	{{ number_format($invoice->grand_total,2) }}
		            </td>
		            @php
		            	$sub_total += $invoice->sub_total;
		            	$discount_amount += $invoice->discount_amount;
		            	$tax_amount += $invoice->tax_amount;
                        $total_amount += $invoice->total_amount;
		            	$round_off += $invoice->round_off;
		            	$grand_total += $invoice->grand_total;
		            @endphp
		        </tr>
		    @endforeach
		    <tr>
		    	<th colspan="5" class="text-right">Total</th>
		    	<th class="text-right">{{ number_format($sub_total,2) }}</th>
                @if($setting->discounts_in == 'inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
		    	<th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($setting->discounts_in == 'exc')
		    	<th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
		    	<th class="text-right">{{ number_format($round_off,2) }}</th>
		    	<th class="text-right">{{ number_format($grand_total,2) }}</th>
		    </tr>
		@endif
		@if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($invoices as $invoice)
                <tr>
                    <th colspan="9">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $invoice->Customer->contact_name }}</div>
                        <div>{{ $invoice->Customer->billing_address }}</div>
                        <div>{{ $invoice->Customer->shipping_address }}</div>
                        <div>{{ $invoice->Customer->email }}</div>
                        <div>{{ $invoice->Customer->mobile_no }}</div>
                        <div>{{ $invoice->Customer->pan_no }}</div>
                        <div>{{ $invoice->Customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>Invoice Type:{{ $invoice->invoice_type }}</div>
                        <div>Invoice No:{{ $invoice->invoice_no }}</div>
                        <div>Invoice Date:{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</div>
                        <div> Ref No :{{ $invoice->reference_no }}</div>
                        <div>Ref Date :{{ $invoice->reference_date ? date('d-m-Y',strtotime($invoice->reference_date)) : '' }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">S.Rate(Exc)</th>
                    <th class="text-right">S.Rate(Inc)</th>
                    @if($setting->discounts_in == 'inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($setting->discounts_in == 'exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($invoice->InvoiceProducts as $invoice_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $invoice_product->product_code }}
                        </td>
                        <td>
                            {{ $invoice_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($invoice_product->sales_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($invoice_product->sales_rate_inc,2) }}
                        </td>
                        @if($setting->discounts_in == 'inc')
                        <td>
                            {{ number_format($invoice_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($invoice_product->discount_amount,2) }}
                        </td>
                        @if($setting->discounts_in == 'exc')
                        <td>
                            {{ number_format($invoice_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($invoice_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($invoice_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $invoice->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->sub_total,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->discount_amount,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $invoice->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
	</table>
	<htmlpagefooter name="page-footer">
		<span class="text-right">PAGE {PAGENO}</span>
	</htmlpagefooter>
</body>
</html>