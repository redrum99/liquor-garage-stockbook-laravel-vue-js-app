<!DOCTYPE html>
<html>
<head>
    <title>{{ $invoice_return->invoice_return_no }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/pdf.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <h3 class="text-center">Tax Invoice Return</h3>
    </htmlpageheader>
    <table class="table">
        <tr>
            
            <td width="40%" colspan="2" rowspan="2" style="border-right: 0px !important">
                <h3>{{ $org->org_name }}</h3>
                <p>{{ $org->address }}</p>
                <p>GSTIN/UIN : {{ $org->gstin_no }}</p>
                <p>State Name : {{ $org->Places->place_name }}, Code : {{ $org->Places->place_code }}</p>
                <p>Contact : {{ $org->mobile_no }}</p>
                <p>E-mail : {{ $org->email }}</p>
                <p>Website : {{ $org->website }}</p>
            </td>
            <td width="25%">
                Invoice Return No <br> 
                <strong>{{ $invoice_return->invoice_return_no }}</strong>
            </td>
            <td width="25%">
                Invoice Return Date <br> 
                <strong>{{ date('d-m-Y',strtotime($invoice_return->invoice_return_date)) }}</strong>
            </td>
        </tr>
        <tr>
            <td width="25%">
                Reference No <br> 
                <strong>{{ $invoice_return->reference_no }}</strong>
            </td>
            <td width="25%">
                Reference Date <br> 
                @if($invoice_return->reference_date)
                    <strong>{{ $invoice_return->reference_date ? date('d-m-Y',strtotime($invoice_return->reference_date)) : '' }}</strong>
                @endif  
            </td>
        </tr>
       
       
        <tr>
            <td colspan="2" width="50%">
                <p>Consignee</p>
                <h4>{{ $invoice_return->Customer->contact_name }}</h4>
                @if($invoice_return->Customer->billing_address!='')
                    <p>{{ $invoice_return->Customer->billing_address }}</p>
                @endif
                @if($invoice_return->Customer->shipping_address!='')
                    <p>{{ $invoice_return->Customer->shipping_address }}</p>
                @endif
                @if($invoice_return->Customer->mobile_no!='')
                    <p>Contact : {{ $invoice_return->Customer->mobile_no }}</p>
                @endif
                @if($invoice_return->Customer->email!='')
                    <p>E-mail : {{ $invoice_return->Customer->email }}</p>
                @endif
                <p>GSTIN/UIN : {{ $invoice_return->Customer->gstin_no }}</p>
                <p>State Name : {{ $invoice_return->SourcePlace->place_name }}, Code : {{ $invoice_return->DestinationPlace->place_code }}</p>
            </td>
            <td colspan="2" width="50%">
                Terms of Delivery<br>
                <pre>{{ $invoice_return->terms }}</pre>
            </td>
        </tr>
    </table>
    <table class="grade-table">
        <tr>
            <th class="font">#</th>
            <th class="font">Description of Goods</th>
            <th class="font">HSN/SAC</th>
            <th class="font">Quantity</th>
            <th class="font">Rate</th>
            <th class="font">Amount</th>
        </tr>
        @php 
            $i=1;
            $j=0; 
        @endphp
        @foreach($invoice_return->InvoiceReturnProducts as $key=>$invoice_return_product)
            <tr>
                <td class="text-center">{{ $i++ }}</td>
                <td><strong>{{ $invoice_return_product->product_name }}</strong></td>
                <td>{{ $invoice_return_product->hsn_code}}</td>
                <td class="text-right">
                    <strong>{{ number_format($invoice_return_product->quantity,2) }}</strong>
                </td>
                <td class="text-right">
                    {{ number_format($invoice_return_product->sales_rate_exc,2) }}
                </td>
                <td class="text-right">
                    <strong>{{ number_format($invoice_return_product->amount,2) }}</strong>
                </td>
            </tr>
        @endforeach
        @if($invoice_return->discount_amount!=0)
            <tr>
                <td></td>
                <td class="text-right">
                    <strong class="font-italic">Discount Amount</strong>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right">
                    <strong>{{ number_format($invoice_return->discount_amount,2) }}</strong>
                </td>
            </tr>
        @endif
        @if($invoice_return->tax_amount!=0)
            @if($invoice_return->source_id==$invoice_return->destination_id)
                <tr>
                    <td></td>
                    <td class="text-right">
                        <strong class="font-italic">Output CGST</strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <strong>{{ number_format($invoice_return->tax_amount/2,2) }}</strong>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <strong class="font-italic">Output SGST</strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <strong>{{ number_format($invoice_return->tax_amount/2,2) }}</strong>
                    </td>
                </tr>
            @else
                <tr>
                    <td></td>
                    <td class="text-right">
                        <strong class="font-italic">Output IGST</strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <strong>{{ number_format($invoice_return->tax_amount,2) }}</strong>
                    </td>
                </tr>   
            @endif  
        @endif  
        @if($invoice_return->round_off!=0)
            <tr>
                <td></td>
                <td class="text-right">
                    <strong class="font-italic">Rounded Off</strong>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right">
                    <strong>{{ number_format($invoice_return->round_off,2) }}</strong>
                </td>
            </tr>
        @endif
        @for($j=$i;$j<=12;$j++)
            <tr>
                <td><br></td>
                <td class="text-right"></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right"></td>
            </tr>
        @endfor
        <tr>
            <th></th>
            <th class="text-right">Total</th>
            <th></th>
            <th class="text-right">{{ number_format($invoice_return->InvoiceReturnProducts->sum('quantity'),2) }}</th>
            <th></th>
            <th class="text-right">₹ {{ number_format($invoice_return->grand_total,2) }}</th>
        </tr>
        <tr>
            <td colspan="6">
                <p>Amount Chargeable (in words) : </p>
                <strong>INR {{ convert_number_to_words(round($invoice_return->grand_total)) }} ONLY</strong>
            </td>
        </tr>
    </table>
    <table class="tax-table">
        <tr>
            <th class="font" width="40%" rowspan="2">HSN/SAC</th>
            <th class="font" rowspan="2">Taxable <br> Value</th>
            @if($invoice_return->source_id==$invoice_return->destination_id)
                <th class="font" colspan="2">Central Tax</th>
                <th class="font" colspan="2">State Tax</th>
            @else   
                <th class="font" colspan="2">Central Tax</th>
            @endif  
            <th class="font" rowspan="2">Total <br> Tax Amount</td>
        </tr>
        <tr>
            @if($invoice_return->source_id==$invoice_return->destination_id)
                <th class="font">Rate</th>
                <th class="font">Amount</th>
                <th class="font">Rate</th>
                <th class="font">Amount</th>
            @else
                <th class="font">Rate</th>
                <th class="font">Amount</th>
            @endif  
        </tr>
        @php
            $final_taxable_value = 0;
            $final_gst = 0;
            $final_cgst = 0;
            $final_sgst = 0;
            $final_igst = 0;
            $final_grand_total = 0;
        @endphp
        @foreach($invoice_return->taxSlab($invoice_return->invoice_return_id) as $slab)
            @php
                $taxable_value = $invoice_return->taxableValue($invoice_return->invoice_return_id,$slab->tax_id,$slab->hsn_code);
                $cgst = round(($taxable_value * $slab->Tax->cgst_rate)/100,2);
                $sgst = round(($taxable_value * $slab->Tax->cgst_rate)/100,2);
                $igst = round(($taxable_value * $slab->Tax->igst_rate)/100,2);
                if($invoice_return->source_id==$invoice_return->destination_id)
                {
                    $gst = $cgst + $sgst;
                }
                else
                {
                    $gst = $igst;
                }
                $final_taxable_value += $taxable_value;
                $final_cgst +=$cgst;
                $final_sgst +=$sgst;
                $final_igst +=$igst;
                $final_gst +=$gst;
            @endphp
            <tr>
                <td>{{ $slab->hsn_code }}</td>
                <td class="text-right">{{ number_format($taxable_value,2) }}</td>
                @if($invoice_return->source_id==$invoice_return->destination_id)
                    <td class="text-right">{{ $slab->Tax->cgst_name }}</td>
                    <td class="text-right">{{ number_format($cgst,2) }}</td>
                    <td class="text-right">{{ $slab->Tax->sgst_name }}</td>
                    <td class="text-right">{{ number_format($sgst,2) }}</td>
                @else
                    <td class="text-right">{{ $slab->Tax->igst_name }}</td>
                    <td class="text-right">{{ number_format($igst,2) }}</td>
                @endif  
                <td class="text-right">{{ number_format($gst,2) }}</td>
            </tr>
        @endforeach 
        <tr>
            <th class="text-right">Total</th>
            <td class="text-right">{{ number_format($final_taxable_value,2) }}</td>
            @if($invoice_return->source_id==$invoice_return->destination_id)
                <th class="text-right"></th>
                <th class="text-right">{{ number_format($final_cgst,2) }}</th>
                <th class="text-right"></th>
                <th class="text-right">{{ number_format($final_sgst,2) }}</th>
            @else
                <th class="text-right"></th>
                <th class="text-right">{{ number_format($final_igst,2) }}</th>
            @endif  
            <th class="text-right">{{ number_format($final_gst,2) }}</th>
        </tr>
    </table>
    <table class="footer-table">
        <tr>
            <td class="extra-padding" colspan="7">
                <p>Tax Amount (in words) : </p>
                <strong>INR {{ convert_number_to_words(round($final_gst)) }} ONLY</strong>
            </td>
        </tr>
        <tr>
            <td width="50%" class="vertical-align">
                <pre>Company's PAN    :   <strong>{{ $org->pan_no }}</strong></pre>
            </td>
            <td width="50%">
                @if($account)
                    <strong>Company's Bank Details</strong>
                    <pre>Bank Name    : {{ $account->bank_name }}</pre>
                    <pre>Branch Name  : {{ $account->branch_name }}</pre>
                    <pre>Account No   : {{ $account->account_no }}</pre>
                    <pre>IFSC Code    : {{ $account->ifsc_code }}</pre>
                @endif
            </td>
        </tr>
    </table>
    <table class="table">   
        <tr>
            <td width="50%">
                <strong>Declaration</strong>
                <p>We declare that this invoice return shows the actual price of the goods described and that all particulars are true and correct.</p>
            </td>
            <td width="50%" class="text-right">
                <strong>for {{ $org->org_name }}</strong> <br><br><br><br>
                <p>Authorised Signatory</p>
            </td>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
         <p class="text-center">Subject to {{$org->org_code}} Jurisdiction</p>
        <p class="text-center">This is a Computer Generated Invoice Return</p>
    </htmlpagefooter>
</body>
</html>
@php
    function convert_number_to_words($no)
    {   
        $words = array('0'=> 'ZERO' ,'1'=> 'ONE' ,'2'=> 'TWO' ,'3' => 'THREE','4' => 'FOUR','5' => 'FIVE','6' => 'SIX','7' => 'SEVEN','8' => 'EIGHT','9' => 'NINE','10' => 'TEN','11' => 'ELEVEN','12' => 'TWELVE','13' => 'THIRTEEN','14' => 'FOURTEEN','15' => 'FIFTEEN','16' => 'SIXTEEN','17' => 'SEVENTEEN','18' => 'EIGHTEEN','19' => 'NINETEEN','20' => 'TWENTY','30' => 'THIRTY','40' => 'FOURTY','50' => 'FIFTY','60' => 'SIXTY','70' => 'SEVENTY','80' => 'EIGHTY','90' => 'NINTY','100' => 'HUNDRED','1000' => 'THOUSAND','100000' => 'LAKH','10000000' => 'CRORE');
        if($no == 0)
        {
            return '';
        }
        else 
        {
            $novalue='';
            $highno=$no;
            $remainno=0;
            $value=100;
            $value1=1000;       
            while($no>=100)    
            {
                if(($value <= $no) &&($no  < $value1))    
                {
                    $novalue=$words["$value"];
                    $highno = (int)($no/$value);
                    $remainno = $no % $value;
                    break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
            if(array_key_exists("$highno",$words))
            {
                return $words["$highno"]." ".$novalue." ".convert_number_to_words($remainno);
            }
            else 
            {
                $unit=$highno%10;
                $ten =(int)($highno/10)*10;            
                return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_number_to_words($remainno);
            }
        }
    }
@endphp