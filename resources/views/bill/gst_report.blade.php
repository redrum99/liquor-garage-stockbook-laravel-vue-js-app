@if($request->display_type=="excel")
@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=bill-gst-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
    <title>Bill</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/bill/new.css') }}">
</head>
<style type="text/css">
    .text-center{
        text-align: center;
    }
    .text-right{
        text-align: right;
    }
</style>
<body>
    <table width="100%" border="1">
        <tr>
            <th class="text-center" colspan="9">BILL GST REPORT</th>
        </tr>
        <tr>
            <td colspan="5">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        <tr>
            <th class="text-center">#</th>
            <th>Bill No</th>
            <th>Bill Date</th>
            <th>Status</th>
            <th>Vendor</th>
            <th>Vendor Code</th>
            <th>GSTIN</th>
            <th class="text-right">Grand Total</th>
        </tr>
        @php
            $i=0;
            $overall_total_amount=0;
            $overall_total_cgst=0;
            $overall_total_sgst=0;
            $overall_total_igst=0;
            $overall_total_sub=0;
        @endphp
        @foreach($bills as $bill)
            <tr>
                <th colspan="9">{{ ++$i }}</th>
            </tr>
            <tr>
                <td colspan="2">{{ $bill->bill_no }}</td>
                <td>{{  date('d-m-Y',strtotime($bill->bill_date)) }}</td>
                <td>{{ $bill->bill_status }}</td>
                <td>{{ $bill->Vendor->contact_name }}</td>
                <td>{{ $bill->Vendor->contact_code }}</td>
                <td>{{ $bill->Vendor->gstin_no }}</td>
                <td class="text-right">{{ number_format($bill->grand_total,2) }}</td>
            </tr>
            <tr>
                <th colspan="2" rowspan="2" class="text-center">#</th>
                <th rowspan="2">Taxable Amount</th>
                @if($bill->source_id == $bill->destination_id)
                <th colspan="2" class="text-center">CGST</th>
                <th colspan="2" class="text-center">SGST</th>
                @endif
                @if($bill->source_id != $bill->destination_id)
                <th colspan="4" class="text-center">IGST</th>
                @endif
                <th rowspan="2">Total Amount</th>
            </tr>
            <tr>
                @if($bill->source_id == $bill->destination_id)
                    <th>Rate</th>
                    <th>Amount</th>
                    <th>Rate</th>
                    <th>Amount</th>
                @endif
                @if($bill->source_id != $bill->destination_id)
                    <th colspan="2">Rate</th>
                    <th colspan="2">Amount</th>
                @endif
            </tr>
            @php
                $j=0;
                $total_amount=0;
                $total_cgst=0;
                $total_sgst=0;
                $total_igst=0;
                $total_sub=0;
            @endphp
            @foreach($bill->BillTax as $taxable)
            <tr>
                <td colspan="2" class="text-center">{{ ++ $j }}</td>
                <td class="text-right">{{ number_format($taxable->amount,2) }}</td>
                @if($bill->source_id == $bill->destination_id)
                    <td class="text-right">{{ $taxable->Tax->cgst_name }}</td>
                    <td class="text-right">{{ number_format($taxable->cgst_amount,2) }}</td>
                    <td class="text-right">{{ $taxable->Tax->sgst_name }}</td>
                    <td class="text-right">{{ number_format($taxable->sgst_amount,2) }}</td>
                @endif
                @if($bill->source_id != $bill->destination_id)
                    <td colspan="2" class="text-right">{{ $taxable->Tax->igst_name }}</td>
                    <td colspan="2" class="text-right">{{ number_format($taxable->igst_amount,2) }}</td>
                @endif
                <td class="text-right">{{ number_format($taxable->sub_total,2) }}</td>
            </tr>
            @php
                $total_amount+=$taxable->amount;
                $total_cgst+=$taxable->cgst_amount;
                $total_sgst+=$taxable->sgst_amount;
                $total_igst+=$taxable->igst_amount;
                $total_sub+=$taxable->sub_total;
            @endphp
            @endforeach
            <tr>
                <th colspan="2" class="text-center">Total</th>
                <td class="text-right">{{ number_format($total_amount,2) }}</td>
                @if($bill->source_id == $bill->destination_id)
                    <td colspan="2" class="text-right">{{ number_format($total_cgst,2) }}</td>
                    <td colspan="2" class="text-right">{{ number_format($total_sgst,2) }}</td>
                    @php
                        $overall_total_cgst+=$total_cgst;
                        $overall_total_sgst+=$total_sgst;
                    @endphp
                @endif
                @if($bill->source_id != $bill->destination_id)
                    <td colspan="4" class="text-right">{{ number_format($total_igst,2) }}</td>
                    @php
                        $overall_total_igst+=$total_igst;
                    @endphp
                @endif
                <td class="text-right">{{ number_format($total_sub,2) }}</td>
            </tr>
            @php
                $overall_total_amount+=$total_amount;
                $overall_total_sub+=$total_sub;
            @endphp
            <tr>
                <td colspan="8"></td>
            </tr>
        @endforeach
        <tr>
            <th colspan="2" class="text-center">Overall Total</th>
            <td class="text-right">{{ number_format($overall_total_amount,2) }}</td>
            <td class="text-right"><b>CGST :</b> {{ number_format($overall_total_cgst,2) }}</td>
            <td class="text-right"><b>SGST :</b> {{ number_format($overall_total_sgst,2) }}</td>
            <td colspan="2" class="text-right"><b>IGST :</b> {{ number_format($overall_total_igst,2) }}</td>
            <td class="text-right">{{ number_format($overall_total_sub,2) }}</td>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
        {{-- <span class="text-right">PAGE {PAGENO}</span> --}}
    </htmlpagefooter>
</body>
</html>