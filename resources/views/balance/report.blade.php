@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=balance.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Delivery Challan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/bill/pdf1.css') }}">
</head>
<body>
    <table class="table-100" border="1">
        <tr>
            <th align="center" colspan="5">BALANCE REPORT</th>
        </tr>
        <tr>
            <td colspan="5">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                From Date: {{ date('d-m-Y',strtotime($request->from_date)) }} --
                To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}
            </td>
        </tr>

        <tr>
            <th align="center" colspan="2">Mode</th>
            <th align="right">Payments</th>
            <th align="right">Receipts</th>
            <th align="right">Balance</th>
        </tr>
        @foreach($payment_modes as $payment_mode)
            <tr>
                <td colspan="2" align="center">{{$payment_mode->master_value}}</td>
                <td align="right"> {{ number_format(get_payment_data($payment_mode->master_value , $payment , $expense) ,2) }} </td>
                <td align="right">{{ number_format(get_receipt_data($payment_mode->master_value,$receipt,$income) ,2) }}</td>
                <td align="right">{{ number_format(get_balance_data($payment_mode->master_value,$payment,$expense,$receipt,$income) ,2) }}</td>
            </tr>

        @endforeach
        <tr>
            <th colspan="2" align="right">Total</th>
            <th align="right">{{ number_format(get_total_payment($payment,$expense) ,2) }}</th>
            <th align="right">{{ number_format(get_total_receipt($receipt,$income) ,2) }}</th>
            <th align="right">{{ number_format(get_balance($payment,$expense,$receipt,$income) ,2) }}</th>
        <tr>
      
    </table>
    <htmlpagefooter name="page-footer">
        <span class="text-right">PAGE {PAGENO}</span>
    </htmlpagefooter>
</body>
</html>
@php
   
      
    function get_payment_data($payment_mode,$payment,$expense)
    { 
        $payment_amount=0;
        $expense_amount=0;
        $sum = 0;
        foreach($payment as $pay){
            if($pay->payment_mode==$payment_mode){
                $payment_amount += $pay->total_amount;
            }
        }
        foreach($expense as $pay){
            if($pay->payment_mode==$payment_mode){
                $expense_amount += $pay->amount;
            }
        }
        $sum = $payment_amount+$expense_amount;
        return $sum;
    }

    function get_receipt_data($payment_mode,$receipt,$income)
    { 
        $receipt_amount=0;
        $income_amount=0;
        $sum = 0;
        foreach($receipt as $pay){
            if($pay->payment_mode==$payment_mode){
                $receipt_amount += $pay->total_amount;
            }
        }
        foreach($income as $pay){
            if($pay->payment_mode==$payment_mode){
                $income_amount += $pay->amount;
            }
        }
        $sum = $receipt_amount+$income_amount;
        return $sum;
    }

    function get_balance_data($payment_mode,$payment,$expense,$receipt,$income)
    {
        $receipt_amount=0;
        $payment_amount=0;
        $receipt_amount = get_receipt_data($payment_mode,$receipt,$income);
        $payment_amount = get_payment_data($payment_mode,$payment,$expense);

        return $receipt_amount-$payment_amount;
    }

    function get_total_payment($payment,$expense){
        $payment_amount=0;
        $expense_amount=0;
        $sum = 0;
        foreach($payment as $pay){
                $payment_amount += $pay->total_amount;
            
        }
        foreach($expense as $pay){
           
            $expense_amount += $pay->amount;
           
        }
        $sum = $payment_amount+$expense_amount;
        return $sum;
    }

    function get_total_receipt($receipt,$income)
    { 
        $receipt_amount=0;
        $income_amount=0;
        $sum = 0;
        foreach($receipt as $pay){
            $receipt_amount += $pay->total_amount;
        }
        foreach($income as $pay){
            $income_amount += $pay->amount;
        }
        $sum = $receipt_amount+$income_amount;
        return $sum;
    }

    function get_balance($payment,$expense,$receipt,$income)
    {
        $receipt_amount=0;
        $payment_amount=0;
        $sum =0;
        $receipt_amount = get_total_receipt($receipt,$income);
        $payment_amount = get_total_payment($payment,$expense);
       
        $sum = $receipt_amount-$payment_amount;

        return $sum;
    }




    
@endphp