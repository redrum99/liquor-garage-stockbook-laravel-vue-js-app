@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=delivery-return-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Delivery Return</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/delivery_return/report.css') }}">
</head>
<body>
    <table border="1">
        <tr> 
            <th colspan="10">DELIVERY RETURN REPORT</th>
        </tr>
        <tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
                @if($customer!='')
                    <div>
                        <strong>{{ $customer->contact_name }}</strong>
                    </div>
                    <div>{{ $customer->billing_address }}</div>
                    <div>Phone: {{ $customer->mobile_no }}</div>
                    <div>GSTIN: {{ $customer->gstin_no }}</div>
                @endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
            <tr>
                <th class="text-center">#</th>
                <th>Dc No</th>
                <th>Dc Date</th>
                <th>Customer</th>
                <th class="text-right">Sub Total</th>
                @if($setting->discounts_in == 'inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($setting->discounts_in == 'exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
                <th class="text-right">Round off</th>
                <th class="text-right">Grand Total</th>
            </tr>
            @php
                $i=0; 
                $sub_total = 0;
                $tax_amount = 0;
                $discount_amount = 0;
                $total_amount = 0;
                $round_off = 0;
                $grand_total = 0;
            @endphp
            @foreach($delivery_returns as $delivery_return)
                <tr>
                    <td class="text-center">
                        {{ ++$i }}
                    </td>
                    <td>
                        {{ $delivery_return->delivery_return_no }}
                    </td>
                    <td>
                        {{ date('d-m-Y',strtotime($delivery_return->delivery_return_date)) }}
                    </td>
                    <td>
                        {{ $delivery_return->Customer->contact_name }}
                    </td>
                    <td class="text-right">
                        {{ number_format($delivery_return->sub_total,2) }}
                    </td>
                    @if($setting->discounts_in == 'inc')
                    <td class="text-right">
                        {{ number_format($delivery_return->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($delivery_return->discount_amount,2) }}
                    </td>
                    @if($setting->discounts_in == 'exc')
                    <td class="text-right">
                        {{ number_format($delivery_return->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($delivery_return->total_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($delivery_return->round_off,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($delivery_return->grand_total,2) }}
                    </td>
                    @php
                        $sub_total += $delivery_return->sub_total;
                        $discount_amount += $delivery_return->discount_amount;
                        $tax_amount += $delivery_return->tax_amount;
                        $total_amount += $delivery_return->total_amount;
                        $round_off += $delivery_return->round_off;
                        $grand_total += $delivery_return->grand_total;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <th colspan="4" class="text-right">Total</th>
                <th class="text-right">{{ number_format($sub_total,2) }}</th>
                @if($setting->discounts_in == 'inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($setting->discounts_in == 'exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
                <th class="text-right">{{ number_format($round_off,2) }}</th>
                <th class="text-right">{{ number_format($grand_total,2) }}</th>
            </tr>
        @endif
        @if($request->report_type =='Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($delivery_returns as $delivery_return)
                <tr>
                    <th colspan="10">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="6">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $delivery_return->Customer->contact_name }}</div>
                        <div>{{ $delivery_return->Customer->billing_address }}</div>
                        <div>{{ $delivery_return->Customer->shipping_address }}</div>
                        <div>{{ $delivery_return->Customer->email }}</div>
                        <div>{{ $delivery_return->Customer->mobile_no }}</div>
                        <div>{{ $delivery_return->Customer->pan_no }}</div>
                        <div>{{ $delivery_return->Customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>DC No:{{ $delivery_return->delivery_return_no }}</div>
                        <div>DC Date:{{ date('d-m-Y',strtotime($delivery_return->delivery_return_date)) }}</div>
                        <div> Ref No :{{ $delivery_return->reference_no }}</div>
                        <div>Ref Date :{{ $delivery_return->reference_date ? date('d-m-Y',strtotime($delivery_return->reference_date)) : '' }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">S.Rate(Exc)</th>
                    <th class="text-right">S.Rate(Inc)</th>
                    @if($setting->discounts_in == 'inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($setting->discounts_in == 'exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($delivery_return->DeliveryReturnProducts as $delivery_return_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $delivery_return_product->product_code }}
                        </td>
                        <td>
                            {{ $delivery_return_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($delivery_return_product->sales_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($delivery_return_product->sales_rate_inc,2) }}
                        </td>
                        @if($setting->discounts_in == 'inc')
                        <td>
                            {{ number_format($delivery_return_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($delivery_return_product->discount_amount,2) }}
                        </td>
                        @if($setting->discounts_in == 'exc')
                        <td>
                            {{ number_format($delivery_return_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td>
                            {{ number_format($delivery_return_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($delivery_return_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="6">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $delivery_return->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->sub_total,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->discount_amount,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in == 'exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="6">
                        <strong>Note : </strong><br>
                        {{ $delivery_return->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($delivery_return->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
    </table>
    <htmlpagefooter name="page-footer">
        <span class="text-right">PAGE {PAGENO}</span>
    </htmlpagefooter>
</body>
</html>