<!DOCTYPE html>
<html>
<head>
    <title>{{ $delivery_return->delivery_return_no}}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/delivery_return/pdf2.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100">
            <tr>
                <td align="center"><b>DELIVEY RETURN</b></td>
            </tr>
        </table>
        <table class="table-100 table-border th-font">
            <tr>
                <td colspan="2"><img src="{{asset('public/images/organization/header.png')}}"></td>
            </tr>
            <tr>
                <td align="left"><b>PAN NO : </b> {{ $org->pan_no }}</td>
                <td align="right"><b>GSTIN : </b> {{ $org->gstin_no }}</td>
            </tr>
        </table>
    </htmlpageheader>
    <table class="table-100 table-border th-font" border="1">
        <tr>
            <td width="50%"><span> FROM,</span><br>
                <span><b>{{ $org->org_name}}</b></span>
                <br/><span>{{ $org->address}}</span>
                <br/><span>{{ $org->Places->place_name}}-{{ $org->Places->place_code}}</span>
                <br/><span>PAN NO : {{ $org->pan_no}}</span>
                <br/>Mobile No : <span>{{ $org->mobile_no}}</span>
                <br/>Email : <span>{{ $org->email}}</span>
                <br/><span>GSTIN No : {{ $org->gstin_no}}</span>
            </td>
            <td width="50%"><span> DELIVERY RETURN &amp; SHIP TO,</span><br>
                <span><b>{{ $delivery_return->Customer->contact_name}}</b></span>
                <br/><span>{{ $delivery_return->Customer->billing_address }}</span>
                <br/><span>{{ $delivery_return->Customer->shipping_address }}</span>
                <br/><span>PAN No : {{ $delivery_return->Customer->pan_no }}</span>
                <br/><span>Mobile No : {{ $delivery_return->Customer->mobile_no }}</span>
                <br/><span>Email : {{ $delivery_return->Customer->email }}</span>
                <br/><span>GSTIN No : {{ $delivery_return->Customer->gstin_no }}</span>
            </td>
        </tr>
    </table>
    <table class="table-100 table-border th-font" border="1">
        <tr>
            <td width="20%" align="left" id="rcorners1">No: {{ $delivery_return->delivery_return_no }}</td>
            <td width="60%" align="center">Received the following items subject to terms &amp; conditions mentioned below</td>
            <td width="20%" align="right" id="rcorners1">Date: {{ date('d-m-Y',strtotime($delivery_return->delivery_return_date))}}</td>
        </tr>
    </table>
    <table class="table-100 table-border th-font" border="1">
        <tr>
            <td width="20%">PO No/ Reference : {{ $delivery_return->reference_no }}</td>
            <td width="60%">Reasson for Shipment :</td>
            <td width="20%">Total No Cases :</td>
        </tr>
    </table>
    <table style="margin-left: -5px !important; margin-right: -5px !important;" class="table-100">
        <tr>
            <td class="width-70">
                <table class="table-border table-100" border="1">
                    <tr>
                        <td align="center">#</td>
                        <td align="center">Code</td>
                        <td align="center">Description</td>
                        <td align="center">HSN/SAC</td>
                        <td align="center">Qty</td>
                    </tr>
                    @php
                        $i=1;
                    @endphp
                    @foreach($delivery_return->DeliveryReturnProducts as $po)
                        <tr>
                            <td>{{ $i++}}</td>
                            <td>{{ $po->product_code }}</td>
                            <td>{{ $po->product_name }}</td>
                            <td>{{ $po->hsn_code }}</td>
                            <td align="center">{{ $po->quantity }}</td>
                        </tr>
                    @endforeach
                    @for($j=$i;$j<=9;$j++)
                        <tr>
                            <td><br></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endfor
                </table>
            </td>
            <td class="table-30">
                <table class="table-100 table-border">
                    <tr>
                        <td><input type="checkbox" name="installation" value="Bike"> Given for Installation</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="approval" value="Car"> Given on Approval Basis</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="demo" value="demo"> Given on Demo</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="repairs" value="repairs"> Given on Repairs</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="replacement" value="replacement"> Replacement</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="goods" value="goods"> Goods Returned</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="returnable" value="returnable"> Returnable</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="non-return" value="non-return"> Non-Returnable</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="billable" value="billable"> Billable</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="non-billable" value="non-billable"> Non-Billable</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="table-border table-100" border="1">
        <tr>
            <td>Warranty is valid upto-   DeliveryReturn/Return Date: {{ date('d-m-Y',strtotime($delivery_return->delivery_return_date)) }} <b>E. &amp; O.E</b> </td>
        </tr>
    </table>
    <table class="table-border table-100 th-font" border="1">
        <tr>
            <td><b><u>Terms &amp; Conditions:</u></b></td>
        </tr>
        <tr>
            <td><span>1. Product which are burnt or physically damaged, warranty sticker tampered or damaged would not come under warranty and no replacement and no action will be taken for such products.</span>
            <br/><span>2.Cash refund will not be made under any circumstances.</span>
            <br/><span>3.Goods will be replaced/repaired in 7-10 working days from the receipt of date.</span>
            <br/><span>4. Warranty as per terms and conditions of manufacturer of the respective product.</span>
            <br/><span>5. The consumer is liable to pay Rs.100/- as fault identification charges for the material being repaired which is out of warranty.</span>
            <br/><span>6. Goods given for repair or upgrade should be taken back before 30 days from the date of intimation.</span>
            </td>
        </tr>
    </table>
    @if(!empty($delivery_return->terms))
        <table class="table-border th-font table-100">
            <tr>
                <td><b><u>Special Instruction(if any):</u></b> <pre>{{ $delivery_return->terms}}</pre></td>
            </tr>
        </table>
    @endif
    <table class="table-100 table-border" style="margin-left: -6px !important;">
        <tr >
            <td class="width-25">
                <table class="table-100 th-font table-border">
                    <tr>
                        <td colspan="2"><b>SECURITY IN/OUT</b></td>
                    </tr>
                    <tr>
                        <td>Date: ...........</td>
                        <td>Time : ..........</td>
                    </tr>
                    <tr>
                        <td>Sl No: ...........</td>
                        <td>Sign : ..........</td>
                    </tr>
                </table>
            </td>
            <td class="width-75">
                <table class="table-100 th-font">
                    <tr>
                        <td>..................</td>
                        <td>..................</td>
                        <td>..................</td>
                    </tr>
                    <tr>
                        <td>Prepared By</td>
                        <td>Checked By</td>
                        <td>Authorized By</td>
                    </tr>
                    <tr>
                        <td>..................</td>
                        <td colspan="2">..................................</td>
                    </tr>
                    <tr>
                        <td>Received By</td>
                        <td colspan="2">Received Date &amp; Stamp</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>