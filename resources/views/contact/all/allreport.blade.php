@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=contact_type.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Delivery Challan</title>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/delivery/report.css') }}"> -->
</head>
<body>
    <table border="1">
        <tr>
            <th colspan="9">CONTACT REPORT</th>
        </tr>
        <tr>
            <th class="text-center">#</th>
            <th>Contact Type</th>
            <th>Contact Code</th>
            <th class="text-right">Email</th>
            <th class="text-right">Mobile No</th>
        </tr>
        @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->contact_name}}</td>
                <td>{{ $contact->contact_type}}</td>
                <td>{{ $contact->contact_code}}</td>
                <td>{{ $contact->email}}</td>
                <td>{{ $contact->mobile_no}}</td>
            </tr>
        @endforeach
    </table>   
   
</body>
</html>