@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=vendor-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
   {{--  <title>{{ $invoice->invoice_no }}</title> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/contact/customer/report.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100 table-border">
            <td colspan="5">
                <tr>
                    <td align="center" colspan="5"><h3>Vendor</h3></td>
                </tr>
                <tr>
                    <td><b>Phone No:</b> {{ $org->phone_no }} </td>
                    <td align="right" colspan="4"><b>Mobile No :</b> {{ $org->mobile_no }} </td>
                </tr>
                <tr>
                    <td><b>PAN NO : {{ $org->pan_no }}</b></td>
                    <td align="right" colspan="4"><b>GSTIN : {{ $org->gstin_no }}</b></td>
                </tr>
                <tr>
                    <td align="center" colspan="5"><h2>{{ $org->org_name }}</h2></td>
                </tr>
                <tr>
                    <td align="center" colspan="5"><span>{{ $org->address }}</span></td>
                </tr>
            </td>
        </table>
    </htmlpageheader>
    <table class="table-100" border="1">
          <tr>
            
            <td colspan="3">
                <span><b>Vendor Details :</b></span>
                @if(!$vendor==0)
                <br/><span><b>{{ $vendor->contact_name }}</b></span>
                <br/><span><b>Phone :</b> {{ $vendor->mobile_no }}</span>
                <br/><span><b>Email :</b> {{ $vendor->email }}</span>
                <br/><span><b>Billing Address :</b>{{ $vendor->billing_address }}</span>
                <br/><span><b>Shipping Address :</b>{{ $vendor->shipping_address }}</span>
                <br/><span><b>GSTIN :</b> {{ $vendor->gstin_no }}</span>
                @endif
            </td>
            <td colspan="2">
                <span><b>From Date : </b><br>{{ date('d-m-Y',strtotime($request->from_date)) }} </span><br>
                <span><b>To Date:</b> <br>{{ date('d-m-Y',strtotime($request->to_date)) }}</span>
            </td>
        </tr>
    </table>
    <table class="table-border table-100 th-font" border="1">
        <tr>
            <td align="center"><b>SL.#</b></td>
            <td align="center"><b>DATE</b></td>
            <td align="center"><b>PARTICULARS</b></td>
            <td align="center"><b>PAYMENT</b></td>
            <td align="center"><b>RECEIPT</b></td>
        </tr>
        <tr>
            <td align="center"><b>#</b></td>
            <td align="center"><b>{{ date('d-m-Y',strtotime($request->from_date)) }}</b></td>
            <td align="center"><b>OPENING BALANCE</b></td>
            <td align="right"><b>{{ number_format($openingPaymentTotal,2) }}</b></td>
            <td align="right"><b>{{ number_format($openingReceiptTotal,2)}}</b></td>
        </tr>
         @php 
            $i = 1;
            $j=0; 
            $bill_amount = 0;
            $payment_amount = 0;
            $income_amount = 0;
            $expense_amount = 0;
        @endphp
        @foreach($bills as $bl)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ date('d-m-Y',strtotime($bl->bill_date)) }}</td>
                <td>{{ $bl->bill_no }}</td>
                <td align="right">{{ number_format($bl->grand_total,2) }}</td>
                <td>-</td>
            </tr>
            @php
            $bill_amount += $bl->grand_total; 
            @endphp
        @endforeach
        @foreach($payments as $pay)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{  date('d-m-Y',strtotime($pay->voucher_date)) }}</td>
                <td>{{ $pay->voucher_no }}</td>
                <td>-</td>
                <td align="right">{{ number_format($pay->total_amount,2) }}</td>
            </tr>
            @php
            $payment_amount += $pay->total_amount; 
            @endphp
        @endforeach
        @foreach($incomes as $inc)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{  date('d-m-Y',strtotime($inc->receipt_date)) }}</td>
                <td>{{ $inc->receipt_no }}</td>
                <td>-</td>
                <td align="right">{{ number_format($inc->amount,2) }}</td>
               
            </tr>
            @php
            $income_amount += $inc->amount; 
            @endphp
        @endforeach
        @foreach($expenses as $exp)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ date('d-m-Y',strtotime($exp->voucher_date)) }}</td>
                <td>{{$exp->voucher_no }}</td>
                <td align="right">{{ number_format($exp->amount,2) }}</td>
                <td>-</td>
            </tr>
            @php
            $expense_amount += $exp->amount; 
            @endphp
        @endforeach
        @php
            $payment_total = $bill_amount + $expense_amount + $openingPaymentTotal;
            $receipt_total = $payment_amount + $income_amount + $openingReceiptTotal;
            $balance_total = $payment_total - $receipt_total;
        @endphp
        @if(!$payment_total == 0 || !$receipt_total == 0)
        <tr>
            <td colspan="3" align="right">TOTAL</td>
            <td align="right"><b>{{ number_format($payment_total,2) }}</b></td>
            <td align="right"><b>{{ number_format($receipt_total,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="4" align="right">BALANCE AMOUNT</td>
            <td align="right"><b>{{ number_format($balance_total,2) }}</b></td>
        </tr>
        @endif
    </table>
     <htmlpagefooter name="page-footer">
        <table class="table-100">
            <tr>
                <td align="left">This is a computer generated Vendor Report and does not require seal and signature.</td>
                <td align="right">PAGE {PAGENO}</td>
            </tr>
        </table>
    </htmlpagefooter>
</body>
</html>