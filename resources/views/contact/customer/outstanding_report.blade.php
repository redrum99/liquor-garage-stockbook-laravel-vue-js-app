{{-- @php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=customer-outstanding-report.xls");
@endphp --}}
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/contact/customer/outstanding_report.css') }}">
</head>
<body>
    <table border="1">
        <tr>
            <th class="text-center" colspan="10">CUSTOMER OUTSTANDING REPORT</th>
        </tr>
        <tr>
            <th class="text-center" colspan="10">From Date: {{ date('d-m-Y',strtotime($request->from_date)) }} - To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</th>
        </tr>
        @if($request->report_type == 'Brief Report')
        @php $k = 0 @endphp
        @foreach($contacts as $contact)
            <tr>
                <th rowspan="{{ count($contact->invoice_receipts)+4 }}">{{ ++$k }}</th>
            </tr>
            <tr>
                <td colspan="5">
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
                <td colspan="4">
                    <div>
                        <strong>{{ $contact->contact_name }}</strong>
                    </div>
                    <div>{{ $contact->billing_address }}</div>
                    <div>Phone: {{ $contact->mobile_no }}</div>
                    <div>GSTIN: {{ $contact->gstin_no }}</div>
                </td>
            </tr>
            <tr>
                <th class="text-center">#</th>
                <th colspan="2">Date</th>
                <th colspan="2">Invoice No</th>
                <th colspan="2">Total</th>
                <th>Paid</th>
                <th>Due</th>
            </tr>
            @php
                $i=0;
                $total = 0;
                $paid = 0;
                $due = 0;
            @endphp
            @foreach($contact->invoice_receipts as $inv)
                <tr>
                    <td class="text-center">
                        {{ ++$i }}
                    </td>
                    <td colspan="2">
                        {{ date('d-m-Y',strtotime($inv->invoice_date)) }}
                    </td>
                    <td colspan="2">
                        {{ $inv->invoice_no }}
                    </td>
                    @php
                        if($inv->new_receipt_total($inv->invoice_id) == null){
                            $paid_amount = 0;
                            $due_amount = $inv->grand_total;    
                        }else{
                            $paid_amount = $inv->new_receipt_total($inv->invoice_id);
                            $due_amount = $inv->grand_total- $inv->new_receipt_total($inv->invoice_id);    
                        }
                    @endphp
                    <td class="text-right" colspan="2">
                        {{ number_format($inv->grand_total,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($paid_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($due_amount,2) }}
                    </td>
                    @php
                        $total += $inv->grand_total;
                        $paid += $paid_amount;
                        $due += $due_amount;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <th colspan="6" class="text-right">Total</th>
                <th class="text-right">{{ number_format($total,2) }}</th>
                <th class="text-right">{{ number_format($paid,2) }}</th>
                <th class="text-right">{{ number_format($due,2) }}</th>
            </tr>
            <tr>
                <th colspan="10"></th>
            </tr>
        @endforeach
        @endif
        {{-- @if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($purchase_orders as $purchase_order)
                <tr>
                    <th colspan="9">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $purchase_order->customer->contact_name }}</div>
                        <div>{{ $purchase_order->customer->billing_address }}</div>
                        <div>{{ $purchase_order->customer->shipping_address }}</div>
                        <div>{{ $purchase_order->customer->email }}</div>
                        <div>{{ $purchase_order->customer->mobile_no }}</div>
                        <div>{{ $purchase_order->customer->pan_no }}</div>
                        <div>{{ $purchase_order->customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>PO No:{{ $purchase_order->purchase_order_no }}</div>
                        <div>PO Date:{{ date('d-m-Y',strtotime($purchase_order->purchase_order_date)) }}</div>
                        <div> Ref No :{{ $purchase_order->reference_no }}</div>
                        <div>Delivery Date :{{ date('d-m-Y',strtotime($purchase_order->delivery_date)) }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">P.Rate(Exc)</th>
                    <th class="text-right">P.Rate(Inc)</th>
                    <th class="text-right">Discount</th>
                    <th class="text-right">Tax</th>
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($purchase_order->PurchaseOrderProducts as $purchase_order_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $purchase_order_product->product_code }}
                        </td>
                        <td>
                            {{ $purchase_order_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->purchase_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($purchase_order_product->purchase_rate_inc,2) }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->discount_amount,2) }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->tax_amount,2) }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($purchase_order_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $purchase_order->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->sub_total,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->discount_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->tax_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $purchase_order->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($purchase_order->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif --}}
    </table>
    <htmlpagefooter name="page-footer">
        <span class="text-right">PAGE {PAGENO}</span>
    </htmlpagefooter>
</body>
</html>
