@php
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;Filename=quotation-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Quotation</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/quotation/report.css') }}">
</head>
<body>
	<table border="1">
		<tr >
			<th colspan="10">QUOTATION REPORT</th>
		</tr>
		<tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
            	@if($customer!='')
            		<div>
                        <strong>{{ $customer->contact_name }}</strong>
                    </div>
                    <div>{{ $customer->billing_address }}</div>
                    <div>Phone: {{ $customer->mobile_no }}</div>
                    <div>GSTIN: {{ $customer->gstin_no }}</div>
            	@endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
			<tr>
	            <th class="text-center">#</th>
	            <th>Quotation No</th>
	            <th>Quotation Date</th>
	            <th>Customer</th>
	            <th class="text-right">Sub Total</th>
                @if($setting->discounts_in=='inc')
                   <th class="text-right">Tax</th>
                @endif
	            <th class="text-right">Discount</th>
                @if($setting->discounts_in=='exc')
	               <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total</th>
	            <th class="text-right">Round off</th>
	            <th class="text-right">Grand Total</th>
	        </tr>
	        @php
	        	$i=0;
	        	$sub_total = 0;
	        	$discount_amount = 0;
	        	$tax_amount = 0;
	        	$round_off = 0;
	        	$grand_total = 0;
                $total_amount = 0;
	        @endphp
	        @foreach($quotations as $quotation)
		        <tr>
		            <td class="text-center">
		                {{ ++$i }}
		            </td>
		            <td>
		                {{ $quotation->quotation_no }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($quotation->quotation_date)) }}
		            </td>
		            <td>
		                {{ $quotation->Customer->contact_name }}
		            </td>
		            <td class="text-right">
		                {{ number_format($quotation->sub_total,2) }}
		            </td>
                    @if($setting->discounts_in=='inc')
                        <td class="text-right">
                            {{ number_format($quotation->tax_amount,2) }}
                        </td>
                    @endif
		            <td class="text-right">
		            	{{ number_format($quotation->discount_amount,2) }}
		            </td>
                    @if($setting->discounts_in=='exc')
    		            <td class="text-right">
    		            	{{ number_format($quotation->tax_amount,2) }}
    		            </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($quotation->total_amount,2) }}
                    </td>
		            <td class="text-right">
		            	{{ number_format($quotation->round_off,2) }}
		            </td>
		            <td class="text-right">
		            	{{ number_format($quotation->grand_total,2) }}
		            </td>
		            @php
		            	$sub_total += $quotation->sub_total;
		            	$discount_amount += $quotation->discount_amount;
		            	$tax_amount += $quotation->tax_amount;
		            	$round_off += $quotation->round_off;
		            	$grand_total += $quotation->grand_total;
                        $total_amount += $quotation->total_amount;
		            @endphp
		        </tr>
		    @endforeach
		    <tr>
		    	<th colspan="4" class="text-right">Total</th>
		    	<th class="text-right">{{ number_format($sub_total,2) }}</th>
                @if($setting->discounts_in=='inc')
                    <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
		    	<th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($setting->discounts_in=='exc')
		    	    <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
		    	<th class="text-right">{{ number_format($round_off,2) }}</th>
		    	<th class="text-right">{{ number_format($grand_total,2) }}</th>
		    </tr>
		@endif
		@if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($quotations as $quotation)
                <tr>
                    <th colspan="9">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $quotation->Customer->contact_name }}</div>
                        <div>{{ $quotation->Customer->billing_address }}</div>
                        <div>{{ $quotation->Customer->shipping_address }}</div>
                        <div>{{ $quotation->Customer->email }}</div>
                        <div>{{ $quotation->Customer->mobile_no }}</div>
                        <div>{{ $quotation->Customer->pan_no }}</div>
                        <div>{{ $quotation->Customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>quotation No:{{ $quotation->quotation_no }}</div>
                        <div>quotation Date:{{ date('d-m-Y',strtotime($quotation->quotation_date)) }}</div>
                        <div> Ref No :{{ $quotation->reference_no }}</div>
                        <div>Ref Date :{{ $quotation->reference_date ? date('d-m-Y',strtotime($quotation->reference_date)) : '' }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">S.Rate(Exc)</th>
                    <th class="text-right">S.Rate(Inc)</th>
                    @if($setting->discounts_in=='inc')
                        <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($setting->discounts_in=='exc')
                        <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($quotation->QuotationProducts as $quotation_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $quotation_product->product_code }}
                        </td>
                        <td>
                            {{ $quotation_product->product_name }}
                        </td>
                        <td>
                            {{ number_format($quotation_product->sales_rate_exc,2) }}
                        </td>
                        <td>
                           {{ number_format($quotation_product->sales_rate_inc,2) }}
                        </td>
                        @if($setting->discounts_in=='inc')
                            <td>
                                {{ number_format($quotation_product->tax_amount,2) }}
                            </td>
                        @endif
                        <td>
                            {{ number_format($quotation_product->discount_amount,2) }}
                        </td>
                        @if($setting->discounts_in=='exc')
                            <td>
                                {{ number_format($quotation_product->tax_amount,2) }}
                            </td>
                        @endif
                        <td>
                            {{ number_format($quotation_product->quantity,2) }}
                        </td>
                        <td>
                            {{ number_format($quotation_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $quotation->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($quotation->sub_total,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in=='inc')
                    <tr>
                        <th class="text-right" colspan="2">Tax</th>
                        <th class="text-right" colspan="2">
                            {{ number_format($quotation->tax_amount,2) }}
                        </th>
                    </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($quotation->discount_amount,2) }}
                    </th>
                </tr>
                @if($setting->discounts_in=='exc')
                    <tr>
                        <th class="text-right" colspan="2">Tax</th>
                        <th class="text-right" colspan="2">
                            {{ number_format($quotation->tax_amount,2) }}
                        </th>
                    </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $quotation->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($quotation->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($quotation->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($quotation->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
	</table>
	<htmlpagefooter name="page-footer">
		<span class="text-right">PAGE {PAGENO}</span>
	</htmlpagefooter>
</body>
</html>