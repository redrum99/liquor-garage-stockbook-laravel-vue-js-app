<!DOCTYPE html>
<html>
<head>
    <title>{{ $quotation->quotation_no }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/quotation/pdf3.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100">
            <tr>
                <td colspan="3"><img src="{{asset('public/images/organization')}}/{{ $org->header }}"></td>
            </tr>
            <tr>
                <td align="left"><b>PAN NO : {{ $org->pan_no }}</b></td>
                <td align="center"><b>QUOTATION</b></td>
                <td align="right"><b>GSTIN : </b> {{ $org->gstin_no }}</td>
            </tr>
        </table>
    </htmlpageheader>
    <table class="table-100" border="1">
        <tr>
            <td rowspan="3">
                <span><b>To,</b></span>
                <br/><span><b>{{ $quotation->Customer->contact_name }}</b>,</span>
                <br/><span><b>Phone :</b>{{ $quotation->Customer->mobile_no }},</span>
                <br/><span><b>Email :</b>{{ $quotation->Customer->email }}</span>
                <br/><span><b>Billing Address :</b>{{ $quotation->Customer->billing_address }},</span>
                <br/><span><b>Shipping Address :</b>{{ $quotation->Customer->shipping_address }}</span>
                <br/><span><b>GSTIN :</b>{{ $quotation->Customer->gstin_no }}</span>
            </td>
            <td><span>Quotation No : {{ $quotation->quotation_no }}</span><br>
                <span>Quotation Date: {{ date('d-m-Y',strtotime($quotation->quotation_date)) }}</span></td>
            <td>
                {{-- <img src="data:image/png;base64, {{DNS1D::getBarcodePNG($quotation->quotation_no,'C128',1,33)}}"> --}}
            </td>
        </tr>
        <tr>
            <td>Reference No: {{ $quotation->reference_no }}</td>
            <td>Expiry Date: {{ date('d-m-Y',strtotime($quotation->expiry_date)) }}</td>
        </tr>
        <tr>
            <td>Source: {{ $quotation->SourcePlace->place_name }}</td>
            <td>Destination: {{ $quotation->DestinationPlace->place_name }}</td>

        </tr>
    </table>
    <table class="table-100 th-font" border="1">
        <tr>
            <td rowspan="2" align="center"><b>#</b></td>
            <td rowspan="2" align="center"><b>PRODUCT <br> CODE</b></td>
            <td rowspan="2" align="center"><b>PRODUCT DESCRIPTION</b></td>
            <td rowspan="2" align="center"><b>HSN/SAC</b></td>
            <td rowspan="2" align="center"><b>QTY</b></td>
            <td rowspan="2" align="center"><b>UNIT</b></td>
            <td rowspan="2" align="center"><b>BASIC</b></td>
            <td rowspan="2" align="center"><b>TOTAL</b></td>
            <td rowspan="2" align="center"><b>DISC</b></td>
            <td rowspan="2" align="center"><b>TAXABLE <br> VALUE</b></td>
            <td colspan="2" align="center"><b> CGST</b></td>
            <td colspan="2" align="center"><b> SGST</b></td>
            <td colspan="2" align="center"><b> IGST</b></td>
        </tr>
        <tr>
            <td align="center"><b>RATE</b></td>
            <td align="center"> <b>AMOUNT</b></td>
            <td align="center"><b>RATE</b></td>
            <td align="center"> <b>AMOUNT</b></td>
            <td align="center"><b>RATE</b></td>
            <td align="center"> <b>AMOUNT</b></td>
        </tr>
        @php 
            $i = 1;
            $j=0; 
            $final_amount = 0;
            $final_cgst = 0;
            $final_sgst = 0;
            $final_igst = 0;
            $final_discount =0;
            $final_price_total = 0;
        @endphp
        @foreach($quotation->QuotationProducts as $po)
            @php 
                $amount = (($po->amount)-($po->discount_amount));
                $sgst = round(($amount * $po->Tax->sgst_rate)/100,2);
                $cgst = round(($amount * $po->Tax->cgst_rate)/100,2);
                $igst = round(($amount * $po->Tax->igst_rate)/100,2);
                $final_amount += $amount;
                $final_cgst += $cgst;
                $final_sgst += $sgst;
                $final_igst += $igst;
                $price_total= round($po->sales_rate_exc * $po->quantity,2);
            @endphp
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $po->product_code }}</td>
                <td>{{ $po->product_name }}</td>
                <td>{{ $po->hsn_code }}</td>
                <td>{{ number_format($po->quantity,2) }}</td>
                <td>{{ $po->product_unit }}</td>
                <td align="right">{{ number_format($po->sales_rate_exc,2) }}</td>
                <td align="right">{{ number_format($price_total,2) }}</td>
                <td align="right">{{ number_format($po->discount_amount,2) }}</td>
                <td align="right">{{ number_format($po->amount,2) }}</td>
                 @if($quotation->source_id==$quotation->destination_id)
                    <td align="right">{{ $po->Tax->cgst_name}}</td>
                    <td align="right">{{ number_format($cgst,2) }}</td>
                    <td align="right">{{ $po->Tax->sgst_name}}</td>
                    <td align="right">{{ number_format($sgst,2)}}</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                 @else
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="right">{{ $po->Tax->igst_name}}</td>
                    <td align="right">{{ number_format($igst,2) }}</td>
                 @endif
                 @php
                    $final_price_total +=$price_total;
                    $final_discount +=$po->discount_amount;
                @endphp
            </tr>
        @endforeach
        @for($j=$i;$j<=15;$j++)
            <tr>
                <td><br></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endfor
        @php
            $total_amount = $quotation->SubTotal($quotation->quotation_id);
            $sub_total = $total_amount - $final_discount;
            $total_tax = $quotation->TotalTax($quotation->quotation_id);
            $total = $sub_total + $total_tax;
            $grand_total = round($total);
            $round_off = $grand_total - $total;
        @endphp
         <tr>
            <td colspan="7" align="right">TOTAL</td>
            <td align="right">{{ number_format($final_price_total,2) }}</td>
            <td align="right">{{ number_format($final_discount,2) }}</td>
            <td align="right">{{ number_format($total_amount,2) }}</td>
            @if($quotation->source_id==$quotation->destination_id)
            <td></td>
            <td align="right">{{ number_format($final_cgst,2) }}</td>
            <td></td>
            <td align="right">{{ number_format($final_sgst,2) }}</td>
            <td align="center">-</td>
            <td align="center">-</td>
            @else
            <td align="center">-</td>
            <td align="center">-</td>
            <td align="center">-</td>
            <td align="center">-</td>
            <td></td>
            <td>{{ number_format($final_igst,2) }}</td>
            @endif
        </tr>
        <tr>
            <td colspan="14" align="right">TOTAL</td>
            <td colspan="2" align="right"><b>{{ number_format($final_price_total,2) }}</b></td>
        </tr>
        @if(!$final_discount == 0)
            <tr>
                <td colspan="14" align="right">DISCOUNT</td>
                <td colspan="2" align="right"><b>{{ number_format($final_discount,2) }}</b></td>
            </tr>
        @endif
        <tr>
            <td colspan="14" align="right">TOTAL TAX</td>
            <td colspan="2" align="right"><b>{{ number_format($total_tax,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="14" align="right">ROUND OFF</td>
            <td colspan="2" align="right"><b>{{ number_format($round_off,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="14" align="right">GRAND TOTAL</td>
            <td colspan="2" align="right"><b>{{ number_format($grand_total,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="16">
                <span>TOTAL PURCHASE ORDER VALUE (IN WORDS)</span><br>
                <span><b>RUPEES {{ convert_number_to_words(round($grand_total)) }} ONLY</b></span>
            </td>
        </tr>
    </table>
    <table style="margin-left: -6px !important;" class="table-100">
        <tr>
            <td class="width-50">
                <table class="table-100 th-font" border="1">
                    <tr>
                        <td rowspan="2"><b>Total</b></td>
                        @if($quotation->source_id==$quotation->destination_id)
                        <td colspan="2"><b>CGST</b></td>
                        <td colspan="2"><b>SGST</b></td>
                        @else
                        <td colspan="2"><b>IGST</b></td>
                        @endif
                        <td rowspan="2"><b>GST</b></td>
                        <td rowspan="2"><b>Grand Total</b></td>
                    </tr>
                    <tr>
                        @if($quotation->source_id==$quotation->destination_id)
                        <td><b>Rate</b></td>
                        <td><b>Amount</b></td>
                        <td><b>Rate</b></td>
                        <td><b>Amount</b></td>
                        @else
                        <td><b>Rate</b></td>
                        <td><b>Amount</b></td>
                        @endif
                    </tr>
                    @php
                        $final_taxable_value = 0;
                        $final_gst = 0;
                        $final_grand_total = 0;
                    @endphp
                    @foreach($quotation->Taxes($quotation->quotation_id) as $tax)
                        @php
                            $taxable_value = $quotation->TaxableValue($quotation->quotation_id,$tax->tax_id);
                            $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
                            $sgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
                            $igst = round(($taxable_value * $tax->Tax->igst_rate)/100,2);
                            if($quotation->source_id==$quotation->destination_id)
                            {
                                $gst = $cgst + $sgst;
                            }
                            else
                            {
                                $gst = $igst;
                            }
                            $grand_total = $gst + $taxable_value;
                            $final_taxable_value += $taxable_value;
                            $final_cgst +=$cgst;
                            $final_sgst +=$sgst;
                            $final_igst +=$igst;
                            $final_gst +=$gst;
                            $final_grand_total +=$grand_total;
                        @endphp
                        <tr>
                            <td align="right">{{ number_format($taxable_value,2) }}</td>
                            @if($quotation->source_id==$quotation->destination_id)
                                <td align="right">{{ $tax->Tax->cgst_name }}</td>
                                <td align="right">{{ number_format($cgst,2) }}</td>
                                <td align="right">{{ $tax->Tax->sgst_name }}</td>
                                <td align="right">{{ number_format($sgst,2) }}</td>
                            @else
                                <td align="right">{{ $tax->Tax->igst_name }}</td>
                                <td align="right">{{ number_format($igst,2) }}</td>
                            @endif
                            <td align="right">{{ number_format($gst,2) }}</td>
                            <td align="right">{{ number_format(round($grand_total),2) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td align="right"><b>{{ number_format($final_taxable_value,2) }}</b></td>
                        @if($quotation->source_id==$quotation->destination_id)
                            <td></td>
                            <td align="right"><b>{{ number_format($final_cgst,2) }}</b></td>
                            <td></td>
                            <td align="right"><b>{{ number_format($final_sgst,2) }}</b></td>
                        @else
                            <td></td>
                            <td align="right"><b>{{ number_format($final_igst,2) }}</b></td>
                        @endif
                        <td align="right"><b>{{ number_format($final_gst,2) }}</b></td>
                        <td align="right"><b>{{ number_format(round($final_grand_total),2) }}</b></td>
                    </tr>       
                </table>
            </td>
            <td class="table-50">
                <table class="table-100">
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <b>For {{ $org->org_name }}</b>
                        </td>
                    </tr>   
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <b>Authorized Signatory</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @if(!empty($quotation->terms))
        <table class="table-100 th-font">
            <tr>
                <td><b>Terms &amp; Conditions</b></td>
            </tr>
            <tr>
                <td>
                    <pre>{{ $quotation->terms }}</pre>
                </td>
            </tr>
        </table>
    @endif
    <htmlpagefooter name="page-footer">
        <table class="table-100 th-font">
            <tr>
                <td><img src="{{asset('public/images/organization')}}/{{ $org->footer }}"></td>
            </tr>
        </table>
    </htmlpagefooter>
</body>
</html>
@php
    function convert_number_to_words($no)
    {   
        $words = array('0'=> 'ZERO' ,'1'=> 'ONE' ,'2'=> 'TWO' ,'3' => 'THREE','4' => 'FOUR','5' => 'FIVE','6' => 'SIX','7' => 'SEVEN','8' => 'EIGHT','9' => 'NINE','10' => 'TEN','11' => 'ELEVEN','12' => 'TWELVE','13' => 'THIRTEEN','14' => 'FOURTEEN','15' => 'FIFTEEN','16' => 'SIXTEEN','17' => 'SEVENTEEN','18' => 'EIGHTEEN','19' => 'NINETEEN','20' => 'TWENTY','30' => 'THIRTY','40' => 'FOURTY','50' => 'FIFTY','60' => 'SIXTY','70' => 'SEVENTY','80' => 'EIGHTY','90' => 'NINTY','100' => 'HUNDRED','1000' => 'THOUSAND','100000' => 'LAKH','10000000' => 'CRORE');
        if($no == 0)
        {
            return '';
        }
        else 
        {
            $novalue='';
            $highno=$no;
            $remainno=0;
            $value=100;
            $value1=1000;       
            while($no>=100)    
            {
                if(($value <= $no) &&($no  < $value1))    
                {
                    $novalue=$words["$value"];
                    $highno = (int)($no/$value);
                    $remainno = $no % $value;
                    break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
            if(array_key_exists("$highno",$words))
            {
                return $words["$highno"]." ".$novalue." ".convert_number_to_words($remainno);
            }
            else 
            {
                $unit=$highno%10;
                $ten =(int)($highno/10)*10;            
                return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_number_to_words($remainno);
            }
        }
    }
@endphp