@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=category-report.xls");
@endphp
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/pdf/product/category_report.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100 table-border">
                <tr>
                    <td align="center" colspan="8" ><h3>CATEGORY REPORT</h3></td>
                </tr>
                 <tr border="1">
                    <td align="center" colspan="8"><b>From :</b> {{ date('d-m-Y',strtotime($request->from_date)) }} -  <b>To :</b> {{ date('d-m-Y',strtotime($request->to_date)) }}</td>
                </tr>
        </table>
    </htmlpageheader>
    <table class="table-100" border="1">
        @foreach($category_invoice_products as $key => $category)
             <tr>
                <th colspan="8">{{ ++$key }}</th>
                {{-- rowspan="{{ count($category->filtered_invoice_products)+4 }}" --}}
            </tr>
            <tr>
                <td align="center" colspan="8"><b>Category Name</b> - {{ $category->category_name  }}</td>
            </tr>
            <tr >
                <th>#</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>SR( Exc )</th>
                <th>SR( Inc )</th>
                <th>Qty</th>
                <th>Sub Total</th>
            </tr> 
            @php
            $total = 0;
            @endphp
            @foreach($category->filtered_invoice_products as $key => $product)
             <tr>
                <td align="center">{{ ++$key }}</td>
                <td>
                    {{ $product->product_code}}
                </td>
                <td>
                    {{ $product->product_name }}
                </td>
                <td align="right">
                    {{ number_format($product->sales_rate_exc,2) }}
                </td>
                <td align="right">
                    {{ number_format($product->sales_rate_inc,2) }}
                </td>
                <td align="center">
                    {{ $product->CategoryInvoice->quantity }}
                </td>
                <td align="right">
                    {{ number_format($product->sales_rate_inc*$product->CategoryInvoice->quantity,2) }}
                    {{-- {{ number_format($product->CategoryInvoice->total,2) }} --}}
                </td>
            </tr>
            @php
            $total += $product->sales_rate_inc*$product->CategoryInvoice->quantity;
            @endphp
            @endforeach
            <tr>
                <th colspan="6" align="right">Total</th>
                <th align="right">{{ number_format($total,2) }}</th>
            </tr> 
         @endforeach
    </table>
</body>
</html>
