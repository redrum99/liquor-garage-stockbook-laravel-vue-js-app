<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <style type="text/css">
    @page  { 
          sheet-size: 2in 1in;
          margin-top: 8px;
          margin-left: 2px;
          margin-right: 2px;
          margin-bottom: 2px;
        }
    .img-container-inline {
        display: flex;
        vertical-align: middle;
        text-align: center
    } 
  </style>
</head>
<body style="font-weight:700;">
    <div style="text-align: center;">
        {{ substr($org->org_name,0,25) }}
    </div>
    <div class="img-container-inline">
      <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($product->product_code, 'C128',1,25)}}" >
    </div>
    <div style="text-align: center">
        {{ substr($product->product_name,0,25) }}
    </div>
    @if($product->Price)
      <div style="text-align: center">
          Rs : {{ number_format($product->Price->sales_rate_inc,2) }}
      </div>
  @endif    
</body>
</html>