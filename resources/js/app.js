require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import moment from 'moment'
Vue.use(require('vue-moment'));
window.moment = require('moment');

// window.myModule = require('./modal');

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import vDialogs from 'v-dialogs';
Vue.use(vDialogs);

// import VueMoment from 'vue-moment';
// Vue.use(VueMoment);

// import { Pagination } from 'bootstrap-vue/es/components';
// Vue.use(Pagination);

// import { Modal } from 'bootstrap-vue/es/components';
// Vue.use(Modal);

Vue.filter('tofix',function(value){
    let fixed = parseFloat((parseFloat(value)*1)).toFixed(2);
    return fixed;
});

import { PaginationPlugin } from 'bootstrap-vue';
Vue.use(PaginationPlugin);

import { ModalPlugin } from 'bootstrap-vue';
Vue.use(ModalPlugin);

let Myheader = require('./components/Header.vue').default;
let Mymenu = require('./components/Menu.vue').default;
let Myaside = require('./components/Aside.vue').default;
let Myfooter = require('./components/Footer.vue').default;

let Home = require('./components/Home.vue').default;
let Configuration = require('./components/Configuration.vue').default;
let Report = require('./components/Report.vue').default;

//Configuration
let Master = require('./components/configuration/Master.vue').default;
let Place = require('./components/configuration/Place.vue').default;
let Organization = require('./components/configuration/Organization.vue').default;
let Tax = require('./components/configuration/Tax.vue').default;
let Account = require('./components/configuration/Account.vue').default;
let Term = require('./components/configuration/Term.vue').default;
let PaymentTerm = require('./components/configuration/PaymentTerm.vue').default;
let Preference = require('./components/configuration/Preference.vue').default;
let Subscription = require('./components/configuration/Subscription.vue').default;
let Migration = require('./components/configuration/Migration.vue').default;
let Settings = require('./components/configuration/Settings.vue').default;
let Points = require('./components/configuration/Point.vue').default;
let RedeemPoints = require('./components/configuration/RedeemPoint.vue').default;
let PointsReport = require('./components/configuration/PointsReport.vue').default;
//Users
let CreateUser = require('./components/user/Create.vue').default;
let DisplayUser = require('./components/user/Display.vue').default;
let Profile = require('./components/user/Profile.vue').default;
let ChangePassword = require('./components/user/ChangePassword.vue').default;


//Contacts
let CreateContact = require('./components/contact/Create.vue').default;
let DisplayContact = require('./components/contact/Display.vue').default;
let ViewContact = require('./components/contact/View.vue').default;
let CustomerReport = require('./components/contact/CustomerReport.vue').default;
let VendorReport = require('./components/contact/VendorReport.vue').default;
let CustomerOutstandingReport = require('./components/contact/CustomerOutstandingReport.vue').default;
let VendorOutstandingReport = require('./components/contact/VendorOutstandingReport.vue').default;

//Product
let Category = require('./components/product/Category.vue').default;
let Warehouse = require('./components/product/Warehouse.vue').default;
let CreateProduct = require('./components/product/Create.vue').default;
let DisplayProduct = require('./components/product/Display.vue').default;
let StockReport = require('./components/product/StockReport2.vue').default;
let ViewProduct = require('./components/product/View.vue').default;
let CategoryReport = require('./components/product/CategoryReport.vue').default;
let BarcodeProduct = require('./components/product/Barcode.vue').default;

//Purchase Order
let CreatePurchaseOrder = require('./components/purchase_order/Create.vue').default;
let DisplayPurchaseOrder = require('./components/purchase_order/Display.vue').default;
let ViewPurchaseOrder = require('./components/purchase_order/View.vue').default;
let PreviewPurchaseOrder = require('./components/purchase_order/Preview.vue').default;
let ReportPurchaseOrder = require('./components/purchase_order/Report.vue').default;

//Bill
let CreateBill = require('./components/bill/Create.vue').default;
let DisplayBill = require('./components/bill/Display.vue').default;
let ViewBill = require('./components/bill/View.vue').default;
let PreviewBill = require('./components/bill/Preview.vue').default;
let ReportBill = require('./components/bill/Report.vue').default;
let ReportBillTax = require('./components/bill/ReportBillTax.vue').default;

//Bill Return
let CreateBillReturn = require('./components/bill_return/Create.vue').default;
let DisplayBillReturn = require('./components/bill_return/Display.vue').default;
let ViewBillReturn = require('./components/bill_return/View.vue').default;
let PreviewBillReturn = require('./components/bill_return/Preview.vue').default;
let ReportBillReturn = require('./components/bill_return/Report.vue').default;

//Payment
let CreatePayment = require('./components/payment/Create.vue').default;
let DisplayPayment = require('./components/payment/Display.vue').default;
let ViewPayment = require('./components/payment/View.vue').default;
let PreviewPayment = require('./components/payment/Preview.vue').default;

//Quotation
let CreateQuotation = require('./components/quotation/Create.vue').default;
let DisplayQuotation = require('./components/quotation/Display.vue').default;
let ViewQuotation = require('./components/quotation/View.vue').default;
let ReportQuotation = require('./components/quotation/Report.vue').default;
let PreviewQuotation = require('./components/quotation/Preview.vue').default;

//Proforma
let CreateProforma = require('./components/proforma/Create.vue').default;
let DisplayProforma = require('./components/proforma/Display.vue').default;
let ViewProforma = require('./components/proforma/View.vue').default;
let ReportProforma = require('./components/proforma/Report.vue').default;
let PreviewProforma = require('./components/proforma/Preview.vue').default;

//Delivery
let CreateDelivery = require('./components/delivery/Create.vue').default;
let DisplayDelivery = require('./components/delivery/Display.vue').default;
let ViewDelivery = require('./components/delivery/View.vue').default;
let ReportDelivery = require('./components/delivery/Report.vue').default;
let PreviewDelivery = require('./components/delivery/Preview.vue').default;

//Delivery Return
let CreateDeliveryReturn = require('./components/delivery_return/Create.vue').default;
let DisplayDeliveryReturn = require('./components/delivery_return/Display.vue').default;
let ViewDeliveryReturn= require('./components/delivery_return/View.vue').default;
let ReportDeliveryReturn = require('./components/delivery_return/Report.vue').default;
let PreviewDeliveryReturn = require('./components/delivery_return/Preview.vue').default;

//Invoice
let CreateInvoice = require('./components/invoice/Create.vue').default;
let DisplayInvoice = require('./components/invoice/Display.vue').default;
let ViewInvoice = require('./components/invoice/View.vue').default;
let PreviewInvoice = require('./components/invoice/Preview.vue').default;
let ReportInvoice = require('./components/invoice/Report.vue').default;

//InvoiceReturn
let CreateInvoiceReturn = require('./components/invoice_return/Create.vue').default;
let DisplayInvoiceReturn = require('./components/invoice_return/Display.vue').default;
let ViewInvoiceReturn = require('./components/invoice_return/View.vue').default;
let PreviewInvoiceReturn = require('./components/invoice_return/Preview.vue').default;
let ReportInvoiceReturn = require('./components/invoice_return/Report.vue').default;
let ReportInvoiceTax = require('./components/invoice/ReportInvoiceTax.vue').default;

//Receipt
let CreateReceipt = require('./components/receipt/Create.vue').default;
let DisplayReceipt = require('./components/receipt/Display.vue').default;
let ViewReceipt = require('./components/receipt/View.vue').default;
let PreviewReceipt = require('./components/receipt/Preview.vue').default;

//Expense
let CreateExpense = require('./components/expense/Create.vue').default;
let DisplayExpense = require('./components/expense/Display.vue').default;
let ViewExpense = require('./components/expense/View.vue').default;
let PreviewExpense = require('./components/expense/Preview.vue').default;
let ReportExpense = require('./components/expense/Report.vue').default;

//Income
let CreateIncome = require('./components/income/Create.vue').default;
let DisplayIncome = require('./components/income/Display.vue').default;
let ViewIncome = require('./components/income/View.vue').default;
let PreviewIncome = require('./components/income/Preview.vue').default;
let ReportIncome = require('./components/income/Report.vue').default;


let Stock = require('./components/Stock.vue').default;
let BalanceReport = require('./components/balance_report/Report.vue').default;
let Transaction = require('./components/transaction/Create.vue').default;



const routes = [
    { path: '/', component: Home },
    { path: '/home', component: Home },
    { path: '/configuration', name:'configuration', component: Configuration },
    { path: '/master', name:'master', component: Master },
    { path: '/place', name:'place', component: Place },
    { path: '/organization', name:'organization', component: Organization },
    { path: '/tax', name:'tax', component: Tax },
    { path: '/account', name:'account', component: Account },
    { path: '/term', name:'term', component: Term },
    { path: '/payment_term', name:'payment_term', component: PaymentTerm },
    { path: '/preference', name:'preference', component: Preference },
    { path: '/subscription', name:'subscription', component: Subscription },
    { path: '/migration', name:'migration', component: Migration },
    { path: '/report', name:'report', component: Report },
    { path: '/settings', name:'settings', component: Settings },
    { path: '/points', name:'points', component: Points },
    { path: '/redeem_points', name:'redeem_points', component: RedeemPoints },
    { path: '/points_report', name:'points_report', component: PointsReport },

    
    { path: '/user/create', name:'user-create', component: CreateUser },
    { path: '/user/display', name:'user-display', component: DisplayUser },
    { path: '/user/edit/:user_id', name:'user-edit', component:CreateUser },
    { path: '/profile',component:Profile },
    { path: '/change_password',component:ChangePassword },


    { path: '/contact/create', name:'contact-create', component: CreateContact },
    { path: '/contact/display', name:'contact-display', component: DisplayContact },
    { path: '/contact/view/:contact_id', name:'contact-view', component: ViewContact }, 
    { path: '/contact/edit/:contact_id', name:'contact-edit', component:CreateContact },
    { path: '/contact/customer_report', name:'customer-report', component: CustomerReport },
    { path: '/contact/vendor_report', name:'vendor-report', component: VendorReport },
    { path: '/contact/customer_outstanding_report', name:'customer-outstanding-report', component: CustomerOutstandingReport },
    { path: '/contact/vendor_outstanding_report', name:'vendor-outstanding-report', component: VendorOutstandingReport },

    { path: '/category', name:'category', component: Category },
    { path: '/warehouse', name:'warehouse', component: Warehouse },
    { path: '/product/create', name:'product-create', component: CreateProduct },
    { path: '/product/display', name:'product-display', component: DisplayProduct },
    // { path: '/product/view/:product_id', name:'product-view', component:CreateProduct },
    { path: '/product/edit/:product_id', name:'product-edit', component:CreateProduct },
    { path: '/product/stock_report', name:'stock-report', component: StockReport },
    { path: '/product/view/:product_id', name:'product-view', component: ViewProduct }, 
    { path: '/product/category_report', name:'category-report', component: CategoryReport },
    { path: '/product/barcode/:product_id', name:'product-barcode', component:BarcodeProduct },


    { path: '/purchase_order/create', name:'purchase-order-create', component: CreatePurchaseOrder },
    { path: '/purchase_order/display', name:'purchase-order-display', component: DisplayPurchaseOrder },
    { path: '/purchase_order/view/:purchase_order_id', name:'purchase-order-view', component:ViewPurchaseOrder },
    { path: '/purchase_order/edit/:purchase_order_id', name:'purchase-order-edit', component:CreatePurchaseOrder },
    { path: '/purchase_order/preview/:purchase_order_id', name:'purchase-order-preview', component:PreviewPurchaseOrder },
    { path: '/purchase_order/report', name:'purchase-order-report', component: ReportPurchaseOrder },

    { path: '/bill/create', name:'bill-create', component: CreateBill },
    { path: '/bill/display', name:'bill-display', component: DisplayBill },
    { path: '/bill/view/:bill_id', name:'bill-view', component: ViewBill },
    { path: '/bill/edit/:bill_id', name:'bill-edit', component: CreateBill },
    { path: '/bill/preview/:bill_id', name:'bill-preview', component:PreviewBill },
    { path: '/bill/report', name:'bill-report', component: ReportBill },
    { path: '/bill/tax_report', name:'bill-tax-report', component: ReportBillTax },
    { path: '/purchase_order/convert/bill/', name:'purchase-order-convert-bill', component: CreateBill },
    
    { path: '/payment/create', name:'payment-create', component: CreatePayment },
    { path: '/payment/display', name:'payment-display', component: DisplayPayment },
    { path: '/payment/view/:payment_id', name:'payment-view', component: ViewPayment },
    { path: '/payment/preview/:payment_id', name:'payment-preview', component:PreviewPayment },

    { path: '/quotation/create', name:'quotation-create', component: CreateQuotation },
    { path: '/quotation/display', name:'quotation-display', component: DisplayQuotation },
    { path: '/quotation/view/:quotation_id', name:'quotation-view', component: ViewQuotation },
    { path: '/quotation/edit/:quotation_id', name:'quotation-edit', component: CreateQuotation },
    { path: '/quotation/report', name:'quotation-report', component: ReportQuotation },
    { path: '/quotation/convert/invoice/', name:'quotation-convert-invoice', component: CreateInvoice },
    { path: '/quotation/preview/:quotation_id', name:'quotation-preview', component:PreviewQuotation },
    
    { path: '/proforma/create', name:'proforma-create', component: CreateProforma },
    { path: '/proforma/display', name:'proforma-display', component: DisplayProforma },
    { path: '/proforma/view/:proforma_id', name:'proforma-view', component: ViewProforma },
    { path: '/proforma/edit/:proforma_id', name:'proforma-edit', component: CreateProforma },
    { path: '/proforma/report', name:'proforma-report', component: ReportProforma },
    { path: '/proforma/convert/invoice/', name:'proforma-convert-invoice', component: CreateInvoice },
    { path: '/proforma/convert/delivery/', name:'proforma-convert-delivery', component: CreateDelivery },
    { path: '/proforma/preview/:proforma_id', name:'proforma-preview', component:PreviewProforma },

    { path: '/delivery/create', name:'delivery-create', component: CreateDelivery },
    { path: '/delivery/display', name:'delivery-display', component: DisplayDelivery },
    { path: '/delivery/view/:delivery_id', name:'delivery-view', component: ViewDelivery },
    { path: '/delivery/edit/:delivery_id', name:'delivery-edit', component: CreateDelivery },
    { path: '/delivery/report', name:'delivery-report', component: ReportDelivery },
    { path: '/delivery/convert/invoice/', name:'delivery-convert-invoice', component: CreateInvoice },
    { path: '/delivery/preview/:delivery_id', name:'delivery-preview', component:PreviewDelivery },

    { path: '/invoice/create', name:'invoice-create', component: CreateInvoice },
    { path: '/invoice/display', name:'invoice-display', component: DisplayInvoice },
    { path: '/invoice/view/:invoice_id', name:'invoice-view', component: ViewInvoice },
    { path: '/invoice/edit/:invoice_id', name:'invoice-edit', component: CreateInvoice },
    { path: '/invoice/preview/:invoice_id', name:'invoice-preview', component:PreviewInvoice },
    { path: '/invoice/convert/delivery/', name:'invoice-convert-delivery', component: CreateDelivery },
    { path: '/invoice/report', name:'invoice-report', component: ReportInvoice },

    { path: '/receipt/create', name:'receipt-create', component: CreateReceipt },
    { path: '/receipt/display', name:'receipt-display', component: DisplayReceipt },
    { path: '/receipt/view/:receipt_id', name:'receipt-view', component: ViewReceipt },
    { path: '/receipt/preview/:receipt_id', name:'receipt-preview', component:PreviewReceipt },

    { path: '/expense/create', name:'expense-create', component: CreateExpense },
    { path: '/expense/display', name:'expense-display', component: DisplayExpense },
    { path: '/expense/view/:expense_id', name:'expense-view', component: ViewExpense },
    { path: '/expense/edit/:expense_id', name:'expense-edit', component: CreateExpense },
    { path: '/expense/preview/:expense_id', name:'expense-preview', component:PreviewExpense },
    { path: '/expense/report', name:'expense-report', component: ReportExpense },

    { path: '/income/create', name:'income-create', component: CreateIncome },
    { path: '/income/display', name:'income-display', component: DisplayIncome },
    { path: '/income/view/:income_id', name:'income-view', component: ViewIncome },
    { path: '/income/edit/:income_id', name:'income-edit', component: CreateIncome },
    { path: '/income/preview/:income_id', name:'income-preview', component:PreviewIncome },
    { path: '/income/report', name:'income-report', component: ReportIncome },

    { path: '/invoice_return/create', name:'invoice-return-create', component: CreateInvoiceReturn },
    { path: '/invoice_return/display', name:'invoice-return-display', component: DisplayInvoiceReturn },
    { path: '/invoice_return/view/:invoice_return_id', name:'invoice-return-view', component: ViewInvoiceReturn },
    { path: '/invoice_return/edit/:invoice_return_id', name:'invoice-return-edit', component: CreateInvoiceReturn },
    { path: '/invoice_return/preview/:invoice_return_id', name:'invoice-return-preview', component:PreviewInvoiceReturn },
    { path: '/invoice/convert/invoice_return/', name:'invoice-convert-return', component: CreateInvoiceReturn },
    { path: '/invoice_return/report', name:'invoice-return-report', component: ReportInvoiceReturn },
    { path: '/invoice/tax_report', name:'invoice-tax-report', component: ReportInvoiceTax },

    { path: '/bill_return/create', name:'bill-return-create', component: CreateBillReturn },
    { path: '/bill_return/display', name:'bill-return-display', component: DisplayBillReturn },
    { path: '/bill_return/view/:bill_return_id', name:'bill-return-view', component: ViewBillReturn },
    { path: '/bill_return/edit/:bill_return_id', name:'bill-return-edit', component: CreateBillReturn },
    { path: '/bill/convert/bill_return/', name:'bill-convert-bill-return', component: CreateBillReturn },
    { path: '/bill_return/preview/:bill_return_id', name:'bill-return--preview', component:PreviewBillReturn },
    { path: '/bill_return/report', name:'bill-return-report', component: ReportBillReturn },
    
    { path: '/delivery_return/create', name:'delivery-return-create', component: CreateDeliveryReturn },
    { path: '/delivery_return/display', name:'delivery-return-display', component: DisplayDeliveryReturn },
    { path: '/delivery_return/view/:delivery_return_id', name:'delivery-return-view', component: ViewDeliveryReturn },
    { path: '/delivery_return/edit/:delivery_return_id', name:'delivery-return-edit', component: CreateDeliveryReturn },
    { path: '/delivery_return/report', name:'delivery-return-report', component: ReportDeliveryReturn },
    { path: '/delivery/convert/delivery_return/', name:'delivery-convert-return', component: CreateDeliveryReturn },
    { path: '/delivery_return/preview/:delivery_return_id', name:'delivery-return-preview', component:PreviewDeliveryReturn },

    
    { path: '/stock/get', name:'stock-get', component: Stock },
    { path: '/balance/report', name:'balance-report', component: BalanceReport },
    { path: '/transaction/create', name:'transaction-create', component: Transaction },

    

];


const router = new VueRouter({
    // mode: 'history',
    routes
})

const app = new Vue({
    el: '#app',
    router,
    components: {Myheader, Mymenu, Myaside, Myfooter},
    methods:{
        windowreload(){
            location.reload()
        }
    },
});
