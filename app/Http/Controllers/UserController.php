<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return User::
            where('name', 'like', '%'.$request->search.'%')
            ->orWhere('username', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
            ->orWhere('user_role', 'like', '%'.$request->search.'%')
            ->withTrashed()
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'username'=> 'required|unique:users,username,NULL,user_id,deleted_at,NULL',
            'email'=> 'required|unique:users,email,NULL,user_id,deleted_at,NULL',
            'password' => 'required|min:6',
            'user_role' => 'required|max:50',
            'mobile_no'=> 'required|numeric|digits:10|unique:users,mobile_no,NULL,user_id,deleted_at,NULL|regex:/[0-9]{10}/',
            'address' => 'required|max:2550',
        ]);

        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address' => $request->address,
        ]);
    }

    public function edit(User $user)
    {
        return $user;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'username'=>'required|unique:users,username,'.$request->user_id.',user_id,deleted_at,NULL',
            'email'=>'required|unique:users,email,'.$request->user_id.',user_id,deleted_at,NULL',
            'user_role' => 'required|max:50',
            'mobile_no'=>'required|numeric|digits:10|unique:users,mobile_no,'.$request->user_id.',user_id,deleted_at,NULL|regex:/[0-9]{10}/',
            'address' => 'required|max:2550',
        ]);

        User::where('user_id',$request->user_id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address' => $request->address,
        ]);
    }

    public function destroy(User $user)
    {
        User::where('user_id',$user->user_id)->delete();
    }

    
    public function get_users()
    {
        return User::where('user_id',Auth::user()->user_id)->first();
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:50',
            'name' => 'required',
            'email' => 'required',
            'mobile_no' => 'required|min:10|max:10',
            'address' => 'max:2550',
        ]);

        User::where('user_id',$request->user_id)->update([
            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
                'user_role' => $request['user_role'],
            'mobile_no' => $request['mobile_no'],
            'address' => $request['address'],
            'updated_by' => Auth::User()->username,         
        ]);

        $data = $request->avatar;
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->avatar);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = "avatar-".time().".".$image_type_aux[1];
            $path = public_path().'/img/user/' . $png_url;
            file_put_contents($path, $image_base64);
            User::where('user_id',$request->user_id)->update([
                'avatar' => $png_url,
            ]);
        }
        return User::where('user_id',$request->user_id)->first();

    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        User::where('user_id',$request->user_id)->update([
                'password' => bcrypt($request->password),
                'updated_by' => Auth::User()->username,         
            ]);
    }

    public function change_status(Request $request)
    {
        $user = User::where('user_id',$request->user_id)->withTrashed()->first();

        if($user->deleted_at!=null){
            User::withTrashed()->find($user->user_id)->restore();
        }else{
            User::whereUserId($user->user_id)->delete();
        }
    }

    public function get_user()
    {
        return User::get();
    }
}
