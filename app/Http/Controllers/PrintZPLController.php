<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Product;
//*********************************
// IMPORTANT NOTE 
// ==============
// If your website requires user authentication, then
// THIS FILE MUST be set to ALLOW ANONYMOUS access!!!
//
//*********************************
 
//Includes WebClientPrint classes
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');
use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\Utils;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\PrintFile;
use Neodynamic\SDK\Web\PrintFilePDF;
use Neodynamic\SDK\Web\ClientPrintJob;
 
use Session;
 
class PrintZPLController extends Controller
{
    public function index(){
 
        $wcpScript = WebClientPrint::createScript(action('WebClientPrintController@processRequest'), action('PrintZPLController@printCommands'), Session::getId());    
 
        return view('print.printZPL', ['wcpScript' => $wcpScript]);
    }
 
    public function printCommands1(Request $request){
         
       if ($request->exists(WebClientPrint::CLIENT_PRINT_JOB)) {
 
            $useDefaultPrinter = ($request->input('useDefaultPrinter') === 'checked');
            $printerName = urldecode($request->input('printerName'));

            $filetype = $request->input('filetype');
            $fileName = uniqid() . '.' . $filetype;

            $filePath = public_path().'/files/'.$request->name;
            // $fileName = 'helloWorld';
            // $filePath = public_path().'/files/2.pdf';
            //Create ZPL commands for sample label
            $cmds =  "^XA";
            $cmds .= "^FO20,30^GB750,1100,4^FS";
            $cmds .= "^FO20,30^GB750,200,4^FS";
            $cmds .= "^FO20,30^GB750,400,4^FS";
            $cmds .= "^FO20,30^GB750,700,4^FS";
            $cmds .= "^FO20,226^GB325,204,4^FS";
            $cmds .= "^FO30,40^ADN,36,20^FDShip to:^FS";
            $cmds .= "^FO30,260^ADN,18,10^FDPart number #^FS";
            $cmds .= "^FO360,260^ADN,18,10^FDDescription:^FS";
            $cmds .= "^FO30,750^ADN,36,20^FDFrom:^FS";
            $cmds .= "^FO150,125^ADN,36,20^FDAcme Printing^FS";
            $cmds .= "^FO60,330^ADN,36,20^FD14042^FS";
            $cmds .= "^FO400,330^ADN,36,20^FDScrew^FS";
            $cmds .= "^FO70,480^BY4^B3N,,200^FD12345678^FS";
            $cmds .= "^FO150,800^ADN,36,20^FDMacks Fabricating^FS";
            $cmds .= "^XZ";
 
            // Create a ClientPrintJob obj that will be processed at the client side by the WCPP
            $cpj = new ClientPrintJob();
            //set ZPL commands to print...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
             
            if ($useDefaultPrinter || $printerName === 'null') {
                $cpj->clientPrinter = new DefaultPrinter();
            } else {
                $cpj->clientPrinter = new InstalledPrinter($printerName);
            }
         
            //Send ClientPrintJob back to the client
            return response($cpj->sendToClient())
                        ->header('Content-Type', 'application/octet-stream');
                

            // if (!Utils::isNullOrEmptyString($filePath)) {
            //     //Create a ClientPrintJob obj that will be processed at the client side by the WCPP
            //     $cpj = new ClientPrintJob();
            //     $cpj->printFile = new PrintFile($filePath, $fileName, null);
            //     if ($useDefaultPrinter || $printerName === 'null') {
            //         $cpj->clientPrinter = new DefaultPrinter();
            //     } else {
            //         $cpj->clientPrinter = new InstalledPrinter($printerName);
            //     }

            //     //Send ClientPrintJob back to the client
            //     return response($cpj->sendToClient())
            //                 ->header('Content-Type', 'application/octet-stream');
                
            // } 
             
        }
    }  

    public function printCommands(Request $request){
        
        if ($request->exists(WebClientPrint::CLIENT_PRINT_JOB)) {

            $useDefaultPrinter = ($request->input('useDefaultPrinter') === 'checked');
            $printerName = urldecode($request->input('printerName'));
            $filetype = $request->input('filetype');

            $fileName = uniqid();
            $filePath = public_path().'/files/'.$request->name;

            //Create PrintFilePDF obj
            $myfile = new PrintFilePDF($filePath, $fileName, null);

            //Create a ClientPrintJob obj that will be processed at the client side by the WCPP
            $cpj = new ClientPrintJob();
            $cpj->printFile = $myfile;
            
            //Create an InstalledPrinter obj
            if ($useDefaultPrinter || $printerName === 'null') {
                $myPrinter = new DefaultPrinter();
            } else {
                $myPrinter = new InstalledPrinter($printerName);
            }

            
             $cpj->clientPrinter = $myPrinter;
            
           //Send ClientPrintJob back to the client
            return response($cpj->sendToClient())
                        ->header('Content-Type', 'application/octet-stream');
        }
    }     
}
    