<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Place;
use App\Organization;
use Image;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_organization()
    {
        return Organization::first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'org_code' => 'required|max:50',
            'org_name' => 'required|max:50',
            'email' => 'sometimes|nullable|email|max:50',
            'mobile_no'=>'nullable|numeric|digits:10|regex:/[0-9]{10}/',
            'phone_no' => 'nullable|regex:/[0-9]{10}/',
            'address' => 'max:255',
            'pan_no' => 'sometimes|nullable|max:10|min:10',
            'gstin_no' => 'sometimes|nullable|max:15|min:15',
            'place_id' => 'sometimes|nullable|max:10',
        ]);

        Organization::where('org_id',$request->org_id)->update([
            'org_code' => $request->org_code,
            'org_name' => $request->org_name,
            'email' => $request->email,
            'mobile_no' => $request->mobile_no,
            'phone_no' => $request->phone_no,
            'address' => $request->address,
            'pan_no' => $request->pan_no,
            'gstin_no' => $request->gstin_no,
            'place_id' => $request->place_id,
            // 'logo' => $request['logo'],
            // 'header' => $request['header'],
            // 'footer' => $request['footer'],
            'updated_by' => Auth::User()->username,         
        ]);


        $data = $request->logo;
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->logo);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = "logo-".time().".".$image_type_aux[1];
            $path = public_path().'/img/organization/' . $png_url;
            file_put_contents($path, $image_base64);
            Organization::where('org_id',$request->org_id)->update([
                'logo' => $png_url,
            ]);
        }

        $data = $request->header;
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->header);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = "header-".time().".".$image_type_aux[1];
            $path = public_path().'/img/organization/' . $png_url;
            file_put_contents($path, $image_base64);
            Organization::where('org_id',$request->org_id)->update([
                'header' => $png_url,
            ]);
        }

        $data = $request->footer;
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->footer);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = "footer-".time().".".$image_type_aux[1];
            $path = public_path().'/img/organization/' . $png_url;
            file_put_contents($path, $image_base64);
            Organization::where('org_id',$request->org_id)->update([
                'footer' => $png_url,
            ]);
        }

        return Organization::where('org_id',$request->org_id)->first();
        
    }
}
