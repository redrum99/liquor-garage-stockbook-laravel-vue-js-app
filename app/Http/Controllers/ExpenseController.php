<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use App\Expense;
use App\Account;
use App\Contact;
use App\Term;
use App\Preference;
use App\Organization;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_voucher_no(Request $request)
    {
        $preference = Preference::first();
        $id = Expense::where('fiscal_year',$preference->fiscal_year)->count('expense_id');
       
        $id = $id + 1;
        $n = $preference->voucher_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $voucher_no = $preference->voucher_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $voucher_no;
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'voucher_no' => 'required|max:50',
            'voucher_date' => 'required|date',
            'account_id' => 'required|numeric',
            'contact_id' => 'required|numeric',
            'payment_mode' => 'required',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'amount' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);	

        $preference = Preference::first();

        $expense = Expense::create([
            'fiscal_year' => $preference->fiscal_year,
            'voucher_no' => $request->voucher_no,
            'voucher_date'=> date("Y-m-d", strtotime($request->voucher_date)),
            'account_id'=> $request->account_id,
            'contact_id'=> $request->contact_id,
            'payment_mode'=> $request->payment_mode,
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'amount'=> $request->amount,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'created_by' => Auth::User()->username,
        ]);
    }

    public function display(Request $request)
    {
        return Expense::
            whereHas('Contact', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhereHas('Account', function($query) use($request){
                $query->where('account_name','like', "%$request->search%");
            })
            ->orWhere('voucher_no', 'like', '%'.$request->search.'%')
            ->orWhere('voucher_date', 'like', '%'.$request->search.'%')
            ->orWhere('amount', 'like', '%'.$request->search.'%')
            ->with('Contact')
            ->with('Account')
            ->orderBy('expense_id','DESC')
            ->paginate(10);
    }

    public function view(Expense $expense)
    {
        return  Expense::where('expense_id',$expense->expense_id)
            ->with('Contact')->with('Account')
            ->first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'voucher_no' => 'required|max:50',
            'voucher_date' => 'required|date',
            'account_id' => 'required|numeric', 
            'contact_id' => 'required|numeric',
            'payment_mode' => 'required',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'amount' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]); 

        $preference = Preference::first();

        $expense = Expense::where('expense_id',$request->expense_id)->update([
            'fiscal_year' => $preference->fiscal_year,
            'voucher_no' => $request->voucher_no,
            'voucher_date'=> date("Y-m-d", strtotime($request->voucher_date)),
            'account_id'=> $request->account_id,
            'contact_id'=> $request->contact_id,
            'payment_mode'=> $request->payment_mode,
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'amount'=> $request->amount,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);
    }

    public function destroy(Expense $expense)
    {
        Expense::where('expense_id',$expense->expense_id)->delete();
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $expenses = Expense::whereBetween('voucher_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
            ->with(['Contact','Account','Term'])->orderBy('voucher_date','desc')->get();

        $contact = Contact::where('contact_id',$request->contact_id)->first();

        if($request->contact_id!=null) {
                $expenses = $expenses->filter(function($value,$key) use($request){
                    return ($value->contact_id == $request->contact_id);
                })->values();
            }

        if($request->display_type=='display')
        {
            return compact('expenses','contact');
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('expense.report', compact('org','request','expenses','contact'), [], [
                'margin_top' => 41.8
            ])->stream('expense-report.pdf');
        }
        else
        {
            return view('expense.report', compact('org','request','expenses','contact'));
        }
    }

    public function pdf(Expense $expense)
    {
        $org = Organization::first();

        $preference = Preference::first();
        $default_pdf = 'expense.'.$preference->voucher_pdf;
        if($preference->voucher_pdf == 'pdf1' )
        {
            $margin_top = 49;
        }
        elseif($preference->voucher_pdf == 'pdf2')
        {
            $margin_top = 49;
        }
        elseif($preference->voucher_pdf == 'pdf3')
        {
            $margin_top = 49;
        }
        elseif($preference->voucher_pdf == 'pdf4')
        {
            $margin_top = 49;
        }
        elseif($preference->voucher_pdf == 'pdf5')
        {
            $margin_top = 49;
        }
        else
        {
            $margin_top = 10;
        }

        PDF::loadView($default_pdf, compact('org','preference','expense'), [], [
            'margin_top' => $margin_top,
        ])->stream($expense->voucher_no.'.pdf');

    }
}
