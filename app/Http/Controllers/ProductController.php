<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

use App\Product;
use App\Price;
use App\Store;
use App\Warehouse;
use App\BillProduct;
use App\InvoiceProduct;
use App\DeliveryProduct;
use App\Preference;
use App\Organization;
use App\Category;
use App\Tax;
use Auth;
use PDF;
use DNS1D;

class ProductController extends Controller
{
    public $products = [];
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_product_code(Request $request)
    {
        $id = Product::max('product_id');
        $product_code = $id + 1;
        $length = strlen($product_code);
        $n = 4 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $product_code = '0'.$product_code;
        }
        return $product_code;
    }

    public function display(Request $request)
    {
        $products = Product::
            where('product_type', 'like', '%'.$request->search.'%')
            ->orWhere('product_code', 'like', '%'.$request->search.'%')
            ->orWhere('hsn_code', 'like', '%'.$request->search.'%')
            ->orWhere('product_name', 'like', '%'.$request->search.'%')
            ->orWhere('product_unit', 'like', '%'.$request->search.'%')
            ->with('Category')
            ->paginate(10);
        $products->each(function($product,$key){
            $product->current_stock = $product->CurrentStock($product->product_id);
        }); 
        return $products;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_type' => 'required|max:50',
            'product_code' => 'required|max:50|unique:products',
            'hsn_code'  => 'max:50',
            'category_id' => 'required|numeric',
            'product_name' => 'required|unique:products',
            'product_unit' => 'max:50',
            'product_size' => 'nullable|numeric',
            'min_stock' => 'numeric',
            'vendor_id' => 'sometimes|nullable|numeric',
            'purchase_rate_exc' => 'required|numeric|min:0',
            'sales_rate_exc' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'purchase_rate_inc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'opening_stock' => 'required|numeric|min:0',
            'opening_stock_rate' => 'required|numeric|min:0',
        ]);
        
        $product = Product::create([
            'product_type' => $request->product_type,
            'product_code' => $request->product_code,
            'hsn_code' => $request->hsn_code,
            'category_id' => $request->category_id,
            'product_name' => $request->product_name,
            'description' => $request->description,
            'product_unit' => $request->product_unit,
            'product_size' => $request->product_size?$request->product_size:0,
            'min_stock' => $request->min_stock,
            'vendor_id' => $request->vendor_id,
            'created_by' => Auth::User()->username,
        ]);

        $price = Price::create([
            'product_id' => $product->product_id,
            'purchase_rate_exc' => $request->purchase_rate_exc,
            'sales_rate_exc' => $request->sales_rate_exc,
            'tax_id' => $request->tax_id,
            'purchase_rate_inc' => $request->purchase_rate_inc,
            'sales_rate_inc' => $request->sales_rate_inc,
            'created_by' => Auth::User()->username,
        ]);

        $warehouses = Warehouse::get();

        foreach ($warehouses as $warehouse) 
        {
            Store::create([
                'product_id' => $product->product_id,
                'price_id' => $price->price_id,
                'warehouse_id' => $warehouse->warehouse_id,
                'opening_stock' => $request->opening_stock,
                'opening_stock_rate' => $request->opening_stock_rate,
                'created_by' => Auth::User()->username,
            ]);
        }
        return Product::where('product_id',$product['product_id'])->with('Price','Store')->first();
    }

    public function edit(Product $product)
    {
        return Product::where('product_id',$product->product_id)->with('Price','Store')->first();
    }

    public function get_products(Request $request)
    {
        $products = Product::where('product_code', 'like', '%'.$request->search.'%')
            ->orWhere('product_name', 'like', '%'.$request->search.'%')
            ->orWhere('hsn_code', 'like', '%'.$request->search.'%')
            ->limit(5)
            ->orderBy('product_name')
            ->orderBy('product_code')
            ->with(['Price','Store'])
            ->get();
        $products->each(function($product,$key){
            $product->current_stock = $product->CurrentStock($product->product_id);
        }); 
        return $products;
    }

    public function get_products_more(Request $request)
    {
        $products = Product::where('product_code', 'like', '%'.$request->search.'%')
            ->orWhere('product_name', 'like', '%'.$request->search.'%')
            ->orWhere('hsn_code', 'like', '%'.$request->search.'%')
            ->limit(10)
            ->orderBy('product_name')
            ->orderBy('product_code')
            ->with(['Price','Store'])
            ->get();
        $products->each(function($product,$key){
            $product->current_stock = $product->CurrentStock($product->product_id);
        }); 
        return $products;
    }

    public function stock_report(Request $request)
    {
        $org = Organization::first();
        $query = Product::query();
        if($request->category_id!='') 
        {
            $query->where('category_id',$request->category_id);
        }
        // $products = $query->orderBy('product_name')->orderBy('category_id')->get();

        $query->orderBy('product_name')->orderBy('category_id')->chunk(50, function ($prods) use($request) {
            foreach ($prods as $product) {
                $os = $product->OpeningStock($product,$request);
                $ps = $product->PurchaseStock($product,$request);
                $ss = $product->SalesStock($product,$request);
                $cs = $os + $ps - $ss;
                $value = round($cs * $product->Price->sales_rate_inc,2);
                array_push($this->products, [
                    'product_name' => $product->product_name,
                    'category_name' => $product->Category->category_name,
                    'os' => $os,
                    'ps' => $ps,
                    'ss' => $ss,
                    'cs' => $cs,
                    'value' => $value,
                ]);
            }
        });
        $products = $this->products;
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('stock', compact('org','request','products'), [], [
                'margin_top' => 10
                ])->stream('Stock-Report.pdf');
                break;

            case 'excel':
                return view('stock', compact('org','request','products'));
                break;

            default:
                return view('stock', compact('org','request','products'));
                break;
        } 
    }

    public function stock_report_2(Request $request)
    {
        $org = Organization::first();
        $query = Product::query();
        if($request->category_id!='') 
        {
            $category_id = explode(",", $request->category_id);
            $query->whereIn('category_id',$category_id);
        }
        // $products = $query->orderBy('product_name')->orderBy('category_id')->get();

        $query->orderBy('product_name')->orderBy('category_id')->chunk(50, function ($prods) use($request) {
            foreach ($prods as $product) {
                // $os = $product->OpeningStock($product,$request);
                $os = $product->OpeningStock2($product,$request);
                // $os = $product->CurrentStock($product->product_id)['opening_stock'];
                $ps = $product->PurchaseStock($product,$request);
                $ss = $product->SalesStock($product,$request);
                $cs = $os + $ps - $ss;
                if($request->closing_stock == 'notZero' && $cs <= 0) {
                    continue;
                }
                $value = round($cs * $product->Price->sales_rate_inc,2);
                array_push($this->products, [
                    'product_name' => $product->product_name,
                    'category_name' => $product->Category->category_name,
                    'os' => $os,
                    'pRate' => $product->Price->purchase_rate_inc,
                    'ps' => $ps,
                    'sRate' => $product->Price->sales_rate_inc,
                    'ss' => $ss,
                    'cs' => $cs,
                    'value' => $value,
                ]);
            }
        });
        $products = $this->products;
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('stock2', compact('org','request','products'), [], [
                'margin_top' => 10
                ])->stream('Stock-Report.pdf');
                break;

            case 'excel':
                return view('stock2', compact('org','request','products'));
                break;

            default:
                return view('stock2', compact('org','request','products'));
                break;
        } 
    }

    /**
     * Author: Ronald
     */
    public function stock_report_order_by_category_name(Request $request)
    {
        $org = Organization::first();
        $query = Product::query();

        $categories = array();
        if($request->category_id != '') 
        {
            $categories = Category::where('category_id', $request->category_id)->get();
        } else {
            $categories = Category::all()->sortBy("category_name");
        }

        $products = array();
        foreach($categories as $category) {
            $prods = Product::where('category_id', $category->category_id)->orderBy('product_name', 'ASC')->get();
            foreach($prods as $product) {
                $up = Price::where('product_id', $product->product_id)->pluck('sales_rate_inc')->first();
                $os = $product->OpeningStock($product,$request);
                $ps = $product->PurchaseStock($product,$request);
                $ss = $product->SalesStock($product,$request);
                $cs = $os + $ps - $ss;
                $value = round($cs * $product->Price->sales_rate_inc,2);
                array_push($products, [
                    'product_name' => $product->product_name,
                    'category_name' => $product->Category->category_name,
                    'up' => $up,
                    'os' => $os,
                    'ps' => $ps,
                    'tq' => $os + $ps,
                    'ss' => $ss,
                    'cs' => $cs,
                    'value' => $value,
                ]);
            }
        }

        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('stock', compact('org','request','products'), [], [
                'margin_top' => 10
                ])->stream('Stock-Report.pdf');
                break;

            case 'excel':
                return view('stock', compact('org','request','products'));
                break;

            default:
                return view('stock', compact('org','request','products', 'test'));
                break;
        } 
    }

    public function excel_upload(Request $request)
    {
        $this->validate($request, [
            'product_excel' => 'required',
            
        ]);
        if($request->hasFile('product_excel')){
            Validator::make(
                [
                    'extension' => strtolower($request['product_excel']->getClientOriginalExtension()),
                ],
                [
                    'extension'      => 'required|in:xlsx,xls,ods',
                ]
            )->validate();

            $excel_products =  (new ProductImport)->toCollection(request()->file('product_excel'));
            $product_array = $excel_products->toArray();
            $products = $product_array[0];


            Validator::make($products, [
                "*.product_type" => 'nullable',
                "*.product_code" => 'required',
                "*.product_name" => 'required',
                "*.category_code"     => 'nullable',
                "*.hsn_code"     => 'nullable',
                "*.category"     => 'required',
                "*.min_stock"    => 'required|numeric',
                "*.purchase_rate_inc" => 'required|numeric',
                "*.sales_rate_inc"    => 'required|numeric',
                "*.tax"               => 'required|exists:taxes,tax_id',
                "*.purchase_rate_exc" => 'nullable',
                "*.sales_rate_exc"    => 'nullable',
            ])->validate();

            foreach($products as $product){
                $category = Category::where('category_name',$product['category'])->where('category_code',$product['category_code'])->first();
                if(!$category){
                    $id = Category::count('category_id');
                    $category_code = $id + 1;
                    $length = strlen($category_code);
                    $n = 4 - $length;
                    for ($i=0; $i < $n; $i++) 
                    { 
                        $category_code = '0'.$category_code;
                    }
                    $new_category = Category::create([
                        'category_code' => $product['category_code']?$product['category_code']:$category_code,
                        'category_name' => $product['category']
                    ]);
                }
                $new_product = Product::create([
                    'product_type' => $product['product_type'],
                    'product_code' => $product['product_code'],
                    'hsn_code'     => $product['hsn_code'],
                    'category_id'  => $category?$category->category_id:$new_category->category_id,
                    'product_name' => $product['product_name'],
                    'description'  => '',
                    'product_unit' => 'No',
                    'min_stock'    => 2,
                    'created_by'   => Auth::User()->username,
                ]);
                $tax = Tax::where('tax_id',$product['tax'])->first();
                $purchase_rate_inc = $product['purchase_rate_inc']*$tax->tax_rate/100;
                $sales_rate_inc = $product['sales_rate_inc']*$tax->tax_rate/100;
                $purchase_rate_exc = $product['purchase_rate_inc'] -  $purchase_rate_inc;
                $sales_rate_exc = $product['sales_rate_inc'] -  $sales_rate_inc;
                $new_price = Price::create([
                    'product_id'        => $new_product['product_id'],
                    'purchase_rate_exc' => $purchase_rate_exc,
                    'sales_rate_exc'    => $sales_rate_exc,
                    'tax_id'            => $product['tax'],
                    'purchase_rate_inc' => $product['purchase_rate_inc'],
                    'sales_rate_inc'    => $product['sales_rate_inc'],
                    'created_by'        => Auth::User()->username,
                ]);
                $warehouse = Warehouse::first();
                Store::create([
                    'product_id'   => $new_product->product_id,
                    'price_id'     => $new_price->price_id,
                    'opening_stock'=> 0,
                    'warehouse_id' => $warehouse->warehouse_id,
                    'created_by'   => Auth::User()->username,
                ]);  
            }
        }
        // if($request->hasFile('stock_excel')){
        //         Store::create([
        //             'product_id' => $product->product_id,
        //             'price_id' => $price->price_id,
        //             'warehouse_id' => 1,
        //             'created_by' => Auth::User()->username,
        //         ]);
        // }
    }

    public function view(Product $product)
    {
        return Product::where('product_id',$product->product_id)->with('Category')->first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'product_type' => 'required|max:50',
            'product_code'=>'required|max:50|unique:products,product_code,'.$request->product_id.',product_id,deleted_at,NULL',
            'hsn_code'  => 'max:50',
            'category_id' => 'required|numeric',
            'product_name' => 'required',
            'product_unit' => 'max:50',
            'product_size' => 'nullable|numeric',
            'min_stock' => 'numeric',
            'vendor_id' => 'sometimes|nullable|numeric',
            'purchase_rate_exc' => 'required|numeric|min:0',
            'sales_rate_exc' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'purchase_rate_inc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'opening_stock' => 'required|numeric|min:0',
            'opening_stock_rate' => 'required|numeric|min:0',
        ]);

        Product::where('product_id',$request->product_id)->update([
            'product_type' => $request->product_type,
            'product_code' => $request->product_code,
            'hsn_code' => $request->hsn_code,
            'category_id' => $request->category_id,
            'product_name' => $request->product_name,
            'description' => $request->description,
            'product_unit' => $request->product_unit,
            'product_size' => $request->product_size?$request->product_size:0,
            'min_stock' => $request->min_stock,
            'vendor_id' => $request->vendor_id,
            'updated_by' => Auth::User()->username, 
        ]);

        Price::where('product_id',$request->product_id)->update([
            'purchase_rate_exc' => $request->purchase_rate_exc,
            'sales_rate_exc' => $request->sales_rate_exc,
            'tax_id' => $request->tax_id,
            'purchase_rate_inc' => $request->purchase_rate_inc,
            'sales_rate_inc' => $request->sales_rate_inc,
            'updated_by' => Auth::User()->username,
        ]);

        Store::where('product_id',$request->product_id)->update([
            'opening_stock' => $request->opening_stock,
            'opening_stock_rate' => $request->opening_stock_rate,
            'updated_by' => Auth::User()->username,
        ]);
    }

    public function destroy(Request $request)
    {
        
        Product::where('product_id',$request->product_id)->delete();
    }

    public function get_barcode(Request $request)
    {
        //dd($request->input('barcode'));
        return Product::where('product_code',$request->input('barcode'))->with('Store','Price','Category')->first();
    }

    /*
    let purc_qty = 0; let sale_qty = 0;
                purc_qty = calcStock(product.bill_stock,product.bill_return_stock);
                if(typeof product.invoice_stock != 'undefined'){
                    sale_qty = calcStock(product.invoice_stock,product.invoice_return_stock)
                }else{
                    sale_qty = calcStock(product.delivery_stock,product.delivery_return_stock)
                }
                return (purc_qty-sale_qty);
                function calcStock(stock,stock_return){
                    let stock_qty = 0;
                    if(!_.isNull(stock)){
                        stock_qty = stock.quantity;
                    }
                    if(!_.isNull(stock_return)){
                        stock_qty -= stock_return.quantity;
                    }
                    return stock_qty;
                }
    */

    public function current_stock(Request $request)
    {
        $products = Product::orderBy('product_name')->get();
        return $products->filter(function($product,$key){
            $product->current_stock = $product->CurrentStock($product->product_id);
            return $product->current_stock['opening_stock']<=$product->current_stock['min_stock'];
        })->values();
        // $array[] = '';
        // $array_product[]='';
        // $int=0;
        // $preference = Preference::first();
        // switch($preference->inventory_on_calculation){
        //     case 'Invoice':
        //         $products->load(['BillStock','InvoiceStock']);
        //     break;
        //     case 'Delivery Challan':
        //         $products->load(['BillStock','DeliveryStock']);
        //     break;
        //     default:
        //     $products;
        // }
        // foreach($products as $product){
        //     //array_push($array ,$product['min_stock']) ;
        //     //if(($product['BillStock'])!=''){
        //         if($preference->inventory_on_calculation=='Invoice'){
        //             if($product['BillStock']!=''){
        //                 if(($product['BillStock']['quantity']-$product['InvoiceStock']['quantity']) <= $product['min_stock']){
        //                    array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
        //                    array_push($array_product,$product);
        //                 }
        //                 //array_push($array_product,$product['min_stock']);
        //             }
        //         }else{
        //             if(($product['BillStock']['quantity']-$product['DeliveryStock']['quantity']) <= $product['min_stock']){
        //                 array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
        //                 array_push($array_product,$product);
        //             }
        //         }
        //     }
        // //}

        // return array_except($array_product,0);
        

        //return (count($array)-1);
    }

    public function pdf(Product $product)
    {
       // return view('product.barcode', compact('product'));
        // $org = Organization::first();
        // PDF::loadView('product.barcode', compact('product','org'))->stream($product->product_code.'.pdf');

        $org = Organization::first();
        $name = $product->product_id.'.pdf';
        $filePath = public_path().'/files/'.$name;

        // PDF::loadView('product.barcode', compact('product','org'), [], [
        //     'format' => 'A5-L'
        // ])->stream($product->product_code.'.pdf');

        PDF::loadView('product.barcode', compact('product','org'))->save($filePath);
        PDF::loadView('product.barcode', compact('product','org'))->stream($product->product_code.'.pdf');        
    }
}
