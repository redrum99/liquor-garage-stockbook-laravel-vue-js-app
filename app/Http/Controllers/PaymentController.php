<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Preference;
use App\Organization;
use App\Contact;
use App\Payment;
use App\PaymentParticular;
use App\Account;
use App\Expense;
use App\Income;
use App\Receipt;
use App\Master;

use Auth;
use PDF;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_voucher_no(Request $request)
    {
        $preference = Preference::first();
        $id = Payment::where('fiscal_year',$preference->fiscal_year)->count('payment_id');
        $id = $id + 1;
        $n = $preference->voucher_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $voucher_no = $preference->voucher_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $voucher_no;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'vendor_id' => 'required|numeric',
            'voucher_no' => 'required|max:50',
            'voucher_date' => 'required|date',
            'account_id' => 'required|numeric',
            'payment_mode' => 'max:50|required',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'total_amount' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->vendor_id)->first();

        $payment = Payment::create([
            'fiscal_year' => $preference->fiscal_year,
            'vendor_id'=> $request->vendor_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'voucher_no' => $request->voucher_no,
            'voucher_date'=> date("Y-m-d", strtotime($request->voucher_date)),
            'account_id' => $request->account_id,
            'payment_mode' => $request->payment_mode,
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'total_amount'=> $request->total_amount,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->bills as $bill) 
        {
            if($bill['payable_amount']!=0 || $bill['payable_amount']!='')
            {
                PaymentParticular::create([
                    'payment_id' => $payment->payment_id,
                    'reference' => 'App\Bill',
                    'reference_id' => $bill['bill_id'],
                    'paid_amount' => $bill['payable_amount'],
                    'created_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function display(Request $request)
    {
        return Payment::
            whereHas('Vendor', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhereHas('Account', function($query) use($request){
                $query->where('account_name','like', "%$request->search%");
            })
            ->orWhere('voucher_no', 'like', '%'.$request->search.'%')
            ->orWhere('voucher_date', 'like', '%'.$request->search.'%')
            ->orWhere('total_amount', 'like', '%'.$request->search.'%')
            ->with('Vendor')
            ->with('Account')
            ->orderBy('payment_id','DESC')
            ->paginate(10);
    }

    public function view(Payment $payment)
    {
        return  Payment::where('payment_id',$payment->payment_id)
            ->with('PaymentParticulars')->with('Vendor')->with('Account')
            ->first();
    }

    public function destroy(Payment $payment)
    {
        PaymentParticular::where('payment_id',$payment->payment_id)->delete();
        Payment::where('payment_id',$payment->payment_id)->delete();
    }

    public function pdf(Payment $payment)
    {
        $org = Organization::first();
        $payment = Payment::where('payment_id',$payment->payment_id)->with('PaymentParticulars','Vendor','Account')->first();

        $pdf = PDF::loadView('payment.pdf1', compact('payment','org'), [], [
            'margin_top' => 49,
        ])->stream($payment->voucher_no.'.pdf');

    }

    public function get_balance_report(Request $request)
    {
        //$from_date
        $org = Organization::first();
        $payment = Payment::WhereBetween('voucher_date', [$request->from_date, $request->to_date])
                -> selectRaw('payment_mode, sum(total_amount) as total_amount')->groupBy('payment_mode')
                ->get();
        $expense = Expense::WhereBetween('voucher_date', [$request->from_date, $request->to_date])
                -> selectRaw('payment_mode, sum(amount) as amount')->groupBy('payment_mode')
                ->get();

        $receipt = Receipt::WhereBetween('receipt_date', [$request->from_date, $request->to_date])
                -> selectRaw('payment_mode, sum(total_amount) as total_amount')->groupBy('payment_mode')
                ->get();
        $income = Income::WhereBetween('receipt_date', [$request->from_date, $request->to_date])
                        -> selectRaw('payment_mode, sum(amount) as amount')->groupBy('payment_mode')
                        ->get();
        $payment_modes = Master::where('master_name','Payment Mode')->get();
        //dd($payment_modes);

        $data =  compact('payment','expense','receipt','income','org','request','payment_modes');


        //dd($data);

        if($request->display_type=='display')
        {
            return $data;
        }
        else if($request->display_type=='pdf')
        {
            //dd($data);
            PDF::loadView('balance.report',compact('payment','expense','receipt','income','org','request','payment_modes'), [], [
                'margin_top' => 10
            ])->stream('balance-report.pdf');
        }
        else
        {
            return view('balance.report', $data);
        }
    }
}
