<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;
use Auth;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_settings(Request $request)
    {
        $setting = Setting::get();
        $array = $setting->toArray();
        $data_req[] =''; 
        foreach($array as $data){
            array_push($data_req,$data['discounts_in'],$data['enable_barcode']);
        }
        
        return $data_req;
    }
    public function fetch_settings(Request $request)
    {
        $settings = Setting::first();
        return $settings;
    }
    public function update(Request $request)
    {
        Setting::where('setting_id',$request->setting_id)->update([
            'discounts_in'    => $request->discounts_in,
            'enable_barcode'  => $request->enable_barcode,
            'migrations'      => $request->migrations,
            'purchase_orders' => $request->purchase_orders,
            'bills'           => $request->bills,
            'bill_returns'    => $request->bill_returns,
            'payments'        => $request->payments,
            'quotations'      => $request->quotations,
            'proformas'       => $request->proformas,
            'deliveries'      => $request->deliveries,
            'delivery_returns'=> $request->delivery_returns,
            'invoices'        => $request->invoices,
            'invoice_returns' => $request->invoice_returns,
            'receipts'        => $request->receipts,
            'incomes'         => $request->incomes,
            'expenses'        => $request->expenses,
            'enable_printer'  => $request->enable_printer,
            'transactions'        => $request->transactions,
            'updated_by'      => Auth::User()->username,
        ]);
        return Setting::first();
    }
}