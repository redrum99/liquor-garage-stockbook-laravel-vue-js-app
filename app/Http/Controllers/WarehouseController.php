<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;
use Auth;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Warehouse::
        	where('warehouse_name', 'like', '%'.$request->search.'%')
        	->orWhere('warehouse_location', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'warehouse_name' => 'required|max:50',
            'warehouse_location' => 'required|max:50',
        ]);
        Warehouse::create([
            'warehouse_name' => $request->warehouse_name,
            'warehouse_location' => $request->warehouse_location,
            'created_by' => Auth::User()->username,         
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'warehouse_name' => 'required|max:50',
            'warehouse_location' => 'required|max:50',
        ]);
        Warehouse::where('warehouse_id',$request->warehouse_id)->update([
            'warehouse_name' => $request->warehouse_name,
            'warehouse_location' => $request->warehouse_location,
            'updated_by' => Auth::User()->username,         
        ]);
    }

    public function destroy(Warehouse $warehouse)
    {
    	return Warehouse::where('warehouse_id',$warehouse->warehouse_id)->delete();
    }
}
