<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceReturn;
use App\InvoiceReturnProduct;
use App\Preference;
use App\Setting;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;
use App\Receipt;
use App\ReceiptParticular;
use App\Account;
use Auth;
use PDF;

class InvoiceReturnController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_invoice_return_no(Request $request)
    {
        $preference = Preference::first();
        $id = InvoiceReturn::where('fiscal_year',$preference->fiscal_year)->count('invoice_return_id');
        $id = $id + 1;
        $n = $preference->invoice_return_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $invoice_return_no = $preference->invoice_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $invoice_return_no;
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'sales_rate_exc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
       
        $this->validate($request, [
            'invoice_return_no' => 'required|max:50',
            'invoice_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $invoice_return = InvoiceReturn::create([
            'fiscal_year' => $preference->fiscal_year,
            'invoice_return_no' => $request->invoice_return_no,
            'invoice_return_date'=> date("Y-m-d", strtotime($request->invoice_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'invoice_return_status'=>'Open',
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->invoice_return_products as $invoice_return_product) 
        {
            $product = Product::where('product_id',$invoice_return_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_return_product['price_id'])->first();
            $store = Store::where('store_id',$invoice_return_product['store_id'])->first();

            $invoice_return_product_up = InvoiceReturnProduct::create([
                'invoice_return_id' => $invoice_return->invoice_return_id,
                'product_id' => $invoice_return_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $invoice_return_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $invoice_return_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $invoice_return_product['price_id'],
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc' => $invoice_return_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc' => $invoice_return_product['sales_rate_inc'],
                'store_id' => $invoice_return_product['store_id'],
                'warehouse_id' => $invoice_return_product['warehouse_id'],
                'quantity' => $invoice_return_product['quantity'],
                'amount' => $invoice_return_product['amount'],
                'discount' => $invoice_return_product['discount'],
                'discount_type' => $invoice_return_product['discount_type'],
                'discount_amount' => $invoice_return_product['discount_amount'],
                'tax_id' => $invoice_return_product['tax_id'],
                'tax_amount' => $invoice_return_product['tax_amount'],
                'sub_total' => $invoice_return_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
            if(!empty($request->reference)){
                InvoiceReturnProduct::where('invoice_return_product_id',$invoice_return_product_up['invoice_return_product_id'])->update([
                    'reference' => 'App'."\\".$request->reference,
                    'reference_id' => $invoice_return_product[$request->reference_id],
                ]);
                //update invoice product status
            }
        }
    }

    public function display(Request $request)
    {
        return InvoiceReturn::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('invoice_return_no', 'like', '%'.$request->search.'%')
            ->orWhere('invoice_return_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('invoice_return_id','DESC')
            ->paginate(10);
    }

    public function view(InvoiceReturn $invoice_return)
    {
        return  InvoiceReturn::where('invoice_return_id',$invoice_return->invoice_return_id)->with('InvoiceReturnProducts')->with('Customer')->first();
    }

    public function destroy(InvoiceReturn $invoice_return)
    {
        InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->delete();
        InvoiceReturn::where('invoice_return_id',$invoice_return->invoice_return_id)->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'invoice_return_no' => 'required|max:50',
            'invoice_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $invoice_return = InvoiceReturn::where('invoice_return_id',$request->invoice_return_id)->first();
        $invoice_products = InvoiceReturn::where('invoice_return_id',$request->invoice_return_id)->get();
        InvoiceReturn::where('invoice_return_id',$request->invoice_return_id)->update([
            'fiscal_year' => $invoice_return->fiscal_year,
            'invoice_return_no' => $request->invoice_return_no,
            'invoice_return_date'=> date("Y-m-d", strtotime($request->invoice_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $invoice_return->reference_date,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);

        foreach ($request->deleted_invoice_return_products as $invoice_return_product) 
        {
            InvoiceReturnProduct::where('invoice_return_product_id',$invoice_return_product['invoice_return_product_id'])->delete();
        }

        foreach ($request->invoice_return_products as $invoice_return_product) 
        {
            $product = Product::where('product_id',$invoice_return_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_return_product['price_id'])->first();
            $store = Store::where('store_id',$invoice_return_product['store_id'])->first();

            if(empty($invoice_return_product['invoice_return_product_id'])) {
                InvoiceReturnProduct::create([
                    'invoice_return_id' => $invoice_return->invoice_return_id,
                    'product_id' => $invoice_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $invoice_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $invoice_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $invoice_return_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $invoice_return_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $invoice_return_product['sales_rate_inc'],
                    'store_id' => $invoice_return_product['store_id'],
                    'warehouse_id' => $invoice_return_product['warehouse_id'],
                    'quantity' => $invoice_return_product['quantity'],
                    'amount' => $invoice_return_product['amount'],
                    'discount' => $invoice_return_product['discount'],
                    'discount_type' => $invoice_return_product['discount_type'],
                    'discount_amount' => $invoice_return_product['discount_amount'],
                    'tax_id' => $invoice_return_product['tax_id'],
                    'tax_amount' => $invoice_return_product['tax_amount'],
                    'sub_total' => $invoice_return_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                InvoiceReturnProduct::where('invoice_return_product_id',$invoice_return_product['invoice_return_product_id'])->update([
                    'invoice_return_id' => $invoice_return->invoice_return_id,
                    'product_id' => $invoice_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $invoice_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $invoice_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $invoice_return_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $invoice_return_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $invoice_return_product['sales_rate_inc'],
                    'store_id' => $invoice_return_product['store_id'],
                    'warehouse_id' => $invoice_return_product['warehouse_id'],
                    'quantity' => $invoice_return_product['quantity'],
                    'amount' => $invoice_return_product['amount'],
                    'discount' => $invoice_return_product['discount'],
                    'discount_type' => $invoice_return_product['discount_type'],
                    'discount_amount' => $invoice_return_product['discount_amount'],
                    'tax_id' => $invoice_return_product['tax_id'],
                    'tax_amount' => $invoice_return_product['tax_amount'],
                    'sub_total' => $invoice_return_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function pdf(InvoiceReturn $invoice_return)
    {
        $org = Organization::first();
        $preference = Preference::first();
        $default_pdf = 'invoice_return.'.$preference->invoice_return_pdf;
        $account = Account::orderBy('account_id','ASC')->first();
        if($preference->invoice_return_pdf == 'pdf1' )
        {
            $margin_top = 15;
        }
        elseif($preference->invoice_return_pdf == 'pdf2')
        {
            $margin_top = 69;
        }
        elseif($preference->invoice_return_pdf == 'pdf3')
        {
            $margin_top = 73;
        }
        elseif($preference->invoice_return_pdf == 'pdf4')
        {
            $margin_top = 20;
        }
        elseif($preference->invoice_return_pdf == 'pdf5')
        {
            $margin_top = 66;
        }
        else
        {
            $margin_top = 10;
        }

        // $invoice = Invoice::where('invoice_id',$invoice->invoice_id)->with('InvoiceProducts','Customer','SourcePlace','DestinationPlace')->first();

        PDF::loadView($default_pdf, compact('org','preference','invoice_return','account'), [], [
            'margin_top' => $margin_top,
        ])->stream($invoice_return->invoice_return_no.'.pdf');
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $setting = Setting::first();
        if($request->customer_id=='')
        {
            $invoice_returns = InvoiceReturn::
            whereBetween('invoice_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
            ->with('InvoiceReturnProducts','Customer')->get();
            $customer = '';
        }else{
           $invoice_returns = InvoiceReturn::where('customer_id',$request->customer_id)->
            whereBetween('invoice_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
            ->with('InvoiceReturnProducts','Customer')->get();
            $customer = Contact::where('contact_id',$request->customer_id)->first();
        }
        if($request->display_type=='display')
        {
            return $invoice_returns;
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('invoice_return.report', compact('invoice_returns','setting','org','request','customer'), [], [
                'margin_top' => 10
            ])->stream('invoice-return-report.pdf');
        }
        else
        {
            return view('invoice_return.report', compact('invoice_returns','setting','org','request','customer'));
        }
    }

   
}
