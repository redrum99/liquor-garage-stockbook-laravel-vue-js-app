<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Preference;
use App\Organization;
use App\Contact;
use App\Receipt;
use App\ReceiptParticular;
use Auth;
use PDF;


class ReceiptController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_receipt_no(Request $request)
    {
        $preference = Preference::first();
        $id = Receipt::where('fiscal_year',$preference->fiscal_year)->count('receipt_id');
        $id = $id + 1;
        $n = $preference->receipt_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $receipt_no = $preference->receipt_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $receipt_no;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required|numeric',
            'receipt_no' => 'required|max:50',
            'receipt_date' => 'required|date',
            'account_id' => 'required|numeric',
            'payment_mode' => 'max:50|required',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'total_amount' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $receipt = Receipt::create([
            'fiscal_year' => $preference->fiscal_year,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'receipt_no' => $request->receipt_no,
            'receipt_date'=> date("Y-m-d", strtotime($request->receipt_date)),
            'account_id' => $request->account_id,
            'payment_mode' => $request->payment_mode,
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'total_amount'=> $request->total_amount,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->invoices as $invoice) 
        {
            if($invoice['payable_amount']!=0 || $invoice['payable_amount']!='')
            {
                ReceiptParticular::create([
                    'receipt_id' => $receipt->receipt_id,
                    'reference' => 'App\Invoice',
                    'reference_id' => $invoice['invoice_id'],
                    'paid_amount' => $invoice['payable_amount'],
                    'created_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function display(Request $request)
    {
        return Receipt::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhereHas('Account', function($query) use($request){
                $query->where('account_name','like', "%$request->search%");
            })
            ->orWhere('receipt_no', 'like', '%'.$request->search.'%')
            ->orWhere('receipt_date', 'like', '%'.$request->search.'%')
            ->orWhere('total_amount', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->with('Account')
            ->orderBy('receipt_id','DESC')
            ->paginate(10);
    }

    public function view(Receipt $receipt)
    {
        return  Receipt::where('receipt_id',$receipt->receipt_id)
            ->with('ReceiptParticulars')->with('Customer')->with('Account')
            ->first();
    }

    public function destroy(Receipt $receipt)
    {
        ReceiptParticular::where('receipt_id',$receipt->receipt_id)->delete();
        Receipt::where('receipt_id',$receipt->receipt_id)->delete();
    }

    public function pdf(Receipt $receipt)
    {
        $org = Organization::first();
        $receipt = Receipt::where('receipt_id',$receipt->receipt_id)->with('ReceiptParticulars','Customer','Account')->first();

        $pdf = PDF::loadView('receipt.pdf1', compact('receipt','org'), [], [
            'margin_top' => 49,
        ])->stream($receipt->receipt_no.'.pdf');

    }
}
