<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use Auth;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Subscription::
            where('subscription_key', 'like', '%'.$request->search.'%')
            ->orWhere('no_of_days', 'like', '%'.$request->search.'%')
            ->orWhere('start_date', 'like', '%'.$request->search.'%')
            ->orWhere('end_date', 'like', '%'.$request->search.'%')
            ->paginate(5);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'subscription_key' => 'required|max:255',
    		'no_of_days' => 'required|numeric',
    		'start_date' => 'required|date',
    		'end_date' => 'required|date',
    	]);

    	Subscription::create([
    		'subscription_key' => $request->subscription_key,
    		'no_of_days' => $request->no_of_days,
    		'start_date' => date("Y-m-d", strtotime($request->start_date)),
    		'end_date' => date("Y-m-d", strtotime($request->end_date)),
    		'created_by' => Auth::User()->username, 
    	]);
	}
}
