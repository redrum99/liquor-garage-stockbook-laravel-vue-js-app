<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Account;
use App\Bill;
use App\BillProduct;
use App\BillReturn;
use App\BillReturnProduct;
use App\Category;
use App\Contact;
use App\Delivery;
use App\DeliveryProduct;
use App\DeliveryReturn;
use App\DeliveryReturnProduct;
use App\Expense;
use App\Income;
use App\Invoice;
use App\InvoiceProduct;
use App\InvoiceReturn;
use App\InvoiceReturnProduct;
use App\Master;
use App\Organization;
use App\Payment;
use App\PaymentParticular;
use App\PaymentTerm;
use App\Place;
use App\Preference;
use App\Price;
use App\Product;
use App\Proforma;
use App\ProformaProduct;
use App\PurchaseOrder;
use App\PurchaseOrderProduct;
use App\Quotation;
use App\QuotationProduct;
use App\Receipt;
use App\ReceiptParticular;
use App\Setting;
use App\Store;
use App\Tax;
use App\Term;
use App\User;
use App\Warehouse;

class PreferenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_preference()
    {
        return Preference::first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
          
            'purchase_order_prefix' => 'required|max:50',
            'purchase_order_no_length' => 'required|max:50',
           	'purchase_order_pdf'=>'required|max:50',
           	'quotation_prefix' => 'required|max:50',
            'quotation_no_length' => 'required|max:50',
            'quotation_pdf'=>'required|max:50',
            'proforma_prefix' => 'required|max:50',
            'proforma_no_length' => 'required|max:50',
            'proforma_pdf'=>'required|max:50',
            'invoice_prefix' => 'required|max:50',
            'invoice_no_length' => 'required|max:50',
            'invoice_pdf'=>'required|max:50',
            'delivery_prefix' => 'required|max:50',
            'delivery_no_length' => 'required|max:50',
            'delivery_pdf'=>'required|max:50',
            'voucher_prefix' => 'required|max:50',
            'voucher_no_length' => 'required|max:50',
          	'voucher_pdf'=>'required|max:50',
            'receipt_prefix' => 'required|max:50',
            'receipt_no_length' => 'required|max:50',
           	'receipt_pdf'=>'required|max:50',
            'receipt_prefix' => 'required|max:50',
            'bill_return_no_length' => 'required|max:50',
            'bill_return_pdf'=>'required|max:50',
            'invoice_return_prefix' => 'required|max:50',
            'invoice_return_no_length' => 'required|max:50',
            'invoice_return_pdf'=>'required|max:50',
            'delivery_return_prefix' => 'required|max:50',
            'delivery_return_no_length' => 'required|max:50',
            'delivery_return_pdf'=>'required|max:50',
            'fiscal_year'=>'required|max:50',
            'discount_on'=> 'required|max:50',
            'inventory_on_calculation' => 'required|max:50',
            'printer_name' => 'required|max:50',
            'min_redeem_points' => 'required|min:0|numeric',
        ]);

        return Preference::where('preference_id',$request->preference_id)->update([
        	'purchase_order_prefix' => $request->purchase_order_prefix,
        	'purchase_order_no_length' => $request->purchase_order_no_length,
        	'purchase_order_pdf' => $request->purchase_order_pdf,
        	'quotation_prefix' => $request->quotation_prefix,
        	'quotation_no_length' => $request->quotation_no_length,
        	'quotation_pdf' => $request->quotation_pdf,
        	'proforma_prefix' => $request->proforma_prefix,
        	'proforma_no_length' => $request->proforma_no_length,
        	'proforma_pdf' => $request->proforma_pdf,
        	'invoice_prefix' => $request->invoice_prefix,
        	'invoice_no_length' => $request->invoice_no_length,
        	'invoice_pdf' => $request->invoice_pdf,
        	'delivery_pdf' => $request->delivery_pdf,
        	'delivery_no_length' => $request->delivery_no_length,
        	'delivery_pdf' => $request->delivery_pdf,
        	'voucher_prefix' => $request->voucher_prefix,
        	'voucher_no_length' => $request->voucher_no_length,
        	'voucher_pdf' => $request->voucher_pdf,
        	'receipt_prefix' => $request->receipt_prefix,
        	'receipt_no_length' => $request->receipt_no_length,
        	'receipt_pdf' => $request->receipt_pdf,
            'bill_return_prefix' => $request->bill_return_prefix,
            'bill_return_no_length' => $request->bill_return_no_length,
            'bill_return_pdf' => $request->bill_return_pdf,
            'invoice_return_prefix' => $request->invoice_return_prefix,
            'invoice_return_no_length' => $request->invoice_return_no_length,
            'invoice_return_pdf' => $request->invoice_return_pdf,
            'delivery_return_prefix' => $request->delivery_return_prefix,
            'delivery_return_no_length' => $request->delivery_return_no_length,
            'delivery_return_pdf' => $request->delivery_return_pdf,
            'fiscal_year' => $request->fiscal_year,
            'discount_on' => $request->discount_on,
            'inventory_on_calculation' => $request->inventory_on_calculation,
            'allow_negative_stock' => $request->allow_negative_stock,
            'printer_name' => $request->printer_name,
            'min_redeem_points' => $request->min_redeem_points,
            'updated_by' => Auth::User()->username,         
        ]);
    }

    public function get_table_names(){
        return \DB::connection('mysql')->select('SHOW TABLES');
    }

    public function migrate_data(Request $request)
    {//v3
        // dd($request);
        $db_int = \DB::connection('mysql');
        $db_ext = \DB::connection('mysql_external');
        $preference = Preference::first();
        foreach($request->input() as $table){
         switch($table['table_name']){
                case "accounts"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('accounts')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('accounts')->get();
                    foreach($table_data as $account){
                        Account::create([
                            'account_code' => $account->account_code,
                            'account_name' => $account->account_name,
                            'account_no'   => $account->account_no,
                            'bank_name'    => $account->bank_name,
                            'branch_name'  => $account->branch_name,
                            'ifsc_code'    => $account->ifsc_code,
                            'created_by'   => $account->created_by,
                            'updated_by'   => $account->updated_by ? $account->updated_by:NULL,
                            'created_at'   => $account->created_at ? date('Y-m-d',strtotime($account->created_at)) : NULL,
                            'updated_at'   => $account->updated_at ? date('Y-m-d',strtotime($account->updated_at)) : NULL,
                            'deleted_at'   => $account->deleted_at ? date('Y-m-d',strtotime($account->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "bills"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('bill_products')->truncate();
                    $db_int->table('bills')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    $table_data = $db_ext->table('purchases')->get();
                    foreach($table_data as $purc){
                        $contact = $preference->fetchContact($purc->contact_id);
                        $src = $preference->fetchPlace($purc->source_place);
                        $dest = $preference->fetchPlace($purc->destination_place);
                        $bill = Bill::create([
                            'fiscal_year' => $preference->fiscal_year,
                            'bill_no'     => $purc->bill_no,
                            'bill_date'   => date("Y-m-d", strtotime($purc->bill_date)),
                            'due_date'    => date("Y-m-d", strtotime($purc->due_date)),
                            'reference_no'=> $purc->order_no,
                            'reference_date'   => $purc->bill_date?date('Y-m-d',strtotime($purc->bill_date)):NULL,
                            'vendor_id'        => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,
                            'shipping_address' => $contact->shipping_address,
                            'payment_term_id'  => $purc->payment_term_id?$purc->payment_term_id:1,
                            'source_id'        => $src->place_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'        => 0,
                            'discount_amount'  => 0,
                            'tax_amount'       => 0,
                            'total_amount'     => 0,
                            'round_off'        => 0,
                            'grand_total'      => 0,
                            // 'term_id'=>$purc->term_id,
                            // 'terms'=>$purc->terms,
                            'note'         => $purc->e_way_bill ? 'e-Sugam-'.$purc->e_way_bill.','.$purc->note : $purc->note,
                            'bill_status'  => $purc->purchase_status,
                            'created_by'   => $purc->created_by,
                            'updated_by'   => $purc->updated_by ? $purc->updated_by:NULL,
                            'created_at'   => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
                            'updated_at'   => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
                        ]);
                        $purchase_products = $db_ext->table('purchase_products')->where('purchase_id',$purc->purchase_id)->get();
                        foreach ($purchase_products as $purchase_product) 
                        {
                            //calculations
                            $category = $preference->fetchCategory($purchase_product->category_id);
                            $tax     = $preference->fetchTax($purchase_product->tax_id);
                            $product  = $preference->fetchProduct($purchase_product->product_id);
                            $price    = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();

                            $amount  = $preference->amountCalc($purchase_product->sales_rate_exc,$purchase_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$purchase_product->discount,$purchase_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;

                            $bill_product = BillProduct::create([
                                'bill_id'      => $bill->bill_id,
                                'product_id'   => $product->product_id,
                                'product_type' => $purchase_product->product_type,
                                'product_code' => $purchase_product->product_code,
                                'hsn_code'     => $purchase_product->hsn_code,
                                'category_id'  => $category->category_id,
                                'product_name' => $purchase_product->product_name,
                                // 'description'  => $purchase_product->description,
                                'product_unit' => $purchase_product->product_unit,
                                'price_id'     => $price->price_id,
                                'purchase_rate_exc' => $purchase_product->purchase_rate_exc?$purchase_product->purchase_rate_exc:0,
                                'sales_rate_exc'    => $purchase_product->sales_rate_exc?$purchase_product->sales_rate_exc:0,
                                'purchase_rate_inc' => $purchase_product->purchase_rate_inc?$purchase_product->purchase_rate_inc:0,
                                'sales_rate_inc'    => $purchase_product->sales_rate_exc?$purchase_product->sales_rate_exc:0,
                                'store_id'      => $store->store_id,
                                'warehouse_id'  => $store->warehouse_id,
                                'quantity'      => $purchase_product->quantity,
                                'amount'        => $amount,//
                                'discount'      => $purchase_product->discount,
                                'discount_type' => $purchase_product->discount_type,
                                'discount_amount' => $discount_amount,//
                                'tax_id'        => $tax->tax_id,
                                'tax_amount'    => $tax_amount,//
                                'sub_total'     => $sub_total,//
                                'created_by'    => $purchase_product->created_by,
                                'updated_by'    => $purchase_product->updated_by ? $purchase_product->updated_by:NULL,
                                'created_at'    => $purchase_product->created_at ? date('Y-m-d',strtotime($purchase_product->created_at)) : NULL,
                                'updated_at'    => $purchase_product->updated_at ? date('Y-m-d',strtotime($purchase_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = BillProduct::where('bill_id',$bill->bill_id)->sum('amount');
                        $tax_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('tax_amount');
                        $discount_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('discount_amount');
                        $total_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                            Bill::where('bill_id',$bill->bill_id)->update([
                                'sub_total'   => $sub_total,
                                'tax_amount'  => $tax_amount,
                                'total_amount'=> $total_amount,
                                'round_off'   => $round_off,
                                'grand_total' => $grand_total,
                            ]);
                    }
                break;
                case "bill_returns"://no data
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('bill_return_products')->truncate();
                    $db_int->table('bill_returns')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('purchase_returns')->get();
                    foreach($table_data as $purc){
                        $contact = $preference->fetchContact($purc->contact_id);
                        $src = $preference->fetchPlace($purc->source_place);
                        $dest = $preference->fetchPlace($purc->destination_place);

                        $bill_return = BillReturn::create([
                            'fiscal_year'     => $purc->purchase_return_year,
                            'bill_return_no'  => $purc->purchase_return_no,
                            'bill_return_date'=> date("Y-m-d", strtotime($purc->purchase_return_date)),
                            'reference_no'    => $purc->bill_no,
                            'reference_date'  => $purc->purchase_return_date ? date('Y-m-d',strtotime($purc->purchase_return_date)) : NULL,
                            'vendor_id'       => $contact->contact_id,
                            'billing_address' => $contact->billing_address,
                            'shipping_address'=> $contact->shipping_address,
                            'source_id'       => $src->place_id,
                            'destination_id'  => $dest->place_id,
                            'sub_total'       => 0,
                            'discount_amount' => 0,
                            'tax_amount'      => 0,
                            'total_amount'    => 0,
                            'round_off'       => 0,
                            'grand_total'     => 0,
                            // 'term_id'=>$purc->term_id,
                            // 'terms'=>$purc->terms,
                            'note'            => $purc->e_way_bill ? 'e-Sugam-'.$purc->e_way_bill.','.$purc->note : $purc->note,
                            'bill_return_status'=> $purc->purchase_return_status,
                            'created_by'    => $purc->created_by,
                            'updated_by'    => $purc->updated_by ? $purc->updated_by:NULL,
                            'created_at'    => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
                            'updated_at'    => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
                        ]);

                        $purchase_return_products = $db_ext->table('purchase_return_products')->where('purchase_return_id',$purc->purchase_return_id)->get();
                        foreach ($purchase_return_products as $purchase_return_product) 
                        {
                            //calculations//old table structure
                            $category = $preference->fetchCategory($purchase_return_product->category_id);
                            $tax     = $preference->fetchTax($purchase_return_product->tax_id);
                            $product  = $preference->fetchProduct($purchase_return_product->product_id);
                            $price    = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();

                            $amount  = $preference->amountCalc($purchase_return_product->sales_rate,$purchase_return_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$purchase_return_product->discount,$purchase_return_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;

                            $bill_return_product = BillReturnProduct::create([
                                'bill_return_id' => $bill_return->bill_return_id,
                                'product_id'     => $product->product_id,
                                'product_type'   => $purchase_return_product->product_type,
                                'product_code'   => $purchase_return_product->product_code,
                                'hsn_code'       => $product->hsn_code,
                                'category_id'    => $category->category_id,
                                'product_name'   => $purchase_return_product->product_name,
                                // 'description'    => $product->description,
                                'product_unit'   => $purchase_return_product->product_unit,
                                'price_id'       => $price->price_id,
                                'purchase_rate_exc' => $purchase_return_product->purchase_rate?$purchase_return_product->purchase_rate:0,
                                'sales_rate_exc'    => $purchase_return_product->sales_rate?$purchase_return_product->sales_rate:0,
                                'purchase_rate_inc' => $purchase_return_product->purchase_rate?$purchase_return_product->purchase_rate:0,
                                'sales_rate_inc'    => $purchase_return_product->mrp?$purchase_return_product->mrp:0,
                                'store_id'      => $store->store_id,
                                'warehouse_id'  => $store->warehouse_id,
                                'quantity'      => $purchase_return_product->quantity,
                                'amount'        => $amount,
                                'discount'      => $purchase_return_product->discount,
                                'discount_type' => $purchase_return_product->discount_type,
                                'discount_amount' => $discount_amount,
                                'tax_id'        => $tax->tax_id,
                                'tax_amount'    => $tax_amount,
                                'sub_total'     => $sub_total,
                                'created_by'    => $purchase_return_product->created_by,
                                'updated_by'    => $purchase_return_product->updated_by ? $purchase_return_product->updated_by:NULL,
                                'created_at'    => $purchase_return_product->created_at ? date('Y-m-d',strtotime($purchase_return_product->created_at)) : NULL,
                                'updated_at'    => $purchase_return_product->updated_at ? date('Y-m-d',strtotime($purchase_return_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('amount');
                        $tax_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('tax_amount');
                        $discount_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('discount_amount');
                        $total_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                            BillReturn::where('bill_return_id',$bill_return->bill_return_id)->update([
                                'sub_total'   => $sub_total,
                                'tax_amount'  => $tax_amount,
                                'total_amount'=> $total_amount,
                                'round_off'   => $round_off,
                                'grand_total' => $grand_total,
                            ]);
                    }
                break;
                case "categories"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('categories')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('categories')->get();
                    foreach($table_data as $category){
                        Category::create([
                            'category_code' => $category->category_code,
                            'category_name' => $category->category_name,
                            'created_by'    => $category->created_by,
                            'updated_by'    => $category->updated_by ? $category->updated_by:NULL,
                            'created_at'    => $category->created_at ? date('Y-m-d',strtotime($category->created_at)) : NULL,
                            'updated_at'    => $category->updated_at ? date('Y-m-d',strtotime($category->updated_at)) : NULL,
                            'deleted_at'    => $category->deleted_at ? date('Y-m-d',strtotime($category->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "contacts"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('contacts')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('contacts')->get();
                    foreach($table_data as $contact){
                        $count = 0;
                        if(!is_null($contact->mobile_no)){
                            $count = Contact::where('mobile_no',$contact->mobile_no)->count();    
                        }
                        if($count == 0){
                            $place = $preference->fetchPlace($contact->place_id);
                            Contact::create([
                                'contact_type' => $contact->contact_type=='Supplier'?'Vendor':$contact->contact_type,//fk
                                'contact_code' => $contact->contact_code,
                                'contact_name' => $contact->contact_name,
                                'email'        => $contact->email,
                                'mobile_no'    => $contact->mobile_no,
                                'phone_no'     => $contact->phone_no,
                                'pan_no'       => $contact->pan_no,
                                'gstin_no'     => $contact->gstin_no,
                                'billing_address'  => $contact->billing_address_1,
                                'shipping_address' => $contact->shipping_address_1,
                                'account_no'   => $contact->account_no,
                                'bank_name'    => $contact->bank_name,
                                'branch_name'  => $contact->branch_name,
                                'ifsc_code'    => $contact->ifsc_code,
                                'payment_term_id'  => $contact->payment_term_id,//fk
                                'place_id'     => $place->place_id?$place->place_id:1,
                                'date_of_birth'=> $contact->dob ? date('Y-m-d',strtotime($contact->dob)) : NULL,
                                'wedding_anniversary' => $contact->wedding_anniversary ? date('Y-m-d',strtotime($contact->wedding_anniversary)) : NULL,
                                'created_by'   => $contact->created_by,
                                'updated_by'   => $contact->updated_by ? $contact->updated_by:NULL,
                                'created_at'   => $contact->created_at ? date('Y-m-d',strtotime($contact->created_at)) : NULL,
                                'updated_at'   => $contact->updated_at ? date('Y-m-d',strtotime($contact->updated_at)) : NULL,
                                'deleted_at'   => $contact->deleted_at ? date('Y-m-d',strtotime($contact->deleted_at)) : NULL,
                            ]);    
                        }
                    }
                break;
                case "deliveries"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('delivery_products')->truncate();
                    $db_int->table('deliveries')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('delivery_challans')->get();
                    foreach($table_data as $deli){
                        $contact = $preference->fetchContact($deli->contact_id);
                        $term = $preference->fetchTerm($deli->terms);
                        $src = $preference->fetchPlace($deli->source_place);
                        $dest = $preference->fetchPlace($deli->destination_place);
                        $delivery = Delivery::create([
                            'fiscal_year'      => $deli->delivery_challan_year,
                            'delivery_no'      => $deli->delivery_challan_no,
                            'delivery_date'    => date("Y-m-d", strtotime($deli->delivery_challan_date)),
                            'reference_no'     => $deli->reference_no,
                            'reference_date'   => $deli->delivery_challan_date ? date('Y-m-d',strtotime($deli->delivery_challan_date)) : NULL,//no reference date in old db
                            'customer_id'      => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,//new db
                            'shipping_address' => $contact->shipping_address,//new db
                            'source_id'        => $src->place_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'        => 0,//new field
                            'discount_amount'  => 0,//new field
                            'tax_amount'       => 0,//new field
                            'total_amount'     => 0,//new field
                            'round_off'        => 0,//new field
                            'grand_total'      => 0,//new field
                            'term_id'          => $term?$term->term_id:NULL,//fk
                            'terms'            => $term?$term->term:NULL,//fk
                            'note'             => $deli->e_way_bill ? 'e-Sugam-'.$deli->e_way_bill.','.$deli->note : $deli->note,
                            'delivery_status'  => $deli->delivery_challan_status,
                            'created_by'       => $deli->created_by,
                            'updated_by'       => $deli->updated_by ? $deli->updated_by:NULL,
                            'created_at'       => $deli->created_at ? date('Y-m-d',strtotime($deli->created_at)) : NULL,
                            'updated_at'       => $deli->updated_at ? date('Y-m-d',strtotime($deli->updated_at)) : NULL,
                        ]);

                        $delivery_products = $db_ext->table('delivery_challan_products')->where('delivery_challan_id',$deli->delivery_challan_id)->get();
                        foreach ($delivery_products as $delivery_product)
                        {
                            //calculations
                            $category= $preference->fetchCategory($delivery_product->category_id);
                            $product = $preference->fetchProduct($delivery_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $tax     = $preference->fetchTax($price->tax_id);
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
                            $amount = $preference->amountCalc($price->sales_rate_exc,$delivery_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $sub_total = $amount+$tax_amount;
                            $delivery_product = DeliveryProduct::create([
                                'delivery_id'  => $delivery->delivery_id,
                                'product_id'   => $product->product_id,
                                'product_type' => $delivery_product->product_type,
                                'product_code' => $delivery_product->product_code,
                                'hsn_code'     => $delivery_product->hsn_code,
                                'category_id'  => $category->category_id,
                                'product_name' => $delivery_product->product_name,
                                // 'description'  => $delivery_product->description,
                                'product_unit' => $delivery_product->product_unit,
                                'price_id'     => $price->price_id,
                                'purchase_rate_exc' => $price->purchase_rate_exc?$price->purchase_rate_exc:0,
                                'sales_rate_exc'    => $price->sales_rate_exc?$price->sales_rate_exc:0,
                                'purchase_rate_inc' => $price->purchase_rate_inc?$price->purchase_rate_inc:0,
                                'sales_rate_inc'    => $price->sales_rate_inc?$price->sales_rate_inc:0,
                                'store_id'          => $store->store_id,
                                'warehouse_id'      => $store->warehouse_id,
                                'quantity'          => $delivery_product->quantity,
                                'amount'            => $amount,//new fields
                                'discount'          => 0,//new fields
                                'discount_type'     => "%",//new fields
                                'discount_amount'   => 0,//new fields
                                'tax_id'            => $tax->tax_id,//new fields
                                'tax_amount'        => $tax_amount,//new fields
                                'sub_total'         => $sub_total,//new fields
                                'created_by'        => $delivery_product->created_by,
                                'updated_by'        => $delivery_product->updated_by ? $delivery_product->updated_by:NULL,
                                'created_at'        => $delivery_product->created_at ? date('Y-m-d',strtotime($delivery_product->created_at)) : NULL,
                                'updated_at'        => $delivery_product->updated_at ? date('Y-m-d',strtotime($delivery_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('amount');
                        $tax_amount = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('tax_amount');
                        $total_amount = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                        Delivery::where('delivery_id',$delivery->delivery_id)->update([
                            'sub_total'   => $sub_total,
                            'tax_amount'  => $tax_amount,
                            'total_amount'=> $total_amount,
                            'round_off'   => $round_off,
                            'grand_total' => $grand_total,
                        ]);
                    }
                break;
                case "delivery_returns"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('delivery_return_products')->truncate();
                    $db_int->table('delivery_returns')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('delivery_returns')->get();
                    foreach($table_data as $deli){
                        $contact = $preference->fetchContact($deli->contact_id);
                        $term = $preference->fetchTerm($deli->terms);
                        $src = $preference->fetchPlace($deli->source_place);
                        $dest = $preference->fetchPlace($deli->destination_place);
                        $delivery_return = DeliveryReturn::create([
                            'fiscal_year'         => $deli->delivery_return_year,
                            'delivery_return_no'  => $deli->delivery_return_no,
                            'delivery_return_date'=> date("Y-m-d", strtotime($deli->delivery_return_date)),
                            'reference_no'        => $deli->delivery_challan_no,
                            'reference_date'      => $deli->delivery_return_date ? date('Y-m-d',strtotime($deli->delivery_return_date)) : NULL,
                            'customer_id'      => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,
                            'shipping_address' => $contact->shipping_address,
                            'source_id'        => $src->place_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'        => 0,//new field
                            'discount_amount'  => 0,//new field
                            'tax_amount'       => 0,//new field
                            'total_amount'     => 0,//new field
                            'round_off'        => 0,//new field
                            'grand_total'      => 0,//new field
                            'term_id'          => $term?$term->term_id:NULL,//fk
                            'terms'            => $term?$term->term:NULL,//fk
                            'note'             => $deli->e_way_bill ? 'e-Sugam-'.$deli->e_way_bill.','.$deli->note : $deli->note,
                            'delivery_return_status'  => $deli->delivery_return_status,
                            'created_by'       => $deli->created_by,
                            'updated_by'       => $deli->updated_by ? $deli->updated_by:NULL,
                            'created_at'       => $deli->created_at ? date('Y-m-d',strtotime($deli->created_at)) : NULL,
                            'updated_at'       => $deli->updated_at ? date('Y-m-d',strtotime($deli->updated_at)) : NULL,
                        ]);

                        $delivery_return_products = $db_ext->table('delivery_return_products')->where('delivery_return_id',$deli->delivery_return_id)->get();
                        foreach ($delivery_return_products as $delivery_return_product)
                        {
                            //calculations
                            $category= $preference->fetchCategory($delivery_return_product->category_id);
                            $product = $preference->fetchProduct($delivery_return_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $tax     = $preference->fetchTax($price->tax_id);
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                        
                            
                            $amount = $preference->amountCalc($price->sales_rate_exc,$delivery_return_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $sub_total = $amount+$tax_amount;
                            DeliveryReturnProduct::create([
                                'delivery_return_id'=> $delivery_return->delivery_return_id,
                                'product_id'        => $product->product_id,
                                'product_type'      => $delivery_return_product->product_type,
                                'product_code'      => $delivery_return_product->product_code,
                                'hsn_code'          => $delivery_return_product->hsn_code,
                                'category_id'       => $category->category_id,
                                'product_name'      => $delivery_return_product->product_name,
                                // 'description' => $product->description,
                                'product_unit'       => $delivery_return_product->product_unit,
                                'price_id'          => $price->price_id,
                                'purchase_rate_exc' => $price->purchase_rate_exc?$price->purchase_rate_exc:0,
                                'sales_rate_exc'    => $price->sales_rate_exc?$price->sales_rate_exc:0,
                                'purchase_rate_inc' => $price->purchase_rate_inc?$price->purchase_rate_inc:0,
                                'sales_rate_inc'    => $price->sales_rate_inc?$price->sales_rate_inc:0,
                                'store_id'          => $store->store_id,
                                'warehouse_id'      => $store->warehouse_id,
                                'quantity'          => $delivery_return_product->quantity,
                                'amount'            => $amount,//new fields
                                'discount'          => 0,//new fields
                                'discount_type'     => "%",//new fields
                                'discount_amount'   => 0,//new fields
                                'tax_id'            => $tax->tax_id,//new fields
                                'tax_amount'        => $tax_amount,//new fields
                                'sub_total'         => $sub_total,//new fields
                                'created_by'        => $delivery_return_product->created_by,
                                'updated_by'        => $delivery_return_product->updated_by ? $delivery_return_product->updated_by:NULL,
                                'created_at'        => $delivery_return_product->created_at ? date('Y-m-d',strtotime($delivery_return_product->created_at)) : NULL,
                                'updated_at'        => $delivery_return_product->updated_at ? date('Y-m-d',strtotime($delivery_return_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('amount');
                        $tax_amount = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('tax_amount');
                        $total_amount = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                        DeliveryReturn::where('delivery_return_id',$delivery_return->delivery_return_id)->update([
                            'sub_total'   => $sub_total,
                            'tax_amount'  => $tax_amount,
                            'total_amount'=> $total_amount,
                            'round_off'   => $round_off,
                            'grand_total' => $grand_total,
                        ]);
                    }
                break;
                case "expenses"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('expenses')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('expenses')->get();
                    foreach($table_data as $expense){
                        $contact = $preference->fetchContact($expense->contact_id);
                        Expense::create([
                            'fiscal_year' => $preference->fiscal_year,
                            'voucher_no'  => $expense->voucher_no,
                            'voucher_date'=> date("Y-m-d", strtotime($expense->voucher_date)),
                            'account_id'  => $expense->expense_account,
                            //two fields from accounts table expense_account & paid_through
                            'contact_id'   => $contact->contact_id,
                            'payment_mode'=> $expense->payment_mode,
                            // 'reference_no'=> $expense->reference_no,
                            // 'reference_date'=> $expense->reference_date ? date('Y-m-d',strtotime($expense->reference_date)) : NULL,
                            'amount'     => $expense->amount,
                            // 'term_id'    => $term->term_id,//fk
                            // 'terms'      => $term->term,//fk
                            'note'       => $expense->note,
                            'created_by' => $expense->created_by,
                            'updated_by' => $expense->updated_by ? $expense->updated_by:NULL,
                            'created_at' => $expense->created_at ? date('Y-m-d',strtotime($expense->created_at)) : NULL,
                            'updated_at' => $expense->updated_at ? date('Y-m-d',strtotime($expense->updated_at)) : NULL,
                            'deleted_at' => $expense->deleted_at ? date('Y-m-d',strtotime($expense->deleted_at)) : NULL,
                        ]);
                    }
                    //journal//ft
                    $journal_table_data = $db_ext->table('journals')->where('payment_type','Debit')->get();
                    foreach($journal_table_data as $journal){
                        $contact = $preference->fetchContact($journal->contact_id);
                        $account = $preference->fetchAccount($journal->account_id);
                        Expense::create([
                            'fiscal_year' => $preference->fiscal_year,
                            'voucher_no'  => $journal->journal_no,
                            'voucher_date'=> date("Y-m-d", strtotime($journal->journal_date)),
                            'account_id'  => $account->account_id,
                            //two fields from accounts table journal_account & paid_through
                            'contact_id'   => $contact->contact_id,
                            'payment_mode'=> $journal->payment_type,
                            'reference_no'=> $journal->reference_no,
                            'reference_date'=> $journal->journal_date ? date('Y-m-d',strtotime($journal->journal_date)) : NULL,
                            'amount'     => $journal->amount,
                            // 'term_id'    => $term->term_id,//fk
                            // 'terms'      => $term->term,//fk
                            'note'       => $journal->note,
                            'created_by' => $journal->created_by,
                            'updated_by' => $journal->updated_by ? $journal->updated_by:NULL,
                            'created_at' => $journal->created_at ? date('Y-m-d',strtotime($journal->created_at)) : NULL,
                            'updated_at' => $journal->updated_at ? date('Y-m-d',strtotime($journal->updated_at)) : NULL,
                            'deleted_at' => $journal->deleted_at ? date('Y-m-d',strtotime($journal->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "incomes"://checked+
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('incomes')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('incomes')->get();
                    foreach($table_data as $income){
                        $contact = $preference->fetchContact($income->contact_id);
                        Income::create([
                            'fiscal_year'  => $preference->fiscal_year,
                            'receipt_no'   => $income->receipt_no,
                            'receipt_date' => date("Y-m-d", strtotime($income->receipt_date)),
                            'account_id'   => $income->income_account,
                            //two fields from accounts table income_account & received_from
                            'contact_id'   => $contact->contact_id,
                            'payment_mode' => $income->payment_mode,
                            // 'reference_no' => $income->reference_no,
                            // 'reference_date'=> $income->reference_date ? date('Y-m-d',strtotime($income->reference_date)) : NULL,
                            'amount'       => $income->amount,
                            // 'term_id'      =>$income->term_id,//fk
                            // 'terms'        =>$income->terms,//fk
                            'note'         => $income->note,
                            'created_by'   => $income->created_by,
                            'updated_by'   => $income->updated_by ? $income->updated_by:NULL,
                            'created_at'   => $income->created_at ? date('Y-m-d',strtotime($income->created_at)) : NULL,
                            'updated_at'   => $income->updated_at ? date('Y-m-d',strtotime($income->updated_at)) : NULL,
                            'deleted_at'   => $income->deleted_at ? date('Y-m-d',strtotime($income->deleted_at)) : NULL,
                        ]);
                    }
                     //journal//ft
                    $journal_table_data = $db_ext->table('journals')->where('payment_type','Credit')->get();
                    foreach($journal_table_data as $journal){
                        $contact = $preference->fetchContact($journal->contact_id);
                        $account = $preference->fetchAccount($journal->account_id);
                        Income::create([
                            'fiscal_year' => $preference->fiscal_year,
                            'receipt_no'  => $journal->journal_no,
                            'receipt_date'=> date("Y-m-d", strtotime($journal->journal_date)),
                            'account_id'  => $account->account_id,
                            //two fields from accounts table income_account & received_from
                            'contact_id'   => $contact->contact_id,
                            'payment_mode'=> $journal->payment_type,
                            'reference_no'=> $journal->reference_no,
                            'reference_date'=> $journal->journal_date ? date('Y-m-d',strtotime($journal->journal_date)) : NULL,
                            'amount'     => $journal->amount,
                            // 'term_id'    => $term->term_id,//fk
                            // 'terms'      => $term->term,//fk
                            'note'       => $journal->note,
                            'created_by' => $journal->created_by,
                            'updated_by' => $journal->updated_by ? $journal->updated_by:NULL,
                            'created_at' => $journal->created_at ? date('Y-m-d',strtotime($journal->created_at)) : NULL,
                            'updated_at' => $journal->updated_at ? date('Y-m-d',strtotime($journal->updated_at)) : NULL,
                            'deleted_at' => $journal->deleted_at ? date('Y-m-d',strtotime($journal->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "invoices"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('invoice_products')->truncate();
                    $db_int->table('invoices')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    //services//ft
                    $service_table_data = $db_ext->table('services')->get();
                    foreach($service_table_data as $ser){
                        $contact = $preference->fetchContact($ser->contact_id);
                        $term = $preference->fetchTerm($ser->terms);
                        $src = $preference->fetchPlace($ser->source_place);
                        $dest = $preference->fetchPlace($ser->destination_place);
                        $invoice = Invoice::create([
                            'fiscal_year'  => $ser->invoice_year,
                            'invoice_no'   => $ser->invoice_no,
                            'invoice_date' => date("Y-m-d", strtotime($ser->invoice_date)),
                            'due_date'     => date("Y-m-d", strtotime($ser->due_date)),
                            'invoice_type' => "Service",
                            'reference_no' => $ser->reference_no,
                            'reference_date'=> $ser->due_date ? date('Y-m-d',strtotime($ser->due_date)) : NULL,//no reference date in old db
                            'customer_id'   => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,//new db
                            'shipping_address' => $contact->shipping_address,//new db
                            'source_id'        => $src->place_id,
                            'payment_term_id'  => $ser->payment_term_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'        => 0,
                            'discount_amount'  => 0,
                            'tax_amount'       => 0,
                            'total_amount'     => 0,
                            'round_off'        => 0,
                            'grand_total'      => 0,
                            'term_id'          => $term?$term->term_id:NULL,//fk
                            'terms'            => $term?$term->term:NULL,//fk
                            'note'             => $ser->e_way_bill ? 'e-Sugam-'.$ser->e_way_bill.','.$ser->note : $ser->note,
                            'invoice_status'   => $ser->service_status,
                            'created_by'       => $ser->created_by,
                            'updated_by'       => $ser->updated_by ? $ser->updated_by:NULL,
                            'created_at'       => $ser->created_at ? date('Y-m-d',strtotime($ser->created_at)) : NULL,
                            'updated_at'       => $ser->updated_at ? date('Y-m-d',strtotime($ser->updated_at)) : NULL,
                        ]);
                        $service_products = $db_ext->table('service_products')->where('service_id',$ser->service_id)->get();
                        foreach ($service_products as $service_product) 
                        {
                            //calculations
                            $category = $preference->fetchCategory($service_product->category_id);
                            $tax = $preference->fetchTax($service_product->tax_id);
                            $product = $preference->fetchProduct($service_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
                            $amount = $preference->amountCalc($service_product->sales_rate_exc,$service_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$service_product->discount,$service_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;
                            InvoiceProduct::create([
                                'invoice_id'   => $invoice->invoice_id,
                                'product_id'   => $product->product_id,
                                'product_type' => $service_product->product_type,
                                'product_code' => $service_product->product_code,
                                'hsn_code'     => $service_product->hsn_code,
                                'category_id'  => $category->category_id,
                                'product_name' => $service_product->product_name,
                                // 'description' => $product->description,
                                'product_unit' => $service_product->product_unit,
                                'price_id'     => $price->price_id,
                                'purchase_rate_exc' => $service_product->purchase_rate_exc? $service_product->purchase_rate_exc:0,
                                'sales_rate_exc'    => $service_product->sales_rate_exc?$service_product->sales_rate_exc :0,
                                'purchase_rate_inc' => $service_product->purchase_rate_inc? $service_product->purchase_rate_inc:0,
                                'sales_rate_inc'    => $service_product->sales_rate_inc?$service_product->sales_rate_inc :0,
                                'store_id'          => $store->store_id,
                                'warehouse_id'      => $store->warehouse_id,
                                'quantity'          => $service_product->quantity,
                                'amount'            => $amount,
                                'discount'          => $service_product->discount,
                                'discount_type'     => $service_product->discount_type,
                                'discount_amount'   => $discount_amount,
                                'tax_id'            => $tax->tax_id,
                                'tax_amount'        => $tax_amount,
                                'sub_total'         => $sub_total,
                                'created_by'        => $service_product->created_by,
                                'updated_by'        => $service_product->updated_by ? $service_product->updated_by:NULL,
                                'created_at'        => $service_product->created_at ? date('Y-m-d',strtotime($service_product->created_at)) : NULL,
                                'updated_at'        => $service_product->updated_at ? date('Y-m-d',strtotime($service_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('amount');
                        $tax_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('tax_amount');
                        $discount_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('discount_amount');
                        $total_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                            Invoice::where('invoice_id',$invoice->invoice_id)->update([
                                'sub_total'   => $sub_total,
                                'tax_amount'  => $tax_amount,
                                'discount_amount'  => $discount_amount,
                                'total_amount'=> $total_amount,
                                'round_off'   => $round_off,
                                'grand_total' => $grand_total,
                            ]);
                    }
                    //invoices//ft
                    $invoice_table_data = $db_ext->table('invoices')->get();
                    foreach($invoice_table_data as $inv){
                        
                        $contact = $preference->fetchContact($inv->contact_id);
                        $term = $preference->fetchTerm($inv->terms);
                        $src = $preference->fetchPlace($inv->source_place);
                        $dest = $preference->fetchPlace($inv->destination_place);
                        $invoice = Invoice::create([
                            'fiscal_year'  => $inv->invoice_year,
                            'invoice_no'   => $inv->invoice_no,
                            'invoice_date' => date("Y-m-d", strtotime($inv->invoice_date)),
                            'due_date'     => date("Y-m-d", strtotime($inv->due_date)),
                            'invoice_type' => "Invoice",
                            'reference_no' => $inv->reference_no,
                            'reference_date'=> $inv->due_date ? date('Y-m-d',strtotime($inv->due_date)) : NULL,//no reference date in old db
                            'customer_id'   => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,//new db
                            'shipping_address' => $contact->shipping_address,//new db
                            'source_id'        => $src->place_id,
                            'payment_term_id'  => $inv->payment_term_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'        => 0,
                            'discount_amount'  => 0,
                            'tax_amount'       => 0,
                            'total_amount'     => 0,
                            'round_off'        => 0,
                            'grand_total'      => 0,
                            'term_id'          => $term?$term->term_id:NULL,//fk
                            'terms'            => $term?$term->term:NULL,//fk
                            'note'             => $inv->e_way_bill ? 'e-Sugam-'.$inv->e_way_bill.','.$inv->note : $inv->note,
                            'invoice_status'   => $inv->invoice_status,
                            'created_by'       => $inv->created_by,
                            'updated_by'       => $inv->updated_by ? $inv->updated_by:NULL,
                            'created_at'       => $inv->created_at ? date('Y-m-d',strtotime($inv->created_at)) : NULL,
                            'updated_at'       => $inv->updated_at ? date('Y-m-d',strtotime($inv->updated_at)) : NULL,
                        ]);

                        $invoice_products = $db_ext->table('invoice_products')->where('invoice_id',$inv->invoice_id)->get();
                        foreach ($invoice_products as $invoice_product) 
                        {
                            //calculations
                            $category = $preference->fetchCategory($invoice_product->category_id);
                            $product = $preference->fetchProduct($invoice_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            $tax = $preference->fetchTax($invoice_product->tax_id);
                            $amount = $preference->amountCalc($invoice_product->sales_rate_exc,$invoice_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$invoice_product->discount,$invoice_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;
                            InvoiceProduct::create([
                                'invoice_id'   => $invoice->invoice_id,
                                'product_id'   => $product->product_id,
                                'product_type' => $invoice_product->product_type,
                                'product_code' => $invoice_product->product_code,
                                'hsn_code'     => $invoice_product->hsn_code,
                                'category_id'  => $category->category_id,
                                'product_name' => $invoice_product->product_name,
                                // 'description' => $product->description,
                                'product_unit' => $invoice_product->product_unit,
                                'price_id'     => $price->price_id,
                                'purchase_rate_exc' => $invoice_product->purchase_rate_exc? $invoice_product->purchase_rate_exc:0,
                                'sales_rate_exc'    => $invoice_product->sales_rate_exc? $invoice_product->sales_rate_exc:0,
                                'purchase_rate_inc' => $invoice_product->purchase_rate_inc? $invoice_product->purchase_rate_inc:0,
                                'sales_rate_inc'    => $invoice_product->sales_rate_inc? $invoice_product->sales_rate_inc:0,
                                'store_id'          => $store->store_id,
                                'warehouse_id'      => $store->warehouse_id,
                                'quantity'          => $invoice_product->quantity,
                                'amount'            => $amount,
                                'discount'          => $invoice_product->discount,
                                'discount_type'     => $invoice_product->discount_type,
                                'discount_amount'   => $discount_amount,
                                'tax_id'            => $tax->tax_id,
                                'tax_amount'        => $tax_amount,
                                'sub_total'         => $sub_total,
                                'created_by'        => $invoice_product->created_by,
                                'updated_by'        => $invoice_product->updated_by ? $invoice_product->updated_by:NULL,
                                'created_at'        => $invoice_product->created_at ? date('Y-m-d',strtotime($invoice_product->created_at)) : NULL,
                                'updated_at'        => $invoice_product->updated_at ? date('Y-m-d',strtotime($invoice_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('amount');
                        $tax_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('tax_amount');
                        $discount_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('discount_amount');
                        $total_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                            Invoice::where('invoice_id',$invoice->invoice_id)->update([
                                'sub_total'   => $sub_total,
                                'tax_amount'  => $tax_amount,
                                'discount_amount'  => $discount_amount,
                                'total_amount'=> $total_amount,
                                'round_off'   => $round_off,
                                'grand_total' => $grand_total,
                            ]);
                    }
                break;
                case "invoice_returns"://no data
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('invoice_return_products')->truncate();
                    $db_int->table('invoice_returns')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('invoice_returns')->get();
                    foreach($table_data as $inv){
                        $contact = $preference->fetchContact($inv->contact_id);
                        $term = $preference->fetchTerm($inv->terms);
                        $src = $preference->fetchPlace($inv->source_place);
                        $dest = $preference->fetchPlace($inv->destination_place);

                        $invoice_return = InvoiceReturn::create([
                            'fiscal_year'        => $inv->invoice_ret_year,
                            'invoice_return_no'  => $inv->invoice_ret_no,
                            'invoice_return_date'=> date("Y-m-d", strtotime($inv->invoice_ret_date)),
                            'reference_no'       => $inv->dc_proforma_no,
                            'reference_date'     => $inv->due_date ? date('Y-m-d',strtotime($inv->due_date)) : NULL,
                            'customer_id'      => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,
                            'shipping_address' => $contact->shipping_address,
                            'source_id'        => $src->place_id,
                            'destination_id'   => $dest->place_id,
                            'sub_total'      => 0,
                            'discount_amount'=> 0,
                            'tax_amount'     => 0,
                            'total_amount'   => 0,
                            'round_off'      => 0,
                            'grand_total'    => 0,
                            'term_id'        => $term?$term->term_id:NULL,
                            'terms'          => $term?$term->term:NULL,
                            'note'           => $inv->note,
                            'invoice_return_status'=>$inv->invoice_return_status,
                            'created_by'        => $inv->created_by,
                            'updated_by'        => $inv->updated_by ? $inv->updated_by:NULL,
                            'created_at'        => $inv->created_at ? date('Y-m-d',strtotime($inv->created_at)) : NULL,
                            'updated_at'        => $inv->updated_at ? date('Y-m-d',strtotime($inv->updated_at)) : NULL,
                        ]);
                        $invoice_return_products = $db_ext->table('invoice_return_products')->where('invoice_return_id',$inv->invoice_return_id)->get();
                        foreach ($invoice_return_products as $invoice_return_product) 
                        {
                            //calculations//old table structure
                            $category = $preference->fetchCategory($invoice_return_product->category_id);
                            $tax = $preference->fetchTax($invoice_return_product->tax_id);
                            $product = $preference->fetchProduct($invoice_return_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
                            $amount = $preference->amountCalc($invoice_return_product->sales_rate,$invoice_return_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$invoice_return_product->discount,$invoice_return_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;

                            InvoiceReturnProduct::create([
                                'invoice_return_id' => $invoice_return->invoice_return_id,
                                'product_id'        => $product->product_id,
                                'product_type'      => $invoice_return_product->product_type,
                                'product_code'      => $invoice_return_product->product_code,
                                'hsn_code'          => $invoice_return_product->hsn_code,
                                'category_id'       => $category->category_id,
                                'product_name'      => $invoice_return_product->product_name,
                                // 'description' => $product->description,
                                'product_unit'      => $invoice_return_product->product_unit,
                                'price_id'     => $price->price_id,
                                'purchase_rate_exc' => $invoice_return_product->purchase_rate? $invoice_return_product->purchase_rate:0,
                                'sales_rate_exc'    => $invoice_return_product->sales_rate?$invoice_return_product->sales_rate :0,
                                'purchase_rate_inc' => $invoice_return_product->purchase_rate? $invoice_return_product->purchase_rate:0,
                                'sales_rate_inc'    => $invoice_return_product->mrp?$invoice_return_product->mrp :0,
                                'store_id'          => $store->store_id,
                                'warehouse_id'      => $store->warehouse_id,
                                'quantity'          => $invoice_return_product->quantity,
                                'amount'            => $amount,
                                'discount'          => $invoice_return_product->discount,
                                'discount_type'     => $invoice_return_product->discount_type,
                                'discount_amount'   => $discount_amount,
                                'tax_id'            => $tax->tax_id,
                                'tax_amount'        => $tax_amount,
                                'sub_total'         => $sub_total,
                                'created_by'        => $invoice_return_product->created_by,
                                'updated_by'        => $invoice_return_product->updated_by ? $invoice_return_product->updated_by:NULL,
                                'created_at'        => $invoice_return_product->created_at ? date('Y-m-d',strtotime($invoice_return_product->created_at)) : NULL,
                                'updated_at'        => $invoice_return_product->updated_at ? date('Y-m-d',strtotime($invoice_return_product->updated_at)) : NULL,
                            ]);
                        //calculations
                        $sub_total = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('amount');
                        $tax_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('tax_amount');
                        $discount_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('discount_amount');
                        $total_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                            InvoiceReturn::where('invoice_return_id',$invoice_return->invoice_return_id)->update([
                                'sub_total'   => $sub_total,
                                'tax_amount'  => $tax_amount,
                                'discount_amount'  => $discount_amount,
                                'total_amount'=> $total_amount,
                                'round_off'   => $round_off,
                                'grand_total' => $grand_total,
                            ]);
                        }
                    }
                break;
                case "masters"://not tested
                    dd($request);
                    // $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    // $db_int->table('masters')->truncate();
                    // $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('masters')->get();
                    foreach($table_data as $master){
                        Master::create([
                            'master_name'  => $master->master_name,
                            'master_value' => $master->master_value,
                            'created_by'   => $master->created_by,
                            'updated_by'   => $master->updated_by ? $master->updated_by:NULL,
                            'created_at'   => $master->created_at ? date('Y-m-d',strtotime($master->created_at)) : NULL,
                            'updated_at'   => $master->updated_at ? date('Y-m-d',strtotime($master->updated_at)) : NULL,
                            'deleted_at'   => $master->deleted_at ? date('Y-m-d',strtotime($master->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "organizations"://not tested
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('organizations')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('organizations')->get();
                    foreach($table_data as $organization){
                        Organization::create([
                            'org_code'  => $organization->org_code,
                            'org_name'  => $organization->org_name,
                            'email'     => $organization->email,
                            'mobile_no' => $organization->mobile_no,
                            'phone_no'  => $organization->phone_no,
                            'address'   => $organization->address1.$organization->address2,
                            'pan_no'    => $organization->pan_no,
                            'gstin_no'  => $organization->gstin_no,
                            'place_id'  => 1,//$organization->place_id//fk
                            'logo'      => $organization->logo,
                            'header'    => $organization->header,
                            'footer'    => $organization->footer,
                            'created_by'=> $organization->created_by,
                            'updated_by'=> $organization->updated_by ? $organization->updated_by:NULL,
                            'created_at'=> $organization->created_at ? date('Y-m-d',strtotime($organization->created_at)) : NULL,
                            'updated_at'=> $organization->updated_at ? date('Y-m-d',strtotime($organization->updated_at)) : NULL,
                        ]);
                    }
                break;
                case "payments"://no data
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('payment_particulars')->truncate();
                    $db_int->table('payments')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('payments')->get();
                    foreach($table_data as $pay){
                        $contact = $preference->fetchContact($pay->contact_id);
                        $payment = Payment::create([
                            'fiscal_year'     => $preference->fiscal_year,
                            'vendor_id'       => $contact->contact_id,
                            'billing_address' => $contact->billing_address,//new db
                            'shipping_address'=> $contact->shipping_address,//new db
                            'voucher_no'      => $pay->voucher_no,
                            'voucher_date'    => date("Y-m-d", strtotime($pay->voucher_date)),
                            'account_id'      => $pay->paid_through,//fk
                            'payment_mode'    => $pay->payment_mode,
                            'reference_no'    => $pay->reference_no,
                            'reference_date'  => $pay->voucher_date ? date('Y-m-d',strtotime($pay->voucher_date)) : NULL,//no data for reference date
                            'total_amount'    => 0,
                            // 'term_id'         => $pay->term_id,//fk
                            // 'terms'           => $pay->terms,//fk
                            'note'            => $pay->note,
                            'created_by'      => $pay->created_by,
                            'updated_by'      => $pay->updated_by ? $pay->updated_by:NULL,
                            'created_at'      => $pay->created_at ? date('Y-m-d',strtotime($pay->created_at)) : NULL,
                            'updated_at'      => $pay->updated_at ? date('Y-m-d',strtotime($pay->updated_at)) : NULL,
                        ]);
                         //ft
                        $bills = $db_ext->table('payment_bills')->where('payment_id',$pay->payment_id)->get();
                        if(count($bills)>0){
                            foreach ($bills as $bill) 
                            {
                                $curr_bill = $preference->fetchBill($bill->bill_id);
                                PaymentParticular::create([
                                    'payment_id'   => $payment->payment_id,
                                    'reference'    => 'App\Bill',
                                    'reference_id' => $curr_bill->bill_id,
                                    //need to be modified if bill table is present
                                    'paid_amount'  => $bill->paid_amount,
                                    //need to be modified if bill table is present
                                    'created_by'   => $bill->created_by,
                                    'updated_by'   => $bill->updated_by ? $bill->updated_by:NULL,
                                    'created_at'   => $bill->created_at ? date('Y-m-d',strtotime($bill->created_at)) : NULL,
                                    'updated_at'   => $bill->updated_at ? date('Y-m-d',strtotime($bill->updated_at)) : NULL,
                                ]);
                            }    
                        }
                         //ft
                        $purchases = $db_ext->table('payment_purchases')->where('payment_id',$pay->payment_id)->get();
                        if(count($purchases)>0){
                            foreach ($purchases as $purchase) 
                            {
                                $curr_purchase = $preference->fetchPurchase($purchase->purchase_id);
                                PaymentParticular::create([
                                    'payment_id'   => $payment->payment_id,
                                    'reference'    => 'App\Purchase',
                                    'reference_id' => $curr_purchase->bill_id,
                                    //need to be modified if purchase table is present
                                    'paid_amount'  => $purchase->payment,
                                    //need to be modified if purchase table is present
                                    'created_by'   => $purchase->created_by,
                                    'updated_by'   => $purchase->updated_by ? $purchase->updated_by:NULL,
                                    'created_at'   => $purchase->created_at ? date('Y-m-d',strtotime($purchase->created_at)) : NULL,
                                    'updated_at'   => $purchase->updated_at ? date('Y-m-d',strtotime($purchase->updated_at)) : NULL,
                                ]);
                            }    
                        }
                        //calculations
                        $total_amount=PaymentParticular::where('payment_id',$payment->payment_id)->sum('paid_amount');
                        Payment::where('payment_id',$payment->payment_id)->update([
                            'total_amount' => $total_amount,
                        ]);
                    }
                break;
                case "payment_terms"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('payment_terms')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    $table_data = $db_ext->table('payment_terms')->get();
                    foreach($table_data as $payment_term){
                        PaymentTerm::create([
                            'payment_term' => $payment_term->payment_term,
                            'no_of_days'   => $payment_term->no_of_days,
                            'created_by'   => $payment_term->created_by,
                            'updated_by'   => $payment_term->updated_by ? $payment_term->updated_by:NULL,
                            'created_at'   => $payment_term->created_at ? date('Y-m-d',strtotime($payment_term->created_at)) : NULL,
                            'updated_at'   => $payment_term->updated_at ? date('Y-m-d',strtotime($payment_term->updated_at)) : NULL,
                            'deleted_at' => $payment_term->deleted_at ? date('Y-m-d',strtotime($payment_term->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "places"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('places')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    $table_data = $db_ext->table('places')->get();
                    foreach($table_data as $place){
                        Place::create([
                            'place_code' => $place->place_code,
                            'place_name' => $place->place_name,
                            'created_by' => $place->created_by,
                            'updated_by' => $place->updated_by ? $place->updated_by:NULL,
                            'created_at' => $place->created_at ? date('Y-m-d',strtotime($place->created_at)) : NULL,
                            'updated_at' => $place->updated_at ? date('Y-m-d',strtotime($place->updated_at)) : NULL,
                            'deleted_at' => $place->deleted_at ? date('Y-m-d',strtotime($place->deleted_at)) : NULL,
                        ]);
                    }
                break;
                case "preferences"://not tested or modified
                    dd($request);
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('preferences')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    $table_data = $db_ext->table('preferences')->get();
                    foreach($table_data as $pre){
                        // Preference::create([
                        //     'purchase_order_prefix' => $pre->purchase_order_prefix,
                        //     'purchase_order_no_length' => $pre->purchase_order_no_length,
                        //     'purchase_order_pdf' => $pre->purchase_order_pdf,
                        //     'quotation_prefix' => $pre->quotation_prefix,
                        //     'quotation_no_length' => $pre->quotation_no_length,
                        //     'quotation_pdf' => $pre->quotation_pdf,
                        //     'proforma_prefix' => $pre->proforma_prefix,
                        //     'proforma_no_length' => $pre->proforma_no_length,
                        //     'proforma_pdf' => $pre->proforma_pdf,
                        //     'invoice_prefix' => $pre->invoice_prefix,
                        //     'invoice_no_length' => $pre->invoice_no_length,
                        //     'invoice_pdf' => $pre->invoice_pdf,
                        //     'delivery_pdf' => $pre->delivery_pdf,
                        //     'delivery_no_length' => $pre->delivery_no_length,
                        //     'delivery_pdf' => $pre->delivery_pdf,
                        //     'voucher_prefix' => $pre->voucher_prefix,
                        //     'voucher_no_length' => $pre->voucher_no_length,
                        //     'voucher_pdf' => $pre->voucher_pdf,
                        //     'receipt_prefix' => $pre->receipt_prefix,
                        //     'receipt_no_length' => $pre->receipt_no_length,
                        //     'receipt_pdf' => $pre->receipt_pdf,
                        //     'bill_return_prefix' => $pre->bill_return_prefix,
                        //     'bill_return_no_length' => $pre->bill_return_no_length,
                        //     'bill_return_pdf' => $pre->bill_return_pdf,
                        //     'invoice_return_prefix' => $pre->invoice_return_prefix,
                        //     'invoice_return_no_length' => $pre->invoice_return_no_length,
                        //     'invoice_return_pdf' => $pre->invoice_return_pdf,
                        //     'delivery_return_prefix' => $pre->delivery_return_prefix,
                        //     'delivery_return_no_length' => $pre->delivery_return_no_length,
                        //     'delivery_return_pdf' => $pre->delivery_return_pdf,
                        //     'fiscal_year' => $pre->fiscal_year,
                        //     'discount_on' => $pre->discount_on,
                        //     'inventory_on_calculation' => $pre->inventory_on_calculation,
                        //     'updated_by' => Auth::User()->username,         
                        // ]);
                    }
                break;
                case "products"://
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('products')->truncate();
                    $db_int->table('prices')->truncate();
                    $db_int->table('stores')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
                    $table_data = $db_ext->table('products')->get();
                    foreach($table_data as $prod){
                        $category = $preference->fetchCategory($prod->category_id);
                        $product = Product::create([
                            'product_type' => $prod->product_type,
                            'product_code' => $prod->product_code,
                            'hsn_code'     => $prod->hsn_code,
                            'category_id'  => $category->category_id,
                            'product_name' => $prod->product_name,
                            'product_unit' => $prod->product_unit,
                            // 'product_size' => $prod->product_size,
                            'min_stock'    => $prod->min_stock?$prod->min_stock:0,
                            // 'vendor_id'    => $prod->contact_id,//fk
                            'created_by'   => $prod->created_by,
                            'updated_by'   => $prod->updated_by ? $prod->updated_by:NULL,
                            'created_at'   => $prod->created_at ? date('Y-m-d',strtotime($prod->created_at)) : NULL,
                            'updated_at'   => $prod->updated_at ? date('Y-m-d',strtotime($prod->updated_at)) : NULL,
                            'deleted_at'   => $prod->deleted_at ? date('Y-m-d',strtotime($prod->deleted_at)) : NULL,
                        ]);
                        
                    //     $tax = $preference->fetchTax($prod->tax_id);
                    //     $price = Price::create([
                    //         'product_id'        => $product->product_id,
                    //         'purchase_rate_exc' => $prod->purchase_rate_exc?$prod->purchase_rate_exc:0,
                    //         'sales_rate_exc'    => $prod->sales_rate_exc?$prod->sales_rate_exc:0,
                    //         'tax_id'            => $tax?$tax->tax_id:1,
                    //         'purchase_rate_inc' => $prod->purchase_rate_inc?$prod->purchase_rate_inc:0,
                    //         'sales_rate_inc'    => $prod->purchase_rate_inc?$prod->purchase_rate_inc:0,
                    //         'created_by'        => $prod->created_by,
                    //         'updated_by'        => $prod->updated_by ? $prod->updated_by:NULL,
                    //         'created_at'        => $prod->created_at ? date('Y-m-d',strtotime($prod->created_at)) : NULL,
                    //         'updated_at'        => $prod->updated_at ? date('Y-m-d',strtotime($prod->updated_at)) : NULL,
                    //         'deleted_at'        => $prod->deleted_at ? date('Y-m-d',strtotime($prod->deleted_at)) : NULL,
                    //     ]);
                    // $batches = $db_ext->table('batches')->where('product_id',$prod->product_id)->get();
                    //     foreach ($batches as $batch) 
                    //     {
                    //     $ware = $db_ext->table('warehouses')->where('warehouse_id',$batch->warehouse_id)->first();//fk
                    //     $warehouse = Warehouse::where('warehouse_name',$ware->warehouse_name)->first();
                    //         Store::create([
                    //             'product_id'         => $product->product_id,
                    //             'price_id'           => $price->price_id,
                    //             'warehouse_id'       => $warehouse->warehouse_id,
                    //             'opening_stock'      => $batch->opening_stock,
                    //             'opening_stock_rate' => $batch->opening_stock_rate,
                    //             'created_by'         => $batch->created_by,
                    //             'updated_by'         => $batch->updated_by ? $batch->updated_by:NULL,
                    //             'created_at'         => $batch->created_at ? date('Y-m-d',strtotime($batch->created_at)) : NULL,
                    //             'updated_at'         => $batch->updated_at ? date('Y-m-d',strtotime($batch->updated_at)) : NULL,
                    //             'deleted_at'         => $batch->deleted_at ? date('Y-m-d',strtotime($batch->deleted_at)) : NULL,
                    //         ]);    
                    //     }   
                    }  
                break;
                case "proformas"://no data
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                    $db_int->table('proforma_products')->truncate();
                    $db_int->table('proformas')->truncate();
                    $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                    $table_data = $db_ext->table('proformas')->get();
                    foreach($table_data as $prof){
                        $contact = $preference->fetchContact($prof->contact_id);
                        $src = $preference->fetchPlace($prof->source_place);
                        $dest = $preference->fetchPlace($prof->destination_place);
                        $proforma = Proforma::create([
                            'fiscal_year'   => $prof->proforma_year,
                            'proforma_no'   => $prof->proforma_no,
                            'proforma_date' => date("Y-m-d", strtotime($prof->proforma_date)),
                            'reference_no'  => $prof->reference_no,
                            'reference_date'=> $prof->proforma_date ? date('Y-m-d',strtotime($prof->proforma_date)) : NULL,//no data for reference date
                            'customer_id'      => $contact->contact_id,
                            'billing_address'  => $contact->billing_address,
                            'shipping_address' => $contact->shipping_address,
                            'source_id'      => $src->place_id,
                            'destination_id' => $dest->place_id,
                            'sub_total'      => 0,
                            'discount_amount'=> 0,
                            'tax_amount'     => 0,
                            'total_amount'   => 0,
                            'round_off'      => 0,
                            'grand_total'    => 0,
                            // 'term_id'        => $term->term_id,//fk
                            // 'terms'          => $term->term,//fk
                            'note'           => $prof->note,
                            'proforma_status'=> $prof->proforma_status,
                            'created_by'     => $prof->created_by,
                            'updated_by'     => $prof->updated_by ? $prof->updated_by:NULL,
                            'created_at'     => $prof->created_at ? date('Y-m-d',strtotime($prof->created_at)) : NULL,
                            'updated_at'     => $prof->updated_at ? date('Y-m-d',strtotime($prof->updated_at)) : NULL,
                        ]);

                        $proforma_products = $db_ext->table('proforma_products')->where('proforma_id',$prof->proforma_id)->get();
                        foreach ($proforma_products as $proforma_product) 
                        {
                            //calculations
                            $category = $preference->fetchCategory($proforma_product->category_id);
                            $tax = $preference->fetchTax($proforma_product->tax_id);
                            $product = $preference->fetchProduct($proforma_product->product_id);
                            $price   = Price::where('product_id',$product->product_id)->first();
                            $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
                            $amount = $preference->amountCalc($proforma_product->sales_rate_exc,$proforma_product->quantity);
                            $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                            $discount_amount = $preference->discountAmountCalc($amount,$proforma_product->discount,$proforma_product->discount_type);
                            $sub_total = $amount+$tax_amount-$discount_amount;
                            ProformaProduct::create([
                                'proforma_id'   => $proforma->proforma_id,
                                'product_id'    => $product->product_id,
                                'product_type'  => $proforma_product->product_type,
                                'product_code'  => $proforma_product->product_code,
                                'hsn_code'      => $proforma_product->hsn_code,
                                'category_id'   => $category->category_id,
                                'product_name'  => $proforma_product->product_name,
                                // 'description'   => $product->description,
                                'product_unit'  => $proforma_product->product_unit,
                                'price_id'      => $price->price_id,
                                'purchase_rate_exc' => $proforma_product->purchase_rate_exc?$proforma_product->purchase_rate_exc:0,
                                'sales_rate_exc'    => $proforma_product->sales_rate_exc?$proforma_product->sales_rate_exc:0,
                                'purchase_rate_inc' => $proforma_product->purchase_rate_inc?$proforma_product->purchase_rate_inc:0,
                                'sales_rate_inc'    => $proforma_product->sales_rate_inc?$proforma_product->sales_rate_inc:0,
                                'store_id'     => $store->store_id,
                                'warehouse_id' => $store->warehouse_id,
                                'quantity'     => $proforma_product->quantity,
                                'amount'       => $amount,
                                'discount'        => $proforma_product->discount,
                                'discount_type'   => $proforma_product->discount_type,
                                'discount_amount' => $discount_amount,
                                'tax_id'       => $tax->tax_id,
                                'tax_amount'   => $tax_amount,
                                'sub_total'    => $sub_total,
                                'created_by'   => $proforma_product->created_by,
                                'updated_by'   => $proforma_product->updated_by ? $proforma_product->updated_by:NULL,
                                'created_at'   => $proforma_product->created_at ? date('Y-m-d',strtotime($proforma_product->created_at)) : NULL,
                                'updated_at'   => $proforma_product->updated_at ? date('Y-m-d',strtotime($proforma_product->updated_at)) : NULL,
                            ]);
                        }
                        //calculations
                        $sub_total = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('amount');
                        $tax_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('tax_amount');
                        $discount_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('discount_amount');
                        $total_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('sub_total');
                        $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                        $round_off = $grand_total-$total_amount;
                        Proforma::where('proforma_id',$proforma->proforma_id)->update([
                            'sub_total'   => $sub_total,
                            'tax_amount'  => $tax_amount,
                            'discount_amount'  => $discount_amount,
                            'total_amount'=> $total_amount,
                            'round_off'   => $round_off,
                            'grand_total' => $grand_total,
                        ]);
                    }
                break;
            case "purchase_orders"://no data
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('purchase_order_products')->truncate();
                $db_int->table('purchase_orders')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

                $table_data = $db_ext->table('purchase_orders')->get();
                foreach($table_data as $purc){
                    $contact = $preference->fetchContact($purc->contact_id);
                    $src = $preference->fetchPlace($purc->source_place);
                    $dest = $preference->fetchPlace($purc->destination_place);
                    $purchase_order = PurchaseOrder::create([
                        'fiscal_year'        => $purc->purchase_order_year,
                        'purchase_order_no'  => $purc->purchase_order_no,
                        'purchase_order_date'=> date("Y-m-d", strtotime($purc->purchase_order_date)),
                        'reference_no'       => $purc->reference_no,
                        'delivery_date'      => $purc->delivery_date ? date('Y-m-d',strtotime($purc->delivery_date)) : NULL,
                        'vendor_id'          => $contact->contact_id,
                        'billing_address'    => $contact->billing_address,//new db
                        'shipping_address'   => $contact->shipping_address,//new db
                        'source_id'          => $src->place_id,
                        'destination_id'     => $dest->place_id,
                        'sub_total'          => 0,
                        'discount_amount'    => 0,
                        'tax_amount'         => 0,
                        'total_amount'       => 0,
                        'round_off'          => 0,
                        'grand_total'        => 0,
                        // 'term_id'            =>$purc->term_id,//fk
                        // 'terms'              =>$purc->terms,//fk
                        'note'               => $purc->note,
                        'purchase_order_status'=> $purc->purchase_order_status,
                        'created_by'         => $purc->created_by,
                        'updated_by'         => $purc->updated_by ? $purc->updated_by:NULL,
                        'created_at'         => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
                        'updated_at'         => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
                    ]);

                    $purchase_order_products = $db_ext->table('purchase_order_products')->where('purchase_order_id',$purc->purchase_order_id)->get();
                    foreach ($purchase_order_products as $purchase_order_product) 
                    {
                        //calculations
                        $category = $preference->fetchCategory($purchase_order_product->category_id);
                        $tax = $preference->fetchTax($purchase_order_product->tax_id);
                        $product = $preference->fetchProduct($purchase_order_product->product_id);
                        $price   = Price::where('product_id',$product->product_id)->first();
                        $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                        
                        $amount = $preference->amountCalc($purchase_order_product->sales_rate_exc,$purchase_order_product->quantity);
                        $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                        $sub_total = $amount+$tax_amount;
                        PurchaseOrderProduct::create([
                            'purchase_order_id' => $purchase_order->purchase_order_id,
                            'product_id'        => $product->product_id,
                            'product_type'      => $purchase_order_product->product_type,
                            'product_code'      => $purchase_order_product->product_code,
                            'hsn_code'          => $purchase_order_product->hsn_code,
                            'category_id'       => $category->category_id,
                            'product_name'      => $purchase_order_product->product_name,
                            // 'description'       => $purchase_order_product->description,
                            'product_unit'      => $purchase_order_product->product_unit,
                            'price_id'          => $price->price_id,
                            'purchase_rate_exc' => $purchase_order_product->purchase_rate_exc?$purchase_order_product->purchase_rate_exc:0,
                            'sales_rate_exc'    => $purchase_order_product->sales_rate_exc?$purchase_order_product->sales_rate_exc:0,
                            'purchase_rate_inc' => $purchase_order_product->purchase_rate_inc?$purchase_order_product->purchase_rate_inc:0,
                            'sales_rate_inc'    => $purchase_order_product->sales_rate_inc?$purchase_order_product->sales_rate_inc:0,
                            'store_id'          => $store->store_id,
                            'warehouse_id'      => $store->warehouse_id,
                            'quantity'          => $purchase_order_product->quantity,
                            'amount'            => $amount,
                            'discount'          => 0,
                            'discount_type'     => "%",
                            'discount_amount'   => 0,
                            'tax_id'            => $tax->tax_id,
                            'tax_amount'        => $tax_amount,
                            'sub_total'         => $sub_total,
                            'created_by'        => $purchase_order_product->created_by,
                            'updated_by'        => $purchase_order_product->updated_by ? $purchase_order_product->updated_by:NULL,
                            'created_at'        => $purchase_order_product->created_at ? date('Y-m-d',strtotime($purchase_order_product->created_at)) : NULL,
                            'updated_at'        => $purchase_order_product->updated_at ? date('Y-m-d',strtotime($purchase_order_product->updated_at)) : NULL,
                        ]);
                    }
                    //calculations
                    $sub_total = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('amount');
                    $tax_amount = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('tax_amount');
                    $total_amount = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('sub_total');
                    $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                    $round_off = $grand_total-$total_amount;
                    PurchaseOrder::where('purchase_order_id',$purchase_order->purchase_order_id)->update([
                        'sub_total'   => $sub_total,
                        'tax_amount'  => $tax_amount,
                        'total_amount'=> $total_amount,
                        'round_off'   => $round_off,
                        'grand_total' => $grand_total,
                    ]);
                }
            break;
            case "quotations"://
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('quotation_products')->truncate();
                $db_int->table('quotations')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('quotations')->get();
                foreach($table_data as $quot){
                    $contact = $preference->fetchContact($quot->contact_id);
                    $src = $preference->fetchPlace($quot->source_place);
                    $dest = $preference->fetchPlace($quot->destination_place);

                    $quotation = Quotation::create([
                        'fiscal_year'   => $quot->quotation_year,
                        'quotation_no'  => $quot->quotation_no,
                        'quotation_date'=> date("Y-m-d", strtotime($quot->quotation_date)),
                        'reference_no'  => $quot->reference_no,
                        'expiry_date'   => $quot->expiry_date ? date('Y-m-d',strtotime($quot->expiry_date)) : NULL,
                        'customer_id'   => $contact->contact_id,
                        'billing_address'  => $contact->billing_address,
                        'shipping_address' => $contact->shipping_address,
                        'source_id'      => $src->place_id,
                        'destination_id' => $dest->place_id,
                        'sub_total'      => 0,
                        'discount_amount'=> 0,
                        'tax_amount'     => 0,
                        'total_amount'   => 0,
                        'round_off'      => 0,
                        'grand_total'    => 0,
                        // 'term_id'=>$quot->term_id,//fk
                        // 'terms'=>$quot->terms,//fk
                        'note'            => $quot->note,
                        'quotation_status'=> $quot->quotation_status,
                        'created_by'      => $quot->created_by,
                        'updated_by'      => $quot->updated_by ? $quot->updated_by:NULL,
                        'created_at'      => $quot->created_at ? date('Y-m-d',strtotime($quot->created_at)) : NULL,
                        'updated_at'      => $quot->updated_at ? date('Y-m-d',strtotime($quot->updated_at)) : NULL,
                    ]);

                    $quotation_products = $db_ext->table('quotation_products')->where('quotation_id',$quot->quotation_id)->get();
                    foreach ($quotation_products as $quotation_product) 
                    {
                        //calculations
                        $category = $preference->fetchCategory($quotation_product->category_id);
                        $tax = $preference->fetchTax($quotation_product->tax_id);
                        $product = $preference->fetchProduct($quotation_product->product_id);
                        $price   = Price::where('product_id',$product->product_id)->first();
                        $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                        
                        $amount = $preference->amountCalc($quotation_product->sales_rate_exc,$quotation_product->quantity);
                        $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
                        $discount_amount = $preference->discountAmountCalc($amount,$quotation_product->discount,$quotation_product->discount_type);
                        $sub_total = $amount+$tax_amount-$discount_amount;
                        QuotationProduct::create([
                            'quotation_id' => $quotation->quotation_id,
                            'product_id'   => $product->product_id,
                            'product_type' => $quotation_product->product_type,
                            'product_code' => $quotation_product->product_code,
                            'hsn_code'     => $quotation_product->hsn_code,
                            'category_id'  => $category->category_id,
                            'product_name' => $quotation_product->product_name,
                            // 'description'  => $product->description,
                            'product_unit' => $quotation_product->product_unit,
                            'price_id'     => $price->price_id,
                            'purchase_rate_exc' => $quotation_product->purchase_rate_exc?$quotation_product->purchase_rate_exc:0,
                            'sales_rate_exc'    => $quotation_product->sales_rate_exc?$quotation_product->sales_rate_exc:0,
                            'purchase_rate_inc' => $quotation_product->purchase_rate_inc?$quotation_product->purchase_rate_inc:0,
                            'sales_rate_inc'    => $quotation_product->sales_rate_inc?$quotation_product->sales_rate_inc:0,
                            'store_id'        => $store->store_id,
                            'warehouse_id'    => $store->warehouse_id,
                            'quantity'        => $quotation_product->quantity,
                            'amount'          => $amount,
                            'discount'        => $quotation_product->discount,
                            'discount_type'   => $quotation_product->discount_type,
                            'discount_amount' => $discount_amount,
                            'tax_id'          => $tax->tax_id,
                            'tax_amount'      => $tax_amount,
                            'sub_total'       => $sub_total,
                            'created_by'      => $quotation_product->created_by,
                            'updated_by'      => $quotation_product->updated_by ? $quotation_product->updated_by:NULL,
                            'created_at'      => $quotation_product->created_at ? date('Y-m-d',strtotime($quotation_product->created_at)) : NULL,
                            'updated_at'      => $quotation_product->updated_at ? date('Y-m-d',strtotime($quotation_product->updated_at)) : NULL,
                        ]);
                    }
                    //calculations
                    $sub_total = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('amount');
                    $tax_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('tax_amount');
                    $discount_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('discount_amount');
                    $total_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('sub_total');
                    $grand_total = number_format(round((float)$total_amount,0),2,'.','');
                    $round_off = $grand_total-$total_amount;
                    Quotation::where('quotation_id',$quotation->quotation_id)->update([
                        'sub_total'   => $sub_total,
                        'tax_amount'  => $tax_amount,
                        'discount_amount'  => $discount_amount,
                        'total_amount'=> $total_amount,
                        'round_off'   => $round_off,
                        'grand_total' => $grand_total,
                    ]);
                }
            break;
            case "receipts"://
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('receipt_particulars')->truncate();
                $db_int->table('receipts')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('receipts')->get();
                foreach($table_data as $rec){
                    $contact = $preference->fetchContact($rec->contact_id);
                    
                    $receipt = Receipt::create([
                        'fiscal_year'      => $preference->fiscal_year,
                        'customer_id'      => $contact->contact_id,
                        'billing_address'  => $contact->billing_address,
                        'shipping_address' => $contact->shipping_address,
                        'receipt_no'       => $rec->receipt_no,
                        'receipt_date'     => date("Y-m-d", strtotime($rec->receipt_date)),
                        'account_id'       => $rec->deposit_to,//fk
                        'payment_mode'     => $rec->payment_mode,
                        'reference_no'     => $rec->reference_no,
                        'reference_date'   => $rec->receipt_date ? date('Y-m-d',strtotime($rec->receipt_date)) : NULL,
                        'total_amount'     => 0,
                        // 'term_id'=>$rec->term_id,//fk
                        // 'terms'=>$rec->terms,//fk
                        'note'=> $rec->note,
                        'created_by'      => $rec->created_by,
                        'updated_by'      => $rec->updated_by ? $rec->updated_by:NULL,
                        'created_at'      => $rec->created_at ? date('Y-m-d',strtotime($rec->created_at)) : NULL,
                        'updated_at'      => $rec->updated_at ? date('Y-m-d',strtotime($rec->updated_at)) : NULL,
                    ]);

                    $receipt_particulars = $db_ext->table('receipt_invoices')->where('receipt_id',$rec->receipt_id)->get();
                    foreach ($receipt_particulars as $receipt_particular) 
                    {
                        $inv = $preference->fetchInvoice($receipt_particular->invoice_id);
                        ReceiptParticular::create([
                            'receipt_id'   => $receipt->receipt_id,
                            'reference'    => 'App\Invoice',
                            'reference_id' => $inv->invoice_id,
                            'paid_amount'  => $receipt_particular->payment,
                            'created_by'   => $receipt_particular->created_by,
                            'updated_by'   => $receipt_particular->updated_by ? $receipt_particular->updated_by:NULL,
                            'created_at'   => $receipt_particular->created_at ? date('Y-m-d',strtotime($receipt_particular->created_at)) : NULL,
                            'updated_at'   => $receipt_particular->updated_at ? date('Y-m-d',strtotime($receipt_particular->updated_at)) : NULL,
                        ]);
                    }
                    //calculations
                    $total_amount=ReceiptParticular::where('receipt_id',$receipt->receipt_id)->sum('paid_amount');
                    Receipt::where('receipt_id',$receipt->receipt_id)->update([
                        'total_amount' => $total_amount,
                    ]);
                }
            break;
            case "taxes"://
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('taxes')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('taxes')->get();
                foreach($table_data as $tax){
                    Tax::create([
                        'tax_name'  => $tax->tax_name,
                        'tax_rate'  => $tax->tax_rate,
                        'cgst_name' => $tax->cgst_name,
                        'cgst_rate' => $tax->cgst_rate,
                        'sgst_name' => $tax->sgst_name,
                        'sgst_rate' => $tax->sgst_rate,
                        'igst_name' => $tax->igst_name,
                        'igst_rate' => $tax->igst_rate,
                        'created_by' => $tax->created_by,
                        'updated_by' => $tax->updated_by ? $tax->updated_by:NULL,
                        'created_at' => $tax->created_at ? date('Y-m-d',strtotime($tax->created_at)) : NULL,
                        'updated_at' => $tax->updated_at ? date('Y-m-d',strtotime($tax->updated_at)) : NULL,
                        'deleted_at' => $tax->deleted_at ? date('Y-m-d',strtotime($tax->deleted_at)) : NULL,
                    ]);
                }
            break;
            case "terms"://no data
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('terms')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('terms')->get();
                foreach($table_data as $term){
                    Term::create([
                        'term_type'  => $term->term_type,
                        'label'      => $term->term_condition, 
                        'term'       => $term->term,
                        'created_by' => $term->created_by,
                        'updated_by' => $term->updated_by ? $term->updated_by:NULL,
                        'created_at' => $term->created_at ? date('Y-m-d',strtotime($term->created_at)) : NULL,
                        'updated_at' => $term->updated_at ? date('Y-m-d',strtotime($term->updated_at)) : NULL,
                        'deleted_at' => $term->deleted_at ? date('Y-m-d',strtotime($term->deleted_at)) : NULL,
                    ]);    
                }
            break;
            case "users"://
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('users')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('users')->get();
                foreach($table_data as $user){
                    User::create([
                        'name'       => $user->name,
                        'username'   => $user->username,
                        'email'      => $user->email,
                        'password'   => $user->password,
                        'user_role'  => $user->user_role,
                        'mobile_no'  => $user->mobile_no,
                        'address'    => $user->address,
                        'created_by' => $user->created_by,
                        'updated_by' => $user->updated_by ? $user->updated_by:NULL,
                        'created_at' => $user->created_at ? date('Y-m-d',strtotime($user->created_at)) : NULL,
                        'updated_at' => $user->updated_at ? date('Y-m-d',strtotime($user->updated_at)) : NULL,
                        'deleted_at' => $user->deleted_at ? date('Y-m-d',strtotime($user->deleted_at)) : NULL, 
                    ]);
                }
            break;
            case "warehouses"://
                $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
                $db_int->table('warehouses')->truncate();
                $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                
                $table_data = $db_ext->table('warehouses')->get();
                foreach($table_data as $warehouse){
                    Warehouse::create([
                        'warehouse_name'     => $warehouse->warehouse_name,
                        'warehouse_location' => $warehouse->location,//$warehouse->address?$warehouse->address.','.$warehouse->location:
                        'created_by' => $warehouse->created_by,
                        'updated_by' => $warehouse->updated_by ? $warehouse->updated_by:NULL,
                        'created_at' => $warehouse->created_at ? date('Y-m-d',strtotime($warehouse->created_at)) : NULL,
                        'updated_at' => $warehouse->updated_at ? date('Y-m-d',strtotime($warehouse->updated_at)) : NULL,
                        'deleted_at' => $warehouse->deleted_at ? date('Y-m-d',strtotime($warehouse->deleted_at)) : NULL,  
                    ]);
                }
            break;
            default: NULL;
         }
        }
    }

    // public function migrate_data(Request $request)
    // {//v2
    //     // dd($request);
    //     $db_int = \DB::connection('mysql');
    //     $db_ext = \DB::connection('mysql_external');
    //     $preference = Preference::first();
    //     foreach($request->input() as $table){
    //         switch($table['table_name']){
    //             case "accounts"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('accounts')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('accounts')->get();
    //                 foreach($table_data as $account){
    //                     Account::create([
    //                         'account_code' => $account->account_code,
    //                         'account_name' => $account->account_name,
    //                         'account_no'   => $account->account_no,
    //                         'bank_name'    => $account->bank_name,
    //                         'branch_name'  => $account->branch_name,
    //                         'ifsc_code'    => $account->ifsc_code,
    //                         'created_by'   => $account->created_by,
    //                         'updated_by'   => $account->updated_by ? $account->updated_by:NULL,
    //                         'created_at'   => $account->created_at ? date('Y-m-d',strtotime($account->created_at)) : NULL,
    //                         'updated_at'   => $account->updated_at ? date('Y-m-d',strtotime($account->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "bills"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('bill_products')->truncate();
    //                 $db_int->table('bills')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('purchases')->get();
    //                 foreach($table_data as $purc){
    //                     $contact = $preference->fetchContact($purc->contact_id);
    //                     $src = $preference->fetchPlace($purc->source_place);
    //                     $dest = $preference->fetchPlace($purc->destination_place);
    //                     $bill = Bill::create([
    //                         'fiscal_year' => $preference->fiscal_year,
    //                         'bill_no'     => $purc->bill_no,
    //                         'bill_date'   => date("Y-m-d", strtotime($purc->bill_date)),
    //                         'due_date'    => date("Y-m-d", strtotime($purc->due_date)),
    //                         'reference_no'=> $purc->order_no,
    //                         'reference_date'   => $purc->bill_date ? date('Y-m-d',strtotime($purc->bill_date)) : NULL,
    //                         'vendor_id'        => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'payment_term_id'  => $purc->payment_term_id?$purc->payment_term_id:1,
    //                         'source_id'        => $src->place_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'        => 0,
    //                         'discount_amount'  => 0,
    //                         'tax_amount'       => 0,
    //                         'total_amount'     => 0,
    //                         'round_off'        => 0,
    //                         'grand_total'      => 0,
    //                         // 'term_id'=>$purc->term_id,
    //                         // 'terms'=>$purc->terms,
    //                         'note'         => $purc->e_way_bill ? 'e-Sugam-'.$purc->e_way_bill.','.$purc->note : $purc->note,
    //                         'bill_status'  => $purc->purchase_status,
    //                         'created_by'   => $purc->created_by,
    //                         'updated_by'   => $purc->updated_by ? $purc->updated_by:NULL,
    //                         'created_at'   => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
    //                         'updated_at'   => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
    //                     ]);
    //                     $purchase_products = $db_ext->table('purchase_products')->where('purchase_id',$purc->purchase_id)->get();
    //                     foreach ($purchase_products as $purchase_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($purchase_product->category_id);
    //                         $tax     = $preference->fetchTax($purchase_product->tax_id);
    //                         $product  = $preference->fetchProduct($purchase_product->product_id);
    //                         $price    = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();

    //                         $amount  = $preference->amountCalc($purchase_product->sales_rate,$purchase_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$purchase_product->discount,$purchase_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;

    //                         $bill_product = BillProduct::create([
    //                             'bill_id'      => $bill->bill_id,
    //                             'product_id'   => $product->product_id,
    //                             'product_type' => $purchase_product->product_type,
    //                             'product_code' => $purchase_product->product_code,
    //                             'hsn_code'     => $purchase_product->hsn_code,
    //                             'category_id'  => $category->category_id,
    //                             'product_name' => $purchase_product->product_name,
    //                             // 'description'  => $purchase_product->description,
    //                             'product_unit' => $purchase_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $purchase_product->purchase_rate?$purchase_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $purchase_product->sales_rate?$purchase_product->sales_rate:0,
    //                             'purchase_rate_inc' => $purchase_product->purchase_rate?$purchase_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $purchase_product->sales_rate?$purchase_product->sales_rate:0,
    //                             'store_id'      => $store->store_id,
    //                             'warehouse_id'  => $store->warehouse_id,
    //                             'quantity'      => $purchase_product->quantity,
    //                             'amount'        => $amount,//
    //                             'discount'      => $purchase_product->discount,
    //                             'discount_type' => $purchase_product->discount_type,
    //                             'discount_amount' => $discount_amount,//
    //                             'tax_id'        => $tax->tax_id,
    //                             'tax_amount'    => $tax_amount,//
    //                             'sub_total'     => $sub_total,//
    //                             'created_by'    => $purchase_product->created_by,
    //                             'updated_by'    => $purchase_product->updated_by ? $purchase_product->updated_by:NULL,
    //                             'created_at'    => $purchase_product->created_at ? date('Y-m-d',strtotime($purchase_product->created_at)) : NULL,
    //                             'updated_at'    => $purchase_product->updated_at ? date('Y-m-d',strtotime($purchase_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = BillProduct::where('bill_id',$bill->bill_id)->sum('amount');
    //                     $tax_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('tax_amount');
    //                     $discount_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('discount_amount');
    //                     $total_amount = BillProduct::where('bill_id',$bill->bill_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                         Bill::where('bill_id',$bill->bill_id)->update([
    //                             'sub_total'   => $sub_total,
    //                             'tax_amount'  => $tax_amount,
    //                             'total_amount'=> $total_amount,
    //                             'round_off'   => $round_off,
    //                             'grand_total' => $grand_total,
    //                         ]);
    //                 }
    //             break;
    //             case "bill_returns"://no data
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('bill_return_products')->truncate();
    //                 $db_int->table('bill_returns')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('purchase_returns')->get();
    //                 foreach($table_data as $purc){
    //                     $contact = $preference->fetchContact($purc->contact_id);
    //                     $src = $preference->fetchPlace($purc->source_place);
    //                     $dest = $preference->fetchPlace($purc->destination_place);

    //                     $bill_return = BillReturn::create([
    //                         'fiscal_year'     => $purc->purchase_return_year,
    //                         'bill_return_no'  => $purc->purchase_return_no,
    //                         'bill_return_date'=> date("Y-m-d", strtotime($purc->purchase_return_date)),
    //                         'reference_no'    => $purc->bill_no,
    //                         'reference_date'  => $purc->purchase_return_date ? date('Y-m-d',strtotime($purc->purchase_return_date)) : NULL,
    //                         'vendor_id'       => $contact->contact_id,
    //                         'billing_address' => $contact->billing_address,
    //                         'shipping_address'=> $contact->shipping_address,
    //                         'source_id'       => $src->place_id,
    //                         'destination_id'  => $dest->place_id,
    //                         'sub_total'       => 0,
    //                         'discount_amount' => 0,
    //                         'tax_amount'      => 0,
    //                         'total_amount'    => 0,
    //                         'round_off'       => 0,
    //                         'grand_total'     => 0,
    //                         // 'term_id'=>$purc->term_id,
    //                         // 'terms'=>$purc->terms,
    //                         'note'            => $purc->e_way_bill ? 'e-Sugam-'.$purc->e_way_bill.','.$purc->note : $purc->note,
    //                         'bill_return_status'=> $purc->purchase_return_status,
    //                         'created_by'    => $purc->created_by,
    //                         'updated_by'    => $purc->updated_by ? $purc->updated_by:NULL,
    //                         'created_at'    => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
    //                         'updated_at'    => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
    //                     ]);

    //                     $purchase_return_products = $db_ext->table('purchase_return_products')->where('purchase_return_id',$purc->purchase_return_id)->get();
    //                     foreach ($purchase_return_products as $purchase_return_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($purchase_return_product->category_id);
    //                         $tax     = $preference->fetchTax($purchase_return_product->tax_id);
    //                         $product  = $preference->fetchProduct($purchase_return_product->product_id);
    //                         $price    = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();

    //                         $amount  = $preference->amountCalc($purchase_return_product->sales_rate,$purchase_return_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$purchase_return_product->discount,$purchase_return_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;

    //                         $bill_return_product = BillReturnProduct::create([
    //                             'bill_return_id' => $bill_return->bill_return_id,
    //                             'product_id'     => $product->product_id,
    //                             'product_type'   => $purchase_return_product->product_type,
    //                             'product_code'   => $purchase_return_product->product_code,
    //                             'hsn_code'       => $product->hsn_code,
    //                             'category_id'    => $category->category_id,
    //                             'product_name'   => $purchase_return_product->product_name,
    //                             // 'description'    => $product->description,
    //                             'product_unit'   => $purchase_return_product->product_unit,
    //                             'price_id'       => $price->price_id,
    //                             'purchase_rate_exc' => $purchase_return_product->purchase_rate?$purchase_return_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $purchase_return_product->sales_rate?$purchase_return_product->sales_rate:0,
    //                             'purchase_rate_inc' => $purchase_return_product->purchase_rate?$purchase_return_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $purchase_return_product->sales_rate?$purchase_return_product->sales_rate:0,
    //                             'store_id'      => $store->store_id,
    //                             'warehouse_id'  => $store->warehouse_id,
    //                             'quantity'      => $purchase_return_product->quantity,
    //                             'amount'        => $amount,
    //                             'discount'      => $purchase_return_product->discount,
    //                             'discount_type' => $purchase_return_product->discount_type,
    //                             'discount_amount' => $discount_amount,
    //                             'tax_id'        => $tax->tax_id,
    //                             'tax_amount'    => $tax_amount,
    //                             'sub_total'     => $sub_total,
    //                             'created_by'    => $purchase_return_product->created_by,
    //                             'updated_by'    => $purchase_return_product->updated_by ? $purchase_return_product->updated_by:NULL,
    //                             'created_at'    => $purchase_return_product->created_at ? date('Y-m-d',strtotime($purchase_return_product->created_at)) : NULL,
    //                             'updated_at'    => $purchase_return_product->updated_at ? date('Y-m-d',strtotime($purchase_return_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('amount');
    //                     $tax_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('tax_amount');
    //                     $discount_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('discount_amount');
    //                     $total_amount = BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                         BillReturn::where('bill_return_id',$bill_return->bill_return_id)->update([
    //                             'sub_total'   => $sub_total,
    //                             'tax_amount'  => $tax_amount,
    //                             'total_amount'=> $total_amount,
    //                             'round_off'   => $round_off,
    //                             'grand_total' => $grand_total,
    //                         ]);
    //                 }
    //             break;
    //             case "categories"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('categories')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('categories')->get();
    //                 foreach($table_data as $category){
    //                     Category::create([
    //                         'category_code' => $category->category_code,
    //                         'category_name' => $category->category_name,
    //                         'created_by'    => $category->created_by,
    //                         'updated_by'    => $category->updated_by ? $category->updated_by:NULL,
    //                         'created_at'    => $category->created_at ? date('Y-m-d',strtotime($category->created_at)) : NULL,
    //                         'updated_at'    => $category->updated_at ? date('Y-m-d',strtotime($category->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "contacts"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('contacts')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('contacts')->get();
    //                 foreach($table_data as $contact){
    //                     $place = $preference->fetchPlace($contact->place_id);
    //                     Contact::create([
    //                         'contact_type' => $contact->contact_type=='Supplier'?'Vendor':$contact->contact_type,
    //                         'contact_code' => $contact->contact_code,
    //                         'contact_name' => $contact->contact_name,
    //                         'email'        => $contact->email,
    //                         'mobile_no'    => $contact->mobile_no,
    //                         'phone_no'     => $contact->phone_no,
    //                         'pan_no'       => $contact->pan_no,
    //                         'gstin_no'     => $contact->gstin_no,
    //                         'billing_address'  => $contact->billing_address_1,
    //                         'shipping_address' => $contact->shipping_address_1,
    //                         'account_no'   => $contact->account_no,
    //                         'bank_name'    => $contact->bank_name,
    //                         'branch_name'  => $contact->branch_name,
    //                         'ifsc_code'    => $contact->ifsc_code,
    //                         'payment_term_id'  => $contact->payment_term_id,
    //                         'place_id'     => $place->place_id,
    //                         // 'date_of_birth'=> $contact->dob ? date('Y-m-d',strtotime($contact->dob)) : NULL,
    //                         // 'wedding_anniversary' => $contact->wedding_anniversary ? date('Y-m-d',strtotime($contact->wedding_anniversary)) : NULL,
    //                         'created_by'   => $contact->created_by,
    //                         'updated_by'   => $contact->updated_by ? $contact->updated_by:NULL,
    //                         'created_at'   => $contact->created_at ? date('Y-m-d',strtotime($contact->created_at)) : NULL,
    //                         'updated_at'   => $contact->updated_at ? date('Y-m-d',strtotime($contact->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "deliveries"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('delivery_products')->truncate();
    //                 $db_int->table('deliveries')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('delivery_challans')->get();
    //                 foreach($table_data as $deli){
    //                     $contact = $preference->fetchContact($deli->contact_id);
    //                     $term = $preference->fetchTerm($deli->terms);
    //                     $src = $preference->fetchPlace($deli->source_place);
    //                     $dest = $preference->fetchPlace($deli->destination_place);
    //                     $delivery = Delivery::create([
    //                         'fiscal_year'      => $deli->delivery_challan_year,
    //                         'delivery_no'      => $deli->delivery_challan_no,
    //                         'delivery_date'    => date("Y-m-d", strtotime($deli->delivery_challan_date)),
    //                         'reference_no'     => $deli->reference_no,
    //                         'reference_date'   => $deli->delivery_challan_date ? date('Y-m-d',strtotime($deli->delivery_challan_date)) : NULL,//no reference date in old db
    //                         'customer_id'      => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,//new db
    //                         'shipping_address' => $contact->shipping_address,//new db
    //                         'source_id'        => $src->place_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'        => 0,//new field
    //                         'discount_amount'  => 0,//new field
    //                         'tax_amount'       => 0,//new field
    //                         'total_amount'     => 0,//new field
    //                         'round_off'        => 0,//new field
    //                         'grand_total'      => 0,//new field
    //                         'term_id'          => $term?$term->term_id:NULL,
    //                         'terms'            => $term?$term->term:NULL,
    //                         'note'             => $deli->e_way_bill ? 'e-Sugam-'.$deli->e_way_bill.','.$deli->note : $deli->note,
    //                         'delivery_status'  => $deli->delivery_challan_status,
    //                         'created_by'       => $deli->created_by,
    //                         'updated_by'       => $deli->updated_by ? $deli->updated_by:NULL,
    //                         'created_at'       => $deli->created_at ? date('Y-m-d',strtotime($deli->created_at)) : NULL,
    //                         'updated_at'       => $deli->updated_at ? date('Y-m-d',strtotime($deli->updated_at)) : NULL,
    //                     ]);

    //                     $delivery_products = $db_ext->table('delivery_challan_products')->where('delivery_challan_id',$deli->delivery_challan_id)->get();
    //                     foreach ($delivery_products as $delivery_product)
    //                     {
    //                         //calculations
    //                         $category= $preference->fetchCategory($delivery_product->category_id);
    //                         $tax     = $preference->fetchTax($delivery_product->tax_id);
    //                         $product = $preference->fetchProduct($delivery_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($delivery_product->sales_rate,$delivery_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $sub_total = $amount+$tax_amount;
    //                         $delivery_product = DeliveryProduct::create([
    //                             'delivery_id'  => $delivery->delivery_id,
    //                             'product_id'   => $product->product_id,
    //                             'product_type' => $delivery_product->product_type,
    //                             'product_code' => $delivery_product->product_code,
    //                             'hsn_code'     => $delivery_product->hsn_code,
    //                             'category_id'  => $category->category_id,
    //                             'product_name' => $delivery_product->product_name,
    //                             // 'description'  => $delivery_product->description,
    //                             'product_unit' => $delivery_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $delivery_product->purchase_rate?$delivery_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $delivery_product->sales_rate?$delivery_product->sales_rate:0,
    //                             'purchase_rate_inc' => $delivery_product->purchase_rate?$delivery_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $delivery_product->mrp?$delivery_product->mrp:0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $delivery_product->quantity,
    //                             'amount'            => $amount,//new fields
    //                             'discount'          => 0,//new fields
    //                             'discount_type'     => "%",//new fields
    //                             'discount_amount'   => 0,//new fields
    //                             'tax_id'            => $tax->tax_id,//new fields
    //                             'tax_amount'        => $tax_amount,//new fields
    //                             'sub_total'         => $sub_total,//new fields
    //                             'created_by'        => $delivery_product->created_by,
    //                             'updated_by'        => $delivery_product->updated_by ? $delivery_product->updated_by:NULL,
    //                             'created_at'        => $delivery_product->created_at ? date('Y-m-d',strtotime($delivery_product->created_at)) : NULL,
    //                             'updated_at'        => $delivery_product->updated_at ? date('Y-m-d',strtotime($delivery_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('amount');
    //                     $tax_amount = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('tax_amount');
    //                     $total_amount = DeliveryProduct::where('delivery_id',$delivery->delivery_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                     Delivery::where('delivery_id',$delivery->delivery_id)->update([
    //                         'sub_total'   => $sub_total,
    //                         'tax_amount'  => $tax_amount,
    //                         'total_amount'=> $total_amount,
    //                         'round_off'   => $round_off,
    //                         'grand_total' => $grand_total,
    //                     ]);
    //                 }
    //             break;
    //             case "delivery_returns"://no data
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('delivery_return_products')->truncate();
    //                 $db_int->table('delivery_returns')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('delivery_returns')->get();
    //                 foreach($table_data as $deli){
    //                     $contact = $preference->fetchContact($deli->contact_id);
    //                     $term = $preference->fetchTerm($deli->terms);
    //                     $src = $preference->fetchPlace($deli->source_place);
    //                     $dest = $preference->fetchPlace($deli->destination_place);

    //                     $delivery_return = DeliveryReturn::create([
    //                         'fiscal_year'         => $deli->delivery_return_year,
    //                         'delivery_return_no'  => $deli->delivery_return_no,
    //                         'delivery_return_date'=> date("Y-m-d", strtotime($deli->delivery_return_date)),
    //                         'reference_no'        => $deli->delivery_challan_no,
    //                         'reference_date'      => $deli->delivery_return_date ? date('Y-m-d',strtotime($deli->delivery_return_date)) : NULL,
    //                         'customer_id'      => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'source_id'        => $src->place_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'        => 0,//new field
    //                         'discount_amount'  => 0,//new field
    //                         'tax_amount'       => 0,//new field
    //                         'total_amount'     => 0,//new field
    //                         'round_off'        => 0,//new field
    //                         'grand_total'      => 0,//new field
    //                         'term_id'          => $term?$term->term_id:NULL,
    //                         'terms'            => $term?$term->term:NULL,
    //                         'note'             => $deli->e_way_bill ? 'e-Sugam-'.$deli->e_way_bill.','.$deli->note : $deli->note,
    //                         'delivery_return_status'  => $deli->delivery_return_status,
    //                         'created_by'       => $deli->created_by,
    //                         'updated_by'       => $deli->updated_by ? $deli->updated_by:NULL,
    //                         'created_at'       => $deli->created_at ? date('Y-m-d',strtotime($deli->created_at)) : NULL,
    //                         'updated_at'       => $deli->updated_at ? date('Y-m-d',strtotime($deli->updated_at)) : NULL,
    //                     ]);

    //                     $delivery_return_products = $db_ext->table('delivery_return_products')->where('delivery_return_id',$deli->delivery_return_id)->get();
    //                     foreach ($delivery_return_products as $delivery_return_product)
    //                     {
    //                         //calculations
    //                         $category= $preference->fetchCategory($delivery_return_product->category_id);
    //                         $tax     = $preference->fetchTax($delivery_return_product->tax_id);
    //                         $product = $preference->fetchProduct($delivery_return_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($delivery_return_product->sales_rate,$delivery_return_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $sub_total = $amount+$tax_amount;
    //                         DeliveryReturnProduct::create([
    //                             'delivery_return_id' => $delivery_return->delivery_return_id,
    //                             'product_id' => $product->product_id,
    //                             'product_type' => $delivery_return_product->product_type,
    //                             'product_code' => $delivery_return_product->product_code,
    //                             'hsn_code' => $delivery_return_product->hsn_code,
    //                             'category_id' => $category->category_id,
    //                             'product_name' => $delivery_return_product->product_name,
    //                             // 'description' => $product->description,
    //                             'product_unit' => $delivery_return_product->product_unit,
    //                             'price_id' => $price->price_id,
    //                             'purchase_rate_exc' => $delivery_return_product->purchase_rate?$delivery_return_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $delivery_return_product->sales_rate?$delivery_return_product->sales_rate:0,
    //                             'purchase_rate_inc' => $delivery_return_product->purchase_rate?$delivery_return_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $delivery_return_product->mrp?$delivery_return_product->mrp:0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $delivery_return_product->quantity,
    //                             'amount'            => $amount,//new fields
    //                             'discount'          => 0,//new fields
    //                             'discount_type'     => "%",//new fields
    //                             'discount_amount'   => 0,//new fields
    //                             'tax_id'            => $tax->tax_id,//new fields
    //                             'tax_amount'        => $tax_amount,//new fields
    //                             'sub_total'         => $sub_total,//new fields
    //                             'created_by'        => $delivery_return_product->created_by,
    //                             'updated_by'        => $delivery_return_product->updated_by ? $delivery_return_product->updated_by:NULL,
    //                             'created_at'        => $delivery_return_product->created_at ? date('Y-m-d',strtotime($delivery_return_product->created_at)) : NULL,
    //                             'updated_at'        => $delivery_return_product->updated_at ? date('Y-m-d',strtotime($delivery_return_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('amount');
    //                     $tax_amount = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('tax_amount');
    //                     $total_amount = DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                     DeliveryReturn::where('delivery_return_id',$delivery_return->delivery_return_id)->update([
    //                         'sub_total'   => $sub_total,
    //                         'tax_amount'  => $tax_amount,
    //                         'total_amount'=> $total_amount,
    //                         'round_off'   => $round_off,
    //                         'grand_total' => $grand_total,
    //                     ]);
    //                 }
    //             break;
    //             case "expenses"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('expenses')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('expenses')->get();
    //                 foreach($table_data as $expense){
    //                     $contact = $preference->fetchContact($expense->contact_id);
    //                     Expense::create([
    //                         'fiscal_year' => $preference->fiscal_year,
    //                         'voucher_no'  => $expense->voucher_no,
    //                         'voucher_date'=> date("Y-m-d", strtotime($expense->voucher_date)),
    //                         'account_id'  => $expense->expense_account,
    //                         //two fields from accounts table expense_account & paid_through
    //                         'contact_id'   => $contact->contact_id,
    //                         'payment_mode'=> $expense->payment_mode,
    //                         // 'reference_no'=> $expense->reference_no,
    //                         // 'reference_date'=> $expense->reference_date ? date('Y-m-d',strtotime($expense->reference_date)) : NULL,
    //                         'amount'     => $expense->amount,
    //                         // 'term_id'    => $term->term_id,
    //                         // 'terms'      => $term->term,
    //                         'note'       => $expense->note,
    //                         'created_by' => $expense->created_by,
    //                         'updated_by' => $expense->updated_by ? $expense->updated_by:NULL,
    //                         'created_at' => $expense->created_at ? date('Y-m-d',strtotime($expense->created_at)) : NULL,
    //                         'updated_at' => $expense->updated_at ? date('Y-m-d',strtotime($expense->updated_at)) : NULL,
    //                     ]);
    //                 }
    //                 //journal
    //                 $journal_table_data = $db_ext->table('journals')->where('payment_type','Debit')->get();
    //                 foreach($journal_table_data as $journal){
    //                     $contact = $preference->fetchContact($journal->contact_id);
    //                     $account = $preference->fetchAccount($journal->account_id);
    //                     Expense::create([
    //                         'fiscal_year' => $preference->fiscal_year,
    //                         'voucher_no'  => $journal->journal_no,
    //                         'voucher_date'=> date("Y-m-d", strtotime($journal->journal_date)),
    //                         'account_id'  => $account->account_id,
    //                         //two fields from accounts table journal_account & paid_through
    //                         'contact_id'   => $contact->contact_id,
    //                         'payment_mode'=> $journal->payment_type,
    //                         'reference_no'=> $journal->reference_no,
    //                         'reference_date'=> $journal->journal_date ? date('Y-m-d',strtotime($journal->journal_date)) : NULL,
    //                         'amount'     => $journal->amount,
    //                         // 'term_id'    => $term->term_id,
    //                         // 'terms'      => $term->term,
    //                         'note'       => $journal->note,
    //                         'created_by' => $journal->created_by,
    //                         'updated_by' => $journal->updated_by ? $journal->updated_by:NULL,
    //                         'created_at' => $journal->created_at ? date('Y-m-d',strtotime($journal->created_at)) : NULL,
    //                         'updated_at' => $journal->updated_at ? date('Y-m-d',strtotime($journal->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "incomes"://checked+
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('incomes')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('incomes')->get();
    //                 foreach($table_data as $income){
    //                     $contact = $preference->fetchContact($income->contact_id);
    //                     Income::create([
    //                         'fiscal_year'  => $preference->fiscal_year,
    //                         'receipt_no'   => $income->receipt_no,
    //                         'receipt_date' => date("Y-m-d", strtotime($income->receipt_date)),
    //                         'account_id'   => $income->income_account,
    //                         //two fields from accounts table income_account & received_from
    //                         'contact_id'   => $contact->contact_id,
    //                         'payment_mode' => $income->payment_mode,
    //                         // 'reference_no' => $income->reference_no,
    //                         // 'reference_date'=> $income->reference_date ? date('Y-m-d',strtotime($income->reference_date)) : NULL,
    //                         'amount'       => $income->amount,
    //                         // 'term_id'      =>$income->term_id,
    //                         // 'terms'        =>$income->terms,
    //                         'note'         => $income->note,
    //                         'created_by'   => $income->created_by,
    //                         'updated_by'   => $income->updated_by ? $income->updated_by:NULL,
    //                         'created_at'   => $income->created_at ? date('Y-m-d',strtotime($income->created_at)) : NULL,
    //                         'updated_at'   => $income->updated_at ? date('Y-m-d',strtotime($income->updated_at)) : NULL,
    //                     ]);
    //                 }
    //                  //journal
    //                 $journal_table_data = $db_ext->table('journals')->where('payment_type','Credit')->get();
    //                 foreach($journal_table_data as $journal){
    //                     $contact = $preference->fetchContact($journal->contact_id);
    //                     $account = $preference->fetchAccount($journal->account_id);
    //                     Income::create([
    //                         'fiscal_year' => $preference->fiscal_year,
    //                         'receipt_no'  => $journal->journal_no,
    //                         'receipt_date'=> date("Y-m-d", strtotime($journal->journal_date)),
    //                         'account_id'  => $account->account_id,
    //                         //two fields from accounts table income_account & received_from
    //                         'contact_id'   => $contact->contact_id,
    //                         'payment_mode'=> $journal->payment_type,
    //                         'reference_no'=> $journal->reference_no,
    //                         'reference_date'=> $journal->journal_date ? date('Y-m-d',strtotime($journal->journal_date)) : NULL,
    //                         'amount'     => $journal->amount,
    //                         // 'term_id'    => $term->term_id,
    //                         // 'terms'      => $term->term,
    //                         'note'       => $journal->note,
    //                         'created_by' => $journal->created_by,
    //                         'updated_by' => $journal->updated_by ? $journal->updated_by:NULL,
    //                         'created_at' => $journal->created_at ? date('Y-m-d',strtotime($journal->created_at)) : NULL,
    //                         'updated_at' => $journal->updated_at ? date('Y-m-d',strtotime($journal->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "invoices"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('invoice_products')->truncate();
    //                 $db_int->table('invoices')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 //services
    //                 $service_table_data = $db_ext->table('services')->get();
    //                 foreach($service_table_data as $ser){
    //                     $contact = $preference->fetchContact($ser->contact_id);
    //                     $term = $preference->fetchTerm($ser->terms);
    //                     $src = $preference->fetchPlace($ser->source_place);
    //                     $dest = $preference->fetchPlace($ser->destination_place);
    //                     $invoice = Invoice::create([
    //                         'fiscal_year'  => $ser->invoice_year,
    //                         'invoice_no'   => $ser->invoice_no,
    //                         'invoice_date' => date("Y-m-d", strtotime($ser->invoice_date)),
    //                         'due_date'     => date("Y-m-d", strtotime($ser->due_date)),
    //                         'invoice_type' => "Service",
    //                         'reference_no' => $ser->reference_no,
    //                         'reference_date'=> $ser->due_date ? date('Y-m-d',strtotime($ser->due_date)) : NULL,//no reference date in old db
    //                         'customer_id'   => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,//new db
    //                         'shipping_address' => $contact->shipping_address,//new db
    //                         'source_id'        => $src->place_id,
    //                         'payment_term_id'  => $ser->payment_term_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'        => 0,
    //                         'discount_amount'  => 0,
    //                         'tax_amount'       => 0,
    //                         'total_amount'     => 0,
    //                         'round_off'        => 0,
    //                         'grand_total'      => 0,
    //                         'term_id'          => $term?$term->term_id:NULL,
    //                         'terms'            => $term?$term->term:NULL,
    //                         'note'             => $ser->e_way_bill ? 'e-Sugam-'.$ser->e_way_bill.','.$ser->note : $ser->note,
    //                         'invoice_status'   => $ser->service_status,
    //                         'created_by'       => $ser->created_by,
    //                         'updated_by'       => $ser->updated_by ? $ser->updated_by:NULL,
    //                         'created_at'       => $ser->created_at ? date('Y-m-d',strtotime($ser->created_at)) : NULL,
    //                         'updated_at'       => $ser->updated_at ? date('Y-m-d',strtotime($ser->updated_at)) : NULL,
    //                     ]);
    //                     $service_products = $db_ext->table('service_products')->where('service_id',$ser->service_id)->get();
    //                     foreach ($service_products as $service_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($service_product->category_id);
    //                         $tax = $preference->fetchTax($service_product->tax_id);
    //                         $product = $preference->fetchProduct($service_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($service_product->sales_rate,$service_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$service_product->discount,$service_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;
    //                         InvoiceProduct::create([
    //                             'invoice_id'   => $invoice->invoice_id,
    //                             'product_id'   => $product->product_id,
    //                             'product_type' => $service_product->product_type,
    //                             'product_code' => $service_product->product_code,
    //                             'hsn_code'     => $service_product->hsn_code,
    //                             'category_id'  => $category->category_id,
    //                             'product_name' => $service_product->product_name,
    //                             // 'description' => $product->description,
    //                             'product_unit' => $service_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $service_product->purchase_rate? $service_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $service_product->sales_rate?$service_product->sales_rate :0,
    //                             'purchase_rate_inc' => $service_product->purchase_rate? $service_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $service_product->mrp?$service_product->mrp :0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $service_product->quantity,
    //                             'amount'            => $amount,
    //                             'discount'          => $service_product->discount,
    //                             'discount_type'     => $service_product->discount_type,
    //                             'discount_amount'   => $discount_amount,
    //                             'tax_id'            => $tax->tax_id,
    //                             'tax_amount'        => $tax_amount,
    //                             'sub_total'         => $sub_total,
    //                             'created_by'        => $service_product->created_by,
    //                             'updated_by'        => $service_product->updated_by ? $service_product->updated_by:NULL,
    //                             'created_at'        => $service_product->created_at ? date('Y-m-d',strtotime($service_product->created_at)) : NULL,
    //                             'updated_at'        => $service_product->updated_at ? date('Y-m-d',strtotime($service_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('amount');
    //                     $tax_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('tax_amount');
    //                     $discount_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('discount_amount');
    //                     $total_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                         Invoice::where('invoice_id',$invoice->invoice_id)->update([
    //                             'sub_total'   => $sub_total,
    //                             'tax_amount'  => $tax_amount,
    //                             'discount_amount'  => $discount_amount,
    //                             'total_amount'=> $total_amount,
    //                             'round_off'   => $round_off,
    //                             'grand_total' => $grand_total,
    //                         ]);
    //                 }
    //                 //invoices
    //                 $invoice_table_data = $db_ext->table('invoices')->get();
    //                 foreach($invoice_table_data as $inv){
    //                     $contact = $preference->fetchContact($inv->contact_id);
    //                     $term = $preference->fetchTerm($inv->terms);
    //                     $src = $preference->fetchPlace($inv->source_place);
    //                     $dest = $preference->fetchPlace($inv->destination_place);
    //                     $invoice = Invoice::create([
    //                         'fiscal_year'  => $inv->invoice_year,
    //                         'invoice_no'   => $inv->invoice_no,
    //                         'invoice_date' => date("Y-m-d", strtotime($inv->invoice_date)),
    //                         'due_date'     => date("Y-m-d", strtotime($inv->due_date)),
    //                         'invoice_type' => "Invoice",
    //                         'reference_no' => $inv->reference_no,
    //                         'reference_date'=> $inv->due_date ? date('Y-m-d',strtotime($inv->due_date)) : NULL,//no reference date in old db
    //                         'customer_id'   => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,//new db
    //                         'shipping_address' => $contact->shipping_address,//new db
    //                         'source_id'        => $src->place_id,
    //                         'payment_term_id'  => $inv->payment_term_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'        => 0,
    //                         'discount_amount'  => 0,
    //                         'tax_amount'       => 0,
    //                         'total_amount'     => 0,
    //                         'round_off'        => 0,
    //                         'grand_total'      => 0,
    //                         'term_id'          => $term?$term->term_id:NULL,
    //                         'terms'            => $term?$term->term:NULL,
    //                         'note'             => $inv->e_way_bill ? 'e-Sugam-'.$inv->e_way_bill.','.$inv->note : $inv->note,
    //                         'invoice_status'   => $inv->invoice_status,
    //                         'created_by'       => $inv->created_by,
    //                         'updated_by'       => $inv->updated_by ? $inv->updated_by:NULL,
    //                         'created_at'       => $inv->created_at ? date('Y-m-d',strtotime($inv->created_at)) : NULL,
    //                         'updated_at'       => $inv->updated_at ? date('Y-m-d',strtotime($inv->updated_at)) : NULL,
    //                     ]);
    //                     $invoice_products = $db_ext->table('invoice_products')->where('invoice_id',$inv->invoice_id)->get();
    //                     foreach ($invoice_products as $invoice_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($invoice_product->category_id);
    //                         $product = $preference->fetchProduct($invoice_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
    //                         $tax = $preference->fetchTax($invoice_product->tax_id);
    //                         $amount = $preference->amountCalc($invoice_product->sales_rate,$invoice_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$invoice_product->discount,$invoice_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;
    //                         InvoiceProduct::create([
    //                             'invoice_id'   => $invoice->invoice_id,
    //                             'product_id'   => $product->product_id,
    //                             'product_type' => $invoice_product->product_type,
    //                             'product_code' => $invoice_product->product_code,
    //                             'hsn_code'     => $invoice_product->hsn_code,
    //                             'category_id'  => $category->category_id,
    //                             'product_name' => $invoice_product->product_name,
    //                             // 'description' => $product->description,
    //                             'product_unit' => $invoice_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $invoice_product->purchase_rate? $invoice_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $invoice_product->sales_rate? $invoice_product->sales_rate:0,
    //                             'purchase_rate_inc' => $invoice_product->purchase_rate? $invoice_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $invoice_product->mrp? $invoice_product->mrp:0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $invoice_product->quantity,
    //                             'amount'            => $amount,
    //                             'discount'          => $invoice_product->discount,
    //                             'discount_type'     => $invoice_product->discount_type,
    //                             'discount_amount'   => $discount_amount,
    //                             'tax_id'            => $tax->tax_id,
    //                             'tax_amount'        => $tax_amount,
    //                             'sub_total'         => $sub_total,
    //                             'created_by'        => $invoice_product->created_by,
    //                             'updated_by'        => $invoice_product->updated_by ? $invoice_product->updated_by:NULL,
    //                             'created_at'        => $invoice_product->created_at ? date('Y-m-d',strtotime($invoice_product->created_at)) : NULL,
    //                             'updated_at'        => $invoice_product->updated_at ? date('Y-m-d',strtotime($invoice_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('amount');
    //                     $tax_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('tax_amount');
    //                     $discount_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('discount_amount');
    //                     $total_amount = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                         Invoice::where('invoice_id',$invoice->invoice_id)->update([
    //                             'sub_total'   => $sub_total,
    //                             'tax_amount'  => $tax_amount,
    //                             'discount_amount'  => $discount_amount,
    //                             'total_amount'=> $total_amount,
    //                             'round_off'   => $round_off,
    //                             'grand_total' => $grand_total,
    //                         ]);
    //                 }
    //             break;
    //             case "invoice_returns"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('invoice_return_products')->truncate();
    //                 $db_int->table('invoice_returns')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('invoice_returns')->get();
    //                 foreach($table_data as $inv){
    //                     $contact = $preference->fetchContact($inv->contact_id);
    //                     $term = $preference->fetchTerm($inv->terms);
    //                     $src = $preference->fetchPlace($inv->source_place);
    //                     $dest = $preference->fetchPlace($inv->destination_place);

    //                     $invoice_return = InvoiceReturn::create([
    //                         'fiscal_year'        => $inv->invoice_ret_year,
    //                         'invoice_return_no'  => $inv->invoice_ret_no,
    //                         'invoice_return_date'=> date("Y-m-d", strtotime($inv->invoice_ret_date)),
    //                         'reference_no'       => $inv->dc_proforma_no,
    //                         'reference_date'     => $inv->due_date ? date('Y-m-d',strtotime($inv->due_date)) : NULL,
    //                         'customer_id'      => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'source_id'        => $src->place_id,
    //                         'destination_id'   => $dest->place_id,
    //                         'sub_total'      => 0,
    //                         'discount_amount'=> 0,
    //                         'tax_amount'     => 0,
    //                         'total_amount'   => 0,
    //                         'round_off'      => 0,
    //                         'grand_total'    => 0,
    //                         'term_id'        => $term?$term->term_id:NULL,
    //                         'terms'          => $term?$term->term:NULL,
    //                         'note'           => $inv->note,
    //                         'invoice_return_status'=>$inv->invoice_return_status,
    //                         'created_by'        => $inv->created_by,
    //                         'updated_by'        => $inv->updated_by ? $inv->updated_by:NULL,
    //                         'created_at'        => $inv->created_at ? date('Y-m-d',strtotime($inv->created_at)) : NULL,
    //                         'updated_at'        => $inv->updated_at ? date('Y-m-d',strtotime($inv->updated_at)) : NULL,
    //                     ]);
    //                     $invoice_return_products = $db_ext->table('invoice_return_products')->where('invoice_return_id',$inv->invoice_return_id)->get();
    //                     foreach ($invoice_return_products as $invoice_return_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($invoice_return_product->category_id);
    //                         $tax = $preference->fetchTax($invoice_return_product->tax_id);
    //                         $product = $preference->fetchProduct($invoice_return_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($invoice_return_product->sales_rate,$invoice_return_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$invoice_return_product->discount,$invoice_return_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;

    //                         InvoiceReturnProduct::create([
    //                             'invoice_return_id' => $invoice_return->invoice_return_id,
    //                             'product_id'        => $product->product_id,
    //                             'product_type'      => $invoice_return_product->product_type,
    //                             'product_code'      => $invoice_return_product->product_code,
    //                             'hsn_code'          => $invoice_return_product->hsn_code,
    //                             'category_id'       => $category->category_id,
    //                             'product_name'      => $invoice_return_product->product_name,
    //                             // 'description' => $product->description,
    //                             'product_unit'      => $invoice_return_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $invoice_return_product->purchase_rate? $invoice_return_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $invoice_return_product->sales_rate?$invoice_return_product->sales_rate :0,
    //                             'purchase_rate_inc' => $invoice_return_product->purchase_rate? $invoice_return_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $invoice_return_product->mrp?$invoice_return_product->mrp :0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $invoice_return_product->quantity,
    //                             'amount'            => $amount,
    //                             'discount'          => $invoice_return_product->discount,
    //                             'discount_type'     => $invoice_return_product->discount_type,
    //                             'discount_amount'   => $discount_amount,
    //                             'tax_id'            => $tax->tax_id,
    //                             'tax_amount'        => $tax_amount,
    //                             'sub_total'         => $sub_total,
    //                             'created_by'        => $invoice_return_product->created_by,
    //                             'updated_by'        => $invoice_return_product->updated_by ? $invoice_return_product->updated_by:NULL,
    //                             'created_at'        => $invoice_return_product->created_at ? date('Y-m-d',strtotime($invoice_return_product->created_at)) : NULL,
    //                             'updated_at'        => $invoice_return_product->updated_at ? date('Y-m-d',strtotime($invoice_return_product->updated_at)) : NULL,
    //                         ]);
    //                     //calculations
    //                     $sub_total = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('amount');
    //                     $tax_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('tax_amount');
    //                     $discount_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('discount_amount');
    //                     $total_amount = InvoiceReturnProduct::where('invoice_return_id',$invoice_return->invoice_return_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                         InvoiceReturn::where('invoice_return_id',$invoice_return->invoice_return_id)->update([
    //                             'sub_total'   => $sub_total,
    //                             'tax_amount'  => $tax_amount,
    //                             'discount_amount'  => $discount_amount,
    //                             'total_amount'=> $total_amount,
    //                             'round_off'   => $round_off,
    //                             'grand_total' => $grand_total,
    //                         ]);
    //                     }
    //                 }
    //             break;
    //             case "masters"://not tested
    //                 dd($request);
    //                 // $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 // $db_int->table('masters')->truncate();
    //                 // $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('masters')->get();
    //                 foreach($table_data as $master){
    //                     Master::create([
    //                         'master_name'  => $master->master_name,
    //                         'master_value' => $master->master_value,
    //                         'created_by'   => $master->created_by,
    //                         'updated_by'   => $master->updated_by ? $master->updated_by:NULL,
    //                         'created_at'   => $master->created_at ? date('Y-m-d',strtotime($master->created_at)) : NULL,
    //                         'updated_at'   => $master->updated_at ? date('Y-m-d',strtotime($master->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "organizations"://not tested
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('organizations')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('organizations')->get();
    //                 foreach($table_data as $organization){
    //                     Organization::create([
    //                         'org_code'  => $organization->org_code,
    //                         'org_name'  => $organization->org_name,
    //                         'email'     => $organization->email,
    //                         'mobile_no' => $organization->mobile_no,
    //                         'phone_no'  => $organization->phone_no,
    //                         'address'   => $organization->address1.$organization->address2,
    //                         'pan_no'    => $organization->pan_no,
    //                         'gstin_no'  => $organization->gstin_no,
    //                         'place_id'  => $organization->place_id,
    //                         'logo'      => $organization->logo,
    //                         'header'    => $organization->header,
    //                         'footer'    => $organization->footer,
    //                         'created_by'=> $organization->created_by,
    //                         'updated_by'=> $organization->updated_by ? $organization->updated_by:NULL,
    //                         'created_at'=> $organization->created_at ? date('Y-m-d',strtotime($organization->created_at)) : NULL,
    //                         'updated_at'=> $organization->updated_at ? date('Y-m-d',strtotime($organization->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "payments"://no data
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('payment_particulars')->truncate();
    //                 $db_int->table('payments')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('payments')->get();
    //                 foreach($table_data as $pay){
    //                     $contact = $preference->fetchContact($pay->contact_id);
    //                     $payment = Payment::create([
    //                         'fiscal_year'     => $preference->fiscal_year,
    //                         'vendor_id'       => $contact->contact_id,
    //                         'billing_address' => $contact->billing_address,//new db
    //                         'shipping_address'=> $contact->shipping_address,//new db
    //                         'voucher_no'      => $pay->voucher_no,
    //                         'voucher_date'    => date("Y-m-d", strtotime($pay->voucher_date)),
    //                         'account_id'      => $pay->paid_through,
    //                         'payment_mode'    => $pay->payment_mode,
    //                         'reference_no'    => $pay->reference_no,
    //                         'reference_date'  => $pay->voucher_date ? date('Y-m-d',strtotime($pay->voucher_date)) : NULL,//no data for reference date
    //                         'total_amount'    => 0,
    //                         // 'term_id'         => $pay->term_id,
    //                         // 'terms'           => $pay->terms,
    //                         'note'            => $pay->note,
    //                         'created_by'      => $pay->created_by,
    //                         'updated_by'      => $pay->updated_by ? $pay->updated_by:NULL,
    //                         'created_at'      => $pay->created_at ? date('Y-m-d',strtotime($pay->created_at)) : NULL,
    //                         'updated_at'      => $pay->updated_at ? date('Y-m-d',strtotime($pay->updated_at)) : NULL,
    //                     ]);
    //                     $bills = $db_ext->table('payment_bills')->where('payment_id',$pay->payment_id)->get();
    //                     if(count($bills)>0){
    //                         foreach ($bills as $bill) 
    //                         {
    //                             $curr_bill = $preference->fetchBill($bill->bill_id);
    //                             PaymentParticular::create([
    //                                 'payment_id'   => $payment->payment_id,
    //                                 'reference'    => 'App\Bill',
    //                                 'reference_id' => $curr_bill->bill_id,
    //                                 //need to be modified if bill table is present
    //                                 'paid_amount'  => $bill->paid_amount,
    //                                 //need to be modified if bill table is present
    //                                 'created_by'   => $bill->created_by,
    //                                 'updated_by'   => $bill->updated_by ? $bill->updated_by:NULL,
    //                                 'created_at'   => $bill->created_at ? date('Y-m-d',strtotime($bill->created_at)) : NULL,
    //                                 'updated_at'   => $bill->updated_at ? date('Y-m-d',strtotime($bill->updated_at)) : NULL,
    //                             ]);
    //                         }    
    //                     }
    //                     $purchases = $db_ext->table('payment_purchases')->where('payment_id',$pay->payment_id)->get();
    //                     if(count($purchases)>0){
    //                         foreach ($purchases as $purchase) 
    //                         {
    //                             $curr_purchase = $preference->fetchPurchase($purchase->purchase_id);
    //                             PaymentParticular::create([
    //                                 'payment_id'   => $payment->payment_id,
    //                                 'reference'    => 'App\Purchase',
    //                                 'reference_id' => $curr_purchase->purchase_id,
    //                                 //need to be modified if purchase table is present
    //                                 'paid_amount'  => $purchase->payment,
    //                                 //need to be modified if purchase table is present
    //                                 'created_by'   => $purchase->created_by,
    //                                 'updated_by'   => $purchase->updated_by ? $purchase->updated_by:NULL,
    //                                 'created_at'   => $purchase->created_at ? date('Y-m-d',strtotime($purchase->created_at)) : NULL,
    //                                 'updated_at'   => $purchase->updated_at ? date('Y-m-d',strtotime($purchase->updated_at)) : NULL,
    //                             ]);
    //                         }    
    //                     }
    //                     //calculations
    //                     $total_amount=PaymentParticular::where('payment_id',$payment->payment_id)->sum('paid_amount');
    //                     Payment::where('payment_id',$payment->payment_id)->update([
    //                         'total_amount' => $total_amount,
    //                     ]);
    //                 }
    //             break;
    //             case "payment_terms"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('payment_terms')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('payment_terms')->get();
    //                 foreach($table_data as $payment_term){
    //                     PaymentTerm::create([
    //                         'payment_term' => $payment_term->payment_term,
    //                         'no_of_days'   => $payment_term->no_of_days,
    //                         'created_by'   => $payment_term->created_by,
    //                         'updated_by'   => $payment_term->updated_by ? $payment_term->updated_by:NULL,
    //                         'created_at'   => $payment_term->created_at ? date('Y-m-d',strtotime($payment_term->created_at)) : NULL,
    //                         'updated_at'   => $payment_term->updated_at ? date('Y-m-d',strtotime($payment_term->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "places"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('places')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('places')->get();
    //                 foreach($table_data as $place){
    //                     Place::create([
    //                         'place_code' => $place->place_code,
    //                         'place_name' => $place->place_name,
    //                         'created_by' => $place->created_by,
    //                         'updated_by' => $place->updated_by ? $place->updated_by:NULL,
    //                         'created_at' => $place->created_at ? date('Y-m-d',strtotime($place->created_at)) : NULL,
    //                         'updated_at' => $place->updated_at ? date('Y-m-d',strtotime($place->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "preferences"://not tested or modified
    //                 dd($request);
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('preferences')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('preferences')->get();
    //                 foreach($table_data as $pre){
    //                     // Preference::create([
    //                     //     'purchase_order_prefix' => $pre->purchase_order_prefix,
    //                     //     'purchase_order_no_length' => $pre->purchase_order_no_length,
    //                     //     'purchase_order_pdf' => $pre->purchase_order_pdf,
    //                     //     'quotation_prefix' => $pre->quotation_prefix,
    //                     //     'quotation_no_length' => $pre->quotation_no_length,
    //                     //     'quotation_pdf' => $pre->quotation_pdf,
    //                     //     'proforma_prefix' => $pre->proforma_prefix,
    //                     //     'proforma_no_length' => $pre->proforma_no_length,
    //                     //     'proforma_pdf' => $pre->proforma_pdf,
    //                     //     'invoice_prefix' => $pre->invoice_prefix,
    //                     //     'invoice_no_length' => $pre->invoice_no_length,
    //                     //     'invoice_pdf' => $pre->invoice_pdf,
    //                     //     'delivery_pdf' => $pre->delivery_pdf,
    //                     //     'delivery_no_length' => $pre->delivery_no_length,
    //                     //     'delivery_pdf' => $pre->delivery_pdf,
    //                     //     'voucher_prefix' => $pre->voucher_prefix,
    //                     //     'voucher_no_length' => $pre->voucher_no_length,
    //                     //     'voucher_pdf' => $pre->voucher_pdf,
    //                     //     'receipt_prefix' => $pre->receipt_prefix,
    //                     //     'receipt_no_length' => $pre->receipt_no_length,
    //                     //     'receipt_pdf' => $pre->receipt_pdf,
    //                     //     'bill_return_prefix' => $pre->bill_return_prefix,
    //                     //     'bill_return_no_length' => $pre->bill_return_no_length,
    //                     //     'bill_return_pdf' => $pre->bill_return_pdf,
    //                     //     'invoice_return_prefix' => $pre->invoice_return_prefix,
    //                     //     'invoice_return_no_length' => $pre->invoice_return_no_length,
    //                     //     'invoice_return_pdf' => $pre->invoice_return_pdf,
    //                     //     'delivery_return_prefix' => $pre->delivery_return_prefix,
    //                     //     'delivery_return_no_length' => $pre->delivery_return_no_length,
    //                     //     'delivery_return_pdf' => $pre->delivery_return_pdf,
    //                     //     'fiscal_year' => $pre->fiscal_year,
    //                     //     'discount_on' => $pre->discount_on,
    //                     //     'inventory_on_calculation' => $pre->inventory_on_calculation,
    //                     //     'updated_by' => Auth::User()->username,         
    //                     // ]);
    //                 }
    //             break;
    //             case "products"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('products')->truncate();
    //                 $db_int->table('prices')->truncate();
    //                 $db_int->table('stores')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('products')->get();
    //                 foreach($table_data as $prod){
    //                     $category = $preference->fetchCategory($prod->category_id);
    //                     $product = Product::create([
    //                         'product_type' => $prod->product_type,
    //                         'product_code' => $prod->product_code,
    //                         'hsn_code'     => $prod->hsn_code,
    //                         'category_id'  => $category->category_id?$category->category_id:1,
    //                         'product_name' => $prod->product_name,
    //                         // 'product_name' => $prod->updated_by?$prod->updated_by.' '.$prod->product_name:$prod->product_name,
    //                         'product_unit' => $prod->product_unit,
    //                         'min_stock'    => $prod->min_stock?$prod->min_stock:0,
    //                         'vendor_id'    => $prod->contact_id,
    //                         'created_by'   => $prod->created_by,
    //                         'updated_by'   => $prod->updated_by ? $prod->updated_by:NULL,
    //                         'created_at'   => $prod->created_at ? date('Y-m-d',strtotime($prod->created_at)) : NULL,
    //                         'updated_at'   => $prod->updated_at ? date('Y-m-d',strtotime($prod->updated_at)) : NULL,
    //                     ]);

    //                     // $price = Price::create([
    //                     //     'product_id'        => $product->product_id,
    //                     //     'purchase_rate_exc' => 0,
    //                     //     'sales_rate_exc'    => 0,
    //                     //     'tax_id'            => 1,
    //                     //     'purchase_rate_inc' => 0,
    //                     //     'sales_rate_inc'    => 0,
    //                     //     'created_by'        => Null,
    //                     //     'updated_by'        => NULL,
    //                     //     'created_at'        => NULL,
    //                     //     'updated_at'        => NULL,
    //                     // ]);
    //                     // Store::create([
    //                     //     'product_id'         => $product->product_id,
    //                     //     'price_id'           => $price->price_id,
    //                     //     'warehouse_id'       => 1,
    //                     //     'opening_stock'      => 0,
    //                     //     'opening_stock_rate' => 0,
    //                     //     'created_by'         => Null,
    //                     //     'updated_by'         => NULL,
    //                     //     'created_at'         => NULL,
    //                     //     'updated_at'         => NULL,
    //                     // ]); 
    //                     $batches = $db_ext->table('batches')->where('product_id',$prod->product_id)->get();
    //                     foreach($batches as $batch){
    //                         $tax = $preference->fetchTax($batch->tax_id);
    //                         $price = Price::create([
    //                             'product_id'        => $product->product_id,
    //                             'purchase_rate_exc' => $batch->purchase_rate?$batch->purchase_rate:0,
    //                             'sales_rate_exc'    => $batch->sales_rate?$batch->sales_rate:0,
    //                             'tax_id'            => $tax?$tax->tax_id:1,
    //                             'purchase_rate_inc' => $batch->purchase_rate?$batch->purchase_rate:0,
    //                             'sales_rate_inc'    => $batch->mrp?$batch->mrp:0,
    //                             'created_by'        => $batch->created_by,
    //                             'updated_by'        => $batch->updated_by ? $batch->updated_by:NULL,
    //                             'created_at'        => $batch->created_at ? date('Y-m-d',strtotime($batch->created_at)) : NULL,
    //                             'updated_at'        => $batch->updated_at ? date('Y-m-d',strtotime($batch->updated_at)) : NULL,
    //                         ]);
    //                         $warehouses = Warehouse::get();
    //                         foreach($warehouses as $warehouse){
    //                             $ware = $preference->fetchWarehouse($warehouse->warehouse_id);
    //                             Store::create([
    //                                 'product_id'         => $product->product_id,
    //                                 'price_id'           => $price->price_id,
    //                                 'warehouse_id'       => $ware->warehouse_id,
    //                                 'opening_stock'      => $batch->opening_stock?$batch->opening_stock:0,
    //                                 'opening_stock_rate' => $batch->opening_stock_rate?$batch->opening_stock_rate:0,
    //                                 'created_by'         => $batch->created_by,
    //                                 'updated_by'         => $batch->updated_by ? $batch->updated_by:NULL,
    //                                 'created_at'         => $batch->created_at ? date('Y-m-d',strtotime($batch->created_at)) : NULL,
    //                                 'updated_at'         => $batch->updated_at ? date('Y-m-d',strtotime($batch->updated_at)) : NULL,
    //                             ]); 
    //                         } 
    //                     } 
    //                 }
    //             break;
    //             case "proformas"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('proforma_products')->truncate();
    //                 $db_int->table('proformas')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('proformas')->get();
    //                 foreach($table_data as $prof){
    //                     $contact = $preference->fetchContact($prof->contact_id);
    //                     $src = $preference->fetchPlace($prof->source_place);
    //                     $dest = $preference->fetchPlace($prof->destination_place);
    //                     $proforma = Proforma::create([
    //                         'fiscal_year'   => $prof->proforma_year,
    //                         'proforma_no'   => $prof->proforma_no,
    //                         'proforma_date' => date("Y-m-d", strtotime($prof->proforma_date)),
    //                         'reference_no'  => $prof->reference_no,
    //                         'reference_date'=> $prof->proforma_date ? date('Y-m-d',strtotime($prof->proforma_date)) : NULL,//no data for reference date
    //                         'customer_id'      => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'source_id'      => $src->place_id,
    //                         'destination_id' => $dest->place_id,
    //                         'sub_total'      => 0,
    //                         'discount_amount'=> 0,
    //                         'tax_amount'     => 0,
    //                         'total_amount'   => 0,
    //                         'round_off'      => 0,
    //                         'grand_total'    => 0,
    //                         // 'term_id'        => $term->term_id,
    //                         // 'terms'          => $term->term,
    //                         'note'           => $prof->note,
    //                         'proforma_status'=> $prof->proforma_status,
    //                         'created_by'     => $prof->created_by,
    //                         'updated_by'     => $prof->updated_by ? $prof->updated_by:NULL,
    //                         'created_at'     => $prof->created_at ? date('Y-m-d',strtotime($prof->created_at)) : NULL,
    //                         'updated_at'     => $prof->updated_at ? date('Y-m-d',strtotime($prof->updated_at)) : NULL,
    //                     ]);

    //                     $proforma_products = $db_ext->table('proforma_products')->where('proforma_id',$prof->proforma_id)->get();
    //                     foreach ($proforma_products as $proforma_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($proforma_product->category_id);
    //                         $tax = $preference->fetchTax($proforma_product->tax_id);
    //                         $product = $preference->fetchProduct($proforma_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($proforma_product->sales_rate,$proforma_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$proforma_product->discount,$proforma_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;
    //                         ProformaProduct::create([
    //                             'proforma_id'   => $proforma->proforma_id,
    //                             'product_id'    => $product->product_id,
    //                             'product_type'  => $proforma_product->product_type,
    //                             'product_code'  => $proforma_product->product_code,
    //                             'hsn_code'      => $proforma_product->hsn_code,
    //                             'category_id'   => $category->category_id,
    //                             'product_name'  => $proforma_product->product_name,
    //                             // 'description'   => $product->description,
    //                             'product_unit'  => $proforma_product->product_unit,
    //                             'price_id'      => $price->price_id,
    //                             'purchase_rate_exc' => $proforma_product->purchase_rate?$proforma_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $proforma_product->sales_rate?$proforma_product->sales_rate:0,
    //                             'purchase_rate_inc' => $proforma_product->purchase_rate?$proforma_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $proforma_product->mrp?$proforma_product->mrp:0,
    //                             'store_id'     => $store->store_id,
    //                             'warehouse_id' => $store->warehouse_id,
    //                             'quantity'     => $proforma_product->quantity,
    //                             'amount'       => $amount,
    //                             'discount'        => $proforma_product->discount,
    //                             'discount_type'   => $proforma_product->discount_type,
    //                             'discount_amount' => $discount_amount,
    //                             'tax_id'       => $tax->tax_id,
    //                             'tax_amount'   => $tax_amount,
    //                             'sub_total'    => $sub_total,
    //                             'created_by'   => $proforma_product->created_by,
    //                             'updated_by'   => $proforma_product->updated_by ? $proforma_product->updated_by:NULL,
    //                             'created_at'   => $proforma_product->created_at ? date('Y-m-d',strtotime($proforma_product->created_at)) : NULL,
    //                             'updated_at'   => $proforma_product->updated_at ? date('Y-m-d',strtotime($proforma_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('amount');
    //                     $tax_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('tax_amount');
    //                     $discount_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('discount_amount');
    //                     $total_amount = ProformaProduct::where('proforma_id',$proforma->proforma_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                     Proforma::where('proforma_id',$proforma->proforma_id)->update([
    //                         'sub_total'   => $sub_total,
    //                         'tax_amount'  => $tax_amount,
    //                         'discount_amount'  => $discount_amount,
    //                         'total_amount'=> $total_amount,
    //                         'round_off'   => $round_off,
    //                         'grand_total' => $grand_total,
    //                     ]);
    //                 }
    //             break;
    //             case "purchase_orders"://no data
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('purchase_order_products')->truncate();
    //                 $db_int->table('purchase_orders')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');

    //                 $table_data = $db_ext->table('purchase_orders')->get();
    //                 foreach($table_data as $purc){
    //                     $contact = $preference->fetchContact($purc->contact_id);
    //                     $src = $preference->fetchPlace($purc->source_place);
    //                     $dest = $preference->fetchPlace($purc->destination_place);
    //                     $purchase_order = PurchaseOrder::create([
    //                         'fiscal_year'        => $purc->purchase_order_year,
    //                         'purchase_order_no'  => $purc->purchase_order_no,
    //                         'purchase_order_date'=> date("Y-m-d", strtotime($purc->purchase_order_date)),
    //                         'reference_no'       => $purc->reference_no,
    //                         'delivery_date'      => $purc->delivery_date ? date('Y-m-d',strtotime($purc->delivery_date)) : NULL,
    //                         'vendor_id'          => $contact->contact_id,
    //                         'billing_address'    => $contact->billing_address,//new db
    //                         'shipping_address'   => $contact->shipping_address,//new db
    //                         'source_id'          => $src->place_id,
    //                         'destination_id'     => $dest->place_id,
    //                         'sub_total'          => 0,
    //                         'discount_amount'    => 0,
    //                         'tax_amount'         => 0,
    //                         'total_amount'       => 0,
    //                         'round_off'          => 0,
    //                         'grand_total'        => 0,
    //                         // 'term_id'            =>$purc->term_id,
    //                         // 'terms'              =>$purc->terms,
    //                         'note'               => $purc->note,
    //                         'purchase_order_status'=> $purc->purchase_order_status,
    //                         'created_by'         => $purc->created_by,
    //                         'updated_by'         => $purc->updated_by ? $purc->updated_by:NULL,
    //                         'created_at'         => $purc->created_at ? date('Y-m-d',strtotime($purc->created_at)) : NULL,
    //                         'updated_at'         => $purc->updated_at ? date('Y-m-d',strtotime($purc->updated_at)) : NULL,
    //                     ]);

    //                     $purchase_order_products = $db_ext->table('purchase_order_products')->where('purchase_order_id',$purc->purchase_order_id)->get();
    //                     foreach ($purchase_order_products as $purchase_order_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($purchase_order_product->category_id);
    //                         $tax = $preference->fetchTax($purchase_order_product->tax_id);
    //                         $product = $preference->fetchProduct($purchase_order_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($purchase_order_product->sales_rate,$purchase_order_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $sub_total = $amount+$tax_amount;
    //                         PurchaseOrderProduct::create([
    //                             'purchase_order_id' => $purchase_order->purchase_order_id,
    //                             'product_id'        => $product->product_id,
    //                             'product_type'      => $purchase_order_product->product_type,
    //                             'product_code'      => $purchase_order_product->product_code,
    //                             'hsn_code'          => $purchase_order_product->hsn_code,
    //                             'category_id'       => $category->category_id,
    //                             'product_name'      => $purchase_order_product->product_name,
    //                             // 'description'       => $purchase_order_product->description,
    //                             'product_unit'      => $purchase_order_product->product_unit,
    //                             'price_id'          => $price->price_id,
    //                             'purchase_rate_exc' => $purchase_order_product->purchase_rate?$purchase_order_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $purchase_order_product->sales_rate?$purchase_order_product->sales_rate:0,
    //                             'purchase_rate_inc' => $purchase_order_product->purchase_rate?$purchase_order_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $purchase_order_product->mrp?$purchase_order_product->mrp:0,
    //                             'store_id'          => $store->store_id,
    //                             'warehouse_id'      => $store->warehouse_id,
    //                             'quantity'          => $purchase_order_product->quantity,
    //                             'amount'            => $amount,
    //                             'discount'          => 0,
    //                             'discount_type'     => "%",
    //                             'discount_amount'   => 0,
    //                             'tax_id'            => $tax->tax_id,
    //                             'tax_amount'        => $tax_amount,
    //                             'sub_total'         => $sub_total,
    //                             'created_by'        => $purchase_order_product->created_by,
    //                             'updated_by'        => $purchase_order_product->updated_by ? $purchase_order_product->updated_by:NULL,
    //                             'created_at'        => $purchase_order_product->created_at ? date('Y-m-d',strtotime($purchase_order_product->created_at)) : NULL,
    //                             'updated_at'        => $purchase_order_product->updated_at ? date('Y-m-d',strtotime($purchase_order_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('amount');
    //                     $tax_amount = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('tax_amount');
    //                     $total_amount = PurchaseOrderProduct::where('purchase_order_id',$purchase_order->purchase_order_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                     PurchaseOrder::where('purchase_order_id',$purchase_order->purchase_order_id)->update([
    //                         'sub_total'   => $sub_total,
    //                         'tax_amount'  => $tax_amount,
    //                         'total_amount'=> $total_amount,
    //                         'round_off'   => $round_off,
    //                         'grand_total' => $grand_total,
    //                     ]);
    //                 }
    //             break;
    //             case "quotations"://no data
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('quotation_products')->truncate();
    //                 $db_int->table('quotations')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('quotations')->get();
    //                 foreach($table_data as $quot){
    //                     $contact = $preference->fetchContact($quot->contact_id);
    //                     $src = $preference->fetchPlace($quot->source_place);
    //                     $dest = $preference->fetchPlace($quot->destination_place);

    //                     $quotation = Quotation::create([
    //                         'fiscal_year'   => $quot->quotation_year,
    //                         'quotation_no'  => $quot->quotation_no,
    //                         'quotation_date'=> date("Y-m-d", strtotime($quot->quotation_date)),
    //                         'reference_no'  => $quot->reference_no,
    //                         'expiry_date'   => $quot->expiry_date ? date('Y-m-d',strtotime($quot->expiry_date)) : NULL,
    //                         'customer_id'   => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'source_id'      => $src->place_id,
    //                         'destination_id' => $dest->place_id,
    //                         'sub_total'      => 0,
    //                         'discount_amount'=> 0,
    //                         'tax_amount'     => 0,
    //                         'total_amount'   => 0,
    //                         'round_off'      => 0,
    //                         'grand_total'    => 0,
    //                         // 'term_id'=>$quot->term_id,
    //                         // 'terms'=>$quot->terms,
    //                         'note'            => $quot->note,
    //                         'quotation_status'=> $quot->quotation_status,
    //                         'created_by'      => $quot->created_by,
    //                         'updated_by'      => $quot->updated_by ? $quot->updated_by:NULL,
    //                         'created_at'      => $quot->created_at ? date('Y-m-d',strtotime($quot->created_at)) : NULL,
    //                         'updated_at'      => $quot->updated_at ? date('Y-m-d',strtotime($quot->updated_at)) : NULL,
    //                     ]);

    //                     $quotation_products = $db_ext->table('quotation_products')->where('quotation_id',$quot->quotation_id)->get();
    //                     foreach ($quotation_products as $quotation_product) 
    //                     {
    //                         //calculations
    //                         $category = $preference->fetchCategory($quotation_product->category_id);
    //                         $tax = $preference->fetchTax($quotation_product->tax_id);
    //                         $product = $preference->fetchProduct($quotation_product->product_id);
    //                         $price   = Price::where('product_id',$product->product_id)->first();
    //                         $store   = Store::where('product_id',$product->product_id)->where('price_id',$price->price_id)->first();
                            
    //                         $amount = $preference->amountCalc($quotation_product->sales_rate,$quotation_product->quantity);
    //                         $tax_amount = $preference->taxAmountCalc($amount,$tax->tax_rate);
    //                         $discount_amount = $preference->discountAmountCalc($amount,$quotation_product->discount,$quotation_product->discount_type);
    //                         $sub_total = $amount+$tax_amount-$discount_amount;
    //                         QuotationProduct::create([
    //                             'quotation_id' => $quotation->quotation_id,
    //                             'product_id'   => $product->product_id,
    //                             'product_type' => $quotation_product->product_type,
    //                             'product_code' => $quotation_product->product_code,
    //                             'hsn_code'     => $quotation_product->hsn_code,
    //                             'category_id'  => $category->category_id,
    //                             'product_name' => $quotation_product->product_name,
    //                             // 'description'  => $product->description,
    //                             'product_unit' => $quotation_product->product_unit,
    //                             'price_id'     => $price->price_id,
    //                             'purchase_rate_exc' => $quotation_product->purchase_rate?$quotation_product->purchase_rate:0,
    //                             'sales_rate_exc'    => $quotation_product->sales_rate?$quotation_product->sales_rate:0,
    //                             'purchase_rate_inc' => $quotation_product->purchase_rate?$quotation_product->purchase_rate:0,
    //                             'sales_rate_inc'    => $quotation_product->mrp?$quotation_product->mrp:0,
    //                             'store_id'        => $store->store_id,
    //                             'warehouse_id'    => $store->warehouse_id,
    //                             'quantity'        => $quotation_product->quantity,
    //                             'amount'          => $amount,
    //                             'discount'        => $quotation_product->discount,
    //                             'discount_type'   => $quotation_product->discount_type,
    //                             'discount_amount' => $discount_amount,
    //                             'tax_id'          => $tax->tax_id,
    //                             'tax_amount'      => $tax_amount,
    //                             'sub_total'       => $sub_total,
    //                             'created_by'      => $quotation_product->created_by,
    //                             'updated_by'      => $quotation_product->updated_by ? $quotation_product->updated_by:NULL,
    //                             'created_at'      => $quotation_product->created_at ? date('Y-m-d',strtotime($quotation_product->created_at)) : NULL,
    //                             'updated_at'      => $quotation_product->updated_at ? date('Y-m-d',strtotime($quotation_product->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $sub_total = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('amount');
    //                     $tax_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('tax_amount');
    //                     $discount_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('discount_amount');
    //                     $total_amount = QuotationProduct::where('quotation_id',$quotation->quotation_id)->sum('sub_total');
    //                     $grand_total = number_format(round((float)$total_amount,0),2,'.','');
    //                     $round_off = $grand_total-$total_amount;
    //                     Quotation::where('quotation_id',$quotation->quotation_id)->update([
    //                         'sub_total'   => $sub_total,
    //                         'tax_amount'  => $tax_amount,
    //                         'discount_amount'  => $discount_amount,
    //                         'total_amount'=> $total_amount,
    //                         'round_off'   => $round_off,
    //                         'grand_total' => $grand_total,
    //                     ]);
    //                 }
    //             break;
    //             case "receipts"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('receipt_particulars')->truncate();
    //                 $db_int->table('receipts')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('receipts')->get();
    //                 foreach($table_data as $rec){
    //                     $contact = $preference->fetchContact($rec->contact_id);
                        
    //                     $receipt = Receipt::create([
    //                         'fiscal_year'      => $preference->fiscal_year,
    //                         'customer_id'      => $contact->contact_id,
    //                         'billing_address'  => $contact->billing_address,
    //                         'shipping_address' => $contact->shipping_address,
    //                         'receipt_no'       => $rec->receipt_no,
    //                         'receipt_date'     => date("Y-m-d", strtotime($rec->receipt_date)),
    //                         'account_id'       => $rec->deposit_to,
    //                         'payment_mode'     => $rec->payment_mode,
    //                         'reference_no'     => $rec->reference_no,
    //                         'reference_date'   => $rec->receipt_date ? date('Y-m-d',strtotime($rec->receipt_date)) : NULL,
    //                         'total_amount'     => 0,
    //                         // 'term_id'=>$rec->term_id,
    //                         // 'terms'=>$rec->terms,
    //                         'note'=> $rec->note,
    //                         'created_by'      => $rec->created_by,
    //                         'updated_by'      => $rec->updated_by ? $rec->updated_by:NULL,
    //                         'created_at'      => $rec->created_at ? date('Y-m-d',strtotime($rec->created_at)) : NULL,
    //                         'updated_at'      => $rec->updated_at ? date('Y-m-d',strtotime($rec->updated_at)) : NULL,
    //                     ]);

    //                     $receipt_particulars = $db_ext->table('receipt_invoices')->where('receipt_id',$rec->receipt_id)->get();
    //                     foreach ($receipt_particulars as $receipt_particular) 
    //                     {
    //                         $inv = $preference->fetchInvoice($receipt_particular->invoice_id);
    //                         ReceiptParticular::create([
    //                             'receipt_id'   => $receipt->receipt_id,
    //                             'reference'    => 'App\Invoice',
    //                             'reference_id' => $inv->invoice_id,
    //                             'paid_amount'  => $receipt_particular->payment,
    //                             'created_by'   => $receipt_particular->created_by,
    //                             'updated_by'   => $receipt_particular->updated_by ? $receipt_particular->updated_by:NULL,
    //                             'created_at'   => $receipt_particular->created_at ? date('Y-m-d',strtotime($receipt_particular->created_at)) : NULL,
    //                             'updated_at'   => $receipt_particular->updated_at ? date('Y-m-d',strtotime($receipt_particular->updated_at)) : NULL,
    //                         ]);
    //                     }
    //                     //calculations
    //                     $total_amount=ReceiptParticular::where('receipt_id',$receipt->receipt_id)->sum('paid_amount');
    //                     Receipt::where('receipt_id',$receipt->receipt_id)->update([
    //                         'total_amount' => $total_amount,
    //                     ]);
    //                 }
    //             break;
    //             case "taxes"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('taxes')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('taxes')->get();
    //                 foreach($table_data as $tax){
    //                     Tax::create([
    //                         'tax_name'  => $tax->tax_name,
    //                         'tax_rate'  => $tax->tax_rate,
    //                         'cgst_name' => $tax->cgst_name,
    //                         'cgst_rate' => $tax->cgst_rate,
    //                         'sgst_name' => $tax->sgst_name,
    //                         'sgst_rate' => $tax->sgst_rate,
    //                         'igst_name' => $tax->igst_name,
    //                         'igst_rate' => $tax->igst_rate,
    //                         'created_by' => $tax->created_by,
    //                         'updated_by' => $tax->updated_by ? $tax->updated_by:NULL,
    //                         'created_at' => $tax->created_at ? date('Y-m-d',strtotime($tax->created_at)) : NULL,
    //                         'updated_at' => $tax->updated_at ? date('Y-m-d',strtotime($tax->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "terms"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('terms')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('terms')->get();
    //                 foreach($table_data as $term){
    //                     Term::create([
    //                         'term_type'  => $term->term_type,
    //                         'label'      => '', 
    //                         'term'       => $term->term,
    //                         'created_by' => $term->created_by,
    //                         'updated_by' => $term->updated_by ? $term->updated_by:NULL,
    //                         'created_at' => $term->created_at ? date('Y-m-d',strtotime($term->created_at)) : NULL,
    //                         'updated_at' => $term->updated_at ? date('Y-m-d',strtotime($term->updated_at)) : NULL,
    //                     ]);    
    //                 }
    //             break;
    //             case "users"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('users')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('users')->get();
    //                 foreach($table_data as $user){
    //                     User::create([
    //                         'name'       => $user->name,
    //                         'username'   => $user->username,
    //                         'email'      => $user->email,
    //                         'password'   => $user->password,
    //                         'user_role'  => $user->user_role,
    //                         'mobile_no'  => $user->mobile_no,
    //                         'address'    => $user->address,
    //                         'created_by' => $user->created_by,
    //                         'updated_by' => $user->updated_by ? $user->updated_by:NULL,
    //                         'created_at' => $user->created_at ? date('Y-m-d',strtotime($user->created_at)) : NULL,
    //                         'updated_at' => $user->updated_at ? date('Y-m-d',strtotime($user->updated_at)) : NULL,
    //                     ]);
    //                 }
    //             break;
    //             case "warehouses"://
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=0;');
    //                 $db_int->table('warehouses')->truncate();
    //                 $db_int->statement('SET FOREIGN_KEY_CHECKS=1;');
                    
    //                 $table_data = $db_ext->table('warehouses')->get();
    //                 foreach($table_data as $warehouse){
    //                     Warehouse::create([
    //                         'warehouse_name'     => $warehouse->warehouse_name,
    //                         'warehouse_location' => $warehouse->address?$warehouse->address.','.$warehouse->location:$warehouse->location,
    //                         'created_by' => $warehouse->created_by,
    //                         'updated_by' => $warehouse->updated_by ? $warehouse->updated_by:NULL,
    //                         'created_at' => $warehouse->created_at ? date('Y-m-d',strtotime($warehouse->created_at)) : NULL,
    //                         'updated_at' => $warehouse->updated_at ? date('Y-m-d',strtotime($warehouse->updated_at)) : NULL,    
    //                     ]);
    //                 }
    //             break;
    //             default: NULL;
    //         }
    //     }
    // }
}