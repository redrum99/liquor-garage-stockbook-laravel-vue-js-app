<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proforma;
use App\ProformaProduct;
use App\Preference;
use App\Setting;
use App\Organization;
use App\Account;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;

use Auth;
use PDF;

class ProformaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_proforma_no(Request $request)
    {
        $preference = Preference::first();
        $id = Proforma::where('fiscal_year',$preference->fiscal_year)->count('proforma_id');
        $id = $id + 1;
        $n = $preference->proforma_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $proforma_no = $preference->proforma_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $proforma_no;
    }


    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'sales_rate_exc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'proforma_no' => 'required|max:50',
            'proforma_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $proforma = Proforma::create([
            'fiscal_year' => $preference->fiscal_year,
            'proforma_no' => $request->proforma_no,
            'proforma_date'=> date("Y-m-d", strtotime($request->proforma_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'proforma_status'=>'Open',
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->proforma_products as $proforma_product) 
        {
            $product = Product::where('product_id',$proforma_product['product_id'])->first();
            $price = Price::where('price_id',$proforma_product['price_id'])->first();
            $store = Store::where('store_id',$proforma_product['store_id'])->first();

            ProformaProduct::create([
                'proforma_id' => $proforma->proforma_id,
                'product_id' => $proforma_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $proforma_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $proforma_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $proforma_product['price_id'],
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc' => $proforma_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc' => $proforma_product['sales_rate_inc'],
                'store_id' => $proforma_product['store_id'],
                'warehouse_id' => $proforma_product['warehouse_id'],
                'quantity' => $proforma_product['quantity'],
                'amount' => $proforma_product['amount'],
                'discount' => $proforma_product['discount'],
                'discount_type' => $proforma_product['discount_type'],
                'discount_amount' => $proforma_product['discount_amount'],
                'tax_id' => $proforma_product['tax_id'],
                'tax_amount' => $proforma_product['tax_amount'],
                'sub_total' => $proforma_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
        }
    }

    public function display(Request $request)
    {
        return Proforma::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('proforma_no', 'like', '%'.$request->search.'%')
            ->orWhere('proforma_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('proforma_id','DESC')
            ->paginate(10);
    }

    public function view(Proforma $proforma)
    {
        $proforma = Proforma::where('proforma_id',$proforma->proforma_id)->with(['ProformaProducts','Customer'])->first();
        $pre =Preference::first();
        $proforma->frontend_status = $pre->frontEndStatus($proforma,'Proforma',['InvoiceProductsCheck','DeliveryProductsCheck']);
        return $proforma;
    }

    public function destroy(Proforma $proforma)
    {
        ProformaProduct::where('proforma_id',$proforma->proforma_id)->delete();
        Proforma::where('proforma_id',$proforma->proforma_id)->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'proforma_no' => 'required|max:50',
            'proforma_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $proforma = Proforma::where('proforma_id',$request->proforma_id)->first();
        $proforma_products = Proforma::where('proforma_id',$request->proforma_id)->get();
        Proforma::where('proforma_id',$request->proforma_id)->update([
            'fiscal_year' => $proforma->fiscal_year,
            'proforma_no' => $request->proforma_no,
            'proforma_date'=> date("Y-m-d", strtotime($request->proforma_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $proforma->reference_date,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);

        foreach ($request->deleted_proforma_products as $proforma_product) 
        {
            ProformaProduct::where('proforma_product_id',$proforma_product['proforma_product_id'])->delete();
        }

        foreach ($request->proforma_products as $proforma_product) 
        {
            $product = Product::where('product_id',$proforma_product['product_id'])->first();
            $price = Price::where('price_id',$proforma_product['price_id'])->first();
            $store = Store::where('store_id',$proforma_product['store_id'])->first();

            if(empty($proforma_product['proforma_product_id'])) {
                ProformaProduct::create([
                    'proforma_id' => $proforma->proforma_id,
                    'product_id' => $proforma_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $proforma_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $proforma_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $proforma_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $proforma_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $proforma_product['sales_rate_inc'],
                    'store_id' => $proforma_product['store_id'],
                    'warehouse_id' => $proforma_product['warehouse_id'],
                    'quantity' => $proforma_product['quantity'],
                    'amount' => $proforma_product['amount'],
                    'discount' => $proforma_product['discount'],
                    'discount_type' => $proforma_product['discount_type'],
                    'discount_amount' => $proforma_product['discount_amount'],
                    'tax_id' => $proforma_product['tax_id'],
                    'tax_amount' => $proforma_product['tax_amount'],
                    'sub_total' => $proforma_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                ProformaProduct::where('proforma_product_id',$proforma_product['proforma_product_id'])->update([
                    'proforma_id' => $proforma->proforma_id,
                    'product_id' => $proforma_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $proforma_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $proforma_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $proforma_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $proforma_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $proforma_product['sales_rate_inc'],
                    'store_id' => $proforma_product['store_id'],
                    'warehouse_id' => $proforma_product['warehouse_id'],
                    'quantity' => $proforma_product['quantity'],
                    'amount' => $proforma_product['amount'],
                    'discount' => $proforma_product['discount'],
                    'discount_type' => $proforma_product['discount_type'],
                    'discount_amount' => $proforma_product['discount_amount'],
                    'tax_id' => $proforma_product['tax_id'],
                    'tax_amount' => $proforma_product['tax_amount'],
                    'sub_total' => $proforma_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $setting = Setting::first();
        if($request->customer_id=='')
        {
            $proformas = Proforma::
                whereBetween('proforma_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('ProformaProducts','Customer')->get();
            $customer = '';
        }
        else
        {
            $proformas = Proforma::
                where('customer_id',$request->customer_id)
                ->whereBetween('proforma_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('ProformaProducts','Customer')->get();
            $customer = Contact::where('contact_id',$request->customer_id)->first();
        }
        if($request->display_type=='display')
        {
            return $proformas;
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('proforma.report', compact('proformas','org','setting','request','customer'), [], [
                'margin_top' => 10
            ])->stream('proforma-report.pdf');
        }
        else
        {
            return view('proforma.report', compact('proformas','org','setting','request','customer'));
        }
    }
    public function convert_proforma(Request $request)
    {
        return ProformaProduct::whereIn('proforma_product_id',$request->input())->with(['Proforma','Tax'])->get();
    }
    public function pdf(proforma $proforma)
    {
        $org = Organization::first();

        $account = Account::orderBy('account_id','desc')->first();

        $preference = Preference::first();

        $default_pdf = 'proforma.'.$preference->proforma_pdf;
        if($preference->proforma_pdf == 'pdf1' )
        {
            $margin_top = 15;
        }
        elseif($preference->proforma_pdf == 'pdf2')
        {
            $margin_top = 41.8;
        }
        elseif($preference->proforma_pdf == 'pdf3')
        {
            $margin_top = 41.8;
        }
        elseif($preference->proforma_pdf == 'pdf4')
        {
            $margin_top = 41.8;
        }
        elseif($preference->proforma_pdf == 'pdf5')
        {
            $margin_top = 41.8;
        }
        else
        {
            $margin_top = 10;
        }

        // $proforma = Proforma::where('proforma_id',$proforma->proforma_id)->with('Customer','ProformaProducts','SourcePlace','DestinationPlace')->first();
        PDF::loadView($default_pdf, compact('org','preference','proforma','account'), [], [
            'margin_top' => $margin_top,
        ])->stream($proforma->proforma_no.'.pdf');
    }
}
