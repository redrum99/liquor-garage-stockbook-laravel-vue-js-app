<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Term;
use Auth;

class TermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Term::
        	where('term_type', 'like', '%'.$request->search.'%')
        	->orWhere('label', 'like', '%'.$request->search.'%')
        	->orWhere('term', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'term_type' => 'required|max:50',
            'label' => 'required|max:50',
            'term' => 'required',
        ]);
       	Term::create([
            'term_type' => $request->term_type,
            'label' => $request->label,  
            'term' => $request->term,
            'created_by' => Auth::User()->username,          
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'term_type' => 'required|max:50',
            'label' => 'required|max:50',
            'term' => 'required',
        ]);
        Term::where('term_id',$request->term_id)->update([
            'term_type' => $request->term_type,
            'label' => $request->label,  
            'term' => $request->term,
            'updated_by' => Auth::User()->username,              
        ]);
    }

    public function destroy(Term $term)
    {
    	return Term::where('term_id',$term->term_id)->delete();
    }

    public function get_terms(Request $request)
    {
        return Term::where('term_type',$request->term_type)->get();
    }
}
