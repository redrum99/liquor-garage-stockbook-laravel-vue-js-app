<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
 
//Includes WebClientPrint classes
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');
use Neodynamic\SDK\Web\WebClientPrint;

class PrintController extends Controller
{
    public function print(Product $product){
        $wcpScript = WebClientPrint::createScript(action('WebClientPrintController@processRequest'), action('PrintZPLController@printCommands'), Session::getId());    
 
        return view('print.preview', compact('wcpScript','product'));
    }
}
