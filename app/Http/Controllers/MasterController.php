<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Master;
use Auth;

class MasterController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function info(Request $request)
    // {
    //     phpinfo();
    // }

    public function display(Request $request)
    {
        return Master::
        	where('master_name', 'like', '%'.$request->search.'%')
        	->orWhere('master_value', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'master_name' => 'required|max:50',
            'master_value' => 'required|max:50|unique:masters',
        ]);

        return Master::create([
            'master_name' => $request->master_name,
            'master_value' => $request->master_value,
            'created_by' => Auth::User()->username,
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'master_name' => 'required|max:50',
            'master_value' => 'required|max:50',
        ]);

        return Master::where('master_id',$request->master_id)->update([
            'master_name' => $request->master_name,
            'master_value' => $request->master_value,
            'updated_by' => Auth::User()->username,
        ]);
    }

    public function destroy(Master $master)
    {
        Master::where('master_id',$master->master_id)->delete();
    }

    public function get_masters(Request $request)
    {
        return Master::where('master_name',$request->master_name)->get();
    }

}
