<?php

namespace App\Http\Controllers;

use PDF;
use App\Point;
use App\Contact;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Point::
        	where('amount', 'like', '%'.$request->search.'%')
        	->orWhere('points', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'amount'=> 'required|min:0|numeric|unique:points,amount,NULL,point_id,deleted_at,NULL',
            'to_amount'=> 'required|min:0|numeric|unique:points,to_amount,NULL,point_id,deleted_at,NULL',
            'points'=> 'required|min:0|numeric|unique:points,points,NULL,point_id,deleted_at,NULL',
        ]);

        return Point::create([
            'amount' => $request->amount,
            'to_amount' => $request->to_amount,
            'points' => $request->points,
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'amount'=>'required|min:0|numeric|unique:points,amount,'.$request->point_id.',point_id,deleted_at,NULL',
            'to_amount'=>'required|min:0|numeric|unique:points,to_amount,'.$request->point_id.',point_id,deleted_at,NULL',
            'points'=>'required|min:0|numeric|unique:points,points,'.$request->point_id.',point_id,deleted_at,NULL',
        ]);

        return Point::where('point_id',$request->point_id)->update([
            'amount' => $request->amount,
            'to_amount' => $request->to_amount,
            'points' => $request->points,
        ]);
    }

    public function destroy(Point $point)
    {
        Point::where('point_id',$point->point_id)->delete();
    }

    public function get_points()
    {
        return Point::get();
    }

    public function points_report(Request $request)
    {
        $org = Organization::first();
            $contact = '';
            $contacts = Contact::whereIn('contact_id',explode(",",$request->customer_id))->get();
        
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('invoice.points_report', compact('org','request','contacts','contact'))->stream('points-report.pdf');
                break;
            case 'excel':
                return view('invoice.points_report', compact('org','request','contacts','contact'));
                break;
            default:
                return view('invoice.points_report', compact('org','request','contacts','contact'));
                break;
        }
    }
}
