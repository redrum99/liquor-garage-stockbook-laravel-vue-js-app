<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\BillProduct;
use App\BillReturn;
use App\BillReturnProduct;
use App\Preference;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;
use App\Payment;
use App\PaymentParticular;
use App\PurchaseOrderProduct;
use App\PurchaseOrder;
use App\Setting;
use App\Account;
use Auth;
use PDF;

class BillReturnController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'purchase_rate_exc' => 'required|numeric|min:0',
            'purchase_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bill_return_no' => 'required|max:50',
            'bill_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'vendor_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->vendor_id)->first();
        $purchase_order_ids[] = '';

        $bill_return = BillReturn::create([
            'fiscal_year' => $preference->fiscal_year,
            'bill_return_no' => $request->bill_return_no,
            'bill_return_date'=> date("Y-m-d", strtotime($request->bill_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'vendor_id'=> $request->vendor_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'bill_return_status'=>'Open',
            'created_by' => Auth::User()->username,
        ]);

        $bill_ids[] = '';

        foreach ($request->bill_return_products as $bill_return_product) 
        {
            $product = Product::where('product_id',$bill_return_product['product_id'])->first();
            $price = Price::where('price_id',$bill_return_product['price_id'])->first();
            $store = Store::where('store_id',$bill_return_product['store_id'])->first();

            $bill_return_up = BillReturnProduct::create([
                'bill_return_id' => $bill_return->bill_return_id,
                'product_id' => $bill_return_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $bill_return_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $bill_return_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $bill_return_product['price_id'],
                'purchase_rate_exc' => $bill_return_product['purchase_rate_exc'],
                'sales_rate_exc' => $price->sales_rate_exc,
                'purchase_rate_inc' => $bill_return_product['purchase_rate_inc'],
                'sales_rate_inc' => $price->sales_rate_inc,
                'store_id' => $bill_return_product['store_id'],
                'warehouse_id' => $bill_return_product['warehouse_id'],
                'quantity' => $bill_return_product['quantity'],
                'amount' => $bill_return_product['amount'],
                'discount' => $bill_return_product['discount'],
                'discount_type' => $bill_return_product['discount_type'],
                'discount_amount' => $bill_return_product['discount_amount'],
                'tax_id' => $bill_return_product['tax_id'],
                'tax_amount' => $bill_return_product['tax_amount'],
                'sub_total' => $bill_return_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
            if (!empty($request->reference)) {
                BillReturnProduct::where('bill_return_product_id',$bill_return_up['bill_return_product_id'])->update([
                    'reference' => 'App'."\\".$request->reference,
                    'reference_id' => $bill_return_product['bill_product_id'],
                ]);

                $bill_id = BillProduct::where('bill_product_id',$bill_return_product['bill_product_id'])->first();

                $bill_length = BillProduct::where('bill_id',$bill_id['bill_id'])->count();
                array_push($bill_ids, $bill_id);
            }

        }
        if (!empty($request->reference)) {
            $size = count($bill_ids)-1;

            if($size == $bill_length ){
                Bill::where('bill_id' , $bill_id->bill_id )->update(['bill_status'=>'BillReturn']);
            }else{
                Bill::where('bill_id' , $bill_id->bill_id )->update(['bill_status'=>'PartiallyBillReturn']);
            }

            return compact('bill_length','size');
        }

    }

    public function view(BillReturn $bill_return)
    {
        return  BillReturn::where('bill_return_id',$bill_return->bill_return_id)->with('BillReturnProducts')->with('Vendor')->first();
    }

    public function destroy(BillReturn $bill_return)
    {
        BillReturnProduct::where('bill_return_id',$bill_return->bill_return_id)->delete();
        BillReturn::where('bill_return_id',$bill_return->bill_return_id)->delete();
    }

    public function display(Request $request)
    {
        return BillReturn::
            whereHas('Vendor', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('bill_return_no', 'like', '%'.$request->search.'%')
            ->orWhere('bill_return_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Vendor')
            ->orderBy('bill_return_id','DESC')
            ->paginate(10);
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'bill_return_no' => 'required|max:50',
            'bill_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'vendor_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->vendor_id)->first();
        $bill_return = BillReturn::where('bill_return_id',$request->bill_return_id)->first();
        //$bill_return_products = BillReturn::where('bill_return_id',$request->bill_return_id)->get();
        BillReturn::where('bill_return_id',$request->bill_return_id)->update([
            'fiscal_year' => $bill_return->fiscal_year,
            'bill_return_no' => $request->bill_return_no,
            'bill_return_date'=> date("Y-m-d", strtotime($request->bill_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $bill_return->reference_date,
            'vendor_id'=> $request->vendor_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);

        foreach ($request->deleted_bill_return_products as $bill_return_product) 
        {
            BillReturnProduct::where('bill_return_product_id',$bill_return_product['bill_return_product_id'])->delete();
        }

        foreach ($request->bill_return_products as $bill_return_product) 
        {
            $product = Product::where('product_id',$bill_return_product['product_id'])->first();
            $price = Price::where('price_id',$bill_return_product['price_id'])->first();
            $store = Store::where('store_id',$bill_return_product['store_id'])->first();

            if(empty($bill_return_product['bill_return_product_id'])) {
                BillReturnProduct::create([
                    'bill_return_id' => $bill_return->bill_return_id,
                    'product_id' => $bill_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $bill_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $bill_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $bill_return_product['price_id'],
                    'purchase_rate_exc' => $bill_return_product['purchase_rate_exc'],
                    'sales_rate_exc' => $price->sales_rate_exc,
                    'purchase_rate_inc' => $bill_return_product['purchase_rate_inc'],
                    'sales_rate_inc' => $price->sales_rate_inc,
                    'store_id' => $bill_return_product['store_id'],
                    'warehouse_id' => $bill_return_product['warehouse_id'],
                    'quantity' => $bill_return_product['quantity'],
                    'amount' => $bill_return_product['amount'],
                    'discount' => $bill_return_product['discount'],
                    'discount_type' => $bill_return_product['discount_type'],
                    'discount_amount' => $bill_return_product['discount_amount'],
                    'tax_id' => $bill_return_product['tax_id'],
                    'tax_amount' => $bill_return_product['tax_amount'],
                    'sub_total' => $bill_return_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                BillReturnProduct::where('bill_return_product_id',$bill_return_product['bill_return_product_id'])->update([
                    'bill_return_id' => $bill_return->bill_return_id,
                    'product_id' => $bill_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $bill_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $bill_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $bill_return_product['price_id'],
                    'purchase_rate_exc' => $bill_return_product['purchase_rate_exc'],
                    'sales_rate_exc' => $price->sales_rate_exc,
                    'purchase_rate_inc' => $bill_return_product['purchase_rate_inc'],
                    'sales_rate_inc' => $price->sales_rate_inc,
                    'store_id' => $bill_return_product['store_id'],
                    'warehouse_id' => $bill_return_product['warehouse_id'],
                    'quantity' => $bill_return_product['quantity'],
                    'amount' => $bill_return_product['amount'],
                    'discount' => $bill_return_product['discount'],
                    'discount_type' => $bill_return_product['discount_type'],
                    'discount_amount' => $bill_return_product['discount_amount'],
                    'tax_id' => $bill_return_product['tax_id'],
                    'tax_amount' => $bill_return_product['tax_amount'],
                    'sub_total' => $bill_return_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function convert_bill(Request $request)
    {
        //dd($request->input());
        $bill_product = BillProduct::whereIn('bill_product_id',$request->input())->with('Tax','Bill')->get();
        //dd($bill_product);
        return $bill_product;
    }
    // public function report(Request $request)
    // {
    //     $org = Organization::first();
    //     if($request->vendor_id=='')
    //     {
    //         $purchase_orders = PurchaseOrder::
    //             whereBetween('purchase_order_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
    //             ->with('PurchaseOrderProducts','Vendor')->get();
    //         $vendor = '';
    //     }
    //     else
    //     {
    //         $purchase_orders = PurchaseOrder::
    //             where('vendor_id',$request->vendor_id)
    //             ->whereBetween('purchase_order_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
    //             ->with('PurchaseOrderProducts','Vendor')->get();
    //         $vendor = Contact::where('contact_id',$request->vendor_id)->first();
    //     }
    //     if($request->display_type=='display')
    //     {
    //         return $purchase_orders;
    //     }
    //     else if($request->display_type=='pdf')
    //     {
    //         PDF::loadView('purchase_order.report', compact('purchase_orders','org','request','vendor'), [], [
    //             'margin_top' => 10
    //         ])->stream('purchase-order-report.pdf');
    //     }
    //     else
    //     {
    //         return view('purchase_order.report', compact('purchase_orders','org','request','vendor'));
    //     }
    // }

    public function report(Request $request)
    {
        //dd($request->input());
        $org = Organization::first();
        $setting = Setting::first();
        if($request->vendor_id=='')
        {
            $bill_returns = BillReturn::
                whereBetween('bill_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('BillReturnProducts')
                ->with('Vendor')->get();
            $vendor = '';
        }
        else
        {
            $bill_returns = BillReturn::
                where('vendor_id',$request->vendor_id)
                ->whereBetween('bill_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
               ->with('BillReturnProducts')
                ->with('Vendor')->get();
            $vendor = Contact::where('contact_id',$request->vendor_id)->first();
        }
        if($request->display_type=='display')
        {
            //dd($bill_returns);
            return $bill_returns;
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('bill_return.report', compact('bill_returns','org','request','vendor','setting'), [], [
                'margin_top' => 10
            ])->stream('bill-report.pdf');
        }
        else
        {
            return view('bill_return.report', compact('bill_returns','org','request','vendor','setting'));
        }
    }

    
    public function pdf(BillReturn $bill_return)
    {
        $org = Organization::first();
        $setting = Setting::first();
        $account = Account::orderBy('account_id','ASC')->first();
        $bill_return = BillReturn::where('bill_return_id',$bill_return->bill_return_id)->with('BillReturnProducts')->first();


        PDF::loadView('bill_return.pdf1', compact('bill_return','org','setting','account'), [], [
            'margin_top' => 15,
        ])->stream($bill_return->bill_return_no.'.pdf');
    }
}
