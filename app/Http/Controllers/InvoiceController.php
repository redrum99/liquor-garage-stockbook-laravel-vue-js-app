<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\InvoiceProduct;
use App\Preference;
use App\Setting;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;
use App\Tax;
use App\Receipt;
use App\ReceiptParticular;
use App\Account;
use App\PaymentTerm;
use App\Master;
use App\Point;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;

use Auth;
use PDF;

class InvoiceController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_invoice_no(Request $request)
    {
        $preference = Preference::first();
        $id = Invoice::where('fiscal_year',$preference->fiscal_year)->count('invoice_id');
        $id = $id + 1;
        $n = $preference->invoice_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $invoice_no = $preference->invoice_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $invoice_no;
    }


    public function validation(Request $request)
    {
      
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'sales_rate_exc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);

        $CurrentStock = Product::first()->CurrentStock($request->product_id);
        if($CurrentStock['opening_stock'] < $request->quantity) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'quantity' => ['Item out of stock'],
                ]
            ], 422);
        }
        
        return $request;
    }

    public function store(Request $request)
    {  
        $this->validate($request, [
            'invoice_no' => 'required|max:50',
            'invoice_date' => 'required|date',
            'invoice_type' => 'required|max:50',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
            'due_date' => 'required|date',
            'payment_term_id' => 'required|numeric',
        ]);

        $points = 0;
        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        // if(!empty($request->barcode))
        // {   
            $max_point = Point::where('amount','<>',$request->total_amount)
                            ->orderBy('amount','DES')
                            ->first();
            if($request->total_amount>=$max_point['points'])
            {
                $points = $max_point['points'];
            }
            $get_points = Point::where('amount','=',$request->total_amount)->first();
            if(empty($get_points))
            {
                $get_points = Point::where('amount','<',$request->total_amount)
                                    ->orderBy('amount','DESC')
                                    ->first();
                if(empty($get_points)){
                    $points = 0;
                }else{
                    $points = $get_points->points;
                }
            }elseif(!empty($get_points)){
                $points = $get_points->points;
            }
        // }
        // $highest_point = Point::where('amount','<=',$request->total_amount)->orderBy('amount','DES')->first();

        // $highest_point = Point::where([['amount','<=',$request->total_amount],['to_amount','>=',$request->total_amount]])->first();
        // if($highest_point) {
        //     $points = $highest_point['points'];
        // } else {
        //     $points = Point::orderBy('amount','desc')->pluck('points')->first();
        // }

        $invoice = Invoice::create([
            'fiscal_year' => $preference->fiscal_year,
            'invoice_no' => $request->invoice_no,
            'invoice_date'=> date("Y-m-d", strtotime($request->invoice_date)),
            'due_date'=> date("Y-m-d", strtotime($request->due_date)),
            'invoice_type' => $request->invoice_type,
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'payment_term_id'=> $request->payment_term_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'invoice_status'=>'Open',
            'created_by' => Auth::User()->username,
            'points' => $points,
            'barcode' => $request->barcode,
        ]);

        foreach ($request->invoice_products as $invoice_product) 
        {
            $product = Product::where('product_id',$invoice_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_product['price_id'])->first();
            $store = Store::where('store_id',$invoice_product['store_id'])->first();

            $invoice_product_up = InvoiceProduct::create([
                'invoice_id' => $invoice->invoice_id,
                'product_id' => $invoice_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $invoice_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $invoice_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $invoice_product['price_id'],
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc' => $invoice_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc' => $invoice_product['sales_rate_inc'],
                'store_id' => $invoice_product['store_id'],
                'warehouse_id' => $invoice_product['warehouse_id'],
                'quantity' => $invoice_product['quantity'],
                'amount' => $invoice_product['amount'],
                'discount' => $invoice_product['discount'],
                'discount_type' => $invoice_product['discount_type'],
                'discount_amount' => $invoice_product['discount_amount'],
                'tax_id' => $invoice_product['tax_id'],
                'tax_amount' => $invoice_product['tax_amount'],
                'sub_total' => $invoice_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
            if(!empty($request->reference)){
                InvoiceProduct::where('invoice_product_id',$invoice_product_up['invoice_product_id'])->update([
                    'reference' => 'App'."\\".$request->reference,
                    'reference_id' => $invoice_product[$request->reference_id],
                ]);
                //update quotation product status
            }
        }

        // if(!empty($invoice)){
        //     $this->small_print($invoice);
        // }

        if($request->due_date==$request->invoice_date){
            $id = Receipt::where('fiscal_year',$preference->fiscal_year)->max('receipt_id');
            $id = $id + 1;
            $n = $preference->receipt_no_length - strlen($id);
            for($i=0;$i<$n;$i++)
            {
                $id = '0'.$id;
            }
            $receipt_no = $preference->receipt_prefix.'/'.$id.'/'.$preference->fiscal_year;
        //return $receipt_no;
            $payment = Master::where('master_name','Payment Mode')->first();
            $account = Account::first();
            $receipt = Receipt::create([
                'fiscal_year' => $preference->fiscal_year,
                'customer_id'=> $request->customer_id,
                'billing_address' => $contact->billing_address,
                'shipping_address' => $contact->shipping_address,
                'receipt_no' => $receipt_no,
                'receipt_date'=> date("Y-m-d", strtotime($request->invoice_date)),
                'account_id' => $account['account_id'],
                'payment_mode' => $payment['master_value'],
                'reference_no'=> $request->reference_no,
                'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
                'total_amount'=> $request->grand_total,
                'term_id'=>$request->term_id,
                'terms'=>$request->terms,
                'note'=> $request->note,
                'created_by' => Auth::User()->username,
            ]);

            ReceiptParticular::create([
                    'receipt_id' => $receipt->receipt_id,
                    'reference' => 'App\Invoice',
                    'reference_id' => $invoice['invoice_id'],
                    'paid_amount' => $request->grand_total,
                    'created_by' => Auth::User()->username,
            ]);
        }
        return $invoice;
    }

    public function display(Request $request)
    {
        return Invoice::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('invoice_no', 'like', '%'.$request->search.'%')
            ->orWhere('invoice_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('invoice_id','DESC')
            ->paginate(10);
    }

    public function view(Invoice $invoice)
    {
        $invoice = Invoice::where('invoice_id',$invoice->invoice_id)->with(['InvoiceProducts','Customer'])->first();
        $pre = Preference::first();
        $invoice->frontend_status = $pre->frontEndStatus($invoice,'Invoice',['DeliveryProductsCheck','ReceiptParticularsCheck']);
        return $invoice;
    }

    public function destroy(Invoice $invoice)
    {
        InvoiceProduct::where('invoice_id',$invoice->invoice_id)->delete();
        Invoice::where('invoice_id',$invoice->invoice_id)->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'invoice_no' => 'required|max:50',
            'invoice_date' => 'required|date',
            'invoice_type' => 'required|max:50',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
            'due_date' => 'required|date',
            'payment_term_id' => 'required|numeric',
        ]);

        $points = 0;
        // if(!empty($request->barcode))
        // {   
            $max_point = Point::where('amount','<>',$request->total_amount)
                            ->orderBy('amount','DES')
                            ->first();
            if($request->total_amount>=$max_point['points'])
            {
                $points = $max_point['points'];
            }
            $get_points = Point::where('amount','=',$request->total_amount)->first();
            if(empty($get_points))
            {
                $get_points = Point::where('amount','<',$request->total_amount)
                                    ->orderBy('amount','DESC')
                                    ->first();
                if(empty($get_points)){
                    $points = 0;
                }else{
                    $points = $get_points->points;
                }
            }elseif(!empty($get_points)){
                $points = $get_points->points;
            }
        // }

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $invoice = Invoice::where('invoice_id',$request->invoice_id)->first();
        $invoice_products = Invoice::where('invoice_id',$request->invoice_id)->get();
        Invoice::where('invoice_id',$request->invoice_id)->update([
            'fiscal_year' => $invoice->fiscal_year,
            'invoice_no' => $request->invoice_no,
            'invoice_type' => $request->invoice_type,
            'invoice_date'=> date("Y-m-d", strtotime($request->invoice_date)),
            'due_date'=> date("Y-m-d", strtotime($request->due_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'payment_term_id'=> $request->payment_term_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
            'points' => $points,
            'barcode' => $request->barcode,
        ]);

        foreach ($request->deleted_invoice_products as $invoice_product) 
        {
            InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->delete();
        }

        foreach ($request->invoice_products as $invoice_product) 
        {
            $product = Product::where('product_id',$invoice_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_product['price_id'])->first();
            $store = Store::where('store_id',$invoice_product['store_id'])->first();

            if(empty($invoice_product['invoice_product_id'])) {
                InvoiceProduct::create([
                    'invoice_id' => $invoice->invoice_id,
                    'product_id' => $invoice_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $invoice_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $invoice_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $invoice_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $invoice_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $invoice_product['sales_rate_inc'],
                    'store_id' => $invoice_product['store_id'],
                    'warehouse_id' => $invoice_product['warehouse_id'],
                    'quantity' => $invoice_product['quantity'],
                    'amount' => $invoice_product['amount'],
                    'discount' => $invoice_product['discount'],
                    'discount_type' => $invoice_product['discount_type'],
                    'discount_amount' => $invoice_product['discount_amount'],
                    'tax_id' => $invoice_product['tax_id'],
                    'tax_amount' => $invoice_product['tax_amount'],
                    'sub_total' => $invoice_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->update([
                    'invoice_id' => $invoice->invoice_id,
                    'product_id' => $invoice_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $invoice_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $invoice_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $invoice_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $invoice_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $invoice_product['sales_rate_inc'],
                    'store_id' => $invoice_product['store_id'],
                    'warehouse_id' => $invoice_product['warehouse_id'],
                    'quantity' => $invoice_product['quantity'],
                    'amount' => $invoice_product['amount'],
                    'discount' => $invoice_product['discount'],
                    'discount_type' => $invoice_product['discount_type'],
                    'discount_amount' => $invoice_product['discount_amount'],
                    'tax_id' => $invoice_product['tax_id'],
                    'tax_amount' => $invoice_product['tax_amount'],
                    'sub_total' => $invoice_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function get_unpaid_invoices(Request $request)
    {
        $invoices =  Invoice::where('customer_id',$request->customer_id)->get();
        $result = [];
        foreach ($invoices as $invoice) 
        {
            $paid_amount = ReceiptParticular::where('reference_id',$invoice->invoice_id)->sum('paid_amount');
            $due_amount = $invoice->grand_total - $paid_amount;
            if($due_amount!=0)
            {
                $data = [
                    'check' => false,
                    'invoice_id' => $invoice->invoice_id,
                    'invoice_no' => $invoice->invoice_no,
                    'invoice_date' => $invoice->invoice_date,
                    'grand_total' => $invoice->grand_total,
                    'paid_amount' => $paid_amount,
                    'due_amount' => $due_amount,
                    'payable_amount' => 0.00,
                ];
                array_push($result, $data);
            }
        }
        return $result;
    }   
    public function convert_invoice(Request $request)
    {
        return InvoiceProduct::whereIn('invoice_product_id',$request->input())->with(['Invoice','Tax'])->get();
    }
    public function pdf(Invoice $invoice)
    {
        $org = Organization::first();
        $preference = Preference::first();
        PDF::loadView('invoice.small_pdf', compact('org','preference','invoice'), [], [
            'margin_top' => 5,
            'margin_left' => 5,
            'margin_right' => 36,
            'margin_bottom' => 5,
            'format' =>'A',
        ])->stream($invoice->invoice_no.'.pdf');
    }
    public function pdf1(Invoice $invoice)
    {
        $org = Organization::first();
        $preference = Preference::first();

        $account = Account::orderBy('account_id','ASC')->first();

        $default_pdf = 'invoice.'.$preference->invoice_pdf;
        if($preference->invoice_pdf == 'pdf1' )
        {
            $margin_top = 15;
        }
        elseif($preference->invoice_pdf == 'pdf2')
        {
            $margin_top = 88;
        }
        elseif($preference->invoice_pdf == 'pdf3')
        {
            $margin_top = 102;
        }
        elseif($preference->invoice_pdf == 'pdf4')
        {
            $margin_top = 54;
        }
        elseif($preference->invoice_pdf == 'pdf5')
        {
            $margin_top = 57;
        }
        else
        {
            $margin_top = 10;
        }

        // $invoice = Invoice::where('invoice_id',$invoice->invoice_id)->with('InvoiceProducts','Customer','SourcePlace','DestinationPlace')->first();

        PDF::loadView($default_pdf, compact('org','preference','invoice','account'), [], [
            'margin_top' => $margin_top,
        ])->stream($invoice->invoice_no.'.pdf');
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $setting = Setting::first();

        $query = Invoice::query();
        $customer = '';
        if($request->from_date!='' && $request->to_date!='') 
        {
            $query->whereBetween('invoice_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))]);
        }
        if($request->customer_id!='') 
        {
            $query->where('customer_id',$request->customer_id);
            $customer = Contact::where('contact_id',$request->customer_id)->first();
        }
        if($request->username!='') 
        {
            $query->where('created_by',$request->username);
        }
        $invoices = $query->with('InvoiceProducts','Customer')->get();

        if($request->display_type=='display')
        {
            return $invoices;
        }
        else if($request->display_type=='pdf')
        {
              PDF::loadView('invoice.report', compact('invoices','setting','org','request','customer'), [], [
                    'margin_top' => 10
                ])->stream('invoice-report.pdf');
        }
        else
        {
            return view('invoice.report', compact('invoices','setting','org','request','customer'));
        }
    }

    /**
     * Author: Ronald
     * Operation: Returns today's all invoices Total Amount
     */
    public function getReportTotalAmount(Request $request) {
        $today = date('Y-m-d');
        $query = Invoice::query();
        $query->whereBetween('invoice_date', [
            $today, 
            $today
        ]);

        $totalAmount = 0;
        $invoices = $query->get();
        foreach($invoices as $invoice) {
            $totalAmount = $totalAmount + $invoice->total_amount;
        }

        return $totalAmount;
    }

    public function tax_report(Request $request)
    {
        $org = Organization::first();
        $invoices = Invoice::
            whereBetween('invoice_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])->with(['InvoiceTax','Customer'])->get();

        if($request->display_type=='display')
        {
            return view('invoice.gst_report', compact('invoices','org','request'));
        }
        else if($request->display_type=='pdf')
        {
              PDF::loadView('invoice.gst_report', compact('invoices','org','request'), [], [
                    'margin_top' => 10
                ])->stream('invoice-gst-report.pdf');
        }
        else
        {
            return view('invoice.gst_report', compact('invoices','org','request'));
        }
    }

    public function message(Request $request)
    {
        $invoice = Invoice::where('invoice_id',$request->invoice_id)->first();
        $mobileNumber = $invoice->Contact->mobile_no;
        $senderId = 'UOMSOC';
        $message = 'Dear '.$invoice->Contact->contact_name.', '.'Thank you for purchasing from us. Kindly refer details for your Purchase order: '.'Invoice Number:'.$invoice->invoice_no.','.'Invoice Date:'.$invoice->invoice_date.','.'Total Amount:'.$invoice->GrandTotal($invoice->invoice_id);
        $route = 8;

        $postData = array(
            'username' => "mdbstech",
            'password' => "Mdbs@Tech@123",
            'senderid' => $senderId,
            'route' => $route,
            'number' => $mobileNumber,
            'message' => $message,
            'unicode' => 2
        );

        $url="http://bulksms.zobosoft.com/http-api.php";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));


        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);

        if(curl_errno($ch))
        {
            $error =  'error:' . curl_error($ch);
        }

        curl_close($ch);

        return $output;
    }

    public function check()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    public function small_print(Invoice $invoice)
    {
        $preference = Preference::first();
        $org = Organization::first();

        $request = Request();
        if(substr($request->root(),0,17) == "http://localhost/") 
        {
             $printer_name = $preference->printer_name;
             $connector = new NetworkPrintConnector("192.168.1.11", 9100);
        }
        else if($_SERVER['REMOTE_ADDR']=='192.168.1.20')
        {
             $connector = new NetworkPrintConnector("192.168.1.9", 9100); 
        }  
        else if($_SERVER['REMOTE_ADDR']=='192.168.1.21')
        {
            $connector = new NetworkPrintConnector("192.168.1.10", 9100);
        }
        else if($_SERVER['REMOTE_ADDR']=='192.168.1.25')
        {
             $connector = new NetworkPrintConnector("192.168.1.12", 9100); 
        } 
        else 
        {
            $connector = new NetworkPrintConnector("192.168.1.11", 9100); 
        }

        // $printer_name = $preference->printer_name;
        // $connector = new WindowsPrintConnector($printer_name);
        // $connector = new NetworkPrintConnector("192.168.1.10", 9100);
        $printer = new Printer($connector);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->setFont(Printer::FONT_A);
        $printer->setTextSize(2,2);
        $printer->text($org->org_name."\n");
        $printer->text($org->org_code."\n");
        $printer->selectPrintMode(Printer::MODE_FONT_A);
        $printer->setFont(Printer::FONT_A);
        $printer->text($org->address."\n");
        // $printer->text($org->address2."\n");
        if($org->gstin_no!=''){
            $printer->text("GSTIN - ".$org->gstin_no."\n");
        }
        if($org->mobile_no!=''){
            $printer->text("Mobile - ".$org->mobile_no."\n");
        }
        if($org->phone_no!=''){
            $printer->text(" Phone  - ".$org->phone_no."\n");
        }
        $printer->text("................................................\n");
        $printer->text("* * * INVOICE * * * \n");
        $printer->text("................................................\n");
        $printer->setJustification();
        $printer->text(" Invoice No   : " .$invoice->invoice_no."\n");
        $printer->text(" Invoice Date : " .date('d-m-Y',strtotime($invoice->invoice_date))."\n");
        $printer->text(" Invoice Time : " .date('h:i A',strtotime($invoice->created_at))."\n");
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setJustification();
        $printer->text(" Name         : ".$invoice->Customer->contact_name."\n");
        if($invoice->Customer->mobile_no!=''){
            $printer->text(" Mobile No    : ".$invoice->Customer->mobile_no."\n");
        }
        if($invoice->Customer->gstin_no!=''){
            $printer->text(" GSTIN        : ".$invoice->Customer->gstin_no."\n");
        }
        $printer->text(" Payment Term : ".$invoice->PaymentTerm->payment_term."\n");
        $printer->text("............................................\n");
        $printer->text(" Items                 Qty  Rate     Total \n");
        $printer->text("................................................\n");
        $total = 0;
        $final_discount = 0; 
        foreach($invoice->InvoiceProducts as $invoice_product)
        {
            $price_total= round($invoice_product->sales_rate_exc * $invoice_product->quantity,2);
            $a = strlen($invoice_product->product_name);
            if($a>=23)
            {
                $a = 23;
            }
            $a = 23-$a;
            $a = str_repeat(" ", $a);
            $b = strlen($invoice_product->quantity);
            if($b>=3)
            {
                $b = 3;
            }
            $b = 3-$b;
            $b = str_repeat(" ", $b);
            $c = strlen($invoice_product->sales_rate_exc);
            if($c>=7)
            {
                $c = 7;
            }
            $c = 7-$c;
            $c = str_repeat(" ", $c);
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text($invoice_product->product_name.$a);
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text($invoice_product->quantity.$b);
            $printer->text(number_format($invoice_product->sales_rate_exc,2).$c);
            $printer->text(number_format($price_total,2));
            $printer->setJustification();
            $printer->feed();
            $total += $price_total;
            $final_discount +=$invoice_product->discount_amount;
        }
        $printer->text("................................................\n");
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $printer->selectPrintMode(Printer::MODE_FONT_A);
        $printer->setFont(Printer::FONT_A);
        $printer->text("Sub Total : " .number_format($total,2) . "\n");
        if(!$final_discount == 0)
        {
            $printer->text("Discount : " .number_format($final_discount,2) . "\n");
        }
        foreach($invoice->Taxes($invoice->invoice_id) as $tax)
        {
            $taxable_value = $invoice->TaxableValue($invoice->invoice_id,$tax->tax_id);
            $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
            $sgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
            $igst = round(($taxable_value * $tax->Tax->igst_rate)/100,2);

            if($invoice->source_id!=$invoice->destination_id)
            {
                $gst = $cgst + $sgst;
                $printer->text("IGST @ ". $tax->Tax->igst_name . " : " .number_format($gst,2) . "\n");
            }
            else
            {
                if($tax->Tax->cgst_rate !='0' && $tax->Tax->cgst_rate !='0')
                {
                    $printer->text("CGST @ ". $tax->Tax->cgst_name . " : " .number_format($cgst,2) . "\n");
                    $printer->text("SGST @ ". $tax->Tax->sgst_name . " : " .number_format($sgst,2) . "\n");
                }
            }
        }
        $pf = $invoice->pf_amount;
        $sub_total = $invoice->SubTotal($invoice->invoice_id);
        $total_tax = $invoice->TotalTax($invoice->invoice_id);
        $total = $sub_total + $total_tax -$pf;
        $grand_total = round($total-$final_discount);
        $round_off = $grand_total - $total;
        if(!$pf == 0)
        {
            $printer->text("Discount : " .number_format($pf,2) . "\n");
        }
        $printer->text("................................................\n");
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->setFont(Printer::FONT_A);
        $printer->setTextSize(2,2);
        $printer->text("Rs. ".number_format($grand_total,2)."\n");
        $printer->selectPrintMode(Printer::MODE_FONT_A);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setFont(Printer::FONT_A);
        $printer->text("................................................\n");
        $printer->text(" * * No Exchange, No Credit, No Bargain * * \n");
        $printer->text("................................................\n");
        $printer->text(" * * THANK YOU VISIT AGAIN * * \n");
        // $printer->text("................................................\n");
        $printer -> cut();
        $printer -> close();
        return redirect()->back();
    }

    public function add_points(Request $request)
    {
         $this->validate($request, [
            'contact_id' => 'required|numeric',
            'points' => 'required|numeric',
        ]);
        $invoice = Invoice::where('customer_id',$request->contact_id)->orderBy('invoice_id','DESC')->first();
        if($invoice)
        {
            $points = $invoice->points + $request->points;
            Invoice::where('invoice_id',$invoice->invoice_id)->update([
                'points' => $points,
                'note' => 'Manual Points - Added By '.Auth::User()->name,
            ]);
        }
        else
        {
            return response()->json([
                'errors' => [
                    'points' => ['Please generate invoice to add points'],
                ]
            ], 422);
        }
    }
}
