<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use App\QuotationProduct;
use App\Preference;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;
use App\Setting;
use App\Account;
use Auth;
use PDF;

class QuotationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_quotation_no(Request $request)
    {
        $preference = Preference::first();
        $id = Quotation::where('fiscal_year',$preference->fiscal_year)->count('quotation_id');
        $id = $id + 1;
        $n = $preference->quotation_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $quotation_no = $preference->quotation_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $quotation_no;
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'sales_rate_exc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'quotation_no' => 'required|max:50',
            'quotation_date' => 'required|date',
            'reference_no' => 'max:50',
            'expiry_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $quotation = Quotation::create([
            'fiscal_year' => $preference->fiscal_year,
            'quotation_no' => $request->quotation_no,
            'quotation_date'=> date("Y-m-d", strtotime($request->quotation_date)),
            'reference_no'=> $request->reference_no,
            'expiry_date'=> $request->expiry_date ? date('Y-m-d',strtotime($request->expiry_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'quotation_status'=>'Open',
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->quotation_products as $quotation_product) 
        {
            $product = Product::where('product_id',$quotation_product['product_id'])->first();
            $price = Price::where('price_id',$quotation_product['price_id'])->first();
            $store = Store::where('store_id',$quotation_product['store_id'])->first();

            QuotationProduct::create([
                'quotation_id' => $quotation->quotation_id,
                'product_id' => $quotation_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $quotation_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $quotation_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $quotation_product['price_id'],
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc' => $quotation_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc' => $quotation_product['sales_rate_inc'],
                'store_id' => $quotation_product['store_id'],
                'warehouse_id' => $quotation_product['warehouse_id'],
                'quantity' => $quotation_product['quantity'],
                'amount' => $quotation_product['amount'],
                'discount' => $quotation_product['discount'],
                'discount_type' => $quotation_product['discount_type'],
                'discount_amount' => $quotation_product['discount_amount'],
                'tax_id' => $quotation_product['tax_id'],
                'tax_amount' => $quotation_product['tax_amount'],
                'sub_total' => $quotation_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
        }
    }

    public function display(Request $request)
    {
        return Quotation::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('quotation_no', 'like', '%'.$request->search.'%')
            ->orWhere('quotation_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('quotation_id','DESC')
            ->paginate(10);
    }

    public function view(Quotation $quotation)
    {
        $quotation =  Quotation::where('quotation_id',$quotation->quotation_id)->with('QuotationProducts')->with('Customer')->first();
        $pre =Preference::first();
        $quotation->frontend_status = $pre->frontEndStatus($quotation,'Quotation',['InvoiceProductsCheck']);
        return $quotation;
    }

    public function destroy(Quotation $quotation)
    {
        QuotationProduct::where('quotation_id',$quotation->quotation_id)->delete();
        Quotation::where('quotation_id',$quotation->quotation_id)->delete();
    }

     public function update(Request $request)
    {
        $this->validate($request, [
            'quotation_no' => 'required|max:50',
            'quotation_date' => 'required|date',
            'reference_no' => 'max:50',
            'expiry_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $quotation = Quotation::where('quotation_id',$request->quotation_id)->first();
        $quotation_products = Quotation::where('quotation_id',$request->quotation_id)->get();
        Quotation::where('quotation_id',$request->quotation_id)->update([
            'fiscal_year' => $quotation->fiscal_year,
            'quotation_no' => $request->quotation_no,
            'quotation_date'=> date("Y-m-d", strtotime($request->quotation_date)),
            'reference_no'=> $request->reference_no,
            'expiry_date'=> $request->expiry_date ? date('Y-m-d',strtotime($request->expiry_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);

        foreach ($request->deleted_quotation_products as $quotation_product) 
        {
            QuotationProduct::where('quotation_product_id',$quotation_product['quotation_product_id'])->delete();
        }

        foreach ($request->quotation_products as $quotation_product) 
        {
            $product = Product::where('product_id',$quotation_product['product_id'])->first();
            $price = Price::where('price_id',$quotation_product['price_id'])->first();
            $store = Store::where('store_id',$quotation_product['store_id'])->first();

            if(empty($quotation_product['quotation_product_id'])) {
                QuotationProduct::create([
                    'quotation_id' => $quotation->quotation_id,
                    'product_id' => $quotation_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $quotation_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $quotation_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $quotation_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $quotation_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $quotation_product['sales_rate_inc'],
                    'store_id' => $quotation_product['store_id'],
                    'warehouse_id' => $quotation_product['warehouse_id'],
                    'quantity' => $quotation_product['quantity'],
                    'amount' => $quotation_product['amount'],
                    'discount' => $quotation_product['discount'],
                    'discount_type' => $quotation_product['discount_type'],
                    'discount_amount' => $quotation_product['discount_amount'],
                    'tax_id' => $quotation_product['tax_id'],
                    'tax_amount' => $quotation_product['tax_amount'],
                    'sub_total' => $quotation_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                QuotationProduct::where('quotation_product_id',$quotation_product['quotation_product_id'])->update([
                    'quotation_id' => $quotation->quotation_id,
                    'product_id' => $quotation_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $quotation_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $quotation_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $quotation_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $quotation_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $quotation_product['sales_rate_inc'],
                    'store_id' => $quotation_product['store_id'],
                    'warehouse_id' => $quotation_product['warehouse_id'],
                    'quantity' => $quotation_product['quantity'],
                    'amount' => $quotation_product['amount'],
                    'discount' => $quotation_product['discount'],
                    'discount_type' => $quotation_product['discount_type'],
                    'discount_amount' => $quotation_product['discount_amount'],
                    'tax_id' => $quotation_product['tax_id'],
                    'tax_amount' => $quotation_product['tax_amount'],
                    'sub_total' => $quotation_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $setting = Setting::first();
        if($request->customer_id=='')
        {
            $quotations = Quotation::
                whereBetween('quotation_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('QuotationProducts','Customer')->get();
            $customer = '';
        }
        else
        {
            $quotations = Quotation::
                where('customer_id',$request->customer_id)
                ->whereBetween('quotation_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('QuotationProducts','Customer')->get();
            $customer = Contact::where('contact_id',$request->customer_id)->first();
        }
        if($request->display_type=='display')
        {
            return $quotations;
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('quotation.report', compact('quotations','org','request','customer','setting'), [], [
                'margin_top' => 10
            ])->stream('quotation-report.pdf');
        }
        else
        {
            return view('quotation.report', compact('quotations','org','request','customer','setting'));
        }
    }
    public function convert_quotation(Request $request)
    {
        return QuotationProduct::whereIn('quotation_product_id',$request->input())->with(['Quotation','Tax'])->get();
    }
    public function pdf(Quotation $quotation)
    {
        $org = Organization::first();
        $setting = Setting::first();
         $account = Account::orderBy('account_id','ASC')->first();
        $preference = Preference::first();
        $default_pdf = 'quotation.'.$preference->quotation_pdf;
        if($preference->quotation_pdf == 'pdf1' )
        {
            $margin_top = 15;
        }
        elseif($preference->quotation_pdf == 'pdf2')
        {
            $margin_top = 41.8;
        }
        elseif($preference->quotation_pdf == 'pdf3')
        {
            $margin_top = 41.8;
        }
        elseif($preference->quotation_pdf == 'pdf4')
        {
            $margin_top = 41.8;
        }
        elseif($preference->quotation_pdf == 'pdf5')
        {
            $margin_top = 41.8;
        }
        else
        {
            $margin_top = 10;
        }

        // $quotation = Quotation::where('quotation_id',$quotation->quotation_id)->with('Customer','QuotationProducts','SourcePlace','DestinationPlace')->first();
        PDF::loadView($default_pdf, compact('org','preference','quotation','setting','account'), [], [
            'margin_top' => $margin_top,
        ])->stream($quotation->quotation_no.'.pdf');
    }
}
