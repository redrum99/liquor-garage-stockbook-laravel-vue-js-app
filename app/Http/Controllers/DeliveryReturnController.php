<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryReturn;
use App\DeliveryReturnProduct;
use App\Delivery;
use App\DeliveryProduct;
use App\Preference;
use App\Setting;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
use App\Store;
use App\Account;
use Auth;
use PDF;

class DeliveryReturnController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_delivery_return_no(Request $request)
    {
        $preference = Preference::first();
        $id = DeliveryReturn::where('fiscal_year',$preference->fiscal_year)->count('delivery_return_id');
        $id = $id + 1;
        $n = $preference->delivery_return_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $delivery_return_no = $preference->delivery_return_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $delivery_return_no;
    }


    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'product_code' => 'required|max:50',
            'product_name' => 'required|max:2550',
            'price_id' => 'required|numeric',
            'sales_rate_exc' => 'required|numeric|min:0',
            'sales_rate_inc' => 'required|numeric|min:0',
            'store_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'discount' => 'required|numeric|min:0',
            'discount_type' => 'required|max:50',
            'discount_amount' => 'required|numeric|min:0',
            'tax_id' => 'required|numeric',
            'tax_amount' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
            'sub_total' => 'required|numeric|min:0',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'delivery_return_no' => 'required|max:50',
            'delivery_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $delivery_return = DeliveryReturn::create([
            'fiscal_year' => $preference->fiscal_year,
            'delivery_return_no' => $request->delivery_return_no,
            'delivery_return_date'=> date("Y-m-d", strtotime($request->delivery_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'delivery_return_status'=>'Open',
            'created_by' => Auth::User()->username,
        ]);

        foreach ($request->delivery_return_products as $delivery_return_product) 
        {
            $product = Product::where('product_id',$delivery_return_product['product_id'])->first();
            $price = Price::where('price_id',$delivery_return_product['price_id'])->first();
            $store = Store::where('store_id',$delivery_return_product['store_id'])->first();

            $delivery_return_product_up = deliveryReturnProduct::create([
                'delivery_return_id' => $delivery_return->delivery_return_id,
                'product_id' => $delivery_return_product['product_id'],
                'product_type' => $product->product_type,
                'product_code' => $delivery_return_product['product_code'],
                'hsn_code' => $product->hsn_code,
                'category_id' => $product->category_id,
                'product_name' => $delivery_return_product['product_name'],
                'description' => $product->description,
                'product_unit' => $product->product_unit,
                'price_id' => $delivery_return_product['price_id'],
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc' => $delivery_return_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc' => $delivery_return_product['sales_rate_inc'],
                'store_id' => $delivery_return_product['store_id'],
                'warehouse_id' => $delivery_return_product['warehouse_id'],
                'quantity' => $delivery_return_product['quantity'],
                'amount' => $delivery_return_product['amount'],
                'discount' => $delivery_return_product['discount'],
                'discount_type' => $delivery_return_product['discount_type'],
                'discount_amount' => $delivery_return_product['discount_amount'],
                'tax_id' => $delivery_return_product['tax_id'],
                'tax_amount' => $delivery_return_product['tax_amount'],
                'sub_total' => $delivery_return_product['sub_total'],
                'created_by' => Auth::User()->username,
            ]);
            if(!empty($request->reference)){
                DeliveryReturnProduct::where('delivery_return_product_id',$delivery_return_product_up['delivery_return_product_id'])->update([
                    'reference' => 'App'."\\".$request->reference,
                    'reference_id' => $delivery_return_product[$request->reference_id],
                ]);
                //update quotation product status
            }
        }
    }

    public function display(Request $request)
    {
        return DeliveryReturn::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('delivery_return_no', 'like', '%'.$request->search.'%')
            ->orWhere('delivery_return_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('delivery_return_id','DESC')
            ->paginate(10);
    }

    public function view(DeliveryReturn $delivery_return)
    {
        return  DeliveryReturn::where('delivery_return_id',$delivery_return->delivery_return_id)->with('DeliveryReturnProducts')->with('Customer')->first();
    }

    public function destroy(DeliveryReturn $delivery_return)
    {
        DeliveryReturnProduct::where('delivery_return_id',$delivery_return->delivery_return_id)->delete();
        DeliveryReturn::where('delivery_return_id',$delivery_return->delivery_return_id)->delete();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'delivery_return_no' => 'required|max:50',
            'delivery_return_date' => 'required|date',
            'reference_no' => 'max:50',
            'reference_date' => 'sometimes|nullable|date',
            'customer_id' => 'required|numeric',
            'source_id' => 'required|numeric',
            'destination_id' => 'required|numeric',
            'sub_total' =>'required|numeric|min:0',
            'discount_amount' =>'required|numeric|min:0',
            'tax_amount' =>'required|numeric|min:0',
            'round_off' =>'required|numeric',
            'total_amount' =>'required|numeric|min:0',
            'grand_total' =>'required|numeric|min:0',
            'term_id' => 'sometimes|nullable|numeric',
            'terms' => 'max:2550',
            'note' => 'max:2550',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $delivery_return = DeliveryReturn::where('delivery_return_id',$request->delivery_return_id)->first();
        $delivery_return_products = DeliveryReturn::where('delivery_return_id',$request->delivery_return_id)->get();
        DeliveryReturn::where('delivery_return_id',$request->delivery_return_id)->update([
            'fiscal_year' => $delivery_return->fiscal_year,
            'delivery_return_no' => $request->delivery_return_no,
            'delivery_return_date'=> date("Y-m-d", strtotime($request->delivery_return_date)),
            'reference_no'=> $request->reference_no,
            'reference_date'=> $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $delivery_return->reference_date,
            'customer_id'=> $request->customer_id,
            'billing_address' => $contact->billing_address,
            'shipping_address' => $contact->shipping_address,
            'source_id'=> $request->source_id,
            'destination_id'=> $request->destination_id,
            'sub_total'=> $request->sub_total,
            'discount_amount'=> $request->discount_amount,
            'tax_amount'=> $request->tax_amount,
            'total_amount'=> $request->total_amount,
            'round_off'=> $request->round_off,
            'grand_total'=> $request->grand_total,
            'term_id'=>$request->term_id,
            'terms'=>$request->terms,
            'note'=> $request->note,
            'updated_by' => Auth::User()->username,
        ]);
        foreach ($request->deleted_delivery_return_products as $delivery_return_product) 
        {
            DeliveryReturnProduct::where('delivery_return_product_id',$delivery_return_product['delivery_return_product_id'])->delete();
        }

        foreach ($request->delivery_return_products as $delivery_return_product) 
        {
            $product = Product::where('product_id',$delivery_return_product['product_id'])->first();
            $price = Price::where('price_id',$delivery_return_product['price_id'])->first();
            $store = Store::where('store_id',$delivery_return_product['store_id'])->first();

            if(empty($delivery_return_product['delivery_return_product_id'])) {
                DeliveryReturnProduct::create([
                    'delivery_return_id' => $delivery_return->delivery_return_id,
                    'product_id' => $delivery_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $delivery_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $delivery_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $delivery_return_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $delivery_return_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $delivery_return_product['sales_rate_inc'],
                    'store_id' => $delivery_return_product['store_id'],
                    'warehouse_id' => $delivery_return_product['warehouse_id'],
                    'quantity' => $delivery_return_product['quantity'],
                    'amount' => $delivery_return_product['amount'],
                    'discount' => $delivery_return_product['discount'],
                    'discount_type' => $delivery_return_product['discount_type'],
                    'discount_amount' => $delivery_return_product['discount_amount'],
                    'tax_id' => $delivery_return_product['tax_id'],
                    'tax_amount' => $delivery_return_product['tax_amount'],
                    'sub_total' => $delivery_return_product['sub_total'],
                    'created_by' => Auth::User()->username,
                ]);
            }
            else
            {
                DeliveryReturnProduct::where('delivery_return_product_id',$delivery_return_product['delivery_return_product_id'])->update([
                    'delivery_return_id' => $delivery_return->delivery_return_id,
                    'product_id' => $delivery_return_product['product_id'],
                    'product_type' => $product->product_type,
                    'product_code' => $delivery_return_product['product_code'],
                    'hsn_code' => $product->hsn_code,
                    'category_id' => $product->category_id,
                    'product_name' => $delivery_return_product['product_name'],
                    'description' => $product->description,
                    'product_unit' => $product->product_unit,
                    'price_id' => $delivery_return_product['price_id'],
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc' => $delivery_return_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc' => $delivery_return_product['sales_rate_inc'],
                    'store_id' => $delivery_return_product['store_id'],
                    'warehouse_id' => $delivery_return_product['warehouse_id'],
                    'quantity' => $delivery_return_product['quantity'],
                    'amount' => $delivery_return_product['amount'],
                    'discount' => $delivery_return_product['discount'],
                    'discount_type' => $delivery_return_product['discount_type'],
                    'discount_amount' => $delivery_return_product['discount_amount'],
                    'tax_id' => $delivery_return_product['tax_id'],
                    'tax_amount' => $delivery_return_product['tax_amount'],
                    'sub_total' => $delivery_return_product['sub_total'],
                    'updated_by' => Auth::User()->username,
                ]);
            }
        }
    }

    public function report(Request $request)
    {
        $org = Organization::first();
        $setting = Setting::first();
        if($request->customer_id=='')
        {
            $delivery_returns = DeliveryReturn::
                whereBetween('delivery_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('DeliveryReturnProducts','Customer')->get();
            $customer = '';
        }
        else
        {
            $delivery_returns = DeliveryReturn::
                where('customer_id',$request->customer_id)
                ->whereBetween('delivery_return_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
                ->with('DeliveryReturnProducts','Customer')->get();
            $customer = Contact::where('contact_id',$request->customer_id)->first();
        }
        if($request->display_type=='display')
        {
            return $delivery_returns;
        }
        else if($request->display_type=='pdf')
        {
            PDF::loadView('delivery_return.report', compact('delivery_returns','org','setting','request','customer'), [], [
                'margin_top' => 10
            ])->stream('delivery-return-report.pdf');
        }
        else
        {
            return view('delivery_return.report', compact('delivery_returns','org','setting','request','customer'));
        }
    }
    public function pdf(DeliveryReturn $delivery_return)
    {
        $org = Organization::first();

        $preference = Preference::first();
        $default_pdf = 'delivery_return.'.$preference->delivery_return_pdf;
         $account = Account::orderBy('account_id','desc')->first();
        if($preference->delivery_pdf == 'pdf1' )
        {
            $margin_top = 15;
        }
        elseif($preference->delivery_return_pdf == 'pdf2')
        {
            $margin_top = 41.8;
        }
        elseif($preference->delivery_return_pdf == 'pdf3')
        {
            $margin_top = 41.8;
        }
        elseif($preference->delivery_return_pdf == 'pdf4')
        {
            $margin_top = 41.8;
        }
        elseif($preference->delivery_return_pdf == 'pdf5')
        {
            $margin_top = 41.8;
        }
        else
        {
            $margin_top = 10;
        }

        // $delivery = Delivery::where('delivery_id',$delivery->delivery_id)->with('Customer','DeliveryProducts','SourcePlace','DestinationPlace')->first();
        PDF::loadView($default_pdf, compact('org','preference','delivery_return','account'), [], [
            'margin_top' => $margin_top,
        ])->stream($delivery_return->delivery_return_no.'.pdf');
    }
}
