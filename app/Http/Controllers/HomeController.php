<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\Contact;
use App\Category;
use App\Product;
use App\PurchaseOrder;
use App\Bill;
use App\Payment;
use App\Quotation;
use App\Proforma;
use App\Delivery;
use App\Invoice;
use App\Setting;
use App\Preference;
use App\RedeemPoint;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org = Organization::first();
        $settings = Setting::first();
        $preferences = Preference::first();


        $products = Product::orderBy('product_name')
            ->with(['BillStock','Price','Store','InvoiceStock','DeliveryStock'])
            ->get();
        $array[] = '';
        $int=0;
        $array_product[]='';
        foreach($products as $product){
            if($preferences->inventory_on_calculation=='Invoice'){
                if($product['BillStock']!=''){
                    if(($product['BillStock']['quantity']-$product['InvoiceStock']['quantity']) <= $product['min_stock']){
                            
                        array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
                        array_push($array_product,$product);
                    }
                }
            }else{
                //if(($product['BillStock']!=''){
                    if(($product['BillStock']['quantity']-$product['DeliveryStock']['quantity']) <= $product['min_stock']){
                        array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
                        array_push($array_product,$product);
                    }
                //}
            }
        }
        $count =  (count($array)-1);
        return view('home',compact('org','settings','preferences','count','array_product'));
    }

    public function get_dashboard_data(Request $request){
        //$data[] =''; 
        
        $customers =['customers' => Contact::where('contact_type','Customer')->count()] ;
        $vendors =['vendors' => Contact::where('contact_type','Vendor')->count()] ;
        $categories =['categories' => Category::count()] ;
        $products =['products' => Product::count()] ;
        $purchase_orders =['purchase_orders' => number_format(PurchaseOrder::sum('grand_total'),2)] ;
        $bills =['bills' => number_format(Bill::sum('grand_total'),2)] ;
        $payments =['payments' => number_format(Payment::sum('total_amount'),2)] ;
        $quotations =['quotations' => number_format(Quotation::sum('grand_total'),2)] ;
        $proformas =['proformas' => number_format(Proforma::sum('grand_total'),2)] ;
        $delivery_challans =['delivery_challans' => number_format(Delivery::sum('grand_total'),2)] ;
        $invoices =['invoices' => number_format(Invoice::sum('grand_total'),2)] ;
        $redeem_points = ['redeem_points' => number_format((RedeemPoint::sum('redeem_points')),2)];

        $data = array_merge($customers,$vendors,$categories,$products,$purchase_orders,$bills,$payments,$quotations,$proformas,$delivery_challans,$invoices,$redeem_points);

        return $data;

    }

    public function current_stock(Request $request)
    {
        $products = Product::orderBy('product_name')
            ->with(['BillStock','Price','Store','InvoiceStock','DeliveryStock'])
            ->get();
        $array[] = '';
        $int=0;
        $preference = Preference::first();
        // switch($preference->inventory_on_calculation){
        //     case 'Invoice':
        //          $products->load(['BillStock','InvoiceStock']);
        //     break;
        //     case 'Delivery Challan':
        //          $products->load(['BillStock','DeliveryStock']);
        //     break;
        //     default:
        //      $products;
        // }
        foreach($products as $product){
            //array_push($array ,$product['min_stock']) ;
            //if(($product['BillStock'])!=''){
                if($preference->inventory_on_calculation=='Invoice'){
                    if($product['BillStock']!=''){
                        if(($product['BillStock']['quantity']-$product['InvoiceStock']['quantity']) < $product['min_stock']){
                            //dd('hii');
                            //return $product['bill_stock']['quantity']-$product['invoice_stock']['quantity']);
                           array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
                        }
                    }
                }else{
                    if(($product['bill_stock']['quantity']-$product['delivery_stock']['quantity']) < $product['min_stock']){
                            dd('hello');
                            //return ($product['bill_stock']['quantity']-$product['invoice_stock']['quantity']);
                            //$int++;
                    }
                }
            }
        //}

        //return count($array)-1;
        

        return (count($array)-1);
    }
}
