<?php

namespace App\Http\Controllers;

use App\Preference;
use App\Organization;
use App\RedeemPoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;

class RedeemPointController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return RedeemPoint::
            whereHas('Customer', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('redeem_points', 'like', '%'.$request->search.'%')
            ->orWhere('barcode', 'like', '%'.$request->search.'%')
            ->with('Customer')
            ->orderBy('redeem_point_id','DESC')
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $today = date('Y-m-d');
        $this->validate($request, [
            'contact_id'=> 'required|numeric',
            'redeem_points'=> 'required|numeric|min:1',
            'barcode' => 'nullable',
        ]);
        $data = RedeemPoint::create([
            'contact_id' => $request->contact_id,
            'redeem_date' => date("Y-m-d", strtotime($request->redeem_date)),
            'redeem_points' => $request->redeem_points,
            'barcode' => $request->barcode,         
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'contact_id'=> 'required|numeric',
            'redeem_points'=> 'required|numeric|min:1',
            'barcode' => 'nullable',
        ]);
        RedeemPoint::where('redeem_point_id',$request->redeem_point_id)->update([
            'contact_id' => $request->contact_id,
            'redeem_date' => date("Y-m-d", strtotime($request->redeem_date)),
            'redeem_points' => $request->redeem_points,
            'barcode' => $request->barcode,         
        ]);
    }

    public function destroy(RedeemPoint $redeem_point)
    {
    	return RedeemPoint::where('redeem_point_id',$redeem_point->redeem_point_id)->delete();
    }

    public function get_categories()
    {
        return RedeemPoint::get();
    }

    public function small_print($redeem_point_id,$status)
    {
        $redeem_point = RedeemPoint::where('redeem_point_id',$redeem_point_id)
                                    ->with('Customer')
                                    ->first();
        $preference = Preference::first();
        $org = Organization::first();
        $connector = new WindowsPrintConnector($preference->printer_name);
        $printer = new Printer($connector);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->setFont(Printer::FONT_A);
        $printer->setTextSize(2,2);
        $printer->text($org->org_name."\n");
        $printer->selectPrintMode(Printer::MODE_FONT_A);
        $printer->setFont(Printer::FONT_A);
        $printer->text($org->address."\n");
        $printer->text($org->email."/".$org->mobile_no."/".$org->phone_no."\n");
        if($status=='Duplicate')
        {
            $printer->text("................................................\n");
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Duplicate\n");
        }
        $printer->text("................................................\n");
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text(" Customer   : ".$redeem_point->Customer->contact_name."\n");
        $printer->text(" Mobile No  : ".$redeem_point->Customer->mobile_no."\n");
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(2,2);
        $printer->text($redeem_point->redeem_points."\n");
        $printer->selectPrintMode(Printer::MODE_FONT_A);
        $printer->setFont(Printer::FONT_A);
        $printer->text("................................................\n");
        $printer->text(" * * THANK YOU VISIT AGAIN * * \n");
        // $printer->text("................................................\n");
        $printer -> cut();
        $printer -> close();
        return redirect()->back();
    }
}
