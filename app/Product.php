<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_type', 'product_code', 'hsn_code', 'category_id', 'product_name', 'description', 'product_unit','product_size', 'min_stock', 'vendor_id', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'product_id';
    protected $dates = ['deleted_at'];

    public function Category()
    {
    	return $this->hasOne('App\Category','category_id','category_id')->withTrashed();
    }

    public function Price()
    {
        return $this->hasOne('App\Price','product_id','product_id')->with('Tax')->withTrashed();
    }

    public function Store()
    {
        return $this->hasOne('App\Store','product_id','product_id')->with('Warehouse')->withTrashed();
    }

    public function StockBillProducts()
    {
        return $this->hasMany('App\BillProduct','product_id','product_id')->with('Bill');
    }
    public function StockBillReturnProducts()
    {
        return $this->hasMany('App\BillReturnProduct','product_id','product_id')->with('BillReturn');
    }
    public function StockInvoiceProducts()
    {
        return $this->hasMany('App\InvoiceProduct','product_id','product_id')->with('Invoice');
    }
    public function StockInvoiceReturnProducts()
    {
        return $this->hasMany('App\InvoiceReturnProduct','product_id','product_id')->with('InvoiceReturn');
    }
    public function StockDeliveryProducts()
    {
        return $this->hasMany('App\DeliveryProduct','product_id','product_id')->with('Delivery');
    }
    public function StockDeliveryReturnProducts()
    {
        return $this->hasMany('App\DeliveryReturnProduct','product_id','product_id')->with('DeliveryReturn');
    }
    public function BillStock()
    {
        return $this->hasOne('App\BillProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function BillReturnStock()
    {
        return $this->hasOne('App\BillReturnProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function InvoiceStock()
    {
        return $this->hasOne('App\InvoiceProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function InvoiceReturnStock()
    {
        return $this->hasOne('App\InvoiceReturnProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function DeliveryStock()
    {
        return $this->hasOne('App\InvoiceProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function DeliveryReturnStock()
    {
        return $this->hasOne('App\InvoiceProduct','product_id','product_id')
                    ->selectRaw('product_id,SUM(quantity) as quantity')
                    ->groupBy('product_id');
    }
    public function CurrentStock($product_id)
    {
        $preference = Preference::first();
        $product = Product::where('product_id',$product_id)->first();
        $purc_qty = 0;$sale_qty = 0;

        $product->load(['BillStock','BillReturnStock']);
        $purc_qty = $product->CalcStock($product->BillStock,$product->BillReturnStock);

        $product->load('Store');
        
        $purc_qty = $purc_qty + $product->Store->opening_stock;


        switch($preference->inventory_on_calculation){
            case "Invoice":
                $product->load(['InvoiceStock','InvoiceReturnStock']);
                $sale_qty = $product->CalcStock($product->InvoiceStock,$product->InvoiceReturnStock);
            break;
            case 'Delivery Challan':
                $product->load(['DeliveryStock','DeliveryReturnStock']);
                $sale_qty = $product->CalcStock($product->DeliveryStock,$product->DeliveryReturnStock);
            break;
        }

        return [
            'product_id'               => $product_id,
            'min_stock'                => $product->min_stock,
            'opening_stock'            => $purc_qty-$sale_qty,
            'purchase_stock'           => $purc_qty,
            'sales_stock'              => $sale_qty,
            'store_opening_stock'      => $product->Store->first()->opening_stock,
            'store_opening_stock_rate' => $product->Store->first()->opening_stock_rate,
        ];
    }
    public function CalcStock($stock,$stock_return)
    {
        $qty = 0;
        if(!is_null($stock)){
            $qty = $stock->quantity;
        }
        if(!is_null($stock_return)){
            $qty -= $stock_return->quantity;    
        }
        return $qty;
    }

    public function OpeningStock($product, $request)
    {
        $opening_stock = $product->Store->opening_stock;
        $purchase_stock = BillProduct::where('product_id',$product->product_id)
            ->whereHas('Bill', function($query) use($request){
                $query->whereDate('bill_date','<',$request->date);
            })
            ->sum('quantity');
        $sales_stock = InvoiceProduct::where('product_id',$product->product_id)
            ->whereHas('Invoice', function($query) use($request){
                $query->whereDate('invoice_date','<',$request->date);
            })
            ->sum('quantity');   
        return round($opening_stock + $purchase_stock - $sales_stock,2); 
    }

    public function OpeningStock2($product, $request)
    {        
        //purchases
        $bill_product_qty = BillProduct::where('product_id',$product->product_id)
            ->whereHas('Bill', function($query) use($request){
                $query->whereDate('bill_date','<',$request->date);
            })
            ->sum('quantity');

        $bill_return_product_qty = BillReturnProduct::where('product_id',$product->product_id)
            ->whereHas('BillReturn', function($query) use($request){
                $query->whereDate('bill_return_date','<',$request->date);
            })
            ->sum('quantity');

        $purc_qty = ($bill_product_qty - $bill_return_product_qty) + $product->Store->opening_stock;

        // sales
        $invoice_product_qty = InvoiceProduct::where('product_id',$product->product_id)
            ->whereHas('Invoice', function($query) use($request){
                $query->whereDate('invoice_date','<',$request->date);
            })
            ->sum('quantity');   

        $invoice_return_product_qty = InvoiceReturnProduct::where('product_id',$product->product_id)
            ->whereHas('InvoiceReturn', function($query) use($request){
                $query->whereDate('invoice_return_date','<',$request->date);
            })
            ->sum('quantity');

        $sale_qty = $invoice_product_qty - $invoice_return_product_qty;
        return round($purc_qty - $sale_qty,2); 
    }

    public function PurchaseStock($product, $request)
    {
        $purchase_stock = BillProduct::where('product_id',$product->product_id)
            ->whereHas('Bill', function($query) use($request){
                $query->whereDate('bill_date',$request->date);
            })
            ->sum('quantity');  
        return round($purchase_stock,2);  
    }

    public function SalesStock($product, $request)
    {
        $sales_stock = InvoiceProduct::where('product_id',$product->product_id)
            ->whereHas('Invoice', function($query) use($request){
                $query->whereDate('invoice_date',$request->date);
            })
            ->sum('quantity');      
        return round($sales_stock,2);  
    }
}