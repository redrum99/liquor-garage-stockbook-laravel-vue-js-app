<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'account_code','account_name','account_no','bank_name','branch_name','ifsc_code','created_by', 'updated_by',
    ];

    protected $primaryKey = 'account_id';

    protected $dates = ['deleted_at'];
}
