<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id', 'purchase_rate_exc', 'sales_rate_exc', 'tax_id', 'purchase_rate_inc', 'sales_rate_inc', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'price_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
        return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
}
