<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Relations\Relation;
// Relation::morphMap([
//     'reference' => 'App\Invoice',
    
// ]);
class ReceiptParticular extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'receipt_id','reference','reference_id','paid_amount','created_by','updated_by'
    ];

    protected $primaryKey = 'receipt_particular_id';

    protected $dates = ['deleted_at'];

    public function Invoice()
    {
    	return $this->hasOne('App\Invoice','invoice_id','reference_id')->withTrashed();
    }
    public function Receipt()
    {
        return $this->hasOne('App\Receipt','receipt_id','receipt_id')->withTrashed();
    }
    // public function reference()
    // {
    //     return $this->morphTo();
    // }
}
