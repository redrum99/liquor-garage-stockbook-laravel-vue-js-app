<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTerm extends Model
{
   	use SoftDeletes;
    protected $fillable = [
        'payment_term', 'no_of_days','created_by', 'updated_by',
    ];

    protected $primaryKey = 'payment_term_id';

    protected $dates = ['deleted_at'];
}
