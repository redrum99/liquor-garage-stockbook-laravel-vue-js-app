<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'quotation_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','store_id','warehouse_id','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','quotation_product_status','created_by','updated_by'
    ];

    protected $primaryKey = 'quotation_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    public function Quotation()
    {
        return $this->hasOne('App\Quotation','quotation_id','quotation_id')->with('Customer');
    }
    public function InvoiceProductsCheck()
    {
        return $this->hasMany('App\InvoiceProduct','reference_id','quotation_product_id')->where('reference','App\QuotationProduct');
    }
}
