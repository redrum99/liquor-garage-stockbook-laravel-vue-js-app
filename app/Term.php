<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Term extends Model
{
   	use SoftDeletes;
    protected $fillable = [
        'term_id','term_type','label','term','created_by','updated_by',
    ];

    protected $primaryKey = 'term_id';

    protected $dates = ['deleted_at'];
}
