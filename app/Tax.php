<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tax_name', 'tax_rate','cgst_name','cgst_rate','sgst_name','sgst_rate','igst_name','igst_rate','created_by', 'updated_by',
    ];

    protected $primaryKey = 'tax_id';

    protected $dates = ['deleted_at'];
}
