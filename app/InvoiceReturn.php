<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceReturn extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','invoice_return_no','invoice_return_date','reference_no','reference_date','customer_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','invoice_return_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'invoice_return_id';

    protected $dates = ['deleted_at'];

       public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }

    public function InvoiceReturnProducts()
    {
        return $this->hasMany('App\InvoiceReturnProduct','invoice_return_id','invoice_return_id')->with('Tax');
    }
  
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($invoice_return_id)
    {
     $sub_total = round(InvoiceReturnProduct::where('invoice_return_id',$invoice_return_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($invoice_return_id)
    {
        $discount = round(InvoiceReturnProduct::where('invoice_return_id',$invoice_return_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($invoice_return_id)
    {
     $total_tax = round(InvoiceReturnProduct::where('invoice_return_id',$invoice_return_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($invoice_return_id)
    {
     return InvoiceReturnProduct::where('invoice_return_id',$invoice_return_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($invoice_return_id,$tax_id)
    {
        $amount = round(InvoiceReturnProduct::where([['invoice_return_id',$invoice_return_id],['tax_id',$tax_id]])->sum('amount'),2);
        $discount = round(InvoiceReturnProduct::where([['invoice_return_id',$invoice_return_id],['tax_id',$tax_id]])->sum('discount_amount'),2);
        
        return round($amount-$discount);
    }
    // public function QuotationProduct()
    // {
    //     return $this->hasone('App\QuotationProduct','reference_id','quotation_product_id_id');
    // }

     public function taxSlab($invoice_return_id)
    {
        return InvoiceReturnProduct::where('invoice_return_id',$invoice_return_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }
}
