<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'warehouse_name','warehouse_location','created_by', 'updated_by',
    ];

    protected $primaryKey = 'warehouse_id';

    protected $dates = ['deleted_at'];
}
