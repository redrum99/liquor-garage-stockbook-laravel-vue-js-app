<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'category_code', 'category_name','created_by', 'updated_by',
    ];

    protected $primaryKey = 'category_id';

    protected $dates = ['deleted_at'];

    public function CategoryInvoiceProducts()
    {
        return $this->hasMany('App\InvoiceProduct','category_id','category_id');
    }
    
}
