<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','delivery_no','delivery_date','reference_no','reference_date','customer_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','delivery_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'delivery_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }
    public function DeliveryProducts()
    {
        return $this->hasMany('App\DeliveryProduct','delivery_id','delivery_id')->with('Tax');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($delivery_id)
    {
     $sub_total = round(DeliveryProduct::where('delivery_id',$delivery_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($delivery_id)
    {
        $discount = round(DeliveryProduct::where('delivery_id',$delivery_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($delivery_id)
    {
     $total_tax = round(DeliveryProduct::where('delivery_id',$delivery_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($delivery_id)
    {
     return DeliveryProduct::where('delivery_id',$delivery_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($delivery_id,$tax_id)
    {
         $amount = round(DeliveryProduct::where([['delivery_id',$delivery_id],['tax_id',$tax_id]])->sum('amount'),2);

         $discount = round(DeliveryProduct::where([['delivery_id',$delivery_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

         return round($amount-$discount); 
    }

    public function taxSlab($delivery_id)
    {
        return DeliveryProduct::where('delivery_id',$delivery_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }
    
}
