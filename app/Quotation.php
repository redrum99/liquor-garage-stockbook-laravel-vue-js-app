<?php

namespace App;

use App\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;

class Quotation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','quotation_no','quotation_date','reference_no','expiry_date','customer_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','quotation_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'quotation_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }

    public function QuotationProducts()
    {
        return $this->hasMany('App\QuotationProduct','quotation_id','quotation_id')->with('Tax');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($quotation_id)
    {
     $sub_total = round(QuotationProduct::where('quotation_id',$quotation_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($quotation_id)
    {
        $discount = round(QuotationProduct::where('quotation_id',$quotation_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($quotation_id)
    {
     $total_tax = round(QuotationProduct::where('quotation_id',$quotation_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($quotation_id)
    {
     return QuotationProduct::where('quotation_id',$quotation_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($quotation_id,$tax_id)
    {
        $amount = round(QuotationProduct::where([['quotation_id',$quotation_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(QuotationProduct::where([['quotation_id',$quotation_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        return round($amount-$discount); 
    }

    public function taxSlab($quotation_id)
    {
        return QuotationProduct::where('quotation_id',$quotation_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }

    public function TaxableValueHsn($quotation_id,$tax_id,$hsn_code)
    {
        $setting = Setting::first();
        $discount = round(QuotationProduct::where([['quotation_id',$quotation_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('discount_amount'),2);
        if($setting->discounts_in=='exc'){
            $amount = round(QuotationProduct::where([['quotation_id',$quotation_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount-$discount);
        }else{
            $amount = round(QuotationProduct::where([['quotation_id',$quotation_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount);
        } 
    }


}
