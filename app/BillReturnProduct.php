<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BillReturnProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'bill_return_id','reference','reference_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','store_id','warehouse_id','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','created_by','updated_by'
    ];

    protected $primaryKey = 'bill_return_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }

    public function BillReturn()
    {
        return $this->belongsTo('App\BillReturn','bill_return_id','bill_return_id')->withTrashed();
    }
}
