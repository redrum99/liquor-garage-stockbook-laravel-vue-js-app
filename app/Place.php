<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'place_code', 'place_name', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'place_id';

    protected $dates = ['deleted_at'];
}
