<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'delivery_id','reference','reference_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','store_id','warehouse_id','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','delivery_product_status','created_by','updated_by'
    ];

    protected $primaryKey = 'delivery_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
        public function Delivery()
    {
        return $this->belongsTo('App\Delivery','delivery_id','delivery_id')->with('Customer');
    }
    public function InvoiceProductsCheck()
    {
        return $this->hasMany('App\InvoiceProduct','reference_id','delivery_product_id')->where('reference','App\DeliveryProduct');
    }
}
