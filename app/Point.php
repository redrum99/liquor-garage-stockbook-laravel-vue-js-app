<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Point extends Model
{
    use SoftDeletes;

    protected $fillable = ['amount','points','to_amount'];

    protected $primaryKey = 'point_id';

    protected $dates = ['deleted_at'];
}
