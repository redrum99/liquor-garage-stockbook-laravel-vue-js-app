<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Setting extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'discounts_in','enable_barcode','migrations','purchase_orders','bills','bill_returns','payments','quotations','proformas','deliveries','delivery_returns','invoices','invoice_returns','receipts','incomes','expenses','transactions','created_by','updated_by'
    ];

    protected $primaryKey = 'setting_id';

    protected $dates = ['deleted_at'];
}
