<?php

namespace App;

use App\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','purchase_order_no','purchase_order_date','reference_no','delivery_date','vendor_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','purchase_order_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'purchase_order_id';

    protected $dates = ['deleted_at'];

    public function Vendor()
    {
        return $this->hasOne('App\Contact','contact_id','vendor_id')->withTrashed();
    }
    public function PurchaseOrderProducts()
    {
        return $this->hasMany('App\PurchaseOrderProduct','purchase_order_id','purchase_order_id')->with('Tax');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($purchase_order_id)
    {
        $sub_total = round(PurchaseOrderProduct::where('purchase_order_id',$purchase_order_id)->sum('amount'),2);
        return $sub_total;
    }
    public function Discount($purchase_order_id)
    {
        $discount = round(PurchaseOrderProduct::where('purchase_order_id',$purchase_order_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($purchase_order_id)
    {
        $total_tax = round(PurchaseOrderProduct::where('purchase_order_id',$purchase_order_id)->sum('tax_amount'),2);
        return $total_tax;
    }
    public function Taxes($purchase_order_id)
    {
        return PurchaseOrderProduct::where('purchase_order_id',$purchase_order_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($purchase_order_id,$tax_id)
    {
        $amount = round(PurchaseOrderProduct::where([['purchase_order_id',$purchase_order_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(PurchaseOrderProduct::where([['purchase_order_id',$purchase_order_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        return round($amount-$discount);;
    }

    public function taxSlab($purchase_order_id)
    {

        return PurchaseOrderProduct::where('purchase_order_id',$purchase_order_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }
    
    public function TaxableValueHsn($purchase_order_id,$tax_id,$hsn_code)
    {
        $setting = Setting::first();
        $discount = round(PurchaseOrderProduct::where([['purchase_order_id',$purchase_order_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('discount_amount'),2);
        if($setting->discounts_in=='exc'){
            $amount = round(PurchaseOrderProduct::where([['purchase_order_id',$purchase_order_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount-$discount);
        }else{
            $amount = round(PurchaseOrderProduct::where([['purchase_order_id',$purchase_order_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount);
        }
    }  
}
