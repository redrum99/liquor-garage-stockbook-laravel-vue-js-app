<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'contact_type','contact_code','contact_name','email','mobile_no','phone_no','pan_no','gstin_no','billing_address','shipping_address','account_no','bank_name','branch_name','ifsc_code','payment_term_id','place_id','date_of_birth','wedding_anniversary','created_by', 'updated_by','barcode',
    ];

    protected $primaryKey = 'contact_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasMany('App\Contact','contact_id','customer_id');
    }
    public function Vendor()
    {
        return $this->hasMany('App\Contact','contact_id','vendor_id');
    }
    //vendor
    public function VendorPurchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder','vendor_id','contact_id');
    }
    public function VendorBills()
    {
        return $this->hasMany('App\Bill','vendor_id','contact_id');
    }
    public function VendorPayments()
    {
        return $this->hasMany('App\Payment','vendor_id','contact_id')->with('Account');
    }
    //customer
    public function CustomerQuotations()
    {
        return $this->hasMany('App\Quotation','customer_id','contact_id');
    }

    public function CustomerProformas()
    {
        return $this->hasMany('App\Proforma','customer_id','contact_id');
    }
    public function CustomerDeliverys()
    {
        return $this->hasMany('App\Delivery','customer_id','contact_id');
    }
    public function CustomerInvoices()
    {
        return $this->hasMany('App\Invoice','customer_id','contact_id');
    }
    public function CustomerReceipts()
    {
        return $this->hasMany('App\Receipt','customer_id','contact_id')->with('Account');
    }
    public function CustomerInvoiceReceipt()
    {
        return $this->hasMany('App\Invoice','customer_id','contact_id')->with('InvoiceReceiptTotal');
    }
    public function VendorBillPayment()
    {
        return $this->hasMany('App\Bill','vendor_id','contact_id')->with('BillPaymentTotal');
    }
    public function CurrentPoints($contact_id)
    {
        $inv_points = Invoice::where('customer_id',$contact_id)->sum('points');
        $redeem_points = RedeemPoint::where('contact_id',$contact_id)->sum('redeem_points');

        return $inv_points-$redeem_points;
    }

    public function GetInvPoints($contact_id,$from_date)
    {
        return Invoice::where('customer_id',$contact_id)
                        ->whereDate('invoice_date','<',$from_date)
                        ->sum('points');
    }

    public function GetRedeemPoints($contact_id,$from_date)
    {
        return RedeemPoint::where('contact_id',$contact_id)
                        ->whereDate('redeem_date','<',$from_date)
                        ->sum('redeem_points');
    }

    public function CustInvoices($contact_id,$request)
    {
        return Invoice::whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
            ->where('points','>',0)
            ->where('customer_id',$contact_id)
            ->orderBy('invoice_date','desc')
            ->get();
    }

    public function CustRedeems($contact_id,$request)
    {
        return  RedeemPoint::where('contact_id',$contact_id)
            ->whereBetween('redeem_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
            ->orderBy('redeem_date','desc')
            ->get();
    }
}
