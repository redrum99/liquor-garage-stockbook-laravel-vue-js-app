<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','vendor_id','billing_address','shipping_address','voucher_no','voucher_date','account_id','payment_mode','reference_no','reference_date','total_amount','term_id','terms','note','created_by','updated_by'
    ];

    protected $primaryKey = 'payment_id';

    protected $dates = ['deleted_at'];

    public function Vendor()
    {
        return $this->hasOne('App\Contact','contact_id','vendor_id')->withTrashed();
    }

    public function Account()
    {
        return $this->hasOne('App\Account','account_id','account_id')->withTrashed();
    }

    public function PaymentParticulars()
    {
        return $this->hasMany('App\PaymentParticular','payment_id','payment_id')->with('Bill');
    }
}
