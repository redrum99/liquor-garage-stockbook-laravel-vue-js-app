<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'bill_id','reference','reference_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','store_id','warehouse_id','quantity','amount','discount','discount_type','reference','reference_id','discount_amount','tax_id','tax_amount','sub_total','created_by','updated_by'
    ];

    protected $primaryKey = 'bill_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    public function Bill()
    {
        return $this->belongsTo('App\Bill','bill_id','bill_id')->with('Vendor')->withTrashed();
    }
    public function PaymentParticularsCheck()
    {
        return $this->hasMany('App\PaymentParticular','reference_id','bill_id')->where('reference','App\Bill');
    }
}
