<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'subscription_key', 'no_of_days', 'start_date','end_date','status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'subscription_id';

    protected $dates = ['deleted_at'];
}
