<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id', 'price_id', 'warehouse_id', 'opening_stock', 'opening_stock_rate', 'master_value', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'store_id';

    protected $dates = ['deleted_at'];

    public function Warehouse()
    {
    	return $this->hasOne('App\Warehouse','warehouse_id','warehouse_id')->withTrashed();
    }
}
