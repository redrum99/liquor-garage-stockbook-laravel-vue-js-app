<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentParticular extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'payment_id','reference','reference_id','paid_amount','created_by','updated_by'
    ];

    protected $primaryKey = 'payment_particular_id';

    protected $dates = ['deleted_at'];

    public function Bill()
    {
    	return $this->hasOne('App\Bill','bill_id','reference_id')->withTrashed();
    }
    public function Payment()
    {
        return $this->hasOne('App\Payment','payment_id','payment_id')->withTrashed();
    }
}
