<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'org_code', 'org_name', 'email', 'mobile_no',  'phone_no', 'address', 'pan_no', 'gstin_no', 'place_id', 'logo', 'header', 'footer', 'created_by', 'updated_by',
    ];
    protected $primaryKey = 'org_id';

    public function Places()
    {
       return $this->hasOne('App\Place', 'place_id', 'place_id');
    }
}
