<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceReturnProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'invoice_return_id','reference','reference_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','store_id','warehouse_id','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','created_by','updated_by'
    ];

    protected $primaryKey = 'invoice_return_product_id';

    protected $dates = ['deleted_at'];

     public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    public function InvoiceReturn()
    {
        return $this->belongsTo('App\InvoiceReturn','invoice_return_id','invoice_return_id')->with('Customer');
    }
}
