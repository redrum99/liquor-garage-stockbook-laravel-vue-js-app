<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RedeemPoint extends Model
{
   use SoftDeletes;

    protected $fillable = [
        'contact_id','barcode','redeem_points','redeem_date',
    ];

    protected $primaryKey = 'redeem_point_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','contact_id')->withTrashed();
    }
}
