<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BillReturn extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','bill_return_no','bill_return_date','reference_no','reference_date','vendor_id','billing_address','shipping_address','source_id','destination_id','due_date','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','bill_return_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'bill_return_id';

    protected $dates = ['deleted_at'];

     public function Vendor()
    {
        return $this->hasOne('App\Contact','contact_id','vendor_id')->withTrashed();
    }

    public function BillReturnProducts()
    {
        return $this->hasMany('App\BillReturnProduct','bill_return_id','bill_return_id')->with('Tax');
    }

    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($bill_return_id)
    {
        $sub_total = round(BillReturnProduct::where('bill_return_id',$bill_return_id)->sum('amount'),2);
        return $sub_total;
    }
    public function Discount($bill_return_id)
    {
        $discount = round(BillReturnProduct::where('bill_return_id',$bill_return_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($bill_return_id)
    {
        $total_tax = round(BillReturnProduct::where('bill_return_id',$bill_return_id)->sum('tax_amount'),2);
        return $total_tax;
    }
    public function Taxes($bill_return_id)
    {
        return BillReturnProduct::where('bill_return_id',$bill_return_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($bill_return_id,$tax_id)
    {
        $amount =  round(BillReturnProduct::where([['bill_return_id',$bill_return_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(BillReturnProduct::where([['bill_return_id',$bill_return_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        return round($amount-$discount);
    }
    public function taxSlab($bill_return_id)
    {
        return BillReturnProduct::where('bill_return_id',$bill_return_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }
    
}
