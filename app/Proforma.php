<?php

namespace App;

use App\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proforma extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','proforma_no','proforma_date','reference_no','reference_date','customer_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','proforma_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'proforma_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }

    public function ProformaProducts()
    {
        return $this->hasMany('App\ProformaProduct','proforma_id','proforma_id')->with('Tax');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($proforma_id)
    {
     $sub_total = round(ProformaProduct::where('proforma_id',$proforma_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($proforma_id)
    {
        $discount = round(ProformaProduct::where('proforma_id',$proforma_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($proforma_id)
    {
     $total_tax = round(ProformaProduct::where('proforma_id',$proforma_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($proforma_id)
    {
     return ProformaProduct::where('proforma_id',$proforma_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($proforma_id,$tax_id)
    {
         $amount = round(ProformaProduct::where([['proforma_id',$proforma_id],['tax_id',$tax_id]])->sum('amount'),2);

         $discount = round(ProformaProduct::where([['proforma_id',$proforma_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

         return round($amount-$discount); 
    }

    public function taxSlab($proforma_id)
    {
        return ProformaProduct::where('proforma_id',$proforma_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }

    public function TaxableValueHsn($proforma_id,$tax_id,$hsn_code)
    {
        $setting = Setting::first();
        $discount = round(ProformaProduct::where([['proforma_id',$proforma_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('discount_amount'),2);
        if($setting->discounts_in=='exc'){
            $amount = round(ProformaProduct::where([['proforma_id',$proforma_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount-$discount);
        }else{
            $amount = round(ProformaProduct::where([['proforma_id',$proforma_id],['tax_id',$tax_id],['hsn_code',$hsn_code]])->sum('amount'),2);
            return round($amount);
        }
    }

    
}
