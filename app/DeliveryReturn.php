<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryReturn extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','delivery_return_no','delivery_return_date','reference_no','reference_date','customer_id','billing_address','shipping_address','source_id','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','delivery_return_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'delivery_return_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }
    public function DeliveryReturnProducts()
    {
        return $this->hasMany('App\DeliveryReturnProduct','delivery_return_id','delivery_return_id')->with('Tax');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($delivery_return_id)
    {
     $sub_total = round(DeliveryReturnProduct::where('delivery_return_id',$delivery_return_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($delivery_return_id)
    {
        $discount = round(DeliveryReturnProduct::where('delivery_return_id',$delivery_return_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($delivery_return_id)
    {
     $total_tax = round(DeliveryReturnProduct::where('delivery_return_id',$delivery_return_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($delivery_return_id)
    {
     return DeliveryReturnProduct::where('delivery_return_id',$delivery_return_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($delivery_return_id,$tax_id)
    {
         $amount = round(DeliveryReturnProduct::where([['delivery_return_id',$delivery_return_id],['tax_id',$tax_id]])->sum('amount'),2);

         $discount = round(DeliveryReturnProduct::where([['delivery_return_id',$delivery_return_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

         return round($amount-$discount); 
    }
    public function taxSlab($delivery_return_id)
    {
        return DeliveryReturnProduct::where('delivery_return_id',$delivery_return_id)
            ->distinct()
            ->orderBy('tax_id')
            ->get(['hsn_code','tax_id']);
    }
}
