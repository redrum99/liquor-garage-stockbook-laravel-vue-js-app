<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'username', 'email', 'password', 'user_role', 'mobile_no', 'address', 'avatar',
    ];
    
    protected $primaryKey = 'user_id';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
