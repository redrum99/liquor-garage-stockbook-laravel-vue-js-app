<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','customer_id','billing_address','shipping_address','receipt_no','receipt_date','account_id','payment_mode','reference_no','reference_date','total_amount','term_id','terms','note','created_by','updated_by'
    ];

    protected $primaryKey = 'receipt_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
    	return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }

    public function Account()
    {
    	return $this->hasOne('App\Account','account_id','account_id')->withTrashed();
    }

    public function ReceiptParticulars()
    {
        return $this->hasMany('App\ReceiptParticular','receipt_id','receipt_id')->with('Invoice');
    }
}
