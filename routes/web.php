<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/check','InvoiceController@check');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/{name}',function(){
    return redirect('/');
})->where('name','[A-Za-z]+');

//Dashboard
Route::post('/data/view', 'HomeController@get_dashboard_data');

//Master
Route::post('/master/display', 'MasterController@display');
Route::post('/master/store', 'MasterController@store');
Route::post('/master/destroy/{master}', 'MasterController@destroy');
Route::post('/master/update', 'MasterController@update');
Route::post('/master/get_masters', 'MasterController@get_masters');

//Users
Route::post('/user/display', 'UserController@display');
Route::post('/user/store', 'UserController@store');
Route::post('/user/edit/{user}', 'UserController@edit');
Route::post('/user/update', 'UserController@update');
Route::post('/user/destroy/{user}', 'UserController@destroy');
Route::post('/profile/get_users', 'UserController@get_users')->name('profile.get_users');
Route::post('/profile/update', 'UserController@update_profile')->name('profile.update');
Route::post('/change/password', 'UserController@change_password');
Route::post('/user/change_status','UserController@change_status');
Route::post('/get_users','UserController@get_user');


//Place
Route::post('/place/display', 'PlaceController@display');
Route::post('/place/store', 'PlaceController@store');
Route::post('/place/destroy/{place}', 'PlaceController@destroy');
Route::post('/place/update', 'PlaceController@update');
Route::post('/place/get_places', 'PlaceController@get_places');

//Organization
Route::post('/organization/get_organization', 'OrganizationController@get_organization');
Route::post('/organization/update', 'OrganizationController@update');

//Preferences
Route::post('/preference/get_preference', 'PreferenceController@get_preference');
Route::post('/preference/update', 'PreferenceController@update');
//Data migration from DB to DB
Route::post('/migrations/get_table_names', 'PreferenceController@get_table_names');
Route::post('/migrations/migrate_data', 'PreferenceController@migrate_data');

//Taxes
Route::post('/tax/display', 'TaxController@display');
Route::post('/tax/store', 'TaxController@store');
Route::post('/tax/update', 'TaxController@update');
Route::post('/tax/destroy/{tax}', 'TaxController@destroy');
Route::post('/tax/get_taxes', 'TaxController@get_taxes');

//Accounts
Route::post('/account/display', 'AccountController@display');
Route::post('/account/store', 'AccountController@store');
Route::post('/account/destroy/{account}', 'AccountController@destroy');
Route::post('/account/update', 'AccountController@update');
Route::post('/account/get_accounts', 'AccountController@get_accounts');

//Terms
Route::post('/term/display', 'TermController@display');
Route::post('/term/store', 'TermController@store');
Route::post('/term/destroy/{term}', 'TermController@destroy');
Route::post('/term/update', 'TermController@update');
Route::post('/term/get_terms', 'TermController@get_terms');

//Payment Terms
Route::post('/payment_term/display', 'PaymentTermController@display');
Route::post('/payment_term/store', 'PaymentTermController@store');
Route::post('/payment_term/destroy/{payment_term}', 'PaymentTermController@destroy');
Route::post('/payment_term/update', 'PaymentTermController@update');
Route::post('/payment_term/get_payment_terms', 'PaymentTermController@get_payment_terms');
Route::post('/payment_term/change_date', 'PaymentTermController@change_date');

//Settings
Route::post('/setting/get_setting', 'SettingController@get_setting');
Route::post('/settings/fetch_settings', 'SettingController@fetch_settings');
Route::post('/settings/update', 'SettingController@update');

Route::post('settings/get', 'SettingController@get_settings');

//Preference
Route::post('/preference/get_preference', 'PreferenceController@get_preference');
Route::post('/preference/update', 'PreferenceController@update');

//Subscription
Route::post('/subscription/store', 'SubscriptionController@store');
Route::post('/subscription/display', 'SubscriptionController@display');

//Categories
Route::post('/category/display', 'CategoryController@display');
Route::post('/category/get_category_code', 'CategoryController@get_category_code');
Route::post('/category/store', 'CategoryController@store');
Route::post('/category/destroy/{category}', 'CategoryController@destroy');
Route::post('/category/update', 'CategoryController@update');
Route::post('/category/get_categories', 'CategoryController@get_categories');
Route::get('/category/report', 'CategoryController@report');
Route::get('/category/pdf/{category}', 'CategoryController@pdf');

//Warehouse
Route::post('/warehouse/display', 'WarehouseController@display');
Route::post('/warehouse/store', 'WarehouseController@store');
Route::post('/warehouse/destroy/{warehouse}', 'WarehouseController@destroy');
Route::post('/warehouse/update', 'WarehouseController@update');

//Contact
Route::post('/contact/get_contact_code', 'ContactController@get_contact_code');
Route::post('/contact/display', 'ContactController@display');
Route::post('/contact/view/{contact}', 'ContactController@view');
Route::post('/contact/store', 'ContactController@store');
Route::post('/contact/edit/{contact}', 'ContactController@edit');
Route::post('/contact/update', 'ContactController@update');
Route::post('/contact/destroy', 'ContactController@destroy'); 
Route::post('/contact/get_contacts', 'ContactController@get_contacts');
Route::post('/contact/excel_upload', 'ContactController@excel_upload');
Route::get('/contact/report', 'ContactController@report');
Route::get('/contact/outstanding_report','ContactController@outstanding_report');
Route::get('/contact/all_contacts_reports', 'ContactController@all_report');

// Route::post('/contact/vendor_report','ContactController@vendor_report');

//Product
Route::post('/product/get_product_code', 'ProductController@get_product_code');
Route::post('/product/display', 'ProductController@display');
Route::post('/product/store', 'ProductController@store');
Route::post('/product/edit/{product}', 'ProductController@edit');
Route::post('/product/update', 'ProductController@update');
Route::post('/product/destroy', 'ProductController@destroy');
Route::post('/product/get_products', 'ProductController@get_products');
Route::post('/product/get_products_more', 'ProductController@get_products_more');
Route::get('/product/stock_report', 'ProductController@stock_report');
Route::get('/product/stock_report_2', 'ProductController@stock_report_2');
Route::get('/product/stock_report_order_by_category_name', 'ProductController@stock_report_order_by_category_name');
Route::post('/product/excel_upload', 'ProductController@excel_upload');
Route::post('/product/view/{product}', 'ProductController@view');
Route::post('/product/get_barcode', 'ProductController@get_barcode');
Route::get('/product/barcode/{product}', 'ProductController@pdf');


//Purchase Order
Route::post('/purchase_order/get_purchase_order_no', 'PurchaseOrderController@get_purchase_order_no');
Route::post('/purchase_order/validation', 'PurchaseOrderController@validation');
Route::post('/purchase_order/store', 'PurchaseOrderController@store');
Route::post('/purchase_order/display', 'PurchaseOrderController@display');
Route::post('/purchase_order/view/{purchase_order}', 'PurchaseOrderController@view');
Route::post('/purchase_order/destroy/{purchase_order}', 'PurchaseOrderController@destroy');
Route::post('/purchase_order/update', 'PurchaseOrderController@update');
Route::get('/purchase_order/pdf/{purchase_order}', 'PurchaseOrderController@pdf');
Route::get('/purchase_order/report', 'PurchaseOrderController@report');

//Bill
Route::post('/bill/validation', 'BillController@validation');
Route::post('/bill/store', 'BillController@store');
Route::post('/bill/display', 'BillController@display');
Route::post('/bill/view/{bill}', 'BillController@view');
Route::post('/bill/destroy/{bill}', 'BillController@destroy');
Route::post('/bill/update', 'BillController@update');
Route::post('/bill/get_unpaid_bills', 'BillController@get_unpaid_bills');
Route::get('/bill/pdf/{bill}', 'BillController@pdf');
Route::get('/bill/pdf2/{bill}', 'BillController@pdf2');
Route::get('/bill/report', 'BillController@report');
Route::get('/bill/tax_report', 'BillController@tax_report');
Route::post('/bill/convert_purchase', 'BillController@convert_purchase');

//Bill Return
Route::post('/bill_return/validation', 'BillReturnController@validation');
Route::post('/bill_return/store', 'BillReturnController@store');
Route::post('/bill_return/view/{bill_return}', 'BillReturnController@view');
Route::post('/bill_return/display', 'BillReturnController@display');
Route::post('/bill_return/update', 'BillReturnController@update');
Route::post('/bill_return/convert_bill', 'BillReturnController@convert_bill');
Route::post('/bill_return/destroy/{bill_return}', 'BillReturnController@destroy');
Route::get('/bill_return/pdf/{bill_return}', 'BillReturnController@pdf');
Route::get('/bill_return/report', 'BillReturnController@report');

//Payment
Route::post('/payment/get_voucher_no', 'PaymentController@get_voucher_no');
Route::post('/payment/store', 'PaymentController@store');
Route::post('/payment/display', 'PaymentController@display');
Route::post('/payment/view/{payment}', 'PaymentController@view');
Route::post('/payment/destroy/{payment}', 'PaymentController@destroy');
Route::get('/payment/pdf/{payment}', 'PaymentController@pdf');

//Quotation
Route::post('/quotation/get_quotation_no', 'QuotationController@get_quotation_no');
Route::post('/quotation/validation', 'QuotationController@validation');
Route::post('/quotation/store', 'QuotationController@store');
Route::post('/quotation/display', 'QuotationController@display');
Route::post('/quotation/view/{quotation}', 'QuotationController@view');
Route::post('/quotation/destroy/{quotation}', 'QuotationController@destroy');
Route::post('/quotation/update', 'QuotationController@update');
Route::get('/quotation/report', 'QuotationController@report');
Route::post('/quotation/convert/invoice', 'QuotationController@convert_quotation');
Route::get('/quotation/pdf/{quotation}', 'QuotationController@pdf');

//Proforma
Route::post('/proforma/get_proforma_no', 'ProformaController@get_proforma_no');
Route::post('/proforma/validation', 'ProformaController@validation');
Route::post('/proforma/store', 'ProformaController@store');
Route::post('/proforma/display', 'ProformaController@display');
Route::post('/proforma/view/{proforma}', 'ProformaController@view');
Route::post('/proforma/destroy/{proforma}', 'ProformaController@destroy');
Route::post('/proforma/update', 'ProformaController@update');
Route::get('/proforma/report', 'ProformaController@report');
Route::get('/proforma/pdf/{proforma}', 'Proforma@pdf');
Route::post('/proforma/convert/invoice', 'ProformaController@convert_proforma');
Route::post('/proforma/convert/delivery', 'ProformaController@convert_proforma');
Route::get('/proforma/pdf/{proforma}', 'ProformaController@pdf');

//Delivery
Route::post('/delivery/get_delivery_no', 'DeliveryController@get_delivery_no');
Route::post('/delivery/validation', 'DeliveryController@validation');
Route::post('/delivery/store', 'DeliveryController@store');
Route::post('/delivery/display', 'DeliveryController@display');
Route::post('/delivery/view/{delivery}', 'DeliveryController@view');
Route::post('/delivery/destroy/{delivery}', 'DeliveryController@destroy');
Route::post('/delivery/update', 'DeliveryController@update');
Route::get('/delivery/report', 'DeliveryController@report');
Route::post('/delivery/convert/invoice', 'DeliveryController@convert_delivery');
Route::post('/delivery/convert/return', 'DeliveryController@convert_delivery');
Route::get('/delivery/pdf/{delivery}', 'DeliveryController@pdf');

//Delivery Return
Route::post('/delivery_return/get_delivery_return_no', 'DeliveryReturnController@get_delivery_return_no');
Route::post('/delivery_return/validation', 'DeliveryReturnController@validation');
Route::post('/delivery_return/store', 'DeliveryReturnController@store');
Route::post('/delivery_return/display', 'DeliveryReturnController@display');
Route::post('/delivery_return/view/{delivery_return}', 'DeliveryReturnController@view');
Route::post('/delivery_return/destroy/{delivery_return}', 'DeliveryReturnController@destroy');
Route::post('/delivery_return/update', 'DeliveryReturnController@update');
Route::get('/delivery_return/report', 'DeliveryReturnController@report');
Route::get('/delivery_return/pdf/{delivery_return}', 'DeliveryReturnController@pdf');

//Invoice
Route::post('/invoice/get_invoice_no', 'InvoiceController@get_invoice_no');
Route::post('/invoice/validation', 'InvoiceController@validation');
Route::post('/invoice/store', 'InvoiceController@store');
Route::post('/invoice/display', 'InvoiceController@display');
Route::post('/invoice/view/{invoice}', 'InvoiceController@view');
Route::post('/invoice/destroy/{invoice}', 'InvoiceController@destroy');
Route::post('/invoice/update', 'InvoiceController@update');
Route::post('/invoice/get_unpaid_invoices', 'InvoiceController@get_unpaid_invoices');
Route::get('/invoice/pdf/{invoice}', 'InvoiceController@pdf');
Route::post('/invoice/convert/delivery', 'InvoiceController@convert_invoice');
Route::post('/invoice/convert/return', 'InvoiceController@convert_invoice');
Route::get('/invoice/report', 'InvoiceController@report');
Route::get('/invoice/report/total', 'InvoiceController@getReportTotalAmount');
Route::get('/invoice/tax_report', 'InvoiceController@tax_report');
Route::get('/invoice/print/{invoice}', 'InvoiceController@small_print');
Route::post('/invoice/add_points', 'InvoiceController@add_points');


//InvoiceReturn
Route::post('/invoice_return/get_invoice_return_no', 'InvoiceReturnController@get_invoice_return_no');
Route::post('/invoice_return/validation', 'InvoiceReturnController@validation');
Route::post('/invoice_return/store', 'InvoiceReturnController@store');
Route::post('/invoice_return/display', 'InvoiceReturnController@display');
Route::post('/invoice_return/view/{invoice_return}', 'InvoiceReturnController@view');
Route::post('/invoice_return/destroy/{invoice_return}', 'InvoiceReturnController@destroy');
Route::post('/invoice_return/update', 'InvoiceReturnController@update');
Route::get('/invoice_return/pdf/{invoice_return}', 'InvoiceReturnController@pdf');
Route::get('/invoice_return/report', 'InvoiceReturnController@report');

//Receipt
Route::post('/receipt/get_receipt_no', 'ReceiptController@get_receipt_no');
Route::post('/receipt/store', 'ReceiptController@store');
Route::post('/receipt/display', 'ReceiptController@display');
Route::post('/receipt/view/{receipt}', 'ReceiptController@view');
Route::post('/receipt/destroy/{receipt}', 'ReceiptController@destroy');
Route::get('/receipt/pdf/{receipt}', 'ReceiptController@pdf');

//Expense
Route::post('/expense/get_voucher_no', 'ExpenseController@get_voucher_no');
Route::post('/expense/store', 'ExpenseController@store');
Route::post('/expense/display', 'ExpenseController@display');
Route::post('/expense/view/{expense}', 'ExpenseController@view');
Route::post('/expense/update', 'ExpenseController@update');
Route::post('/expense/destroy/{expense}', 'ExpenseController@destroy');
Route::get('/expense/pdf/{expense}', 'ExpenseController@pdf');
Route::get('/expense/report', 'ExpenseController@report');

//Income
Route::post('/income/get_receipt_no', 'IncomeController@get_receipt_no');
Route::post('/income/store', 'IncomeController@store');
Route::post('/income/display', 'IncomeController@display');
Route::post('/income/view/{income}', 'IncomeController@view');
Route::post('/income/update', 'IncomeController@update');
Route::post('/income/destroy/{income}', 'IncomeController@destroy');
Route::get('/income/pdf/{income}', 'IncomeController@pdf');
Route::get('/income/report', 'IncomeController@report');

Route::post('/product/stock', 'ProductController@current_stock');
Route::get('/balance/report', 'PaymentController@get_balance_report');


Route::get('print/{product}', 'PrintController@print');
Route::get('PrintZPLController', 'PrintZPLController@printCommands');
Route::any('WebClientPrintController', 'WebClientPrintController@processRequest');

//Points
Route::post('/points/display', 'PointController@display');
Route::post('/points/store', 'PointController@store');
Route::post('/points/destroy/{point}', 'PointController@destroy');
Route::post('/points/update', 'PointController@update');
Route::post('/points/get_pointss', 'PointController@get_terms');

//Redeem Points
Route::post('/redeem_points/display', 'RedeemPointController@display');
Route::post('/redeem_points/store', 'RedeemPointController@store');
Route::post('/redeem_points/destroy/{redeem_point}', 'RedeemPointController@destroy');
Route::post('/redeem_points/update', 'RedeemPointController@update');
Route::post('/redeem_points/print/{redeem_point}/{status}', 'RedeemPointController@small_print');


Route::post('/get_barcode', 'ContactController@get_barcode');
Route::get('/points/report', 'PointController@points_report');