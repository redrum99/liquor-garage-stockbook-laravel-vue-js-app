<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('preference_id');
            $table->string('bill_return_prefix',50);
            $table->string('bill_return_no_length',50);
            $table->string('bill_return_pdf',50);
            $table->string('purchase_order_prefix',50);
            $table->string('purchase_order_no_length',50);
            $table->string('purchase_order_pdf',50);
            $table->string('quotation_prefix',50);
            $table->string('quotation_no_length',50);
            $table->string('quotation_pdf',50);
            $table->string('proforma_prefix',50);
            $table->string('proforma_no_length',50);
            $table->string('proforma_pdf',50);
            $table->string('invoice_prefix',50);
            $table->string('invoice_no_length',50);
            $table->string('invoice_pdf',50);
            $table->string('invoice_return_prefix',50);
            $table->string('invoice_return_no_length',50);
            $table->string('invoice_return_pdf',50);
            $table->string('delivery_prefix',50);
            $table->string('delivery_no_length',50);
            $table->string('delivery_pdf',50);
            $table->string('delivery_return_prefix',50);
            $table->string('delivery_return_no_length',50);
            $table->string('delivery_return_pdf',50);
            $table->string('voucher_prefix',50);
            $table->string('voucher_no_length',50);
            $table->string('voucher_pdf',50);
            $table->string('receipt_prefix',50);
            $table->string('receipt_no_length',50);
            $table->string('receipt_pdf',50);
            $table->string('fiscal_year',50);
            $table->string('discount_on',50);
            $table->string('inventory_on_calculation',50);
            $table->string('allow_negative_stock',50);
            $table->string('printer_name',50)->nullable();
            $table->double('min_redeem_points',15,2)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
         DB::table('preferences')->insert(
            [
                [
                    'bill_return_prefix' => 'BILL-RET',
                    'bill_return_no_length' => '4',
                    'bill_return_pdf' => 'pdf1',
                    'purchase_order_prefix' => 'PO',
                    'purchase_order_no_length' => '4',
                    'purchase_order_pdf' => 'pdf1',
                    'quotation_prefix' => 'QOT',
                    'quotation_no_length' => '4',
                    'quotation_pdf' => 'pdf1',
                    'proforma_prefix' => 'QOT',
                    'proforma_no_length' => '4',
                    'proforma_pdf' => 'pdf1',
                    'invoice_prefix' => 'IN',
                    'invoice_no_length' => '4',
                    'invoice_pdf' => 'pdf1',
                    'invoice_return_prefix' => 'IN-RET',
                    'invoice_return_no_length' => '4',
                    'invoice_return_pdf' => 'pdf1',
                    'delivery_prefix' => 'DC',
                    'delivery_no_length' => '4',
                    'delivery_pdf' => 'pdf1',
                    'delivery_return_prefix' => 'DC-RET',
                    'delivery_return_no_length' => '4',
                    'delivery_return_pdf' => 'pdf1',
                    'voucher_prefix' => 'V',
                    'voucher_no_length' => '4',
                    'voucher_pdf' => 'pdf1',
                    'receipt_prefix' => 'REC',
                    'receipt_no_length' => '4',
                    'receipt_pdf' => 'pdf1',
                    'created_by' => 'mdbstech',
                    'fiscal_year' => '2020-21',
                    'discount_on'=> 'Inclusive',
                    'inventory_on_calculation' => 'Invoice',
                    'allow_negative_stock' => 'Yes',
                    'min_redeem_points' => '100',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}
