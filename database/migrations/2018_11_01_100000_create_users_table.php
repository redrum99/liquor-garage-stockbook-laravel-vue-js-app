<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('name',50);
            $table->string('username',50)->unique();
            $table->string('email',50)->unique();
            $table->string('password',255);
            $table->string('user_role',50);
            $table->string('mobile_no', 20);
            $table->text('address');
            $table->string('avatar',50)->default('avatar.png');
            $table->rememberToken();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('users')->insert(
            [
                [
                    'name' => 'MDBS Tech Private Limited',
                    'username' => 'mdbstech',
                    'email' => 'mdbstech@gmail.com',
                    'password' => Hash::make('qwerty'),
                    'user_role' => 'Super Admin',
                    'mobile_no' => '9535342875',
                    'address' => '#2738/12, 3rd Cross, Kalidasa Road, Mysore - 570020',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
