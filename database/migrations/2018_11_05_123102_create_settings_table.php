<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('setting_id');
            $table->string('discounts_in',50);
            $table->string('enable_barcode',50);
            $table->boolean('enable_printer',2)->default('1');
            $table->boolean('migrations',2)->default('0');
            $table->boolean('purchase_orders',2)->default('1');
            $table->boolean('bills',2)->default('1');
            $table->boolean('bill_returns',2)->default('1');
            $table->boolean('payments',2)->default('1');
            $table->boolean('quotations',2)->default('1');
            $table->boolean('proformas',2)->default('1');
            $table->boolean('deliveries',2)->default('1');
            $table->boolean('delivery_returns',2)->default('1');
            $table->boolean('invoices',2)->default('1'); 
            $table->boolean('invoice_returns',2)->default('1'); 
            $table->boolean('receipts',2)->default('1'); 
            $table->boolean('incomes',2)->default('1'); 
            $table->boolean('expenses',2)->default('1');
            $table->boolean('transactions',2)->default('1');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('settings')->insert(
            [
                [
                    'discounts_in'     => 'exc',
                    'enable_barcode'   => 'Yes',
                    'migrations'       => '0',
                    'purchase_orders'  => '1',
                    'bills'            => '1',
                    'bill_returns'     => '1',
                    'payments'         => '1',
                    'quotations'       => '1',
                    'proformas'        => '1',
                    'deliveries'       => '1',
                    'delivery_returns' => '1',
                    'invoices'         => '1',
                    'invoice_returns'  => '1',
                    'receipts'         => '1',
                    'incomes'          => '1',
                    'expenses'         => '1',
                    'transactions'     => '1',
                    'created_by'       => 'mdbstech',
                ],
            ]
        );
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
