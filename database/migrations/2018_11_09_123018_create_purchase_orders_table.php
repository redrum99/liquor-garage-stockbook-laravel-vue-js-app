<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('purchase_order_id');
            $table->string('fiscal_year',50);
            $table->string('purchase_order_no',50);
            $table->date('purchase_order_date');
            $table->string('reference_no',50)->nullable();
            $table->date('delivery_date')->nullable();
            $table->integer('vendor_id')->unsigned();
            $table->text('billing_address')->nullable();
            $table->text('shipping_address')->nullable();
            $table->integer('source_id')->unsigned();
            $table->integer('destination_id')->unsigned();
            $table->double('sub_total',15,2);
            $table->double('discount_amount',15,2);
            $table->double('tax_amount',15,2);
            $table->double('total_amount',15,2);
            $table->double('round_off',15,2);
            $table->double('grand_total',15,2);
            $table->integer('term_id')->unsigned()->nullable();
            $table->longtext('terms')->nullable();
            $table->longtext('note')->nullable();
            $table->string('purchase_order_status',50)->default('Open');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('vendor_id')->references('contact_id')->on('contacts');
            $table->foreign('source_id')->references('place_id')->on('places');
            $table->foreign('destination_id')->references('place_id')->on('places');
            $table->foreign('term_id')->references('term_id')->on('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
