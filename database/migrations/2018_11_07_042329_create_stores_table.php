<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('store_id');
            $table->integer('product_id')->unsigned();
            $table->integer('price_id')->unsigned();
            $table->integer('warehouse_id')->unsigned();
            $table->double('opening_stock',15,2)->default(0);
            $table->double('opening_stock_rate',15,2)->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('price_id')->references('price_id')->on('prices');
            $table->foreign('warehouse_id')->references('warehouse_id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
