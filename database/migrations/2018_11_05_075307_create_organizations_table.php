<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('org_id');
            $table->string('org_code',50);
            $table->string('org_name',50);
            $table->string('email',50)->nullable();
            $table->string('mobile_no',50)->nullable();
            $table->string('phone_no',50)->nullable();
            $table->text('address')->nullable();
            $table->string('pan_no',50)->nullable();
            $table->string('gstin_no',50)->nullable();
            $table->integer('place_id')->unsigned()->nullable();
            $table->string('logo',50)->default('logo.png');
            $table->string('header',50)->default('header.png');
            $table->string('footer',50)->default('footer.png');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->foreign('place_id')->references('place_id')->on('places');
        });

        DB::table('organizations')->insert(
            [
                [
                    'org_code' => 'Mysuru',
                    'org_name' => 'MDBS Tech Private Limited',
                    'email' => 'mdbstech@gmail.com',
                    'mobile_no' => '9535342875',
                    'phone_no' => '08214265003',
                    'address' => '#2738/12, 3rd Cross Kalidasa Road, Mysore - 570020',
                    'pan_no' => '',
                    'gstin_no' => '',
                    'place_id' => '1',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
