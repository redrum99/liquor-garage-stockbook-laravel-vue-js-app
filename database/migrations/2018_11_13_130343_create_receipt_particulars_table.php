<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptParticularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_particulars', function (Blueprint $table) {
            $table->increments('receipt_particular_id');
            $table->integer('receipt_id')->unsigned();
            $table->string('reference',50);
            $table->integer('reference_id')->unsigned();
            $table->double('paid_amount',15,2);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('receipt_id')->references('receipt_id')->on('receipts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_particulars');
    }
}
