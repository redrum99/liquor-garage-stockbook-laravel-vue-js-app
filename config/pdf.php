<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'MDBS Tech Private Limited',
	'subject'               => 'PDF',
	'keywords'              => 'STOCKBOOK',
	'creator'               => 'MDBS Tech Private Limited',
	'display_mode'          => 'fullpage',
	'tempDir'               => public_path('temp/')
];
